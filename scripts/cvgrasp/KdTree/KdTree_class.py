# -*- coding: utf-8 -*-

import math
from math import sqrt
from math import atan2
import pickle
import numpy as np
from numpy.linalg import norm
from scipy.linalg import eigh
import colorsys
import os
import time
import cv2
# class and function to work with a KdTree
from scipy.spatial.distance import cdist
from sklearn.linear_model import LinearRegression
# from scipy import stats

def sqrDist(p1,p2):
    return  np.linalg.norm(np.array(p1)-np.array(p2))

class PriorityNode :
    def __init__(self, priority, data, next) :
        """ sorted by highest value """
        self.priority = priority
        self.data = data
        self.next = next

class PriorityList :
    def __init__(self):
        self._nbElements = 0
        self._root = None

    def nbElements(self) :
        return self._nbElements

    def insertElement(self, priority, data) :
        self._nbElements += 1

        prev = None
        curr = self._root
        while curr and curr.priority>priority :
            prev = curr
            curr = curr.next

        newNode = PriorityNode(priority, data, curr)
        if not prev :
            self._root = newNode
        else :
            prev.next = newNode

    def popElement(self) :
        assert(self._nbElements>0)
        self._nbElements -= 1
        val = self._root
        self._root = self._root.next
        return (val.priority, val.data)

    def elements(self) :
        L = [None]*self._nbElements
        i = 0
        curr = self._root
        while curr :
            L[i] = (curr.priority, curr.data)
            i+=1
            curr = curr.next
        return L

    def get_points(self) :
        L = [None]*self._nbElements
        i = 0
        curr = self._root
        while curr :
            L[i] = curr.data[0]
            i+=1
            curr = curr.next
        return L

    def maximum(self) :
        assert(self._root)
        return self._root.priority

    def display(self):
        print("NbElement = " + str(self.nbElements()))
        curr = self._root
        while curr:
            print(str(curr.priority) + " " + str(curr.data))
            curr = curr.next

class KdNode :
    
    def __init__(self, point, dim, left_child, right_child, data=None, origin=None):
        # hyperplane defining the cut
        self.point = point
        self.dim = dim
        # additionnal data (normals)
        self.data = data
        self.origin = origin

        # sub-KdTree
        self.left_child = left_child;
        self.right_child = right_child;

    def add(self, point, data=None, origin=None, verbose=False):
        """
        Adds a point to the current node or iteratively
        descends to one of its children.
        Users should call add() only to the topmost tree.
        """
        if verbose:
            print("add point: ", point)
        if not self.contains(point):
            current = self
            while True:

                # Adding has hit an empty leaf-node, add here
                if current.point is None:
                    current.point = point
                    current.data = data
                    current.origin = origin

                    return current

                # split on self.dim, recurse either left or right
                if point[current.dim] < current.point[current.dim]:
                    if current.left_child is None:
                        dim_test = current.dim + 1
                        dim_test = dim_test % 2  # simple strategy to choose cut dimension
                        current.left_child = KdNode(point, dim_test, None, None,data=data, origin=origin)
                        return current.left_child
                    else:
                        current = current.left_child
                else:
                    if current.right_child is None:
                        dim_test = current.dim + 1
                        dim_test = dim_test % 2  # simple strategy to choose cut dimension
                        current.right_child = KdNode(point, dim_test, None, None, data=data, origin=origin)
                        return current.right_child
                    else:
                        current = current.right_child

    def get_list_all(self, points=None, data=None, origins=None):
        if points is None:
            points = []
            data = []
            origins = []

        points.append(self.point)
        data.append(self.data)
        origins.append(self.origin)
        
        if self.left_child is not None:
            _, _, _ = self.left_child.get_list_all(points, data, origins)
        if self.right_child is not None:
            _, _, _ = self.right_child.get_list_all(points, data, origins)

        return points, data, origins

    def get_list_points(self, l=None):
        if l is None:
            l = []

        l.append(self.point)
        if self.left_child is not None:
            _ = self.left_child.get_list_points(l)
        if self.right_child is not None:
            _ = self.right_child.get_list_points(l)

        return l

    def get_list_data(self, l=None):
        if l is None:
            l = []

        l.append(self.data)
        if self.left_child is not None:
            _ = self.left_child.get_list_data(l)
        if self.right_child is not None:
            _ = self.right_child.get_list_data(l)

        return l


    def elements_raw(self, l=None):
        if l is None:
            l = []

        l.append([self.point, self.data])
        if self.left_child is not None:
            _ = self.left_child.elements_raw(l)
        if self.right_child is not None:
            _ = self.right_child.elements_raw(l)

        return l

    def elements(self, l=None):
        if l is None:
            l = []

        l.append([self.point[0], self.point[1], self.data])

        if self.left_child is not None:
            _ = self.left_child.elements(l)
        if self.right_child is not None:
            _ = self.right_child.elements(l)

        return l
    
    def distToHyperplane(self, point):
        """ Compute distance from point to node's hyperplane """
        return point[self.dim] - self.point[self.dim]

    def contains(self, point, tolerance=1):
        """ Checks whether a point is in the kdtree."""
        is_equal = False
        try:
            if abs(self.point[0] -point[0] + self.point[1] -point[1]) <=tolerance :
                is_equal = True
        except:
            if self.point==point:
                is_equal = True

        if is_equal:
            return True
        else:
            dist_1d = self.distToHyperplane(point)
            res = False
            if self.left_child and dist_1d<=0 :
                res = self.left_child.contains(point)
            if self.right_child and dist_1d>=0:
                res = res or self.right_child.contains(point)
            return res

    def nearestNeighbour(self, point, candidate=None):
        """ Find the nearest neighbour to a point in the kdtree"""
        if candidate is None or sqrDist(point, self.point)<sqrDist(point,candidate) :
            candidate = self.point

        dist_1d = self.distToHyperplane(point)
        n1,n2=self.left_child,self.right_child
        if dist_1d>=0 :
            n1,n2=n2,n1

        if n1:
            candidate = n1.nearestNeighbour(point, candidate)
        if n2 and sqrt(dist_1d*dist_1d) < sqrDist(point,candidate):
            candidate = n2.nearestNeighbour(point, candidate);
        return candidate

    """ Find the k nearest neighbours to a point in the kdtree
        returned list contains both points and associated data
    """
    def kNearestNeighboursRecAux(self, point, k, candidates):
        # print("point: ", point)
        # print("self.point: ", self.point)
        candidates.insertElement(sqrDist(point, self.point), (self.point, self.data))
        if candidates.nbElements()>k :
            candidates.popElement()

        dist_1d = self.distToHyperplane(point)
        n1,n2=self.left_child,self.right_child
        if dist_1d>=0 :
            n1,n2=n2,n1

        if n1:
            n1.kNearestNeighboursRecAux(point, k, candidates)
        if n2 and sqrt(dist_1d*dist_1d) < candidates.maximum():
            n2.kNearestNeighboursRecAux(point, k, candidates);


    def kNearestNeighboursRec(self, point, k):

        neighbours = PriorityList()
        self.kNearestNeighboursRecAux(point, k, neighbours)
        return neighbours

    def kNearestNeighboursIter(self, point, k):
        """ Find the k nearest neighbours to a point in the kdtree
            returned list contains both points and associated data
        """
        candidates = PriorityList()

        stack = [ (self, True, 0.0) ] # would be better to allocate larger size...

        while stack :
            node, first, dist = stack.pop()
            if first or dist*dist < candidates.maximum() : 
                # if second node need to stop recursive search !
                candidates.insertElement(sqrDist(point, node.point), (node.point, node.data) )
                if candidates.nbElements()>k :
                    candidates.popElement()

                dist_1d = node.distToHyperplane(point)
                n1,n2=node.left_child,node.right_child
                if dist_1d>=0 :
                    n1,n2=n2,n1

                # push in reverse order !
                if n1:
                    stack.append((n1,True,dist_1d))
                if n2:
                    stack.append((n2,False,dist_1d)) # will be poped first !
                #TODO we could pre-clip!
        return candidates

    def nearestNeighbour_full(self, point, candidate=None, dist=None, data=None, origin=None):
        """ Find the nearest neighbour to a point in the kdtree"""
        potential = sqrDist(point, self.point)
        if candidate is None or potential<dist:
            candidate = self.point
            dist = potential
            data = self.data
            origin = self.origin

        dist_1d = self.distToHyperplane(point)
        n1,n2=self.left_child,self.right_child
        if dist_1d>=0 :
            n1,n2=n2,n1

        if n1:
            candidate, dist, data, origin = n1.nearestNeighbour_full(point, candidate, dist=dist, data=data, origin=origin)
        if n2 and sqrt(dist_1d*dist_1d) < dist:
            candidate, dist, data, origin = n2.nearestNeighbour_full(point, candidate, dist=dist, data=data, origin=origin);
        return candidate, dist, data, origin

def buildKdTree(list_points=None, depth=0, list_angles=None, list_origins=None):
    """ Build a balanced kdtree from a list of points."""
    # if list_points is None:
    if list_points is None:
        return None
    elif len(list_points)==0:
        return None
    else :
        # print("buildKdTree len ", len(list_points))
        b_angles_is_none = list_angles is None
        b_origins_is_none = list_origins is None
        
        dim_test=depth%2 # simple strategy to choose cut dimension
        
        if len(list_points)==1 :
            data = None
            origin = None
            if not b_angles_is_none:
                data = list_angles[0]
            if not b_origins_is_none :
                origin = list_origins[0]
            return KdNode(list_points[0], dim_test, None, None, data=data, origin=origin)

        else :
            # select median
            median_id = (len(list_points)-1)//2

            # sort points
            # list_points.sort(key=lambda p : p[dim_test])
            if b_origins_is_none and b_angles_is_none:#None
                list_points.sort(key=lambda x: x[dim_test])
                left = buildKdTree(list_points[:median_id], depth + 1)
                right = buildKdTree(list_points[median_id + 1:], depth + 1)
                return KdNode(list_points[median_id], dim_test, left, right)
            else:#one of the two
                if b_origins_is_none:#means angle is ok
                        list_points, list_angles = (list(t) for t in zip(*sorted(zip(list_points, list_angles))))
                        left = buildKdTree(list_points[:median_id], depth + 1, list_angles=list_angles[:median_id])
                        right = buildKdTree(list_points[median_id + 1:], depth + 1,
                                            list_angles=list_angles[median_id + 1:])
                        return KdNode(list_points[median_id], dim_test, left, right, data=list_angles[median_id])
                else:
                    if b_angles_is_none:  # means list_origins is ok
                        list_points, list_origins = (list(t) for t in zip(*sorted(zip(list_points, list_origins))))
                        left = buildKdTree(list_points[:median_id], depth + 1, list_origins=list_origins[:median_id])
                        right = buildKdTree(list_points[median_id + 1:], depth + 1,
                                            list_origins=list_origins[median_id + 1:])
                        return KdNode(list_points[median_id], dim_test, left, right, origin=list_origins[median_id])

                    else:
                        # try:
                        list_points, list_angles, list_origins = (list(t) for t in zip(*sorted(zip(list_points, list_angles, list_origins), key=lambda x: x[0][dim_test])))
                        # except Exception as e:
                        #     print(e)
                        #     print("list_points: ", list_points)
                        #     print("list_angles: ", list_angles)
                        #     print("list_origins: ", list_origins)

                        left = buildKdTree(list_points[:median_id], depth + 1, list_angles=list_angles[:median_id],
                                           list_origins=list_origins[:median_id])
                        right = buildKdTree(list_points[median_id + 1:], depth + 1,
                                            list_angles=list_angles[median_id + 1:],
                                            list_origins=list_origins[median_id + 1:])
                        return KdNode(list_points[median_id], dim_test, left, right, data=list_angles[median_id],
                                      origin=list_origins[median_id])

def angleFromNeighbours( points, b_add_with_uncertainty=False, mu=0.0, sigma = 0.0) :#sigma = 0.4) :
    x = []
    y = []

    for i in range(len(points)-1,0,-1):
        p = points[i]
        # print('p: ', p, " p[0]: ", p[0], "p[1]: ", p[1], "p[1][0]: ", p[1][0], "p[1][1]: ", p[1][1])
        # if len(x) == 0:
        x.append(p[1][0][0])
        y.append(p[1][0][1])
    
    if len(x) <= 1:
        return 0.0
    else:
        x = np.array(x).reshape((-1, 1))
        y = np.array(y).reshape((-1, 1))

        angle = get_angle_from_points(x,y)

          # mean and standard deviation
        if b_add_with_uncertainty:
            angle += np.random.normal(mu, sigma, 1)[0] * np.pi / 2

        return angle

# https://www.codeproject.com/Articles/576228/Line-Fitting-in-Images-Using-Orthogonal-Linear-Reg
# y = a + bx, where a is the intercept, and b is the slope
def get_angle_from_points(x, y, verbose=False):
    # N = len(x)
    x_mean = x.mean()
    y_mean = y.mean()

    slope_num = ((x - x_mean) * (y - y_mean)).sum()
    slope_den = ((x - x_mean) ** 2).sum()
    if slope_den == 0.0 or slope_num==0.0:
        std_y = np.std(y)
        std_x = np.std(x)
        # print("std_x : ", std_x)
        # print("std_y : ", std_y)
        if std_x > std_y:
            angle = 0.0
            # angle = np.pi / 2

        else:
            angle = np.pi / 2
            # angle = 0.0
    else:
        slope = slope_num/slope_den
        intercept = y_mean - (slope * x_mean)
        if intercept >= 0.0:
            z = 1.0
        else:
            z = -1.0

        normFactor = z * math.sqrt(slope * slope + 1.0)
        a = (-slope / normFactor)
        b = (1.0 / normFactor)
        c = (intercept / normFactor)

        p1 = np.asarray([0.0, c / a])
        p2 = np.asarray([c / b, 0.0])
        dy = p1[0] - p2[0]
        dx = p1[1] - p2[1]

        angle = np.arctan2(-dy, dx)
    if verbose:
        if angle != 0.0 and angle != np.pi/2:
            std_y = np.std(y)
            std_x = np.std(x)
            print("std_x: ",std_x)
            print("std_y: ",std_y)
            print("angle: ",angle)

    return angle

def computeAnglesIter(kdtree, k=5, b_skip_known=True):
    stack = [ kdtree ] 
    while stack :
        node = stack.pop()
        # if node is not None:
        if node.data is None or b_skip_known is False:
            neighbours = kdtree.kNearestNeighboursRec(node.point, k)
            # neighbours.insertElement(0, (node.point, node.data))
            # node.data = angleFromNeighbours(node.point, neighbours, b_add_point=b_add_point)
            points = neighbours.elements()
            # print("points: ", points)
            # print("node.point: ", node.point)
            node.data = angleFromNeighbours(points)

        if node.right_child:
            stack.append(node.right_child)
        if node.left_child:
            stack.append(node.left_child)


if __name__ == '__main__':
    
    example = "Circle"
    points = pickle.load(open("Circle.points","rb"))

    # print("points: ", points)

    x = [p[0] for p in points]
    y = [p[1] for p in points]
    centroid = np.array([sum(x) / len(points), sum(y) / len(points)])

    # centroid = [300.03213689, 300.56849723]
    # points = []
    line_vertical = []
    line_horizontal = []

    line_diag1 = []
    line_diag2 = []

    for j in range(10):
        i = j*10
        line_vertical.append([centroid[0], centroid[1]+i])
        line_vertical.append([centroid[0], centroid[1]-i])

        line_diag1.append([centroid[0]+i, centroid[1]+i])
        line_diag1.append([centroid[0]-i, centroid[1]-i])

        line_diag2.append([centroid[0]+i, centroid[1]-i])
        line_diag2.append([centroid[0]-i, centroid[1]+i])


        line_horizontal.append([centroid[0]+i, centroid[1]])
        line_horizontal.append([centroid[0]-i, centroid[1]])

    print("centroid: ", centroid)
    points.append(np.array(centroid))
    for p in line_vertical:
        points.append(np.array(p))

    for p in line_horizontal:
        points.append(np.array(p))

    for p in line_diag1:
        points.append(np.array(p))

    for p in line_diag2:
        points.append(np.array(p))

    current_scene_rgb_image = np.zeros((1280, 1280, 3), np.uint8)

    kdtree = buildKdTree(points)

    start = time.perf_counter()

    computeAnglesIter(kdtree)

    build_time = time.perf_counter() - start
    print(build_time)

    # list_dexnet_grasps = kdtree.elements()
    list_dexnet_grasps_raw = kdtree.elements_raw()
    list_dexnet_grasps_raw_p, list_dexnet_grasps_raw_a, list_dexnet_grasps_raw_o = kdtree.get_list_all()

    scene_rgb_image = current_scene_rgb_image.copy()
    all_grasps_from_scene = []
    for t in range(len(list_dexnet_grasps_raw)):
        g_raw = list_dexnet_grasps_raw[t]
        all_grasps_from_scene.append([g_raw[0][1], g_raw[0][0], g_raw[1]])

    from drawings.util_drawings import generate_image_with_grasps

    generate_image_with_grasps(all_grasps_from_scene, scene_rgb_image, destination='test_circle' + '_grasps.png',b_with_rectangles=False)

    cv2.imwrite('test_circle' + '_grasps.png', scene_rgb_image)

