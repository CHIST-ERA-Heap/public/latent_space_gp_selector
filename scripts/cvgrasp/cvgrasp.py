from skimage.morphology import skeletonize, medial_axis, remove_small_objects
from skimage.filters import threshold_otsu
from random import randint
from sklearn.cluster import MiniBatchKMeans
from skimage.segmentation import felzenszwalb
import math
import numpy as np
import cv2
# class and function to work with a KdTree
from scipy.spatial.distance import cdist
from scripts.cvgrasp.KdTree.KdTree_class import buildKdTree, sqrDist, computeAnglesIter

def get_grasp_with_center(x, y, angle, edges_contour, test_angles=[0], grasp_min=10, grasp_max=300):

    best_tweak = 0.0
    is_antipodal = False
    min_length = grasp_max
    for angle_tweak in test_angles:
        # try:
        # todo add this parameters to config
        current_prior_length, is_antipodal = get_width_with_edge(x, y,
                                                                 angle + angle_tweak, edges_contour,
                                                                 grasp_min=grasp_min,
                                                                 grasp_max=grasp_max,
                                                                 verbose=False)

        if current_prior_length < min_length:
            min_length = current_prior_length
            best_tweak = angle_tweak
            is_antipodal = is_antipodal

    return min_length, best_tweak, is_antipodal

def round_grasp(g, precision = 4):
    g[5] = round(g[5], precision)
    g[6] = round(g[6], precision)
    g[7] = round(g[7], precision)
    g[8] = round(g[8], precision)
    return g

def get_perpendicular_to_grasp(g, edges_contour, current_scene_raw_depth_data, tweak=np.pi/2, grasp_min=10, grasp_max=300):
    new_grasp = g.copy()
    new_grasp[8] += tweak
    perpendicular_length, _, is_antipodal = get_grasp_with_center(new_grasp[3], new_grasp[4], new_grasp[8], edges_contour, test_angles=[0], grasp_min=grasp_min, grasp_max=grasp_max)

    perpendicular_depth_min, _ = get_depth_with_width(grasp_position_x=new_grasp[3],
                                                      grasp_position_y=new_grasp[4],
                                                      rotation_in_radian=new_grasp[8],
                                                      cloud_points=current_scene_raw_depth_data,
                                                      gripper_length_pixel=perpendicular_length,
                                                      verbose=False)
    new_grasp[5] = perpendicular_depth_min
    new_grasp[9] =perpendicular_length

    return new_grasp

def get_width_with_edge(grasp_position_x, grasp_position_y, rotation_in_radian, binary_edge, grasp_min=1, grasp_max=100,
                        verbose=False, tolerance=1):
    rotation_matrix = np.zeros((2, 2))
    rotation_matrix[0][0] = math.cos(rotation_in_radian)
    rotation_matrix[0][1] = -math.sin(rotation_in_radian)
    rotation_matrix[1][0] = math.sin(rotation_in_radian)
    rotation_matrix[1][1] = math.cos(rotation_in_radian)
    if verbose:
        print("rotation_matrix: ", rotation_matrix)

    p1 = np.zeros((2, 1))
    p2 = np.zeros((2, 1))

    left_found = False
    right_found = False
    new_grasp_width_left = grasp_max
    new_grasp_width_right = grasp_max
    new_grasp_width = grasp_max
    binary_edge = np.asarray(binary_edge)
    if verbose:
        print("binary_edge.shape: ", binary_edge.shape)
    for test_grasp_width in range(grasp_min, grasp_max):
        try:
            p1[0][0] = -int(test_grasp_width / 2)
            p1_rotated = np.dot(rotation_matrix, p1)
            max_point_left = np.array(
                [int(p1_rotated[1][0] + grasp_position_y), int(p1_rotated[0][0] + grasp_position_x)])
            if left_found == False:
                left_found = binary_edge[max_point_left[0]][max_point_left[1]] != 0
                if left_found:
                    new_grasp_width_left = test_grasp_width

            p2[0][0] = int(test_grasp_width / 2)
            p2_rotated = np.dot(rotation_matrix, p2)
            max_point_right = np.array(
                [int(p2_rotated[1][0] + grasp_position_y), int(p2_rotated[0][0] + grasp_position_x)])
            if right_found == False:
                right_found = binary_edge[max_point_right[0]][max_point_right[1]] != 0
                if right_found:
                    new_grasp_width_right = test_grasp_width

            if left_found and right_found:
                new_grasp_width = test_grasp_width
                break
        except:
            # new_grasp_width = test_grasp_width - 1
            break
    if verbose:
        print("new_grasp_width_left: ",new_grasp_width_left)
        print("new_grasp_width_right: ",new_grasp_width_right)
        print("new_grasp_width: ",new_grasp_width)

    # if new_grasp_width_right > new_grasp_width_left:
    #     new_grasp_width = new_grasp_width_left
    # else:
    #     new_grasp_width = new_grasp_width_right

    # print("before: ", new_grasp_width)
    # new_grasp_width = np.linalg.norm(max_point_left - max_point_right)

    if new_grasp_width > grasp_max:
        new_grasp_width = grasp_max
    if verbose:
        print("after: ", new_grasp_width)

    if new_grasp_width_right > new_grasp_width_left:
        ratio =new_grasp_width_right / new_grasp_width_left
    else:
        ratio = new_grasp_width_left / new_grasp_width_right
    # is_antipodal =  (ratio <= tolerance and (new_grasp_width != grasp_max))
    is_antipodal =  (ratio <= tolerance and (new_grasp_width != grasp_max))

    if verbose:
        print("ratio: ", ratio)
        print("tolerance: ", tolerance)
        print("is_antipodal: ", is_antipodal)



    return new_grasp_width , is_antipodal

def felzenszwalb_segmentation(image):
    # image_felzenszwalb = ndi.median_filter(image, size=20)

    segments_fz = np.uint8(felzenszwalb(image,scale=100, sigma=0.5, min_size=50))

    # segments_fz[cropping_mask > 0] = np.min(segments_fz)
    segments_fz_normed = cv2.normalize(segments_fz, segments_fz, 0, 255, cv2.NORM_MINMAX)
    # print("segments_fz.unique: ", np.unique(segments_fz))
    #
    # print("segments_fz.mean: ", np.mean(segments_fz))
    # print("segments_fz.max: ", np.max(segments_fz))
    # print("segments_fz.min: ", np.min(segments_fz))

    thresh = 127
    segments_fz_normed_bw = cv2.threshold(segments_fz_normed, thresh, 255, cv2.THRESH_BINARY)[1]
    # mask = np.zeros((image.shape[0], image.shape[1]))
    # mask[segments_fz_normed_bw > 30] = 255

    # mask = np.reshape(mask,(image.shape[0], image.shape[1])).astype(np.uint8)
    return segments_fz_normed_bw, segments_fz

def get_grayscale_depth_from_raw(dst_d, min_depth=None, max_depth=None, b_Xue=False, missing_value=0, b_invert=False, max_depth_value = 0.7, blur=False):#https://arxiv.org/pdf/1604.05817.pdf

    invalid_px = np.where(dst_d > max_depth_value)
    dst_d[invalid_px] = max_depth_value

    if b_Xue:
        # dst_d = np.reshape(dst_d, (dst_d.shape[0], dst_d.shape[1], 1))
        im_data = cv2.copyMakeBorder(dst_d, 1, 1, 1, 1, cv2.BORDER_DEFAULT)
        mask = (im_data == missing_value).astype(np.uint8)

        # Scale to keep as float, but has to be in bounds -1:1 to keep opencv happy.
        scale = np.abs(im_data).max()
        # print('scale: ', scale)
        im_data = im_data.astype(np.float32) / scale  # Has to be float32, 64 not supported.
        im_data = cv2.inpaint(im_data, mask, 1, cv2.INPAINT_NS)

        im_data = im_data.astype(np.float32)
        im_data = np.reshape(im_data, (im_data.shape[0], im_data.shape[1], 1))

        # Back to original size and value range.
        im_data = im_data[1:-1, 1:-1]
        im_data = im_data * scale
        im_data = np.asarray(im_data)
        im_data = im_data.reshape((im_data.shape[0], im_data.shape[1], 1))
        # print("min im_data: ", np.min(im_data))
        # print("mean im_data: ", np.mean(im_data))
        # print("max im_data: ", np.max(im_data))

    else:
        twobyte = False
        normalize = True
        BINARY_IM_MAX_VAL = np.iinfo(np.uint8).max
        max_val = BINARY_IM_MAX_VAL
        if twobyte:
            max_val = np.iinfo(np.uint16).max

        if normalize:
            min_depth = np.min(dst_d)
            max_depth = np.max(dst_d)
            depth_data = (dst_d - min_depth) / (max_depth - min_depth)
            depth_data = float(max_val) * depth_data.squeeze()
        else:
            zero_px = np.where(dst_d == 0)
            zero_px = np.c_[zero_px[0], zero_px[1], zero_px[2]]
            depth_data = ((dst_d - min_depth) * \
                          (float(max_val) / (max_depth - min_depth))).squeeze()
            depth_data[zero_px[:, 0], zero_px[:, 1]] = 0
        im_data = np.zeros([dst_d.shape[0], dst_d.shape[1], 3])
        im_data[:, :, 0] = depth_data
        im_data[:, :, 1] = depth_data
        im_data[:, :, 2] = depth_data
        im_data = im_data.astype(np.uint8)


       # generate depth grayscale patch
        # cf autolab perception

        # BINARY_IM_MAX_VAL = np.iinfo(np.uint8).max
        #
        # max_val = BINARY_IM_MAX_VAL
        #
        # min_depth = 0.0
        # # if min_depth is None:
        # #     min_depth = np.min(dst_d)
        #
        # if max_depth is None:
        #     max_depth = np.max(dst_d)
        #
        # depth_data = (dst_d - min_depth) / (max_depth - min_depth)
        # depth_data = float(max_val) * depth_data.squeeze()
        #
        # try:
        #     zero_px = np.where(dst_d == 0)
        #     zero_px = np.c_[zero_px[0], zero_px[1], zero_px[2]]
        # except(IndexError):
        #     pass
        #
        # depth_data = ((dst_d - min_depth) * \
        #               (float(max_val) / (max_depth - min_depth))).squeeze()
        # try:
        #     depth_data[zero_px[:, 0], zero_px[:, 1]] = 0
        # except(IndexError, TypeError):
        #     pass
        #
        # im_data = np.zeros([depth_data.shape[0], depth_data.shape[1], 3]).astype(np.uint8)
        # im_data[:, :, 0] = depth_data
        # im_data[:, :, 1] = depth_data
        # im_data[:, :, 2] = depth_data

        # if twobyte:
        #     return im_data.astype(np.uint16)
        # return im_data.astype(np.uint8)

        # depth_image = im_data.astype(np.uint8)
    if b_invert:
        im_data = 255.0 - im_data


    depth_image = im_data.astype(np.uint8)


    # print("depth_image.shape: ", depth_image.shape)
    if (depth_image.shape[2] == 3):
        depth_image = depth_image[:, :, 0]

    if blur:
        kernel = np.ones((5, 5), np.float32) / 25
        depth_image = cv2.filter2D(depth_image, -1, kernel)

    return depth_image

def get_raw_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                        dim_grasp_patch=96, dim_grasp_patch_y=None, verbose = False):
    rows, cols, canal = depth_data.shape
    DIM = int(dim_grasp_patch / 2)  # diminsion resulting matrix= DIM*2
    if dim_grasp_patch_y is not None:
        DIM_y = int(dim_grasp_patch_y / 2)
    else:
        DIM_y = DIM

    if verbose:
        print("dim_grasp_patch: ", dim_grasp_patch)
        print("DIM: ", DIM)
        print("DIM_y: ", DIM_y)
        print("depth_data.shape: ", depth_data.shape)

    M = cv2.getRotationMatrix2D((int(grasp_position_x), int(grasp_position_y)), image_rotation_angle_degree, 1)
    dst_d = cv2.warpAffine(depth_data, M, (cols, rows))
    dst_d = dst_d[int(grasp_position_y) - DIM_y:int(grasp_position_y) + DIM_y,
            int(grasp_position_x) - DIM:int(grasp_position_x) + DIM]

    # generate depth grayscale patch
    # cf autolab perception

    where_are_NaNs = np.isnan(dst_d)
    max_d = np.max(dst_d)
    # dst_d[where_are_NaNs] = 0.0
    dst_d[where_are_NaNs] = max_d

    where_to_close = np.where(dst_d<0.1)
    # dst_d[where_are_NaNs] = 0.0
    dst_d[where_to_close] = max_d

    if verbose:
        print("dst_d.shape: ", dst_d.shape)
        print("mean dst_d: ", np.mean(dst_d))

    return dst_d  # raw depth

def get_depth_with_width(grasp_position_x, grasp_position_y, rotation_in_radian, cloud_points, gripper_length_pixel,
                         dim_grasp_patch_y=6, fix_delta=0.00, verbose=False):
    if verbose:
        print("grasp_position_x: ", grasp_position_x)
        print("grasp_position_y: ", grasp_position_y)
        print("rotation_in_radian: ", rotation_in_radian)
        print("gripper_length_pixel: ", gripper_length_pixel)
        print("cloud_points.shape: ", cloud_points.shape)
        print("max(cloud_points): ", np.max(cloud_points))

    if gripper_length_pixel <=5:
        gripper_length_pixel = 5
    points_on_line = get_raw_depth_patch(cloud_points, grasp_position_x, grasp_position_y,
                                        math.degrees(rotation_in_radian), dim_grasp_patch=gripper_length_pixel, dim_grasp_patch_y=dim_grasp_patch_y, verbose=verbose)

    if verbose:
        print("points_on_line: ", points_on_line)
        print("points_on_line.shape: ", points_on_line.shape)
        print("max(points_on_line): ", np.max(points_on_line))
    try:
        depth_min = np.min(points_on_line)
        depth_max = np.max(points_on_line)
    except Exception as e:
        print(e)
        depth_min = np.median(cloud_points)
        depth_max = depth_min

    return depth_min+fix_delta, depth_max+fix_delta

def find_center_of_mask(mask, reverse = False):
    centers = []

    print('mask.shape: ', mask.shape)
    print('mask min: ', np.min(mask))
    print('mask max ', np.max(mask))


    # Find contours:
    try:
        contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    except:
        # im, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        _, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for i in range(len(contours)):
        try:
            moments = cv2.moments(contours[i])
            centers.append((int(moments['m10'] / moments['m00']), int(moments['m01'] / moments['m00'])))
            # centers.append([int(moments['m01'] / moments['m00']), (int(moments['m10'] / moments['m00']))])

        except:
            pass
    if len(centers) == 0:
        centers = [[int(mask.shape[0] / 2), int(mask.shape[1] / 2)]]

    # # Calculate image moments of the detected contour
    # M = cv2.moments(contours[0])
    # x = round(M['m10'] / M['m00'])
    # y = round(M['m01'] / M['m00'])
    # # center = (x, y)
    # center = (y, x)

    if reverse:
        for i in range(len(centers)):
            centers[i] = (centers[i][1], centers[i][0])

    return centers

def get_list_of_egde_points(edges_contour, cropping_mask, b_appy_mask = True, keep_ratio=2):
    # gray = cv2.cvtColor(current_scene_rgb_image, cv2.COLOR_BGR2GRAY)
    # k_size = 21
    # gray = cv2.GaussianBlur(gray, (k_size, k_size), 0)
    # edges_contour = canny(gray / 255.)
    # edges_contour = cv2.Canny(gray, 50, 255)
    if b_appy_mask:
        edges_contour[np.where(cropping_mask > 0)] = 0

    # ed_edge_contour = cv2.erode(merged_edge_contour, kernel, iterations=1)  # closing
    #fail if no edge
    try:
        binary_edge = edges_contour > threshold_otsu(edges_contour)
        skel_edge = skeletonize(binary_edge, method='lee')
        try:
            colors = np.unique(skel_edge.reshape(-1, ), axis=0)
        except:
            colors = np.unique(skel_edge.reshape(-1, skel_edge.shape[2]), axis=0)
        for c in colors:
            if c > 0 and c is not False:
                current = list(zip(*np.where(skel_edge == c)))
                if keep_ratio != 1:
                    current = [current[i] for i in range(0,len(current),keep_ratio)]

                # current = np.asarray(current)
                return current
        return []
    except Exception as e:
        print(e)
        return []

def get_list_of_skel_points(binary_mask, keep_ratio=2):
    # skel = skeletonize(binary_mask, method='lee')
    skel = skeletonize(binary_mask/255.0, method='zhang')
    # print('skel.shape: ', skel.shape)
    # cv2.imwrite('./skel.png', np.array(skel*255.0).astype('uint8'))
    # skel = medial_axis(binary_mask)
    # skel = skeletonize_3d(binary_mask)

    try:
        colors = np.unique(skel.reshape(-1, ), axis=0)
    except:
        colors = np.unique(skel.reshape(-1, skel.shape[2]), axis=0)

    for c in colors:
        if c > 0 and c is not False:
            current = list(zip(*np.where(skel == c)))
            if keep_ratio != 1:
                current = [current[i] for i in range(0,len(current),keep_ratio)]
            # current = np.asarray(current)
            return current
    return []

def get_submasks_depth(current_scene_raw_depth_data, current_mask, n_clusters=3):
    # usual = np.median(current_scene_raw_depth_data)
    usual = np.mean(current_scene_raw_depth_data)

    depth_temp = current_scene_raw_depth_data.copy()
    depth_temp[np.where(current_mask == 0)] = usual
    # print('depth_temp.shape: ', depth_temp.shape)

    depth_image = get_grayscale_depth_from_raw(depth_temp, b_invert=False)
    depth_image = cv2.cvtColor(depth_image, cv2.COLOR_GRAY2BGR)

    sub_masks = []
    reconstructed_image = cv2.bitwise_and(depth_image, depth_image, mask=current_mask)
    quant = color_quantization(reconstructed_image, n_clusters=n_clusters)
    # cv2.imwrite('./temp_quant.png', quant)

    # print('quant.shape: ', quant.shape)
    try:
        # colors = np.unique(quant.reshape(-1, ), axis=0)
        colors = np.unique(quant.reshape(-1, quant.shape[2]), axis=0)
    except:
        colors = np.unique(quant.reshape(-1, quant.shape[2]), axis=0)

    # print('colors.shape: ', colors.shape)

    # first = True
    for c in colors:
        # if first:
        #     first = False
        # else:
        # print('c: ', c)
        # if c > 0 and c is not False:
        mask = np.zeros((quant.shape[0], quant.shape[1], 1))
        # mask[np.where(quant == c)] = 255
        mask[np.where((quant == c).all(axis=-1))] = 255
        mask = np.reshape(mask, (quant.shape[0], quant.shape[1])).astype('uint8')
        mask[np.where(current_mask == 0)] = 0

        sub_masks.append(mask)

    return sub_masks

def get_submasks_rgb(current_scene_rgb_image, current_mask, n_clusters=3):
    sub_masks = []
    reconstructed_image = cv2.bitwise_and(current_scene_rgb_image, current_scene_rgb_image, mask=current_mask)
    quant = color_quantization(reconstructed_image, n_clusters=n_clusters)
    # cv2.imwrite('./temp_quant.png', quant)

    # print('quant.shape: ', quant.shape)
    try:
        # colors = np.unique(quant.reshape(-1, ), axis=0)
        colors = np.unique(quant.reshape(-1, quant.shape[2]), axis=0)
    except:
        colors = np.unique(quant.reshape(-1, quant.shape[2]), axis=0)

    # print('colors.shape: ', colors.shape)

    # first = True
    for c in colors:
        # print('c: ', c)
        # if first:
        #     first = False
        # else:
        # if c > 0 and c is not False:
        mask = np.zeros((quant.shape[0], quant.shape[1], 1))
        # mask[np.where(quant == c)] = 255
        mask[np.where((quant == c).all(axis=-1))] = 255
        mask = np.reshape(mask, (quant.shape[0], quant.shape[1])).astype('uint8')
        mask[np.where(current_mask == 0)] = 0

        sub_masks.append(mask)

    return sub_masks

#64 without blur
def color_quantization(image, n_clusters=3):
    (h, w) = image.shape[:2]
    # convert the image from the RGB color space to the L*a*b*
    # color space -- since we will be clustering using k-means
    # which is based on the euclidean distance, we'll use the
    # L*a*b* color space where the euclidean distance implies
    # perceptual meaning
    image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    # reshape the image into a feature vector so that k-means
    # can be applied
    image = image.reshape((image.shape[0] * image.shape[1], 3))
    # apply k-means using the specified number of clusters and
    # then create the quantized image based on the predictions
    clt = MiniBatchKMeans(n_clusters=n_clusters)
    labels = clt.fit_predict(image)
    quant = clt.cluster_centers_.astype("uint8")[labels]
    # reshape the feature vectors to images
    quant = quant.reshape((h, w, 3))
    # image = image.reshape((h, w, 3))
    # convert from L*a*b* to RGB
    quant = cv2.cvtColor(quant, cv2.COLOR_LAB2BGR)
    # image = cv2.cvtColor(image, cv2.COLOR_LAB2BGR)
    return quant

def get_refined_grasp(g, edges_contour, current_scene_raw_depth_data, test_angles = [0], mask = None, grasp_min=10, grasp_max=300):
    new_grasp = g.copy()
    x = new_grasp[3]
    y = new_grasp[4]
    angle = new_grasp[8]
    min_length = new_grasp[9]

    rotation_matrix = np.zeros((2, 2))
    rotation_matrix[0][0] = math.cos(angle)
    rotation_matrix[0][1] = -math.sin(angle)
    rotation_matrix[1][0] = math.sin(angle)
    rotation_matrix[1][1] = math.cos(angle)

    p1 = np.zeros((2, 1))
    p2 = np.zeros((2, 1))

    p1[0][0] = -int(min_length / 4)  # half the length
    p2[0][0] = int(min_length / 4)  # half the length

    p1_rotated = np.dot(rotation_matrix, p1)
    p2_rotated = np.dot(rotation_matrix, p2)
    try:
        p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
        p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))
    except:
        p1_recentered = (int(str(p1_rotated[0][0])) + int(str(x)), int(str(p1_rotated[1][0])) + int(str(y)))
        p2_recentered = (int(str(p2_rotated[0][0])) + int(str(x)), int(str(p2_rotated[1][0])) + int(str(y)))

    try:
        if mask is not None:
            if mask[p1_recentered[1], p1_recentered[0]] ==0:
                return None

        min_length, best_tweak, is_antipodal = get_grasp_with_center(p1_recentered[0], p1_recentered[1], angle, edges_contour, test_angles, grasp_min, grasp_max)
        new_grasp[3] = p1_recentered[0]
        new_grasp[4] = p1_recentered[1]
        new_grasp[9] = min_length

        if not is_antipodal:
            skip = False
            if mask is not None:
                if mask[p2_recentered[1], p2_recentered[0]] == 0:
                    skip = True
            if not skip:
                min_length, best_tweak, is_antipodal = get_grasp_with_center(p2_recentered[0], p2_recentered[1], angle, edges_contour,
                                                                             test_angles, grasp_min, grasp_max)
                new_grasp[3] = p2_recentered[0]
                new_grasp[4] = p2_recentered[1]
                new_grasp[9] = min_length

        depth_min, _ = get_depth_with_width(grasp_position_x=new_grasp[3],
                                                          grasp_position_y=new_grasp[4],
                                                          rotation_in_radian=new_grasp[8],
                                                          cloud_points=current_scene_raw_depth_data,
                                                          gripper_length_pixel=new_grasp[9],
                                                          verbose=False)
        new_grasp[5] = depth_min

        return new_grasp
    except Exception as e:
        return None

def find_candidates(current_scene_rgb_image, current_mask, current_scene_raw_depth_data,
                    b_try_antipodal=True, current_scene_id = -1, missing_types=['centers', 'skel', 'edges'], prior_length=76, prior_width=38, min_distance=16,
                    max_grasps_skel=1000, max_grasps_contour=1000, max_grasps_all=1000, max_distance_all_from_skel_edge=150, mu=0, sigma=0.0, count_grasp_total_min=50,
                    multiple_masks=False, get_gripper_width=True, min_gripper_width_pixel=10, max_gripper_width_pixel=200, keep_ratio=4):
    '''
    current_scene_id = -1
    missing_types = self.type_of_generated_grasps
    prior_length = self.prior_length
    prior_width = self.prior_width
    min_distance = self.min_distance
    max_grasps_skel = self.max_grasps_skel
    max_grasps_contour = self.max_grasps_contour
    max_grasps_all = self.max_grasps_all
    max_distance_all_from_skel_edge = self.max_distance_all_from_skel_edge
    mu = self.mu
    sigma = self.sigma
    count_grasp_total_min = self.count_grasp_total_min
    multiple_masks = self.multiple_masks

    get_gripper_width = self.get_gripper_width
    min_gripper_with_pixel = self.min_gripper_with_pixel
    max_gripper_with_pixel = self.max_gripper_with_pixel
    '''

    # gray = cv2.cvtColor(current_scene_rgb_image.copy(), cv2.COLOR_BGR2GRAY)

    count_grasp_total = 0
    grasps_contour = None
    grasps_skels = None
    points = []
    list_points = []
    list_angles = []
    list_origins = []
    list_grasps_candidates = []

    # print("skel")
    current_masks = [current_mask]
    if multiple_masks:

        # j = 0
        for m in get_submasks_depth(current_scene_raw_depth_data, current_mask, n_clusters=3):
            current_masks.append(m)
            # m = np.reshape(m, (current_scene_rgb_image.shape[0], current_scene_rgb_image.shape[1])).astype('uint8')
            # reconstructed_image_t = cv2.bitwise_and(current_scene_rgb_image, current_scene_rgb_image, mask=m)
            # cv2.imwrite('temp_gray_'+str(j)+'.png', reconstructed_image_t)
            # j+=1

        # j = 0
        for m in get_submasks_rgb(current_scene_rgb_image, current_mask, n_clusters=2):
            current_masks.append(m)
        #     m = np.reshape(m, (current_scene_rgb_image.shape[0], current_scene_rgb_image.shape[1])).astype('uint8')
        #     reconstructed_image_t = cv2.bitwise_and(current_scene_rgb_image, current_scene_rgb_image, mask=m)
        #     cv2.imwrite('temp_rgb_'+str(j)+'.png', reconstructed_image_t)

    if 'skel' in missing_types or 'all' in missing_types:
        for mask in current_masks:
            points_skel = get_list_of_skel_points(mask, keep_ratio=keep_ratio)

            # length_points_skel = len(points_skel)
            for i in range(len(points_skel)):
                if count_grasp_total < max_grasps_skel:
                    p = points_skel[i]
                    if grasps_skels is None:
                        grasps_skels = buildKdTree(list_points=[p], list_origins=['skel'])
                        count_grasp_total += 1
                    else:
                        if not grasps_skels.contains(p):
                            candidate = grasps_skels.nearestNeighbour(p)
                            dist = sqrDist(p, candidate)
                            if dist >= min_distance:
                                grasps_skels.add(p, origin='skel')
                                # list_points.append(p)
                                # list_origins.append('skel')
                                count_grasp_total += 1
                else:
                    break
            if grasps_skels is not None:
                # if not b_try_antipodal:
                computeAnglesIter(grasps_skels)
                points, data, origins = grasps_skels.get_list_all()

                list_points += points
                list_angles += data
                list_origins += origins

                del grasps_skels
                grasps_skels = None
                # print('len list_points: ', len(list_points))

    # print("edge")
    if 'edge' in missing_types or 'all' in missing_types:
        for mask in current_masks:
            edges_contour = cv2.Canny(mask, 50, 255)
            points_contours = get_list_of_egde_points(edges_contour, mask, keep_ratio=keep_ratio)

        # print("points_contours: ", points_contours)
        # print("len(points_contours): ", len(points_contours))

        if len(points_contours) > 0:
            # points = []
            for i in range(len(points_contours)):
                if count_grasp_total < max_grasps_contour:
                    p = points_contours[i]

                    if grasps_contour is None:
                        grasps_contour = buildKdTree(list_points=[p], list_origins=['edge'])
                        count_grasp_total += 1
                    else:
                        if not grasps_contour.contains(p):
                            candidate = grasps_contour.nearestNeighbour(p)
                            dist = sqrDist(p, candidate)
                            if dist >= min_distance:
                                # count += 1
                                grasps_contour.add(p, origin='edge')
                                # points.append(np.array((p[0],p[1])).astype(np.float32))
                                # list_points.append(p)
                                # list_origins.append('edge')
                                count_grasp_total += 1
                else:
                    break

            if grasps_contour is not None:
                # if not b_try_antipodal:
                computeAnglesIter(grasps_contour)
                points, data, origins = grasps_contour.get_list_all()
                list_points += points
                list_angles += data
                list_origins += origins

                del grasps_contour
                grasps_contour = None

    # if multiple_masks:
    edges_contour = cv2.Canny(current_mask, 50, 255)

    if len(list_points) > 0:
        grasps = buildKdTree(list_points=list_points, list_angles=list_angles, list_origins=list_origins)
        # list_grasps_raw_p, list_grasps_raw_a, list_grasps_raw_o = grasps.get_list_all()

    # print("all")
    if 'all' in missing_types:
        try:
            colors = np.unique(current_mask.reshape(-1, ), axis=0)
        except:
            colors = np.unique(current_mask.reshape(-1, current_mask.shape[2]), axis=0)

        min_distance_all = min_distance
        count = 0
        go = True
        while (min_distance_all > 1 and go):
            for c in colors:
                if c > 0 and c is not False:
                    current = list(zip(*np.where(current_mask == c)))
                    # current = np.asarray(current)
                    # print("current: ", current)
                    for p in current:
                        p = p[0:2]
                        if count < max_grasps_all:
                            if grasps is None:
                                grasps = buildKdTree(list_points=[p],
                                                     list_angles=[math.radians(randint(-90, 90))],
                                                     list_origins=['all'])
                                count += 1
                                count_grasp_total += 1
                            else:
                                if not grasps.contains(p):
                                    candidate, dist, data, origin = grasps.nearestNeighbour_full(p)
                                    # dist = sqrDist(p, candidate)
                                    if dist >= min_distance_all and dist <= max_distance_all_from_skel_edge:
                                        if data == None:
                                            data = 0
                                        # print("before: ", data)
                                        data += np.random.normal(mu, sigma, 1)[0] * np.pi / 2
                                        # print("tweak angle: ", data)
                                        grasps.add(p, data=data, origin='all')
                                        count += 1
                                        count_grasp_total += 1

                        else:
                            break
            if count_grasp_total < count_grasp_total_min:
                min_distance_all = min_distance_all / 2
            else:
                break

        computeAnglesIter(grasps)
        points, data, origins = grasps.get_list_all()

        if grasps is not None:
            # if not b_try_antipodal:
            computeAnglesIter(grasps)
            points, data, origins = grasps.get_list_all()
            list_points += points
            list_angles += data
            list_origins += origins

            del grasps
            grasps = None

    if b_try_antipodal and len(list_points) > 0:
        # cv2.imwrite('./temp_edge.png', edges_contour)
        # list_angles = [0.0 for i in range(len(list_points))]
        # test_angles = [0.261799, 0.261799*2.0,0.261799*3.0,0.261799*4.0,0.261799*5.0,0.261799*6.0,-0.261799, -0.261799*2.0,-0.261799*3.0,-0.261799*4.0, -0.261799*5.0 ]
        test_angles = [0.0174533 * float(i) for i in range(-90, 90, 5)]
        for index_points in range(0, len(list_angles)):
            x = list_points[index_points][1]
            y = list_points[index_points][0]
            angle = list_angles[index_points]


            best_tweak = 0.0
            min_length = max_gripper_width_pixel
            for angle_tweak in test_angles:
                # try:
                # todo add this parameters to config
                current_prior_length, is_antipodal = get_width_with_edge(x, y,
                                                                         angle + angle_tweak, edges_contour,
                                                                         grasp_min=min_gripper_width_pixel,
                                                                         grasp_max=max_gripper_width_pixel,
                                                                         verbose=False)

                # print("current_prior_length: ", current_prior_length)
                # print("angle_tweak: ", angle_tweak)
                # print("is_antipodal: ", is_antipodal)

                # except:
                #     current_prior_length = prior_length

                if current_prior_length < min_length:
                    min_length = current_prior_length
                    best_tweak = angle_tweak

            list_angles[index_points] += best_tweak

    # print("indexing")
    if len(points) > 0:
        for index_points in range(0, len(list_points)):
            # if data[index_points] is not None and origins[index_points] in missing_types:*
            if list_angles[index_points] is not None and list_origins[index_points] in missing_types:

                x = list_points[index_points][1]
                y = list_points[index_points][0]
                angle = list_angles[index_points]
                '''
                id integer PRIMARY KEY,
                scene_id integer NOT NULL,
                user_id integer NOT NULL,
                x real NOT NULL,
                y real NOT NULL,
                depth real NOT NULL,
                euler_x real NOT NULL,
                euler_y real NOT NULL,
                euler_z real NOT NULL,
                length real NOT NULL,
                width real NOT NULL,
                quality real NOT NULL,
                FOREIGN KEY (scene_id) REFERENCES scenes (id),
                FOREIGN KEY (user_id) REFERENCES user (id)
                '''
                if get_gripper_width:
                    try:
                        # todo add this parameters to config
                        current_prior_length, is_antipodal = get_width_with_edge(x, y,
                                                                                 angle, edges_contour, grasp_min=min_gripper_width_pixel,
                                                                                 grasp_max=max_gripper_width_pixel,
                                                                                 verbose=False)

                        if current_prior_length < 1 or current_prior_length == 200:
                            current_prior_length = prior_length

                    except:
                        current_prior_length = prior_length
                else:
                    current_prior_length = prior_length

                depth_min, depth_max = get_depth_with_width(grasp_position_x=x,
                                                            grasp_position_y=y,
                                                            rotation_in_radian=angle,
                                                            cloud_points=current_scene_raw_depth_data,
                                                            gripper_length_pixel=current_prior_length,
                                                            verbose=False)

                # list_grasps_candidates.append([-1,-1, origins[index_points], x, y, angle, current_prior_length, prior_width,depth_min, -1.0])
                test_g = [-1, current_scene_id, list_origins[index_points], x, y, depth_min, 0.0, 0.0, angle,
                          float(current_prior_length), prior_width, -1.0]
                test_g = round_grasp(test_g)

                if test_g not in list_grasps_candidates:
                    list_grasps_candidates.append(test_g)

    if 'centers' in missing_types:
        centers = find_center_of_mask(current_mask, reverse = True)
        # print('find candidates centers: ', centers)

        test_angles = [0.0174533 * float(i) for i in range(-90, 90, 1)]
        for center in centers:
            # print('center: ', center)
            x = center[1]
            y = center[0]
            angle = 0

            min_length, best_tweak, is_antipodal = get_grasp_with_center(x, y, angle, edges_contour, test_angles, grasp_min=min_gripper_width_pixel, grasp_max=max_gripper_width_pixel)
            angle += best_tweak

            depth_min, depth_max = get_depth_with_width(grasp_position_x=x,
                                                        grasp_position_y=y,
                                                        rotation_in_radian=angle,
                                                        cloud_points=current_scene_raw_depth_data,
                                                        gripper_length_pixel=min_length,
                                                        verbose=False)

            g = [-1, current_scene_id, 'centers', x, y, depth_min, 0.0, 0.0, angle,
                 float(min_length), prior_width, -1.0]

            g = round_grasp(g)
            if g not in list_grasps_candidates:
                list_grasps_candidates.append(g)

            # add perpendicular to center
            perpendicular_g = get_perpendicular_to_grasp(g, edges_contour, current_scene_raw_depth_data, grasp_min=min_gripper_width_pixel, grasp_max=max_gripper_width_pixel)
            perpendicular_g = round_grasp(perpendicular_g)
            if perpendicular_g not in list_grasps_candidates:
                list_grasps_candidates.append(perpendicular_g)

            if not is_antipodal:

                refined_g = get_refined_grasp(g, edges_contour, current_scene_raw_depth_data,
                                              test_angles=[0.0174533 * float(i) for i in range(-15, 15, 1)],
                                              mask=current_mask, grasp_min=min_gripper_width_pixel, grasp_max=max_gripper_width_pixel)
                if refined_g is not None:
                    refined_g = round_grasp(refined_g)
                    if refined_g not in list_grasps_candidates:
                        list_grasps_candidates.append(refined_g)
                    refined_perpendicular_g = get_perpendicular_to_grasp(refined_g, edges_contour,
                                                                         current_scene_raw_depth_data, grasp_min=min_gripper_width_pixel, grasp_max=max_gripper_width_pixel)
                    refined_perpendicular_g = round_grasp(refined_perpendicular_g)
                    if refined_perpendicular_g not in list_grasps_candidates:
                        list_grasps_candidates.append(refined_perpendicular_g)

                refined_g = get_refined_grasp(perpendicular_g, edges_contour, current_scene_raw_depth_data,
                                              test_angles=[0.0174533 * float(i) for i in range(-15, 15, 1)],
                                              mask=current_mask, grasp_min=min_gripper_width_pixel, grasp_max=max_gripper_width_pixel)
                if refined_g is not None:
                    refined_g = round_grasp(refined_g)
                    if refined_g not in list_grasps_candidates:
                        list_grasps_candidates.append(refined_g)
                    refined_perpendicular_g = get_perpendicular_to_grasp(refined_g, edges_contour,
                                                                         current_scene_raw_depth_data, grasp_min=min_gripper_width_pixel, grasp_max=max_gripper_width_pixel)
                    refined_perpendicular_g = round_grasp(refined_perpendicular_g)
                    if refined_perpendicular_g not in list_grasps_candidates:
                        list_grasps_candidates.append(refined_perpendicular_g)

    return list_grasps_candidates

def rank_grasps(segmask, grasp_list, coef_score_dist_to_mask_center=1.0, coef_score_dist_to_camera=1.0, coef_score_smaller_width=0.5, too_far_to_cam_threshold=0.7, rank_grasps_by_distance_mode='min', rank_grasps_by_distance_dist_metric='euclidean', sort=True, reverse=True):
    centres = find_center_of_mask(segmask)
    # print('centres: ', centres)
    # avg_center = centres
    # avg_center = np.mean(centres, axis=0)
    # avg_center = [(float(avg_center[0]), float(avg_center[1]))]

    if coef_score_dist_to_mask_center > 0.0:
        max_dist = 0.0
        for i in range(len(grasp_list)):
            b = np.array([[grasp_list[i][3], grasp_list[i][4]]])
            # if mode == 'average_score':
            #     dist = cdist(avg_center, b, metric='euclidean')
            # else:
            #     dist = cdist(avg_center, b, metric='euclidean')

            if rank_grasps_by_distance_mode == 'average_score':
                dist = np.mean(cdist(centres, b, metric=rank_grasps_by_distance_dist_metric))
            else:
                dist = np.min(cdist(centres, b, metric=rank_grasps_by_distance_dist_metric))

            grasp_list[i][11] += dist
            if dist > max_dist:
                max_dist = dist

        max_score = -10
        for i in range(len(grasp_list)):
            grasp_list[i][11] = coef_score_dist_to_mask_center * float(1.0 - (grasp_list[i][11].item() / max_dist))

            if max_score < grasp_list[i][11]:
                max_score = grasp_list[i][11]

        print('max_score: ', max_score)

    if coef_score_dist_to_camera > 0.0:

        min_depth = too_far_to_cam_threshold
        max_depth = 0.0

        for i in range(len(grasp_list)):
            # if grasp_list[i][11] == max_score:
            if min_depth > grasp_list[i][5]:
                min_depth = grasp_list[i][5]
            if max_depth < grasp_list[i][5]:
                max_depth = grasp_list[i][5]

        print('min_depth: ', min_depth)
        print('max_depth: ', max_depth)

        for i in range(len(grasp_list)):
            tweak_score = coef_score_dist_to_camera * (max_depth - grasp_list[i][5]) / (max_depth - min_depth)
            print('grasp_list[i]: ', grasp_list[i])
            print('tweak_score: ', tweak_score)
            grasp_list[i][11] += tweak_score

    if coef_score_smaller_width > 0.0:

        min_width = 1000
        max_width = 0.0

        for i in range(len(grasp_list)):
            # if grasp_list[i][11] == max_score:
            if min_width > grasp_list[i][9]:
                min_width = grasp_list[i][9]
            if max_width < grasp_list[i][9]:
                max_width = grasp_list[i][9]

        print('min_width: ', min_width)
        print('max_width: ', max_width)
        if max_width != min_width:
            for i in range(len(grasp_list)):
                tweak_score = coef_score_smaller_width * (1.0 - (grasp_list[i][9] - min_width) / (max_width - min_width))
                print('grasp_list[i]: ', grasp_list[i])
                print('tweak_score: ', tweak_score)
                grasp_list[i][11] += tweak_score

    for i in range(len(grasp_list)):
        grasp_list[i][11] = grasp_list[i][11] / (
                coef_score_dist_to_camera + coef_score_dist_to_mask_center + coef_score_smaller_width)

    # sort list
    if sort:
        grasp_list.sort(key=lambda lol: lol[11], reverse=reverse)
    return grasp_list