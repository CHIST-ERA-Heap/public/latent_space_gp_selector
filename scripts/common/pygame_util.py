import math
import numpy as np
from pygame.math import Vector2
from pygame.transform import rotate


def approximation_pixel_to_meters(pixel_input, max_gripper_width=0.082, ratio_pixel_to_meter=0.000689655):
    w = pixel_input * ratio_pixel_to_meter
    if (w > max_gripper_width):
        return max_gripper_width
    else:
        return w

def move_in_direction(t_x, t_y, image_rotation_angle_degree, rate = 2.0):
    # TODO IF 1280*
    rotation_in_radian = math.radians(image_rotation_angle_degree)
    p1 = np.zeros((2, 1))

    p1[0][0] = t_x * rate
    p1[1][0] = t_y * rate

    rotation_matrix = np.zeros((2, 2))
    rotation_matrix[0][0] = math.cos(rotation_in_radian)
    rotation_matrix[0][1] = -math.sin(rotation_in_radian)
    rotation_matrix[1][0] = math.sin(rotation_in_radian)
    rotation_matrix[1][1] = math.cos(rotation_in_radian)

    p1_rotated = np.dot(rotation_matrix, p1)

    # print(p1_rotated)
    return p1_rotated[0][0], p1_rotated[1][0]

def blitRotate(surf, image, pos, originPos, angle):
    # calculate the axis aligned bounding box of the rotated image
    w, h = image.get_size()
    box = [Vector2(p) for p in [(0, 0), (w, 0), (w, -h), (0, -h)]]
    box_rotate = [p.rotate(angle) for p in box]
    min_box = (min(box_rotate, key=lambda p: p[0])[0], min(box_rotate, key=lambda p: p[1])[1])
    max_box = (max(box_rotate, key=lambda p: p[0])[0], max(box_rotate, key=lambda p: p[1])[1])

    # calculate the translation of the pivot
    pivot = Vector2(originPos[0], -originPos[1])
    pivot_rotate = pivot.rotate(angle)
    pivot_move = pivot_rotate - pivot

    # calculate the upper left origin of the rotated image
    origin = (pos[0] - originPos[0] + min_box[0] - pivot_move[0], pos[1] - originPos[1] - max_box[1] + pivot_move[1])

    # get a rotated image
    rotated_image = rotate(image, angle)

    # rotate and blit the image
    surf.blit(rotated_image, origin)

    # draw rectangle around the image
    # pygame.draw.rect (surf, (255, 0, 0), (*origin, *rotated_image.get_size()),2)
