from shutil import copyfile
import os

def create_dir_for_experiment(path_pkl_results_dir):
    #creating path_pkl_results_dir
    if not os.path.exists(path_pkl_results_dir):
        print("creating target_folder: ", path_pkl_results_dir)
        os.mkdir(path_pkl_results_dir)

    where_to_save_results_root = path_pkl_results_dir + '/test_rectangle_eval/'
    if not os.path.exists(where_to_save_results_root):
        print("creating target_folder: ", where_to_save_results_root)
        os.mkdir(where_to_save_results_root)

    #path to the pkl file where the results will be stored
    path_pkl_results_file = path_pkl_results_dir + 'results.pkl'

    return path_pkl_results_file

def copy_config_files(config_path, path_pkl_results_dir , config_name = "config.ini"):
    copyfile(config_path, path_pkl_results_dir + config_name)
