import cv2
import math
import numpy as np
try:
    from grasps_util import grasp_to_rectangle
except:
    from .grasps_util import grasp_to_rectangle

import sys

# BGR
def convert_to_rgb(val, minval=0.0, maxval=1.0, colors=[(0, 0, 255), (255, 0, 0), (0, 255, 0)]):
    # print('convert_to_rgb val: ', val)
    if val > maxval:
        val = maxval
    EPSILON = sys.float_info.epsilon  # Smallest possible difference.
    # "colors" is a series of RGB colors delineating a series of
    # adjacent linear color gradients between each pair.
    # Determine where the given value falls proportionality within
    # the range from minval->maxval and scale that fractional value
    # by the total number in the "colors" pallette.
    try:
        i_f = float(val - minval) / float(maxval - minval) * (len(colors) - 1)
    except Exception as e:
        print(e)
        i_f = 1.0
    # Determine the lower index of the pair of color indices this
    # value corresponds and its fractional distance between the lower
    # and the upper colors.
    try:
        i, f = int(i_f // 1), i_f % 1  # Split into whole & fractional parts.
    except Exception as e:
        print(e)
        f = 0.0
        i=0
    # Does it fall exactly on one of the color points?
    if f < EPSILON:
        return colors[i]
    else:  # Otherwise return a color within the range between them.
        try:
            (r1, g1, b1), (r2, g2, b2) = colors[i], colors[i + 1]
        except Exception as e:
            (r1, g1, b1), (r2, g2, b2) = (0, 0, 255), (0, 0, 255)

        return (int(r1 + f * (r2 - r1)), int(g1 + f * (g2 - g1)), int(b1 + f * (b2 - b1)))


def generate_image_with_grasps(grasps, cv2_img, destination=None, b_with_rectangles = True, thickness_line=5, default_color=None, b_with_circles=False, normalize_scores=False):
    # print('generate_image_with_grasps len: ', len(grasps))
    scores = [g[11] for g in grasps]
    if normalize_scores:
        # norm = np.linalg.norm(scores)
        # scores = scores / norm
        current_maxval = np.max(scores)
        current_minval = np.min(scores)
    else:
        current_minval = 0.0
        current_maxval = 1.0
    # print("minval: ", current_minval)
    # print("minval: ", current_maxval)

    # for grasp in grasps:
    for i in range(len(grasps)):
        grasp = grasps[i]
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        FOREIGN KEY (scene_id) REFERENCES scenes (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        x = grasp[3]
        y = grasp[4]
        angle = grasp[8]
        if default_color is None:
            # color = convert_to_rgb(grasp[11])
            color = convert_to_rgb(scores[i], minval=current_minval, maxval=current_maxval)

        else:
            color = default_color
        length = grasp[9]
        width = grasp[10]


        if b_with_rectangles:
            rectangle = grasp_to_rectangle(center=(y, x),
                                           angle=angle, length=length,
                                           width=width)

            rectangle = rectangle.astype('int32')

            blue = (255, 0, 0)
            yellow = (51, 255, 255)
            purple = (204, 204, 0)
            orange = (0, 128, 255)

            if b_with_circles:
                cv2_img = cv2.circle(cv2_img, (rectangle[0, 1], rectangle[0, 0]), 3, blue, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[1, 1], rectangle[1, 0]), 3, yellow, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[2, 1], rectangle[2, 0]), 3, orange, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[3, 1], rectangle[3, 0]), 3, purple, thickness=1, lineType=4)
            grasp_thickness = 1
            cv2_img = cv2.line(cv2_img, (rectangle[0, 1], rectangle[0, 0]), (rectangle[1, 1], rectangle[1, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[1, 1], rectangle[1, 0]), (rectangle[2, 1], rectangle[2, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[2, 1], rectangle[2, 0]), (rectangle[3, 1], rectangle[3, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[3, 1], rectangle[3, 0]), (rectangle[0, 1], rectangle[0, 0]),
                               color,
                               grasp_thickness, lineType=4)

        rotation_matrix = np.zeros((2, 2))
        rotation_matrix[0][0] = math.cos(angle)
        rotation_matrix[0][1] = -math.sin(angle)
        rotation_matrix[1][0] = math.sin(angle)
        rotation_matrix[1][1] = math.cos(angle)

        p1 = np.zeros((2, 1))
        p2 = np.zeros((2, 1))

        p1[0][0] = -int(length / 2)
        p2[0][0] = int(length / 2)

        p1_rotated = np.dot(rotation_matrix, p1)
        p2_rotated = np.dot(rotation_matrix, p2)
        try:
            p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
            p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))
        except:
            p1_recentered = (int(str(p1_rotated[0][0])) + int(str(x)), int(str(p1_rotated[1][0])) + int(str(y)))
            p2_recentered = (int(str(p2_rotated[0][0])) + int(str(x)), int(str(p2_rotated[1][0])) + int(str(y)))


        cv2_img = cv2.line(cv2_img, p1_recentered, p2_recentered,
                           color, thickness=thickness_line)#todo hardcoded

    if destination is not None:
        print("saving image to ", destination)
        cv2.imwrite(destination, cv2_img)

    return cv2_img
