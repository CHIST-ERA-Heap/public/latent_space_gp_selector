import configparser
import cv2
import numpy as np
import sensor_msgs.point_cloud2
import math
import sys
import cv2
import rospy
import warnings
import message_filters
import tf2_ros
import sensor_msgs.point_cloud2
from sensor_msgs.point_cloud2 import read_points
from geometry_msgs.msg import Transform, Pose, PoseStamped, TransformStamped
from std_msgs.msg import Bool
import copy
from cv_bridge import CvBridge, CvBridgeError
from grasping_benchmarks.base.transformations import quaternion_to_matrix, matrix_to_quaternion

from grasping_benchmarks_ros.srv import GraspPlanner, GraspPlannerRequest, GraspPlannerResponse
from scipy.spatial.transform import Rotation as R
from grasping_benchmarks_ros.srv import Reranking, RerankingRequest

from grasping_benchmarks_ros.srv import Segmentation, SegmentationRequest, SegmentationResponse

import numpy as np
from cv_bridge import CvBridge
from geometry_msgs.msg import Point32, TransformStamped
from sensor_msgs.msg import CameraInfo, Image, PointCloud2, PointField
from std_msgs.msg import Header
import ast
import configparser
import cv2
import numpy as np
import sensor_msgs.point_cloud2
import math
import sys
import cv2
import rospy
import warnings
import message_filters
import tf2_ros
import sensor_msgs.point_cloud2
from sensor_msgs.point_cloud2 import read_points
from geometry_msgs.msg import Transform, Pose, PoseStamped, TransformStamped
from std_msgs.msg import Bool
import copy
from cv_bridge import CvBridge, CvBridgeError
from grasping_benchmarks.base.transformations import quaternion_to_matrix, matrix_to_quaternion

from grasping_benchmarks_ros.srv import GraspPlanner, GraspPlannerRequest, GraspPlannerResponse
from scipy.spatial.transform import Rotation as R
from grasping_benchmarks_ros.srv import Reranking, RerankingRequest
from scripts.common.process_patch_util import get_grayscale_depth_from_raw
from grasping_benchmarks_ros.srv import Segmentation, SegmentationRequest, SegmentationResponse
import time
import numpy as np
from cv_bridge import CvBridge
from geometry_msgs.msg import Point32, TransformStamped
from sensor_msgs.msg import CameraInfo, Image, PointCloud2, PointField
from std_msgs.msg import Header
from grasping_benchmarks.base.base_grasp_planner import CameraData

import tf
# class CameraData:
#     rgb_img = None
#     background_img = None
#     depth_img = None
#     pc_img = None
#     seg_img = None
#     bounding_box = None
#     intrinsic_params = None
#     camera_extrinsic_matrix = None

def pack_bgr(blue, green, red):
    # Pack the 3 BGR channels into a single UINT32 field as RGB.
    return np.bitwise_or(
        np.bitwise_or(
            np.left_shift(red.astype(np.uint32), 16),
            np.left_shift(green.astype(np.uint32), 8)), blue.astype(np.uint32))


def pack_bgra(blue, green, red, alpha):
    # Pack the 4 BGRA channels into a single UINT32 field.
    return np.bitwise_or(
        np.left_shift(blue.astype(np.uint32), 24),
        np.bitwise_or(
            np.left_shift(green.astype(np.uint32), 16),
            np.bitwise_or(
                np.left_shift(red.astype(np.uint32), 8), alpha.astype(
                    np.uint32))))

def convert_bgrd_to_pcl(bgr_image, depth_image, cam_info, rgb=False):
    cx = cam_info.K[2]
    cy = cam_info.K[5]
    fx = cam_info.K[0]
    fy = cam_info.K[4]

    center_x = cx
    center_y = cy

    constant_x = 1 / fx
    constant_y = 1 / fy
    if rgb:
        pointcloud_xyzrgb_fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
            PointField(name='rgb', offset=12, datatype=PointField.UINT32, count=1)
        ]
    else:
        pointcloud_xyz_fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1)
        ]

    vs = np.array(
        [(v - center_x) * constant_x for v in range(0, depth_image.shape[1])])
    us = np.array(
        [(u - center_y) * constant_y for u in range(0, depth_image.shape[0])])

    # Convert depth from mm to m.
    depth_image = depth_image / 1000.0

    x = np.multiply(depth_image, vs)
    y = depth_image * us[:, np.newaxis]

    if rgb:
        stacked = np.ma.dstack((x, y, depth_image, bgr_image))

        compressed = stacked.compressed()
        pointcloud = compressed.reshape((int(compressed.shape[0] / 6), 6))

        pointcloud = np.hstack((pointcloud[:, 0:3],
                                pack_bgr(*pointcloud.T[3:6])[:, None]))
        pointcloud = [[point[0], point[1], point[2], int(point[3])]
                      for point in pointcloud]
    else:
        stacked = np.ma.dstack((x, y, depth_image))

        compressed = stacked.compressed()
        pointcloud = compressed.reshape((int(compressed.shape[0] / 3), 3))

        # pointcloud = np.hstack((pointcloud[:, 0:3],
        #                         pack_bgr(*pointcloud.T[3:6])[:, None]))
        pointcloud = [[point[0], point[1], point[2]]
                      for point in pointcloud]

    # print('pointcloud: ', pointcloud)
    header = Header()
    header.frame_id = "map"
    header.stamp = rospy.Time.now()

    if rgb:
        pointcloud = sensor_msgs.point_cloud2.create_cloud(header, pointcloud_xyzrgb_fields,
                                      pointcloud)
    else:
        pointcloud = sensor_msgs.point_cloud2.create_cloud(header, pointcloud_xyz_fields,
                                                           pointcloud)
    # pointcloud = sensor_msgs.point_cloud2.create_cloud(header=Header(), fields=pointcloud_xyzrgb_fields, points=pointcloud)

    return pointcloud


def read_intr_files(path_to_f):
    # importing the module

    # reading the data from the file
    with open(path_to_f) as f:
        data = f.read()

    # reconstructing the data as a dictionary
    d = ast.literal_eval(data)
    return d

class Camera:
    def __init__(self, camera_config_path):
        self.cfg = configparser.ConfigParser()
        self.cfg.read(camera_config_path)
        self.read_cfg()
        self.setup_camera()

    def read_cfg(self):
        #todo
        self.aruco_reference_frame = 'aruco_board'
        self.enable_grasp_filter = False

        self.root_reference_frame = self.cfg.get('camera','root_reference_frame')
        self.cam_info_msg_header_frame_id = self.cfg.get('camera','cam_info_msg_header_frame_id')

        self.number_of_seconds_to_wait_image = self.cfg.getint('camera', 'number_of_seconds_to_wait_image')
        self.fake_camera_info = self.cfg.get('camera','fake_camera_info')
        self.data_saving_folder = self.cfg.get('camera', 'data_saving_folder')
        self.camera_resolution_width = self.cfg.getint('camera', 'camera_resolution_width')
        self.camera_resolution_height = self.cfg.getint('camera', 'camera_resolution_height')
        self.camera_is_connected = self.cfg.getboolean('camera', 'camera_is_connected')
        self.rgb_extension = self.cfg.get('camera', 'rgb_extension')
        self.depth_extension = self.cfg.get('camera', 'depth_extension')
        self.path_to_background = self.cfg.get('camera', 'path_to_background')
        if self.path_to_background == 'None':
            self.path_to_background = None
            self.latest_background_rgb_image = None
            # print('self.latest_background_rgb_image is None')

        else:
            self.latest_background_rgb_image = cv2.imread(self.path_to_background)
            # print('self.latest_background_rgb_image.shape: ', self.latest_background_rgb_image.shape)

    def setup_camera(self):
        self.new_camera_data = False
        self.latest_rgb_image_path = self.data_saving_folder + self.rgb_extension
        self.latest_aligned_depth_path = self.data_saving_folder + self.depth_extension
        self.latest_rgb_image = None
        self.latest_aligned_depth = None
        self.cv_bridge = CvBridge()

        self.fill_fake_camera_info()

        # --- robot/camera transform listener --- #
        self.tfBuffer = tf2_ros.buffer.Buffer()
        listener = tf2_ros.transform_listener.TransformListener(self.tfBuffer)

        if self.camera_is_connected:
            # --- camera messages --- #
            self.cam_info_msg = None
            self.rgb_msg = None
            self.depth_msg = None
            self.pc_msg = None
            self.camera_pose = TransformStamped()

    def fill_fake_camera_info(self):
        self.cam_info = CameraInfo()
        '''
        # camera namespace on topic "camera_info" and accompanied by up to five
        # image topics named:
        #
        #   image_raw - raw data from the camera driver, possibly Bayer encoded
        #   image            - monochrome, distorted
        #   image_color      - color, distorted
        #   image_rect       - monochrome, rectified
        #   image_rect_color - color, rectified
        #
        # The image_pipeline contains packages (image_proc, stereo_image_proc)
        # for producing the four processed image topics from image_raw and
        # camera_info. The meaning of the camera parameters are described in
        # detail at http://www.ros.org/wiki/image_pipeline/CameraInfo.
        #
        # The image_geometry package provides a user-friendly interface to
        # common operations using this meta information. If you want to, e.g.,
        # project a 3d point into image coordinates, we strongly recommend
        # using image_geometry.
        #
        # If the camera is uncalibrated, the matrices D, K, R, P should be left
        # zeroed out. In particular, clients may assume that K[0] == 0.0
        # indicates an uncalibrated camera.
        
        #######################################################################
        #                     Image acquisition info                          #
        #######################################################################
        
        # Time of image acquisition, camera coordinate frame ID
        Header header    # Header timestamp should be acquisition time of image
                         # Header frame_id should be optical frame of camera
                         # origin of frame should be optical center of camera
                         # +x should point to the right in the image
                         # +y should point down in the image
                         # +z should point into the plane of the image
        
        
        #######################################################################
        #                      Calibration Parameters                         #
        #######################################################################
        # These are fixed during camera calibration. Their values will be the #
        # same in all messages until the camera is recalibrated. Note that    #
        # self-calibrating systems may "recalibrate" frequently.              #
        #                                                                     #
        # The internal parameters can be used to warp a raw (distorted) image #
        # to:                                                                 #
        #   1. An undistorted image (requires D and K)                        #
        #   2. A rectified image (requires D, K, R)                           #
        # The projection matrix P projects 3D points into the rectified image.#
        #######################################################################
        
        # The image dimensions with which the camera was calibrated. Normally
        # this will be the full camera resolution in pixels.
        uint32 height
        uint32 width
        
        # The distortion model used. Supported models are listed in
        # sensor_msgs/distortion_models.h. For most cameras, "plumb_bob" - a
        # simple model of radial and tangential distortion - is sufficient.
        string distortion_model
        
        # The distortion parameters, size depending on the distortion model.
        # For "plumb_bob", the 5 parameters are: (k1, k2, t1, t2, k3).
        float64[] D
        
        # Intrinsic camera matrix for the raw (distorted) images.
        #     [fx  0 cx]
        # K = [ 0 fy cy]
        #     [ 0  0  1]
        # Projects 3D points in the camera coordinate frame to 2D pixel
        # coordinates using the focal lengths (fx, fy) and principal point
        # (cx, cy).
        float64[9]  K # 3x3 row-major matrix
        
        # Rectification matrix (stereo cameras only)
        # A rotation matrix aligning the camera coordinate system to the ideal
        # stereo image plane so that epipolar lines in both stereo images are
        # parallel.
        float64[9]  R # 3x3 row-major matrix
        
        # Projection/camera matrix
        #     [fx'  0  cx' Tx]
        # P = [ 0  fy' cy' Ty]
        #     [ 0   0   1   0]
        # By convention, this matrix specifies the intrinsic (camera) matrix
        #  of the processed (rectified) image. That is, the left 3x3 portion
        #  is the normal camera intrinsic matrix for the rectified image.
        # It projects 3D points in the camera coordinate frame to 2D pixel
        #  coordinates using the focal lengths (fx', fy') and principal point
        #  (cx', cy') - these may differ from the values in K.
        # For monocular cameras, Tx = Ty = 0. Normally, monocular cameras will
        #  also have R = the identity and P[1:3,1:3] = K.
        # For a stereo pair, the fourth column [Tx Ty 0]' is related to the
        #  position of the optical center of the second camera in the first
        #  camera's frame. We assume Tz = 0 so both cameras are in the same
        #  stereo image plane. The first camera always has Tx = Ty = 0. For
        #  the right (second) camera of a horizontal stereo pair, Ty = 0 and
        #  Tx = -fx' * B, where B is the baseline between the cameras.
        # Given a 3D point [X Y Z]', the projection (x, y) of the point onto
        #  the rectified image is given by:
        #  [u v w]' = P * [X Y Z 1]'
        #         x = u / w
        #         y = v / w
        #  This holds for both images of a stereo pair.
        float64[12] P # 3x4 row-major matrix
        

        '''
        self.cam_info_msg_header_frame_id = "camera_depth_optical_frame"
        # todo investigate
        # Get the camera transform wrt the root reference frame of this class
        try:
            self.camera_pose = self.tfBuffer.lookup_transform(self.root_reference_frame, self.cam_info_msg_header_frame_id,
                                                         rospy.Time())
            print('fill_fake_camera_info - self.camera_pose: ', self.camera_pose)
        # except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
        except Exception as e:
            # warnings.warn("tf listener could not get camera pose. Are you publishing camera poses on tf?")
            print("fill_fake_camera_info - tf listener could not get camera pose. Are you publishing camera poses on tf?")


        if self.fake_camera_info == 'realsense':
            self.cam_info.width = 1280
            self.cam_info.height = 720

            self.cam_info.D = [0.0, 0.0, 0.0, 0.0, 0.0]
            self.cam_info.K = [895.9815063476562, 0.0, 638.4413452148438, 0.0, 895.9815063476562,
                                              360.1416015625, 0.0, 0.0, 1.0]
            self.cam_info.R = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
            self.cam_info.P = [895.9815063476562, 0.0, 638.4413452148438, 0.0, 0.0, 895.9815063476562,
                                              360.1416015625, 0.0, 0.0, 0.0, 1.0, 0.0]
            self.cam_info.distortion_model = "plumb_bob"
            self.cam_info.binning_x = 0
            self.cam_info.binning_y = 0
            self.cam_info.roi.x_offset = 0
            self.cam_info.roi.y_offset = 0
            self.cam_info.roi.height = 0
            self.cam_info.roi.width = 0
            self.cam_info.roi.do_rectify = False
            self.cam_info.header.frame_id = 'realsense'

            self.camera_pose = TransformStamped()
            self.camera_pose.transform.translation.x = 0.5350971426872632
            self.camera_pose.transform.translation.y = 0.10169077163023918
            self.camera_pose.transform.translation.z = 0.6538488949190373

            self.camera_pose.transform.rotation.x = -0.0010397419468549662
            self.camera_pose.transform.rotation.y = 0.9999949281346481
            self.camera_pose.transform.rotation.z = 0.00018072985235736104
            self.camera_pose.transform.rotation.w = 0.00018072985235736104

            self.aruco_board_pose = TransformStamped()
            self.enable_grasp_filter = False

        elif self.fake_camera_info == 'primesense':
            self.cam_info.width = 640
            self.cam_info.height = 480
            self.cam_info.D = [0.0, 0.0, 0.0, 0.0, 0.0]
            self.cam_info.K = [525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0]
            self.cam_info.R = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
            self.cam_info.P = [525.0, 0.0, 319.5, 0.0, 0.0, 525.0, 239.5, 0.0, 0.0, 0.0, 1.0, 0.0]
            self.cam_info.distortion_model = "plumb_bob"
            self.cam_info.binning_x = 0
            self.cam_info.binning_y = 0
            self.cam_info.roi.x_offset = 0
            self.cam_info.roi.y_offset = 0
            self.cam_info.roi.height = 0
            self.cam_info.roi.width = 0
            self.cam_info.roi.do_rectify = False
            self.cam_info.header.frame_id = 'primesense'

            #todo
            '''
            transform: 
            translation: 
            x: 0.42944684476603134
            y: 0.08312375012627417
            z: 0.7186310197019642
            rotation: 
            x: 0.7018607268549014
            y: -0.6759673304256282
            z: 0.1589571514088901
            w: -0.15872096131476807
            '''
            self.camera_pose = TransformStamped()
            self.camera_pose.transform.translation.x = 1.0
            self.camera_pose.transform.translation.y = 0.0
            self.camera_pose.transform.translation.z = 2.0

            self.camera_pose.transform.rotation.x = 0.7018607268549014
            self.camera_pose.transform.rotation.y = -0.6759673304256282
            self.camera_pose.transform.rotation.z = 0.1589571514088901
            self.camera_pose.transform.rotation.w = -0.15872096131476807

            self.aruco_board_pose = TransformStamped()
            self.enable_grasp_filter = False

        '''
       #     [fx  0 cx]
        # K = [ 0 fy cy]
        #     [ 0  0  1]
        '''
        fx = self.cam_info.K[0]
        fy = self.cam_info.K[4]
        cx = self.cam_info.K[2]
        cy = self.cam_info.K[5]
        skew = 0.0
        intrinsic_matrix = np.array([[fx, skew,  cx],
                                     [0,    fy,  cy],
                                     [0,     0,   1]])


        self.camera_data = CameraData()
        self.camera_data.intrinsic_params = {
                                        'fx' : fx,
                                        'fy' : fy,
                                        'cx' : cx,
                                        'cy' : cy,
                                        'skew' : skew,
                                        'w' : self.cam_info.width,
                                        'h' : self.cam_info.height,
                                        'frame' : self.cam_info.header.frame_id,
                                        'matrix' : intrinsic_matrix
                                        }



        orientation = self.camera_pose.transform.rotation
        position = self.camera_pose.transform.translation

        camera_position = np.array([position.x,
                                    position.y,
                                    position.z])
        camera_orientation = quaternion_to_matrix([orientation.x,
                                           orientation.y,
                                           orientation.z,
                                           orientation.w])
        camera_extrinsic_matrix = np.eye(4)
        camera_extrinsic_matrix[:3,:3] = camera_orientation
        camera_extrinsic_matrix[:3,3] = camera_position

        self.camera_data.camera_extrinsic_matrix = camera_extrinsic_matrix

    # ------------------- #
    # Camera data handler #
    # ------------------- #
    def camera_data_callback(self, cam_info, rgb, depth, pc):
        print('camera_data_callback')
        #todo check if you need some update
        self.cam_info_msg = cam_info
        self.rgb_msg = rgb
        self.depth_msg = depth
        self.pc_msg = pc

        # Get the camera transform wrt the root reference frame of this class
        print('Get the camera transform wrt the root reference frame of this class')
        try:
            self.camera_pose = self.tfBuffer.lookup_transform(self.root_reference_frame, self.cam_info_msg.header.frame_id, rospy.Time())
            print('camera_data_callback - self.camera_pose: ', self.camera_pose)

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            # warnings.warn("tf listener could not get camera pose. Are you publishing camera poses on tf?")
            print("camera_data_callback - tf listener could not get camera pose. Are you publishing camera poses on tf?")

            # self.camera_pose = TransformStamped()
            # self.camera_pose.transform.rotation.w = 1.0
            self.camera_pose = TransformStamped()
            self.camera_pose.transform.translation.x = 0.5350971426872632
            self.camera_pose.transform.translation.y = 0.10169077163023918
            self.camera_pose.transform.translation.z = 0.6538488949190373

            self.camera_pose.transform.rotation.x = -0.0010397419468549662
            self.camera_pose.transform.rotation.y = 0.9999949281346481
            self.camera_pose.transform.rotation.z = 0.00018072985235736104
            self.camera_pose.transform.rotation.w = 0.00018072985235736104

        # Get the aruco board transform wrt the root reference frame of this class.
        # If the aruco board is not found an exception is thrown and   _enable_grasp_filter
        # is set false in order to avoid filtering in plan_grasp function in graspnet_grasp_planner.py
        try:
            self.aruco_board_pose = self.tfBuffer.lookup_transform(self.root_reference_frame, self.aruco_reference_frame, rospy.Time(),rospy.Duration(1.0))
            self.enable_grasp_filter = True

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rospy.logwarn("tf listener could not get aruco board pose. Are you publishing aruco board poses on tf?")
            self.enable_grasp_filter = False



        fx = self.cam_info_msg.K[0]
        fy = self.cam_info_msg.K[4]
        cx = self.cam_info_msg.K[2]
        cy = self.cam_info_msg.K[5]
        skew = 0.0
        intrinsic_matrix = np.array([[fx, skew,  cx],
                                     [0,    fy,  cy],
                                     [0,     0,   1]])
        self.camera_data = CameraData()

        self.camera_data.intrinsic_params = {
                                        'fx' : fx,
                                        'fy' : fy,
                                        'cx' : cx,
                                        'cy' : cy,
                                        'skew' : skew,
                                        'w' : self.cam_info_msg.width,
                                        'h' : self.cam_info_msg.height,
                                        'frame' : self.cam_info_msg.header.frame_id,
                                        'matrix' : intrinsic_matrix
                                        }

        orientation = self.camera_pose.transform.rotation
        position = self.camera_pose.transform.translation

        camera_position = np.array([position.x,
                                    position.y,
                                    position.z])
        camera_orientation = quaternion_to_matrix([orientation.x,
                                           orientation.y,
                                           orientation.z,
                                           orientation.w])
        camera_extrinsic_matrix = np.eye(4)
        camera_extrinsic_matrix[:3,:3] = camera_orientation
        camera_extrinsic_matrix[:3,3] = camera_position

        self.camera_data.camera_extrinsic_matrix = camera_extrinsic_matrix

        #todo check if trouble
        self.cam_info = self.cam_info_msg
        self.new_camera_data = True

    def capture_data(self, verbose=True):
        self.cam_info_msg = None
        self.rgb_msg = None
        self.depth_msg = None
        self.pc_msg = None
        self.latest_rgb_image = None
        self.latest_aligned_depth = None
        self.new_camera_data = False

        # --- get images --- #
        if verbose:
            print('subscribing')
        self.cam_info_sub = message_filters.Subscriber('/camera/aligned_depth_to_color/camera_info', CameraInfo)
        self.rgb_sub = message_filters.Subscriber('/camera/color/image_raw', Image)
        self.depth_sub = message_filters.Subscriber('/camera/aligned_depth_to_color/image_raw', Image)
        self.pc_sub = message_filters.Subscriber('/camera/depth_registered/points', PointCloud2)
        # self.seg_sub = rospy.Subscriber('rgb/image_seg', Image, self.seg_img_callback, queue_size=10)

        # --- camera data synchronizer --- #
        self.tss = message_filters.ApproximateTimeSynchronizer(
            [self.cam_info_sub, self.rgb_sub, self.depth_sub, self.pc_sub],
            queue_size=2, slop=self.number_of_seconds_to_wait_image)
        #self.tss = message_filters.TimeSynchronizer([self.cam_info_sub, self.rgb_sub, self.depth_sub, self.pc_sub], queue_size=2)
        self.tss.registerCallback(self.camera_data_callback)

        if verbose:
            print("... waiting for images ...")

        start_time = time.time()
        while not self.new_camera_data:
            elapsed_time = time.time() - start_time
            if elapsed_time > self.number_of_seconds_to_wait_image:
                print("waited and did not received image")
                break

        if verbose:
            print('unsubscribing')
        self.cam_info_sub.unregister()
        self.rgb_sub.unregister()
        self.depth_sub.unregister()
        self.pc_sub.unregister()

        if not self.new_camera_data:
            if verbose:
                print("...no images received")
            return Bool(False)
        else:

            try:
                cv2_color = self.cv_bridge.imgmsg_to_cv2(self.rgb_msg, desired_encoding='bgr8')
                raw_depth = self.cv_bridge.imgmsg_to_cv2(self.depth_msg, desired_encoding='passthrough')
                cv2_depth = np.array(raw_depth, dtype=np.float32)

                cv2_depth *= 0.001

                # Fix NANs
                nans_idx = np.isnan(cv2_depth)
                cv2_depth[nans_idx] = 1000.0

            except CvBridgeError as cv_bridge_exception:
                rospy.logerr(cv_bridge_exception)

            # Check for image and depth size
            if (cv2_color.shape[0] != cv2_depth.shape[0]) or (cv2_color.shape[1] != cv2_depth.shape[1]):
                msg = "Mismatch between depth shape {}x{} and color shape {}x{}".format(cv2_depth.shape[0],
                                                                                        cv2_depth.shape[1],
                                                                                        cv2_color.shape[0],
                                                                                        cv2_color.shape[1])
                rospy.logerr(msg)
                raise rospy.ServiceException(msg)

            self.latest_rgb_image = cv2_color
            self.latest_aligned_depth = cv2_depth

            if verbose:
                print('saving ', self.latest_rgb_image_path)
            cv2.imwrite(self.latest_rgb_image_path, self.latest_rgb_image)
            np.save(self.latest_aligned_depth_path, self.latest_aligned_depth)
            if verbose:
                depth_image = get_grayscale_depth_from_raw(self.latest_aligned_depth, max_depth_value=3.0)
                print('saving ', self.data_saving_folder+'_gray.png')
                cv2.imwrite(self.data_saving_folder+'_gray.png', depth_image)

        self.new_camera_data = False

    def get_segmentation(self, cv_image, depth_raw, background_image=None, cam_info=None, verbose=False):

        #todo create pc_msg
        # PointCloud2
        pc_msg = convert_bgrd_to_pcl(cv_image, np.reshape(depth_raw*1000.0, (depth_raw.shape[0],depth_raw.shape[1])), cam_info)
        depth_msg = self.cv_bridge.cv2_to_imgmsg(depth_raw*1000.0, encoding="passthrough")
        rgb_msg = self.cv_bridge.cv2_to_imgmsg(cv_image, encoding="bgr8")#rgb8

        #load camera intrinsics extrinsics
        # create _rgb_msg with rgb_img
        # create _depth_msg with _depth_msg
        # create cam_info with cam_info_msg

        # Create the service request
        segmentation_req = SegmentationRequest()

        # Fill in the 2D camera data
        segmentation_req.color_image = rgb_msg
        if background_image is not None:
            background_msg = self.cv_bridge.cv2_to_imgmsg(background_image, encoding="bgr8")  # rgb8
            segmentation_req.background_image = background_msg
        segmentation_req.depth_image = depth_msg
        segmentation_req.camera_info = cam_info

        if verbose:
            print("... send request to server ...")

        # Plan for grasps
        try:
            reply = self.segmentation_planner(segmentation_req)
            if verbose:
                print("Service {} reply is: \n{}".format(self.segmentation_planner.resolved_name, reply))
        except rospy.ServiceException as e:
            print("Service {} call failed: {}".format(self.segmentation_planner.resolved_name, e))
            return Bool(False)

        reply = self.cv_bridge.imgmsg_to_cv2(reply.seg_image, desired_encoding='passthrough')
        cv2.imwrite('./reply.png',reply)

def test(cam):
    cam.capture_data()
    # if cam.latest_aligned_depth is not None:
    #     p = cam.image_point_to_cloud(cam.min_width / 2, cam.min_height / 2, np.median(cam.latest_aligned_depth))
    #     print('p: ', p)

if __name__ == '__main__':
    # Initialize the ROS node.
    rospy.init_node("camera_node", anonymous = True)
    #rate = rospy.Rate(10)  # 30hz
    rospy.loginfo("Realsense server is run")
    camera = Camera(camera_config_path='../test_config/test_camera_config.ini')
    test(camera)
    stop=False
    while stop is not True:
        stop = input("stop?y/n \n")
        if stop=="y":
            stop = True
        else:
            test(camera)

