from tensorflow.python.keras import backend as K
import os
import cv2
from tensorflow.keras import backend as K
import numpy as np
import tensorflow as tf
from PIL import Image
from tensorflow.python.framework.ops import disable_eager_execution
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../../')))
from scripts.models.common.model import create_decoder_yolo2, get_vae_example, create_encoder_with_base, create_base_yolo2, get_vae_model_yolo2, get_vae_model_vgg16, get_vae_model_vgg19, get_vae_model_default, get_vae_model_resnet50
from scripts.models.common.utils import LRTensorBoard, select_optimizer
disable_eager_execution()

def test():
    show_preview = False
    max_count = 100
    optimizer = 'ADAM'
    epochs=10
    inputShape = (128, 128, 7)
    batchSize = 4
    latentSize = 32
    reg = 1.0
    base_learning_rate = 1e-3
    result_dir_path = '../../data/test_grasp/'
    test_patches_dir_path = '../../data/test_grasp/test_patches/'
    #img = load_img('./img.jpg', target_size=inputShape[:-1])
    r = cv2.imread(test_patches_dir_path+'rgb.png')
    if show_preview:
        r_show = Image.fromarray(cv2.cvtColor(r, cv2.COLOR_BGR2RGB))
        r_show.show()
    sn = cv2.imread(test_patches_dir_path+'sn_depth.png')
    if show_preview:
        sn_show = Image.fromarray(cv2.cvtColor(sn, cv2.COLOR_BGR2RGB))       
        sn_show.show()   
    
    gray = cv2.imread(test_patches_dir_path+'gray_depth.png', cv2.IMREAD_GRAYSCALE)
    if show_preview:
    	gray_show = Image.fromarray(np.uint8(gray), 'L')   
    	gray_show.show()   
    x = np.dstack((np.asarray(r), np.asarray(gray), np.asarray(sn))).astype(
        np.float32)
    img1 = np.array(x, dtype=np.float32) * (2/255) - 1
    #img.show()
    img = []
    for i in range(batchSize):
        img.append(img1)
    img = np.array(img, dtype=np.float32)

    #img = np.array([img1]*batchSize) # make fake batches to improve GPU utilization
    print('img.shape: ', img.shape)


    # model, t_mean, t_log_var = get_vae_model_yolo2(inputShape, latentSize, n=5,
    #                     preprocessing_function_type='betweenminus1and1', base=None)

    # model, t_mean, t_log_var = get_vae_model_vgg16(inputShape, latentSize,weights=None, b_untrainable=False,
    #                     preprocessing_function_type='betweenminus1and1', base=None)

    # model, t_mean, t_log_var = get_vae_model_vgg19(inputShape, latentSize,weights=None, b_untrainable=False,
    #                     preprocessing_function_type='betweenminus1and1', base=None)

    model, t_mean, t_log_var = get_vae_model_resnet50(inputShape, latentSize,weights=None, b_untrainable=False,
                        preprocessing_function_type='betweenminus1and1', base=None)

    # model, t_mean, t_log_var = get_vae_model_default(inputShape, latentSize,
    #                     preprocessing_function_type='betweenminus1and1', base=None)

    def fc_rc_loss(x, t_decoded, type='mse'):
        if type == 'mae':
            return tf.reduce_mean(
                tf.reduce_sum(
                    tf.keras.losses.mean_absolute_error(x, t_decoded),
                    axis=(1, 2)
                )
            )
        elif type == 'mse':
            return tf.reduce_mean(
                tf.reduce_sum(
                    tf.keras.losses.mean_squared_error(x, t_decoded),
                    axis=(1, 2)
                )
            )
        elif type == 'huber':
            return tf.reduce_mean(
                tf.reduce_sum(
                    tf.keras.losses.huber(x, t_decoded),
                    axis=(1, 2)
                )
            )
        else:
            return tf.reduce_mean(
                tf.reduce_sum(
                    tf.keras.losses.binary_crossentropy(x, t_decoded),
                    axis=(1, 2)
                )
            )

    def fc_kl_loss(x, t_decoded):
        kl_loss = -0.5 * (1 + t_log_var - tf.square(t_mean) - tf.exp(t_log_var))
        return tf.reduce_mean(
            tf.reduce_sum(
                kl_loss,
                axis=1
            )
        )

    # Compute VAE loss
    def vae_loss(x, t_decoded):
        return K.mean(fc_rc_loss(x, t_decoded) + reg * fc_kl_loss(x, t_decoded))
       
    optimizer = select_optimizer(optimizer=optimizer, base_learning_rate=base_learning_rate)

    model.compile(loss=vae_loss, optimizer=optimizer, metrics=['mse', 'mae'])

    count=0
    while count < max_count:
        model.fit(img, img,
                    epochs=epochs,
                    batch_size=batchSize, verbose=1)

        pred = model.predict(img) # get the reconstructed image
        pred = np.uint8((pred + 1)* 255/2) # convert to regular image values
        
        gen_data = pred[0]
        base_name = result_dir_path+str(count)
        # r = batch_x[:, :, :, 0:3]
        r = gen_data[:, :, 0:3]
        rgb_name = base_name + '_rgb.png'
        print("saving ", rgb_name)
        cv2.imwrite(rgb_name, r)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
        if show_preview:
        	r_show = Image.fromarray(cv2.cvtColor(r, cv2.COLOR_BGR2RGB))  
        	r_show.show()   

        # gray = batch_x[:, :, :, 3:4]
        gray = gen_data[:, :, 3:4]
        gray_name = base_name + '_gray_depth.png'
        print("saving ", gray_name)
        cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
        if show_preview:
        	gray_show = Image.fromarray(np.uint8(gray).reshape((inputShape[0], inputShape[1])), 'L')   
        	gray_show.show()   

        # sn = batch_x[:, :, :, 4:]
        sn = gen_data[:, :, 4:]
        sn_name = base_name + '_sn_depth.png'
        print("saving ", sn_name)
        cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
        if show_preview:
        	sn_show = Image.fromarray(cv2.cvtColor(sn, cv2.COLOR_BGR2RGB))
        	sn_show.show()   


        count+=1
if __name__ == "__main__":
    test()
