from tensorflow.keras.callbacks import TensorBoard, Callback
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import SGD, RMSprop, Adam, Adagrad, Adadelta
from tensorflow_addons.optimizers import AdamW

class LRTensorBoard(TensorBoard):
    def __init__(self, log_dir, **kwargs):  # add other arguments to __init__ if you need
        super().__init__(log_dir=log_dir, **kwargs)

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        lr = K.eval(self.model.optimizer.lr)
        logs.update({'lr': lr})
        super().on_epoch_end(epoch, logs)
        print("epoch: ", epoch, " lr: ", lr)


# class EarlyStoppingByLossVal(TensorBoard):
#     def __init__(self, log_dir, **kwargs):  # add other arguments to __init__ if you need
#         super().__init__(log_dir=log_dir, **kwargs)
#
#     def on_epoch_end(self, epoch, logs=None):
#         logs = logs or {}
#         lr = K.eval(self.model.optimizer.lr)
#         logs.update({'lr': lr})
#         super().on_epoch_end(epoch, logs)
#         print("epoch: ", epoch, " lr: ", lr)

class EarlyStoppingByLrVal(Callback):
    def __init__(self, value=0.00001, verbose=0):
        super(Callback, self).__init__()
        self.value = value
        self.verbose = verbose

    def on_Epoch_end(self, Epoch, logs={}):
        current_lr = K.get_value(self.model.optimizer.lr)
        if current_lr <= self.value:
            if self.verbose > 0:
                print("Epoch %05d: early stopping THR" % Epoch)
            self.model.stop_training = True

class Flip_1(object):
    def __call__(self, X):
        for axis in [1]:
            if np.random.rand(1) < 0.5:
                X = np.flip(X, axis)
        return X

class Flip_2(object):
    def __call__(self, x):
        if np.random.rand(1) < 0.5:
            x = np.fliplr(x)
            x = x[..., ::-1] - np.zeros_like(x)

        return x

def find_between(s, first, last):
    try:
        start = s.index(first) + len(first)
        end = s.index(last, start)
        return s[start:end]
    except ValueError:
        return ""

def get_name_network(network_type, use_rgb, use_gray_depth, use_SN, reg, channels_number, bottleneck, dim_resized_patch, random_sizes=None, dim_grasp_patch=None):
    name = network_type

    name += 'from'
    if random_sizes is not None:
        for r in random_sizes:
            name += str(r) + '_'

    if dim_grasp_patch is not None:
        name += str(dim_grasp_patch) + '_'

    name += 'resized' + str(dim_resized_patch)
    if bottleneck is not None:
        name += '_bottleneck' + str(bottleneck)
    name += '_' + str(channels_number) + 'channels'
    if reg is not None:
        name += '_reg' + str(reg)

    name += '_'
    if use_rgb:
        name += 'rgb'
    if use_gray_depth:
        name += 'd'
    if use_SN:
        name += 'SN'

    return name

def get_channels_number(args):
    if (args.use_rgb and (not args.use_gray_depth) and (not args.use_SN)):
        channels_number = 3
    if (args.use_gray_depth and (not args.use_rgb) and (not args.use_SN)):
        channels_number = 1
    if (args.use_SN and (not args.use_rgb) and (not args.use_gray_depth)):
        channels_number = 3

    if (args.use_rgb and args.use_gray_depth and (not args.use_SN)):
        channels_number = 4
    if (args.use_rgb and args.use_SN and (not args.use_gray_depth)):
        channels_number = 6
    if (args.use_SN and args.use_gray_depth and (not args.use_rgb)):
        channels_number = 4
    if (args.use_SN and args.use_gray_depth and args.use_rgb):
        channels_number = 7
    return channels_number

class LRTensorBoard(TensorBoard):
    def __init__(self, log_dir, **kwargs):  # add other arguments to __init__ if you need
        super().__init__(log_dir=log_dir, **kwargs)

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        lr = K.eval(self.model.optimizer.lr)
        logs.update({'lr': lr})
        super().on_epoch_end(epoch, logs)
        print("epoch: ", epoch, " lr: ", lr)



def select_optimizer(optimizer, base_learning_rate):
    if optimizer == 'SGD':
        optimizer = SGD(learning_rate=base_learning_rate, momentum=0.9)
    elif optimizer == 'RMSPROP':
        optimizer = RMSprop(learning_rate=base_learning_rate)
    elif optimizer == 'ADAGRAD':
        optimizer = Adagrad(learning_rate=base_learning_rate)
    elif optimizer == 'ADADELTA':
        optimizer = Adadelta()
    elif optimizer == 'ADAM':
        optimizer = Adam(learning_rate=base_learning_rate)
    elif optimizer == 'ADAMW':
        WEIGHT_DECAY = 1e-4
        optimizer = AdamW(learning_rate=base_learning_rate, weight_decay=WEIGHT_DECAY)
    else:
        raise ImportError
    return optimizer