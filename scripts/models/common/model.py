from tensorflow.keras.utils import get_custom_objects
from tensorflow.keras.backend import sigmoid
from tensorflow.keras.layers import Conv2D, BatchNormalization, UpSampling2D, \
    LeakyReLU, Conv2DTranspose, GlobalAveragePooling2D, Reshape, \
    Dense, Activation, Dropout, add, Flatten, MaxPool2D, Lambda

from tensorflow.keras.models import Model
from tensorflow.keras import backend as K
from tensorflow.keras.applications.vgg19 import VGG19
from tensorflow.keras.applications.resnet import ResNet50
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input
from tensorflow.keras.metrics import Mean
import tensorflow as tf
from tensorflow.keras import layers


def get_vae_example_base(vae_input):
    x = layers.Conv2D(32, 3, activation="relu", strides=2, padding="same")(vae_input)
    x = layers.Conv2D(64, 3, activation="relu", strides=2, padding="same")(x)

    return Model(vae_input, x, name='base')


def get_vae_example_decoder(vae_input):
    x = layers.Conv2D(32, 3, activation="relu", strides=2, padding="same")(vae_input)
    x = layers.Conv2D(64, 3, activation="relu", strides=2, padding="same")(x)

    return Model(vae_input, x, name='base')

def get_vae_example_decoder(latent_dim, image_shape, preprocessing_function_type):
    decoder_input = Input(shape=(latent_dim,), name='t')


    x = Dense(int(image_shape[0]*image_shape[0]*4), activation="relu")(decoder_input)
    x = Reshape((int(image_shape[0]/4), int(image_shape[0]/4), 64))(x)
    x = Conv2DTranspose(64, 3, activation="relu", strides=2, padding="same")(x)
    x = Conv2DTranspose(32, 3, activation="relu", strides=2, padding="same")(x)

    if preprocessing_function_type=='betweenminus1and1':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
            x)
    elif preprocessing_function_type=='between0and1':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image')(
            x)
    elif preprocessing_function_type=='imagenet':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
            x)

    return Model(decoder_input, x, name='decoder')




def get_vae_example(image_shape, latent_dim, weights=None, b_untrainable=False, preprocessing_function_type='between0and1', base=None):
    vae_input = Input(shape=image_shape, name='encoder_input')
    if base is None:
        base = get_vae_example_base(vae_input)

    encoder = create_encoder_with_base_2(base, latent_dim)

    t_mean, t_log_var = encoder(vae_input)
    sampler = create_sampler()
    t = sampler([t_mean, t_log_var])
    conv_shape = K.int_shape(base.layers[-1].output)

    decoder = get_vae_example_decoder(latent_dim, image_shape, preprocessing_function_type=preprocessing_function_type, conv_shape=conv_shape)

    t_decoded = decoder(t)

    # vae = VAE(encoder, decoder)
    vae = Model(vae_input, t_decoded, name='composite')

    # if verbose:
    #     encoder.summary()
    #     decoder.summary()
    # vae.summary()

    return vae, t_mean, t_log_var

def identity_block(input_tensor, kernel_size, filters, stage, block):
    """The identity block is the block that has no conv layer at shortcut.
    # Arguments
        input_tensor: input tensor
        kernel_size: defualt 3, the kernel size of middle conv layer at main path
        filters: list of integers, the filterss of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
    # Returns
        Output tensor for the block.
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Conv2D(filters1, (1, 1), name=conv_name_base + '2a')(input_tensor)

    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)

    x = Activation('relu')(x)

    x = Conv2D(filters2, kernel_size,
               padding='same', name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    x = add([x, input_tensor])
    x = Activation('relu')(x)

    return x


def conv_block(input_tensor, kernel_size, filters, stage, block,
               strides=(2, 2), transpose=False):
    """conv_block is the block that has a conv layer at shortcut
    # Arguments
        input_tensor: input tensor
        kernel_size: defualt 3, the kernel size of middle conv layer at main path
        filters: list of integers, the filterss of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
    # Returns
        Output tensor for the block.
    Note that from stage 3, the first conv layer at main path is with strides=(2,2)
    And the shortcut should have strides=(2,2) as well
    """
    filters1, filters2, filters3 = filters
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    convfn = Conv2D if not transpose else Conv2DTranspose

    x = convfn(filters1, (1, 1), strides=strides,
               name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters2, kernel_size, padding='same',
               name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    shortcut = convfn(filters3, (1, 1), strides=strides,
                      name=conv_name_base + '1')(input_tensor)
    shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)

    x = add([x, shortcut])
    x = Activation('relu')(x)
    return x

def create_dense_layers(stage, width, activation='sigmoid', dropout=0.0):
    dense_name = '_'.join(['enc_conv', str(stage)])
    bn_name = '_'.join(['enc_bn', str(stage)])
    layers = [
        Dense(width, name=dense_name),
        BatchNormalization(name=bn_name),
        Activation(activation),
        Dropout(dropout),
    ]
    return layers

def inst_layers(layers, in_layer):
    x = in_layer
    for layer in layers:
        if isinstance(layer, list):
            x = inst_layers(layer, x)
        else:
            x = layer(x)

    return x

def resnet_layers(x, depth, stage_base, transpose=False):
    assert depth in [i for i in range(50)]

    filters = [2048, 512, 2048, 512, 2048, 512, 1024, 256, 1024, 256, 1024, 256, 1024, 256, 1024, 256, 1024, 256, 512, 128, 512, 128, 512, 128, 512, 128, 256, 64, 256, 64, 256, 64]
    x = conv_block(x, 3, filters, stage=stage_base + 2, block='a', strides=(1, 1), transpose=transpose)
    if depth >= 2:
        x = identity_block(x, 3, filters, stage=stage_base + 2, block='b')
    if depth >= 3:
        x = identity_block(x, 3, filters, stage=stage_base + 2, block='c')

    filters = [128, 128, 512]
    x = conv_block(x, 3, filters, stage=stage_base + 3, block='a', transpose=transpose)
    if depth >= 1:
        x = identity_block(x, 3, filters, stage=stage_base + 3, block='b')
    if depth >= 2:
        x = identity_block(x, 3, filters, stage=stage_base + 3, block='c')
    if depth >= 3:
        x = identity_block(x, 3, filters, stage=stage_base + 3, block='d')

    filters = [256, 256, 1024]
    x = conv_block(x, 3, filters, stage=stage_base + 4, block='a', transpose=transpose)
    if depth >= 1:
        x = identity_block(x, 3, filters, stage=stage_base + 4, block='b')
    if depth >= 2:
        x = identity_block(x, 3, filters, stage=stage_base + 4, block='c')
        x = identity_block(x, 3, filters, stage=stage_base + 4, block='d')
    if depth >= 3:
        x = identity_block(x, 3, filters, stage=stage_base + 4, block='e')
        x = identity_block(x, 3, filters, stage=stage_base + 4, block='f')

    filters = [512, 512, 2048]
    x = conv_block(x, 3, filters, stage=stage_base + 5, block='a', transpose=transpose)
    if depth >= 2:
        x = identity_block(x, 3, filters, stage=stage_base + 5, block='b')
    if depth >= 3:
        x = identity_block(x, 3, filters, stage=stage_base + 5, block='c')

    return x

def get_resnet50_base(encoder_input,weights,image_shape, b_untrainable):
    base = ResNet50(input_tensor=encoder_input, include_top=False, weights=weights, input_shape=image_shape)
    base._name = "base"
    if b_untrainable:
        for layer in base.layers:
            layer.trainable = False
    return base


def get_vae_model_resnet50(image_shape, latent_dim, weights=None, b_untrainable=False, preprocessing_function_type='between0and1', base=None, old=False):
    vae_input = Input(shape=image_shape, name='encoder_input')
    if base is None:
        base = get_resnet50_base(vae_input, weights, image_shape, b_untrainable)

    conv_shape = K.int_shape(base.layers[-1].output)

    encoder = create_encoder_with_base(base, latent_dim)
    # encoder = create_encoder_with_base_3(base, latent_dim)
    decoder = create_decoder_resnet50(image_shape, latent_dim, conv_shape=conv_shape, preprocessing_function_type=preprocessing_function_type)

    # vae = VAE(encoder, decoder)
    t_mean, t_log_var = encoder(vae_input)
    sampler = create_sampler()
    t = sampler([t_mean, t_log_var])
    t_decoded = decoder(t)

    # vae = VAE(encoder, decoder)
    vae = Model(vae_input, t_decoded, name='composite')

    return vae, t_mean, t_log_var

def create_decoder_resnet50(image_shape, latent_dim, preprocessing_function_type='between0and1', conv_shape=None, resnet_depth=50):
    decoder_input = Input(shape=(latent_dim,), name='t')
    # decoder_layers = [
    #     create_dense_layers(stage=10, width=intermediate_dim),
    #     Reshape((4, 4, intermediate_dim // 16)),
    # ]
    # dec_out = inst_layers(decoder_layers, decoder_input)
    x = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3])(decoder_input)
    x = Activation('relu')(x)

    # x = BatchNormalization()(x)
    x = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(x)

    # x = Reshape((1, 1, latent_dim))(decoder_input)
    # x = UpSampling2D((image_shape[1] // 2**5, image_shape[1] // 2**5))(x)

    # x = resnet_layers(x, depth=resnet_depth, transpose=True, stage_base=10)
    # decoder_out = Conv2DTranspose(name='x_decoded', filters=3, kernel_size=1, strides=1, activation='sigmoid')(dec_out)
    # if tanh:
    #     decoder_out = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(x)  # tanh
    # else:
    #     decoder_out = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image')(x)

    bn_axis = -1  # tf order
    x = conv_block(x, 3, [2048, 512, 512], stage=5, block='a', transpose=True)
    x = identity_block(x, 3, [2048, 512, 512], stage=5, block='b')
    x = identity_block(x, 3, [2048, 512, 512], stage=5, block='c')

    x = conv_block(x, 3, [1024, 256, 256], stage=4, block='a', transpose=True)
    x = identity_block(x, 3, [1024, 256, 256], stage=4, block='b')
    x = identity_block(x, 3, [1024, 256, 256], stage=4, block='c')
    x = identity_block(x, 3, [1024, 256, 256], stage=4, block='d')
    x = identity_block(x, 3, [1024, 256, 256], stage=4, block='e')
    x = identity_block(x, 3, [1024, 256, 256], stage=4, block='f')

    x = conv_block(x, 3, [512, 128, 128], stage=3, block='a', transpose=True)
    x = identity_block(x, 3, [512, 128, 128], stage=3, block='b')
    x = identity_block(x, 3, [512, 128, 128], stage=3, block='c')
    x = identity_block(x, 3, [512, 128, 128], stage=3, block='d')

    x = conv_block(x, 3, [256, 64, 64], stage=2, block='a', strides=(2, 2), transpose=True)
    x = identity_block(x, 3, [256, 64, 64], stage=2, block='b')
    x = identity_block(x, 3, [256, 64, 64], stage=2, block='c')

    x = UpSampling2D((2, 2), name='upsample_last_upsample')(x)
    # x = Conv2D(3, (3, 3), padding='same', name='upsample_conv1')(x)

    if preprocessing_function_type=='betweenminus1and1':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
            x)  # tanh ? #todo between
    elif preprocessing_function_type=='between0and1':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image')(
            x)  # tanh ? #todo between
    elif preprocessing_function_type=='imagenet':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
            x)  # tanh ? #todo between

    return Model(inputs=decoder_input, outputs=x, name='decoder')


def get_ae_model_resnet50(image_shape, latent_dim, weights=None, b_untrainable=False, preprocessing_function_type='between0and1', base=None):
    ae_input = Input(shape=image_shape, name='encoder_input')
    if base is None:
        base = get_resnet50_base(ae_input, weights,image_shape, b_untrainable)

    # base.summary()

    encoder, conv_shape = create_Aencoder_with_base(base, latent_dim)
    encoder.summary()

    decoder = create_decoder_resnet50(image_shape, latent_dim, conv_shape=conv_shape, preprocessing_function_type=preprocessing_function_type)
    decoder.summary()

    # vae = VAE(encoder, decoder)
    encoded = encoder(ae_input)
    t_decoded = decoder(encoded)

    # vae = VAE(encoder, decoder)
    ae = Model(ae_input, t_decoded, name='composite')

    return ae


def create_binary_resnet50(image_shape, weights='imagenet', b_untrainable=True, dense_units=128):#224,244,3
    input = Input(shape=image_shape, name='classifier_input')
    base = ResNet50(input_tensor=input, include_top=False, weights=weights, input_shape=image_shape)
    base._name = "base"
    if b_untrainable:
        for layer in base.layers:
            layer.trainable = False

    x = Flatten()(base.layers[-1].output)
    # x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    output = Dense(1, activation='sigmoid')(x)

    return Model(inputs=base.layers[0].input, outputs=output, name='classifer')

def create_base_yolo2(encoder_input, n=5):
    input = encoder_input

    fn_conv2D = Conv2D
    fn_conv2DT = Conv2DTranspose
    fn_dense = Dense

    class ConvBnLRelu(object):
        def __init__(self, filters, kernelSize, strides=1):
            self.filters = filters
            self.kernelSize = kernelSize
            self.strides = strides

        # return conv + bn + leaky_relu model
        def __call__(self, x, training=None):
            x = fn_conv2D(self.filters, self.kernelSize, strides=self.strides, padding='same')(x)
            x = BatchNormalization()(x, training=training)
            x = LeakyReLU()(x)
            return x

    x = ConvBnLRelu(2 ** n, kernelSize=3)(input)  # 1
    x = MaxPool2D((2, 2), strides=(2, 2))(x)

    x = ConvBnLRelu(2 ** (n + 1), kernelSize=3)(x)  # 2
    x = MaxPool2D((2, 2), strides=(2, 2))(x)

    x = ConvBnLRelu(2 ** (n + 2), kernelSize=3)(x)  # 3
    x = ConvBnLRelu(2 ** (n + 1), kernelSize=1)(x)  # 4
    x = ConvBnLRelu(2 ** (n + 2), kernelSize=3)(x)  # 5
    x = MaxPool2D((2, 2), strides=(2, 2))(x)

    x = ConvBnLRelu(2 ** (n + 3), kernelSize=3)(x)  # 6
    x = ConvBnLRelu(2 ** (n + 2), kernelSize=1)(x)  # 7
    x = ConvBnLRelu(2 ** (n + 3), kernelSize=3)(x)  # 8
    x = MaxPool2D((2, 2), strides=(2, 2))(x)

    x = ConvBnLRelu(2 ** (n + 4), kernelSize=3)(x)  # 9
    x = ConvBnLRelu(2 ** (n + 3), kernelSize=1)(x)  # 10
    x = ConvBnLRelu(2 ** (n + 4), kernelSize=3)(x)  # 11
    x = ConvBnLRelu(2 ** (n + 3), kernelSize=1)(x)  # 12
    x = ConvBnLRelu(2 ** (n + 4), kernelSize=3)(x)  # 13
    x = MaxPool2D((2, 2), strides=(2, 2))(x)

    x = ConvBnLRelu(2 ** (n + 5), kernelSize=3)(x)  # 14
    x = ConvBnLRelu(2 ** (n + 4), kernelSize=1)(x)  # 15
    x = ConvBnLRelu(2 ** (n + 5), kernelSize=3)(x)  # 2**4
    x = ConvBnLRelu(2 ** (n + 4), kernelSize=1)(x)  # 17
    output = ConvBnLRelu(2 ** (n + 5), kernelSize=3)(x)  # 18

    return Model(input, output, name='base')

def create_decoder_yolo2(image_shape, latent_dim, preprocessing_function_type='between0and1', conv_shape=None, old=False):
    fn_conv2D = Conv2D
    fn_conv2DT = Conv2DTranspose
    fn_dense = Dense

    class ConvBnLRelu(object):
        def __init__(self, filters, kernelSize, strides=1):
            self.filters = filters
            self.kernelSize = kernelSize
            self.strides = strides

        # return conv + bn + leaky_relu model
        def __call__(self, x, training=None):
            x = fn_conv2DT(self.filters, self.kernelSize, strides=self.strides, padding='same')(x)
            x = BatchNormalization()(x, training=training)
            x = LeakyReLU()(x)
            return x
    inLayer = Input(shape=(latent_dim,), name='t')

    if old:
        net = Reshape((1, 1, latent_dim))(inLayer)
        # darknet downscales input by a factor of 32, so we upsample to the second to last output shape:
        net = UpSampling2D((image_shape[0] // 32, image_shape[1] // 32))(net)
    else:
        net = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3])(inLayer)
        net = Activation('relu')(net)

    # x = BatchNormalization()(x)
    net = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(net)

    # TODO try inverting num filter arangement (e.g. 512, 1204, 512, 1024, 512)
    # and also try (1, 3, 1, 3, 1) for the filter shape
    net = ConvBnLRelu(1024, kernelSize=3)(net)
    net = ConvBnLRelu(512, kernelSize=1)(net)
    net = ConvBnLRelu(1024, kernelSize=3)(net)
    net = ConvBnLRelu(512, kernelSize=1)(net)
    net = ConvBnLRelu(1024, kernelSize=3)(net)

    net = UpSampling2D((2, 2))(net)
    net = ConvBnLRelu(512, kernelSize=3)(net)
    net = ConvBnLRelu(256, kernelSize=1)(net)
    net = ConvBnLRelu(512, kernelSize=3)(net)
    net = ConvBnLRelu(256, kernelSize=1)(net)
    net = ConvBnLRelu(512, kernelSize=3)(net)

    net = UpSampling2D((2, 2))(net)
    net = ConvBnLRelu(256, kernelSize=3)(net)
    net = ConvBnLRelu(128, kernelSize=1)(net)
    net = ConvBnLRelu(256, kernelSize=3)(net)

    net = UpSampling2D((2, 2))(net)
    net = ConvBnLRelu(128, kernelSize=3)(net)
    net = ConvBnLRelu(64, kernelSize=1)(net)
    net = ConvBnLRelu(128, kernelSize=3)(net)

    net = UpSampling2D((2, 2))(net)
    net = ConvBnLRelu(64, kernelSize=3)(net)

    net = UpSampling2D((2, 2))(net)
    net = ConvBnLRelu(32, kernelSize=3)(net)
    net = ConvBnLRelu(64, kernelSize=1)(net)

    # net = ConvBnLRelu(3, kernelSize=1)(net, training=self.training)
    # net = Conv2D(filters=image_shape[-1], kernel_size=(1, 1),
    #              padding='same', activation="tanh")(net)

    if preprocessing_function_type=='betweenminus1and1':
        net = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
            net)  # tanh ? #todo between
    elif preprocessing_function_type=='between0and1':
        net = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image')(
            net)  # tanh ? #todo between
    elif preprocessing_function_type=='imagenet':
        net = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
            net)  # tanh ? #todo between

    return Model(inLayer, net, name='decoder')


def get_vae_model_yolo2(image_shape, latent_dim, n=5, preprocessing_function_type='between0and1', base=None, old=False):
    vae_input = Input(shape=image_shape, name='encoder_input')
    if base is None:
        base = create_base_yolo2(vae_input, n)

    conv_shape = K.int_shape(base.layers[-1].output)

    encoder = create_encoder_with_base(base, latent_dim)
    # encoder = create_encoder_with_base_3(base, latent_dim)

    decoder = create_decoder_yolo2(image_shape, latent_dim, conv_shape=conv_shape, preprocessing_function_type=preprocessing_function_type, old=old)

    # vae = VAE(encoder, decoder)
    t_mean, t_log_var = encoder(vae_input)

    #before
    # sampler = create_sampler()
    # t = sampler([t_mean, t_log_var])

    t= Sampling()([t_mean, t_log_var])

    t_decoded = decoder(t)

    # vae = VAE(encoder, decoder)
    vae = Model(vae_input, t_decoded, name='composite')

    return vae, t_mean, t_log_var

def create_encoder_with_base_2(base, latent_dim, dim=128):
    x = layers.Flatten()(base.layers[-1].output)
    x = layers.Dense(dim, activation="relu")(x)

    z_mean = layers.Dense(latent_dim, name="z_mean")(x)
    z_log_var = layers.Dense(latent_dim, name="z_log_var")(x)

    return Model(base.layers[0].input, [z_mean, z_log_var], name="encoder")

def create_encoder_with_base_3(base, latent_dim, dim=128):
    x = layers.Flatten()(base.layers[-1].output)
    # x = layers.Dense(dim, activation="relu")(x)

    z_mean = layers.Dense(latent_dim, name="z_mean")(x)
    z_log_var = layers.Dense(latent_dim, name="z_log_var")(x)

    return Model(base.layers[0].input, [z_mean, z_log_var], name="encoder")

def create_multilabel_classifier_with_base(base, nb_of_class, b_untrainable=False, dense_units=128):

    if b_untrainable:
        for layer in base.layers:
            layer.trainable = False

    x = Flatten()(base.layers[-1].output)
    # x = Dense(dense_units, activation=activation)(x)#kernel_initializer='he_uniform'
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    output = Dense(nb_of_class, activation='softmax')(x)

    return Model(base.layers[0].input, output, name='classifer')


def create_binary_classifier_with_base(base, b_untrainable=False, dense_units=128, activation='relu'):

    if b_untrainable:
        for layer in base.layers:
            layer.trainable = False

    x = Flatten()(base.layers[-1].output)
    # x = Dense(dense_units, activation=activation)(x)#kernel_initializer='he_uniform'
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    output = Dense(1, activation='sigmoid')(x)

    return Model(base.layers[0].input, output, name='classifer')


def create_binary_classifier_yolo2(image_shape, dense_units=128, activation='relu', n=5):
    input = Input(shape=image_shape, name='classifier_input')

    base = create_base_yolo2(input, image_shape, n)

    # x = layers.Flatten()(x)
    # x = Dense(1, activation='sigmoid')(x)
    # add new classifier layers
    x = Flatten()(base.layers[-1].output)
    # x = Dense(dense_units, activation=activation)(x)#kernel_initializer='he_uniform'
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    output = Dense(1, activation='sigmoid')(x)

    return Model(base.layers[0].input, output, name='classifer')

def create_multilabel_classifier_small(image_shape, nb_of_class, dense_units=1024):
    input = Input(shape=image_shape, name='classifier_input')
    # x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(input)
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(input)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    output = Dense(nb_of_class, activation='softmax')(x)

    return Model(inputs=input, outputs=output, name='classifer')

def create_binary_classifier_small(image_shape, dense_units=1024):
    input = Input(shape=image_shape, name='classifier_input')
    # x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(input)
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(input)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    output = Dense(1, activation='sigmoid')(x)

    return Model(inputs=input, outputs=output, name='classifer')

def create_binary_classifier_vgg16(image_shape, weights='imagenet', b_untrainable=True, dense_units=128):#224,244,3
    input = Input(shape=image_shape, name='classifier_input')
    base = VGG16(input_tensor=input, include_top=False, weights=weights, input_shape=image_shape)
    base._name = "base"
    if b_untrainable:
        for layer in base.layers:
            layer.trainable = False

    x = Flatten()(base.layers[-1].output)
    # x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    output = Dense(1, activation='sigmoid')(x)

    return Model(inputs=base.layers[0].input, outputs=output, name='classifer')

def create_Aencoder_with_base(base, latent_dim, dense_units=1024):

    # encoded = layers.Dense(latent_dim, activation='relu')(base.layers[-1].output)
    conv_shape = K.int_shape(base.layers[-1].output)

    x = Flatten()(base.layers[-1].output)
    # x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    encoded = Dense(latent_dim, activation='relu')(x)

    return Model(base.layers[0].input, encoded, name="encoder"), conv_shape


def create_encoder_with_base(base, latent_dim):

    z_mean = Conv2D(filters=latent_dim, kernel_size=(1, 1),
                                  padding='same')(base.layers[-1].output)

    z_mean = GlobalAveragePooling2D(name='t_mean')(z_mean)

    z_log_var = Conv2D(filters=latent_dim, kernel_size=(1, 1),
                                     padding='same')(base.layers[-1].output)
    z_log_var = GlobalAveragePooling2D(name='t_log_var')(z_log_var)

    # z = Sampling()([z_mean, z_log_var])
    # return Model(base.layers[0].input, [z_mean, z_log_var, z], name="encoder")
    return Model(base.layers[0].input, [z_mean, z_log_var], name="encoder")


def get_vgg16_base(encoder_input,weights,image_shape, b_untrainable):
    base = VGG16(input_tensor=encoder_input, include_top=False, weights=weights, input_shape=image_shape)
    base._name = "base"
    if b_untrainable:
        for layer in base.layers:
            layer.trainable = False
    return base

def get_feature_extraction_model_with_base(base):
    feature_extraction_model = Sequential(
        [
            base,
            GlobalAveragePooling2D(name="avg_pool"),
            BatchNormalization(),
            # BatchNormalization(),
            # GlobalAveragePooling1D(),
        ],
        name="feature_extraction_model",
    )
    return feature_extraction_model

def get_model_feature_extraction_vgg16(image_shape, weights=None, b_untrainable=False, base=None):
    feature_extraction_input = Input(shape=image_shape, name='feature_extraction_input')
    if base is None:
        base = get_vgg16_base(feature_extraction_input, weights, image_shape, b_untrainable)

    feature_extraction_model = Sequential(
        [
            base,
            GlobalAveragePooling2D(name="avg_pool"),
            BatchNormalization(),
            # BatchNormalization(),
            # GlobalAveragePooling1D(),
        ],
        name="feature_extraction_model",
    )
    return feature_extraction_model

def get_model_feature_extraction_vgg19(image_shape, weights=None, b_untrainable=False, base=None):
    feature_extraction_input = Input(shape=image_shape, name='feature_extraction_input')
    if base is None:
        base = get_vgg19_base(feature_extraction_input, weights, image_shape, b_untrainable)

    feature_extraction_model = Sequential(
        [
            base,
            GlobalAveragePooling2D(name="avg_pool"),
            BatchNormalization(),
            # BatchNormalization(),
            # GlobalAveragePooling1D(),
        ],
        name="feature_extraction_model",
    )
    return feature_extraction_model


def get_vae_model_vgg16(image_shape, latent_dim, weights=None, b_untrainable=False, preprocessing_function_type='between0and1', base=None, old=False):
    vae_input = Input(shape=image_shape, name='encoder_input')
    if base is None:
        base = get_vgg16_base(vae_input, weights,image_shape, b_untrainable)

    encoder = create_encoder_with_base(base, latent_dim)
    # encoder = create_encoder_with_base_2(base, latent_dim)
    # encoder = create_encoder_with_base_3(base, latent_dim)

    conv_shape = K.int_shape(base.layers[-1].output)

    decoder = create_decoder_vgg16(image_shape, latent_dim, preprocessing_function_type=preprocessing_function_type, conv_shape=conv_shape, old=old)

    t_mean, t_log_var = encoder(vae_input)
    sampler = create_sampler()
    t = sampler([t_mean, t_log_var])
    t_decoded = decoder(t)

    # vae = VAE(encoder, decoder)
    vae = Model(vae_input, t_decoded, name='composite')

    return vae, t_mean, t_log_var

def get_ae_model_vgg16(image_shape, latent_dim, weights=None, b_untrainable=False, preprocessing_function_type='between0and1', base=None):
    vae_input = Input(shape=image_shape, name='encoder_input')
    if base is None:
        base = get_vgg16_base(vae_input, weights,image_shape, b_untrainable)
    encoder, convshape = create_Aencoder_with_base(base, latent_dim)

    conv_shape = K.int_shape(base.layers[-1].output)

    decoder = create_decoder_vgg16(image_shape, latent_dim, preprocessing_function_type=preprocessing_function_type, conv_shape=conv_shape)

    encoded = encoder(vae_input)
    t_decoded = decoder(encoded)

    # vae = VAE(encoder, decoder)
    vae = Model(vae_input, t_decoded, name='composite')

    return vae

def create_decoder_vgg16(image_shape, latent_dim, preprocessing_function_type='between0and1', conv_shape=None, old=False):
    '''

    Model: "model"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #
    =================================================================
    input_1 (InputLayer)         [(None, 224, 224, 3)]     0
    _________________________________________________________________
    block1_conv1 (Conv2D)        (None, 224, 224, 64)      1792
    _________________________________________________________________
    block1_conv2 (Conv2D)        (None, 224, 224, 64)      36928
    _________________________________________________________________
    block1_pool (MaxPooling2D)   (None, 112, 112, 64)      0
    _________________________________________________________________
    block2_conv1 (Conv2D)        (None, 112, 112, 128)     73856
    _________________________________________________________________
    block2_conv2 (Conv2D)        (None, 112, 112, 128)     147584
    _________________________________________________________________
    block2_pool (MaxPooling2D)   (None, 56, 56, 128)       0
    _________________________________________________________________
    block3_conv1 (Conv2D)        (None, 56, 56, 256)       295168
    _________________________________________________________________
    block3_conv2 (Conv2D)        (None, 56, 56, 256)       590080
    _________________________________________________________________
    block3_conv3 (Conv2D)        (None, 56, 56, 256)       590080
    _________________________________________________________________
    block3_pool (MaxPooling2D)   (None, 28, 28, 256)       0
    _________________________________________________________________
    block4_conv1 (Conv2D)        (None, 28, 28, 512)       1180160
    _________________________________________________________________
    block4_conv2 (Conv2D)        (None, 28, 28, 512)       2359808
    _________________________________________________________________
    block4_conv3 (Conv2D)        (None, 28, 28, 512)       2359808
    _________________________________________________________________
    block4_pool (MaxPooling2D)   (None, 14, 14, 512)       0
    _________________________________________________________________
    block5_conv1 (Conv2D)        (None, 14, 14, 512)       2359808
    _________________________________________________________________
    block5_conv2 (Conv2D)        (None, 14, 14, 512)       2359808
    _________________________________________________________________
    block5_conv3 (Conv2D)        (None, 14, 14, 512)       2359808
    _________________________________________________________________
    block5_pool (MaxPooling2D)   (None, 7, 7, 512)         0

    '''

    decoder_input = Input(shape=(latent_dim,), name='t')
    x = (decoder_input)

    fn_conv2D = Conv2D
    fn_conv2DT = Conv2DTranspose
    fn_dense = Dense

    model = Sequential(name='decoder')
    model.add(decoder_input)
    if old:
        model.add(Reshape((1, 1, latent_dim)))
        model.add(UpSampling2D((image_shape[1] // 2**5, image_shape[1] // 2**5)))
    else:
        model.add(Dense(conv_shape[1] * conv_shape[2] * conv_shape[3], activation="relu"))
        model.add(Reshape((conv_shape[1], conv_shape[2], conv_shape[3])))

    #todo check bellow
    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=128, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=128, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=64, kernel_size=(3, 3), padding="same", activation="relu"))


    # model.add(fn_conv2DT(input_shape=(224, 224, 3), filters=64, kernel_size=(3, 3), padding="same", activation="relu"))
    #if between 0 and 1 -> sigmoid
    # if tanh:
    #     model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image'))  # tanh
    # else:
    #     model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image'))

    if preprocessing_function_type=='betweenminus1and1':
        model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image'))  # tanh ? #todo between
    elif preprocessing_function_type=='between0and1':
        model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image')) # tanh ? #todo between
    elif preprocessing_function_type=='imagenet':
        model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image'))  # tanh ? #todo between

    return model


def create_base_default(encoder_input):
    x = Conv2D(32, 3, activation="relu", strides=2, padding="same")(encoder_input)
    x = Conv2D(64, 3, activation="relu", strides=2, padding="same")(x)
    # x = Flatten()(x)
    # output = Dense(16, activation="relu")(x)
    return Model(encoder_input, x, name='base')
#
# def create_encoder_default(base, latent_dim):
#
#     t_mean = Dense(latent_dim, name="z_mean")(base.layers[-1].output)
#     t_log_var = Dense(latent_dim, name="z_log_var")(base.layers[-1].output)
#
#     return Model(base.layers[0].input, [t_mean, t_log_var], name='encoder')

def create_encoder_default(base, latent_dim):

    z_mean = Conv2D(filters=latent_dim, kernel_size=(1, 1),
                                  padding='same')(base.layers[-1].output)

    z_mean = GlobalAveragePooling2D(name='t_mean')(z_mean)

    z_log_var = Conv2D(filters=latent_dim, kernel_size=(1, 1),
                                     padding='same')(base.layers[-1].output)
    z_log_var = GlobalAveragePooling2D(name='t_log_var')(z_log_var)

    z = Sampling()([z_mean, z_log_var])

    return Model(base.layers[0].input, [z_mean, z_log_var, z], name="encoder")


def create_decoder_default(image_shape, latent_dim, preprocessing_function_type='between0and1', conv_shape=None):
    decoder_input = Input(shape=(latent_dim,), name='t')

    # x = Reshape((1, 1, latent_dim))(decoder_input)
    # x = UpSampling2D((image_shape[1] // 2**5, image_shape[1] // 2**5))(x)

    # x = Dense(int(image_shape[0]*image_shape[0]*4), activation="relu")(x)
    # x = Reshape((int(image_shape[0]/4), int(image_shape[0]/4), 64))(x)

    # x = Dense(int(image_shape[0]*image_shape[0]*4), activation="relu")(decoder_input)
    # x = Reshape((int(image_shape[0]/4), int(image_shape[0]/4), 64))(x)


    x = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3])(decoder_input)
    x = Activation('relu')(x)

    # x = BatchNormalization()(x)
    x = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(x)

    x = Conv2DTranspose(64, 3, activation="relu", strides=2, padding="same")(x)
    x = Conv2DTranspose(32, 3, activation="relu", strides=2, padding="same")(x)
    # if tanh:
    #     x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
    #         x)  # tanh ? #todo between
    # else:
    #     x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image')(
    #         x)  # tanh ? #todo between

    if preprocessing_function_type=='betweenminus1and1':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
            x)  # tanh ? #todo between
    elif preprocessing_function_type=='between0and1':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image')(
            x)  # tanh ? #todo between
    elif preprocessing_function_type=='imagenet':
        x = Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image')(
            x)  # tanh ? #todo between


    return Model(decoder_input, x, name='decoder')

def get_vae_model_default(image_shape, latent_dim, preprocessing_function_type='between0and1', base=None, old=False):
    vae_input = Input(shape=image_shape, name='encoder_input')
    if base is None:
        base = create_base_default(vae_input)
    encoder = create_encoder_with_base(base, latent_dim)
    # encoder = create_encoder_with_base_2(base, latent_dim)
    # encoder = create_encoder_with_base_3(base, latent_dim)

    conv_shape = K.int_shape(base.layers[-1].output)

    decoder = create_decoder_default(image_shape, latent_dim, preprocessing_function_type=preprocessing_function_type, conv_shape=conv_shape, old=old)

    # vae = VAE(encoder, decoder)
    t_mean, t_log_var = encoder(vae_input)
    sampler = create_sampler()
    t = sampler([t_mean, t_log_var])
    t_decoded = decoder(t)

    # vae = VAE(encoder, decoder)
    vae = Model(vae_input, t_decoded, name='composite')

    # if verbose:
    #     encoder.summary()
    #     decoder.summary()
        # vae.summary()

    return vae, t_mean, t_log_var


def create_binary_classifier_vgg19(image_shape, weights='imagenet', b_untrainable=True, dense_units=128):#224,244,3
    input = Input(shape=image_shape, name='classifier_input')
    base = VGG19(input_tensor=input, include_top=False, weights=weights, input_shape=image_shape)
    base._name = "base"
    if b_untrainable:
        for layer in base.layers:
            layer.trainable = False


    x = Flatten()(base.layers[-1].output)
    # x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    # output = Dense(1, activation='sigmoid')(x)
    x = Dense(dense_units, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dense(int(dense_units/2), activation='relu', kernel_initializer='he_uniform')(x)
    output = Dense(1, activation='sigmoid')(x)

    return Model(inputs=base.layers[0].input, outputs=output, name='classifer')

def set_model_as_untrainable(model):
    for layer in model.layers:
        layer.trainable = False
    return model

def get_vgg19_base(encoder_input,weights,image_shape, b_untrainable):
    base = VGG19(input_tensor=encoder_input, include_top=False, weights=weights, input_shape=image_shape)
    base._name = "base"
    if b_untrainable:
        for layer in base.layers:
            layer.trainable = False
    return base

class VAE(Model):
    def __init__(self, encoder, decoder, **kwargs):
        super(VAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder
        self.total_loss_tracker = Mean(name="total_loss")
        self.reconstruction_loss_tracker = Mean(
            name="reconstruction_loss"
        )
        self.kl_loss_tracker = Mean(name="kl_loss")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.kl_loss_tracker,
        ]

    def test_step(self, data):
        if isinstance(data, tuple):
            data = data[0]

        # z_mean, z_log_var, z = self.encoder(data)
        # reconstruction = self.decoder(z)

        z_mean, z_log_var = self.encoder(data)
        reconstruction = self.decoder(z_mean)

        # reconstruction_loss = tf.reduce_mean(
        #     tf.reduce_sum(
        #         binary_crossentropy(data, reconstruction), axis=(1, 2)
        #     )
        # )
        reconstruction_loss = K.sum(K.mean(K.square(data - reconstruction), axis=-1))
        kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
        kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
        total_loss = reconstruction_loss + kl_loss
        return {
            "loss": total_loss,
            "reconstruction_loss": reconstruction_loss,
            "kl_loss": kl_loss,
        }

    def call(self, data):
        # z_mean, z_log_var, z = self.encoder(data)
        # reconstruction = self.decoder(z)
        z_mean, z_log_var = self.encoder(data)
        reconstruction = self.decoder(z_mean)

        # reconstruction_loss = tf.reduce_mean(
        #     tf.reduce_sum(
        #         binary_crossentropy(data, reconstruction), axis=(1, 2)
        #     )
        # )
        reconstruction_loss = K.sum(K.mean(K.square(data - reconstruction), axis=-1))
        kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
        kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
        total_loss = reconstruction_loss + kl_loss
        self.add_metric(kl_loss, name='kl_loss', aggregation='mean')
        self.add_metric(total_loss, name='total_loss', aggregation='mean')
        self.add_metric(reconstruction_loss, name='reconstruction_loss', aggregation='mean')
        return reconstruction

    def train_step(self, data):
        if isinstance(data, tuple):
            data = data[0]

        with tf.GradientTape() as tape:
            # z_mean, z_log_var, z = self.encoder(data)
            # reconstruction = self.decoder(z)
            z_mean, z_log_var = self.encoder(data)
            reconstruction = self.decoder(z_mean)

            # reconstruction_loss = tf.reduce_mean(
            #     tf.reduce_sum(
            #         binary_crossentropy(data, reconstruction), axis=(1, 2)
            #     )
            # )
            reconstruction_loss = K.sum(K.mean(K.square(data - reconstruction), axis=-1))

            kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
            kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
            total_loss = reconstruction_loss + kl_loss
        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.kl_loss_tracker.update_state(kl_loss)
        return {
            "loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "kl_loss": self.kl_loss_tracker.result(),
        }

class Sampling(layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon

def get_vae_model_vgg19(image_shape, latent_dim, weights=None, b_untrainable=False, preprocessing_function_type='between0and1', base=None, old=False):
    vae_input = Input(shape=image_shape, name='encoder_input')
    if base is None:
        base = get_vgg19_base(vae_input, weights,image_shape, b_untrainable)

    encoder = create_encoder_with_base(base, latent_dim)
    # encoder = create_encoder_with_base_3(base, latent_dim)
    conv_shape = K.int_shape(base.layers[-1].output)

    decoder = create_decoder_vgg19(image_shape, latent_dim, preprocessing_function_type=preprocessing_function_type, conv_shape=conv_shape, old=old)

    # vae = VAE(encoder, decoder)
    t_mean, t_log_var = encoder(vae_input)
    sampler = create_sampler()
    t = sampler([t_mean, t_log_var])
    t_decoded = decoder(t)

    # vae = VAE(encoder, decoder)
    vae = Model(vae_input, t_decoded, name='composite')

    # if verbose:
    #     encoder.summary()
    #     decoder.summary()
        # vae.summary()

    return vae, t_mean, t_log_var

def create_decoder_vgg19(image_shape, latent_dim, preprocessing_function_type='between0and1', conv_shape=None, old=False):


    decoder_input = Input(shape=(latent_dim,), name='t')

    fn_conv2D = Conv2D
    fn_conv2DT = Conv2DTranspose
    fn_dense = Dense

    model = Sequential(name='decoder')
    model.add(decoder_input)

    if old:
        model.add(Reshape((1, 1, latent_dim)))
        model.add(UpSampling2D((image_shape[1] // 2**5, image_shape[1] // 2**5)))
    else:
        model.add(Dense(conv_shape[1] * conv_shape[2] * conv_shape[3], activation="relu"))
        model.add(Reshape((conv_shape[1], conv_shape[2], conv_shape[3])))

    '''
    '''
    #todo check bellow
    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))

    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))

    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=128, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=128, kernel_size=(3, 3), padding="same", activation="relu"))

    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=64, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(fn_conv2DT(filters=64, kernel_size=(3, 3), padding="same", activation="relu"))

    model.add(UpSampling2D((2, 2)))
    model.add(fn_conv2DT(filters=64, kernel_size=(3, 3), padding="same", activation="relu"))


    # model.add(fn_conv2DT(input_shape=(224, 224, 3), filters=64, kernel_size=(3, 3), padding="same", activation="relu"))
    #if between 0 and 1 -> sigmoid
    # if tanh:
    #     model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image'))  # tanh
    # else:
    #     model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image'))

    if preprocessing_function_type=='betweenminus1and1':
        model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image'))  # tanh ? #todo between
    elif preprocessing_function_type=='between0and1':
        model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='sigmoid', name='image')) # tanh ? #todo between
    elif preprocessing_function_type=='imagenet':
        model.add(Conv2D(filters=image_shape[-1], kernel_size=(3, 3), padding='same', activation='tanh', name='image'))  # tanh ? #todo between


    return model




def sample(args):
    '''
    Draws samples from a standard normal and scales the samples with
    standard deviation of the variational distribution and shifts them
    by the mean.

    Args:
        args: sufficient statistics of the variational distribution.

    Returns:
        Samples from the variational distribution.
    '''
    t_mean, t_log_var = args
    t_sigma = K.sqrt(K.exp(t_log_var))
    epsilon = K.random_normal(shape=K.shape(t_mean), mean=0., stddev=1.)
    return t_mean + t_sigma * epsilon

def create_sampler():
    '''
    Creates a sampling layer.
    '''
    return Lambda(sample, name='sampler')

def swish(x, beta=1):
    return (x * sigmoid(beta * x))

get_custom_objects().update({'swish': Activation(swish)})

class Sampling(layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon
