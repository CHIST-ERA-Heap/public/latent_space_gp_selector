from tensorflow.keras.utils import Sequence
from scripts.common.process_patch_util import extract_single_feature
import numpy as np
import math
import random
from torch.utils.data import Dataset
from tensorflow.keras.utils import to_categorical


class My_Custom_Generator_Scenes(Sequence):

    def __init__(self,  grasp_dict, number_of_labels_per_scenes, number_of_scenes_per_batch, channels_number, preprocessing_fct, random_angles=[0], allow_flip=True,
                 label_shape=(-1, 282), final_size=2**5, random_sizes=[2**5, 48, 2**6, 80, 96, 112, 2**7, 144, 160],
                 dic_images_rgb=None,
                 dic_depth_raw_data=None, dic_images_masks=None, use_rgb=True, use_gray_depth=True, use_SN=True, b_classifier=False, label_dim=11, b_remember_patch = False, verbose=False, max_depth_value=0.7):
        self.verbose = verbose
        self.grasp_dict = grasp_dict
        self.current_grasp_dict = grasp_dict.copy()

        self.grasp_dict_keys =list(grasp_dict.keys())

        self.number_of_labels_per_scenes = number_of_labels_per_scenes
        self.number_of_scenes_per_batch = number_of_scenes_per_batch

        # self.n = len(self.grasp_dict_keys)  # .shape[0]
        self.n = int(np.floor(len(self.grasp_dict_keys) / self.number_of_scenes_per_batch))

        self.channels_number = channels_number
        self.label_shape = label_shape

        self.allow_flip = allow_flip
        self.random_sizes = random_sizes

        self.random_angles = random_angles
        self.final_size = final_size

        self.dic_images_rgb = dic_images_rgb
        self.dic_depth_raw_data = dic_depth_raw_data
        # self.dic_images_sn = dic_images_sn
        self.dic_images_masks = dic_images_masks

        self.b_classifier = b_classifier
        self.preprocessing_fct=preprocessing_fct

        self.max_depth_value = max_depth_value
        self.use_rgb = use_rgb
        self.use_gray_depth = use_gray_depth
        self.use_SN = use_SN
        self.label_dim = label_dim

        self.b_remember_patch = b_remember_patch
        if self.b_remember_patch:
            self.dict_patch = {}

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        np.random.shuffle(self.grasp_dict_keys)

    #the number of batches per epoch
    def __len__(self):
        # return (np.ceil(len(self.grasp_dict_keys) / float(self.batch_size))).astype(np.int)
        return self.n

    def extract(self, g, size, angle_tweak=0.0, flipped=0):
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        FOREIGN KEY (scene_id) REFERENCES scenes (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        grasp_temp = g.copy()
        grasp_temp[8]=+ angle_tweak
        if self.b_remember_patch:
            key_str = str(int(grasp_temp[1])) + "_" + str(int(grasp_temp[3])) + "_" + str(int(grasp_temp[4])) + "_" + str(
                math.degrees(grasp_temp[8])) + "_" + str(flipped)

            if key_str not in self.dict_patch:
                patch = extract_single_feature(rgb_scene=self.dic_images_rgb[g[1]], depth_raw=self.dic_depth_raw_data[g[1]], grasp_position_x=grasp_temp[3], grasp_position_y=grasp_temp[4], angle_degrees=math.degrees(grasp_temp[8]), use_rgb=self.use_rgb,
                               use_gray_depth=self.use_gray_depth, use_SN=self.use_SN, dim_grasp_patch=size, dim_resized_patch=self.final_size, flipped=flipped, max_depth_value=self.max_depth_value)
                self.dict_patch[key_str] = patch
            else:
                return self.dict_patch[key_str]
        else:
            return extract_single_feature(rgb_scene=self.dic_images_rgb[g[1]], depth_raw=self.dic_depth_raw_data[g[1]], grasp_position_x=grasp_temp[3], grasp_position_y=grasp_temp[4], angle_degrees=math.degrees(grasp_temp[8]), use_rgb=self.use_rgb,
                               use_gray_depth=self.use_gray_depth, use_SN=self.use_SN, dim_grasp_patch=size, dim_resized_patch=self.final_size, flipped=flipped, max_depth_value=self.max_depth_value)
    #get one batch size
    def __getitem__(self, idx):
        batch_x = []
        batch_y = []
        batch_y_edge = []
        batch_y_angle = []
        batch_y_origin = []
        # for iterator_number_of_scenes_per_batch in range(self.number_of_scenes_per_batch):
        iterator_number_of_scenes_per_batch = -1
        while len(batch_x) < self.number_of_scenes_per_batch*self.number_of_labels_per_scenes:
            iterator_number_of_scenes_per_batch+=1
            i_grasp_dict_keys = (idx*self.number_of_scenes_per_batch+iterator_number_of_scenes_per_batch)%(len(self.grasp_dict_keys)-1)
            # print("idx: ", idx, " i_grasp_dict_keys: ", i_grasp_dict_keys, "len(self.grasp_dict_keys): ",len(self.grasp_dict_keys))
            current_scene_id = self.grasp_dict_keys[i_grasp_dict_keys]#
            # print("current_scene id: ", current_scene_id)
            # print("self.grasp_dict[current_scene]: ", self.grasp_dict[current_scene])
            # print("len self.grasp_dict[current_scene]: ", len(self.grasp_dict[current_scene_id]))
            batch_x_grasps = []
            # if len(self.grasp_dict[current_scene_id]) > self.number_of_labels_per_scenes:
            #     # batch_x_grasps = random.sample(self.grasp_dict[current_scene], self.number_of_labels_per_scenes)
            #     random.shuffle(self.current_grasp_dict[current_scene_id])
            #     while(len(batch_x_grasps) < self.number_of_labels_per_scenes):
            #         batch_x_grasps.append(self.current_grasp_dict[current_scene_id].pop(-1))
            #         # print("len(self.current_grasp_dict[current_scene]): ", len(self.current_grasp_dict[current_scene]))
            #         if len(self.current_grasp_dict[current_scene_id]) == 0:
            #             self.current_grasp_dict[current_scene_id] = self.grasp_dict[current_scene_id].copy()
            # else:
            batch_x_grasps = self.grasp_dict[current_scene_id]
            random.shuffle(batch_x_grasps)

            # print("len(batch_x_grasps): ", len(batch_x_grasps))

            # print("len batch_x_grasps: ", len(batch_x_grasps))

            # [center[1], center[0], angle, length, width, scene, 'handlabel', id, 0.0, label]
            current_added_scene_counter = len(batch_x)
            target_scene_counter = current_added_scene_counter+self.number_of_labels_per_scenes
            while len(batch_x) < target_scene_counter:
                for g in batch_x_grasps:
                    if len(batch_x) >= target_scene_counter:
                        break
                    # print(current_added_scene_counter, " - g: ", g)

                    label_vector_edge = np.zeros((2,), dtype=int)
                    label_edge = g[6]
                    # print("label_edge: ", label_edge)

                    try:
                        Y = g[self.label_dim]
                        if self.label_dim == 11:
                            if Y < 1:
                                Y = 0
                        Y = int(Y)
                        # Y = int(g[11])
                    except:
                        Y = 0
                    # print("Y: ", Y)
                    # label_vector = np.zeros((self.label_shape[1],), dtype=int)
                    # label_vector[int(Y)] = 1

                    size = int(self.random_sizes[random.randint(0, len(self.random_sizes) - 1)])
                    Y_index = random.randint(0, len(self.random_angles) - 1)
                    if random.randint(0, 100) <= 25:
                        angle = 0
                    else:
                        angle = float(self.random_angles[Y_index])
                    # print("angle: ", angle)

                    Y_angle = angle / 180

                    # print("Y_angle: ", Y_angle)

                    angle_tweak = math.radians(angle)
                    # extract data
                    flipped = 0
                    if self.allow_flip:
                        if random.randint(0, 100) <= 50:
                            flipped = 1
                    X = self.extract(g, size, angle_tweak=angle_tweak, flipped=flipped)
                    if X is not None:
                        batch_y_angle.append(Y_angle)
                        batch_y.append(Y)
                        batch_x.append(X)
                        # if self.verbose:
                        #     print("len(batch_x): ", len(batch_x), " target_scene_counter: ", target_scene_counter)

                        # print("continue: len(batch_x) < target_scene_counter: ", len(batch_x) < target_scene_counter)
                        # print("stop: len(batch_x) >= target_scene_counter: ", len(batch_x) >= target_scene_counter)
                        if label_edge == 'edge':
                            label_vector_edge[int(0)] = -1
                            batch_y_origin.append(1)

                        if label_edge == 'skel':
                            label_vector_edge[int(1)] = 1
                            batch_y_origin.append(0)

                        batch_y_edge.append(label_vector_edge)

                        # skel', 'edge', 'sn', 'random
                        if label_edge == 'sn':
                            batch_y_origin.append(1)

                        if label_edge == 'random':
                            batch_y_origin.append(-1)

                    if self.verbose and X is None:
                        print('X is None: ', g)

        batch_x = np.array(batch_x)
        batch_x_final = self.preprocessing_fct(batch_x)
        del batch_x

        if self.verbose:
            print("batch_x_final.shape: ", batch_x_final.shape)

        if self.b_classifier:
            batch_y = np.array(batch_y)
            batch_y = batch_y.reshape((-1)).astype(np.uint8)

            # print("batch_x_final.shape: ", batch_x_final.shape)
            # print("batch_y.shape: ", batch_y.shape)
            return batch_x_final, batch_y
        else:
            return batch_x_final, batch_x_final

# class My_Custom_Generator_Scenes(Sequence):
#
#     def __init__(self,  grasp_dict, batch_size, channels_number, preprocessing_fct, random_angles=[0], allow_flip=True,
#                  label_shape=(-1, 282), final_size=2**5, random_sizes=[2**5, 48, 2**6, 80, 96, 112, 2**7, 144, 160],
#                  dic_images_rgb=None,
#                  dic_depth_raw_data=None, dic_images_masks=None, use_rgb=True, use_gray_depth=True, use_SN=True, b_classifier=False, label_dim=11, b_remember_patch = False):
#         self.grasp_dict = grasp_dict
#         self.current_grasp_dict = grasp_dict.copy()
#
#         self.grasp_dict_keys =list(grasp_dict.keys())
#
#         self.n = len(self.grasp_dict_keys)  # .shape[0]
#
#         self.batch_size = batch_size
#         self.channels_number = channels_number
#         self.label_shape = label_shape
#
#         self.allow_flip = allow_flip
#         self.random_sizes = random_sizes
#
#         self.random_angles = random_angles
#         self.final_size = final_size
#
#         self.dic_images_rgb = dic_images_rgb
#         self.dic_depth_raw_data = dic_depth_raw_data
#         # self.dic_images_sn = dic_images_sn
#         self.dic_images_masks = dic_images_masks
#
#         self.b_classifier = b_classifier
#         self.preprocessing_fct=preprocessing_fct
#
#         self.use_rgb = use_rgb
#         self.use_gray_depth = use_gray_depth
#         self.use_SN = use_SN
#         self.label_dim = label_dim
#
#         self.b_remember_patch = b_remember_patch
#         if self.b_remember_patch:
#             self.dict_patch = {}
#
#
#     def __len__(self):
#         # return (np.ceil(len(self.grasp_dict_keys) / float(self.batch_size))).astype(np.int)
#         return self.n
#
#     def extract(self, g, size, angle_tweak=0.0, flipped=0):
#         '''
#         id integer PRIMARY KEY,
#         scene_id integer NOT NULL,
#         user_id integer NOT NULL,
#         x real NOT NULL,
#         y real NOT NULL,
#         depth real NOT NULL,
#         euler_x real NOT NULL,
#         euler_y real NOT NULL,
#         euler_z real NOT NULL,
#         length real NOT NULL,
#         width real NOT NULL,
#         quality real NOT NULL,
#         FOREIGN KEY (scene_id) REFERENCES scenes (id),
#         FOREIGN KEY (user_id) REFERENCES user (id)
#         '''
#         grasp_temp = g.copy()
#         grasp_temp[8]=+ angle_tweak
#         if self.b_remember_patch:
#             key_str = str(int(grasp_temp[1])) + "_" + str(int(grasp_temp[3])) + "_" + str(int(grasp_temp[4])) + "_" + str(
#                 math.degrees(grasp_temp[8])) + "_" + str(flipped)
#
#             if key_str not in self.dict_patch:
#                 patch = extract_single_feature(rgb_scene=self.dic_images_rgb[g[1]], depth_raw=self.dic_depth_raw_data[g[1]], grasp_position_x=grasp_temp[3], grasp_position_y=grasp_temp[4], angle_degrees=math.degrees(grasp_temp[8]), use_rgb=self.use_rgb,
#                                use_gray_depth=self.use_gray_depth, use_SN=self.use_SN, dim_grasp_patch=size, dim_resized_patch=self.final_size, flipped=flipped)
#                 self.dict_patch[key_str] = patch
#             else:
#                 return self.dict_patch[key_str]
#         else:
#             return extract_single_feature(rgb_scene=self.dic_images_rgb[g[1]], depth_raw=self.dic_depth_raw_data[g[1]], grasp_position_x=grasp_temp[3], grasp_position_y=grasp_temp[4], angle_degrees=math.degrees(grasp_temp[8]), use_rgb=self.use_rgb,
#                                use_gray_depth=self.use_gray_depth, use_SN=self.use_SN, dim_grasp_patch=size, dim_resized_patch=self.final_size, flipped=flipped)
#     #get one batch size
#     def __getitem__(self, idx):
#         current_scene_id = self.grasp_dict_keys[idx]#
#         # print("current_scene id: ", current_scene)
#         # print("self.grasp_dict[current_scene]: ", self.grasp_dict[current_scene])
#         # print("len self.grasp_dict[current_scene]: ", len(self.grasp_dict[current_scene]))
#         batch_x_grasps = []
#         if len(self.grasp_dict[current_scene_id]) > self.batch_size:
#             # batch_x_grasps = random.sample(self.grasp_dict[current_scene], self.batch_size)
#             random.shuffle(self.current_grasp_dict[current_scene_id])
#             while(len(batch_x_grasps) < self.batch_size):
#                 batch_x_grasps.append(self.current_grasp_dict[current_scene_id].pop(-1))
#                 # print("len(self.current_grasp_dict[current_scene]): ", len(self.current_grasp_dict[current_scene]))
#                 if len(self.current_grasp_dict[current_scene_id]) == 0:
#                     self.current_grasp_dict[current_scene_id] = self.grasp_dict[current_scene_id].copy()
#         else:
#             batch_x_grasps = self.grasp_dict[current_scene_id]
#         # print("len(batch_x_grasps): ", len(batch_x_grasps))
#
#         # print("len batch_x_grasps: ", len(batch_x_grasps))
#
#         batch_x = []
#         batch_y = []
#         batch_y_edge = []
#         batch_y_angle = []
#         batch_y_origin = []
#
#         # [center[1], center[0], angle, length, width, scene, 'handlabel', id, 0.0, label]
#         while len(batch_x) < self.batch_size:
#             for g in batch_x_grasps:
#                 if len(batch_x) >= self.batch_size:
#                     break
#                 # print("g: ", g)
#
#                 label_vector_edge = np.zeros((2,), dtype=int)
#                 label_edge = g[6]
#                 # print("label_edge: ", label_edge)
#
#                 try:
#                     Y = g[self.label_dim]
#                     if self.label_dim == 11:
#                         if Y < 1:
#                             Y = 0
#                     Y = int(Y)
#                     # Y = int(g[11])
#                 except:
#                     Y = 0
#                 # print("Y: ", Y)
#                 # label_vector = np.zeros((self.label_shape[1],), dtype=int)
#                 # label_vector[int(Y)] = 1
#
#                 size = int(self.random_sizes[random.randint(0, len(self.random_sizes) - 1)])
#                 Y_index = random.randint(0, len(self.random_angles) - 1)
#                 if random.randint(0, 100) <= 25:
#                     angle = 0
#                 else:
#                     angle = float(self.random_angles[Y_index])
#                 # print("angle: ", angle)
#
#                 Y_angle = angle / 180
#
#                 # print("Y_angle: ", Y_angle)
#
#                 angle_tweak = math.radians(angle)
#                 # extract data
#                 flipped = 0
#                 if self.allow_flip:
#                     if random.randint(0, 100) <= 50:
#                         flipped = 1
#                 X = self.extract(g, size, angle_tweak=angle_tweak, flipped=flipped)
#                 if X is not None:
#                     batch_y_angle.append(Y_angle)
#                     batch_y.append(Y)
#                     batch_x.append(X)
#                     if label_edge == 'edge':
#                         label_vector_edge[int(0)] = -1
#                         batch_y_origin.append(1)
#
#                     if label_edge == 'skel':
#                         label_vector_edge[int(1)] = 1
#                         batch_y_origin.append(0)
#
#                     batch_y_edge.append(label_vector_edge)
#
#                     # skel', 'edge', 'sn', 'random
#                     if label_edge == 'sn':
#                         batch_y_origin.append(1)
#
#                     if label_edge == 'random':
#                         batch_y_origin.append(-1)
#
#
#         batch_x = np.array(batch_x)
#         batch_x_final = self.preprocessing_fct(batch_x)
#         del batch_x
#         # print("batch_x_final.shape: ", batch_x_final.shape)
#
#         if self.b_classifier:
#             batch_y = np.array(batch_y)
#             batch_y = batch_y.reshape((-1)).astype(np.uint8)
#
#             # print("batch_x_final.shape: ", batch_x_final.shape)
#             # print("batch_y.shape: ", batch_y.shape)
#             return batch_x_final, batch_y
#         else:
#             return batch_x_final, batch_x_final

class My_Custom_Generator(Sequence):

    def __init__(self,  grasp_list, batch_size, channels_number, preprocessing_fct, random_angles=[0], allow_flip=True,
                 label_shape=(-1, 282), final_size=2**5, random_sizes=[2**5, 48, 2**6, 80, 96, 112, 2**7, 144, 160],
                 dic_images_rgb=None,
                 dic_depth_raw_data=None, dic_images_masks=None, use_rgb=True, use_gray_depth=True, use_SN=True, b_classifier=False, label_dim=11, num_classes=2, b_remember_patch = False, verbose=False, max_depth_value=0.7):
        self.verbose = verbose
        self.grasp_list = grasp_list
        self.n = len(grasp_list)  # .shape[0]

        self.batch_size = batch_size
        self.channels_number = channels_number
        self.label_shape = label_shape

        self.allow_flip = allow_flip
        self.random_sizes = random_sizes

        self.random_angles = random_angles
        self.final_size = final_size

        self.dic_images_rgb = dic_images_rgb
        self.dic_depth_raw_data = dic_depth_raw_data
        # self.dic_images_sn = dic_images_sn
        self.dic_images_masks = dic_images_masks

        self.b_classifier = b_classifier

        self.preprocessing_fct = preprocessing_fct

        self.max_depth_value = max_depth_value
        self.use_rgb = use_rgb
        self.use_gray_depth = use_gray_depth
        self.use_SN = use_SN
        self.label_dim= label_dim
        self.num_classes =num_classes

        self.b_remember_patch = b_remember_patch
        if self.b_remember_patch:
            self.dict_patch = {}

    def __len__(self):
        return (np.ceil(len(self.grasp_list) / float(self.batch_size))).astype(np.int)

    def extract(self, g, size, angle_tweak=0.0, flipped=0):
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        FOREIGN KEY (scene_id) REFERENCES scenes (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        grasp_temp = g.copy()
        grasp_temp[8]=+ angle_tweak
        if self.b_remember_patch:
            key_str = str(int(grasp_temp[1])) + "_" + str(int(grasp_temp[3])) + "_" + str(int(grasp_temp[4])) + "_" + str(
                math.degrees(grasp_temp[8])) + "_" + str(flipped)

            if key_str not in self.dict_patch:
                patch = extract_single_feature(rgb_scene=self.dic_images_rgb[g[1]], depth_raw=self.dic_depth_raw_data[g[1]], grasp_position_x=grasp_temp[3], grasp_position_y=grasp_temp[4], angle_degrees=math.degrees(grasp_temp[8]), use_rgb=self.use_rgb,
                               use_gray_depth=self.use_gray_depth, use_SN=self.use_SN, dim_grasp_patch=size, dim_resized_patch=self.final_size, flipped=flipped, max_depth_value=self.max_depth_value)
                self.dict_patch[key_str] = patch
            else:
                return self.dict_patch[key_str]
        else:
            return extract_single_feature(rgb_scene=self.dic_images_rgb[g[1]], depth_raw=self.dic_depth_raw_data[g[1]], grasp_position_x=grasp_temp[3], grasp_position_y=grasp_temp[4], angle_degrees=math.degrees(grasp_temp[8]), use_rgb=self.use_rgb,
                               use_gray_depth=self.use_gray_depth, use_SN=self.use_SN, dim_grasp_patch=size, dim_resized_patch=self.final_size, flipped=flipped, max_depth_value=self.max_depth_value)

    def __getitem__(self, idx):
        batch_x_grasps = self.grasp_list[idx * self.batch_size: (idx + 1) * self.batch_size]
        if len(batch_x_grasps) == 0:
            batch_x_grasps = self.grasp_list

        batch_x = []
        batch_y = []
        batch_y_edge = []
        batch_y_angle = []
        batch_y_origin = []

        # [center[1], center[0], angle, length, width, scene, 'handlabel', id, 0.0, label]
        for g in batch_x_grasps:
            # print("g: ", g)

            label_vector_edge = np.zeros((2,), dtype=int)
            label_edge = g[6]
            # print("label_edge: ", label_edge)

            if label_edge == 'edge':
                label_vector_edge[int(0)] = -1
                batch_y_origin.append(1)

            if label_edge == 'skel':
                label_vector_edge[int(1)] = 1
                batch_y_origin.append(0)

            batch_y_edge.append(label_vector_edge)

            # skel', 'edge', 'sn', 'random
            if label_edge == 'sn':
                batch_y_origin.append(1)

            if label_edge == 'random':
                batch_y_origin.append(-1)

            # try:
            #     Y = g[self.label_dim]
            #     if self.label_dim == 11:
            #         if Y < 1:
            #             Y = 0
            #     Y = int(Y)
            #     # Y = int(g[11])
            # except:
            #     Y = 0
            # # print("Y: ", Y)
            # # label_vector = np.zeros((self.label_shape[1],), dtype=int)
            # # label_vector[int(Y)] = 1
            # batch_y.append(Y)

            label = g[self.label_dim]
            if self.label_dim == 11:
                if label < 1:
                    # label = -1.0
                    label = 0.0
                batch_y.append(np.array(label))

            else:
                Y = to_categorical(label, self.num_classes)
                batch_y.append(np.array(Y))


            size = self.random_sizes[random.randint(0, len(self.random_sizes) - 1)]
            Y_index = random.randint(0, len(self.random_angles) - 1)
            angle = float(self.random_angles[Y_index])
            # print("angle: ", angle)

            Y_angle = angle / 90

            # print("Y_angle: ", Y_angle)
            batch_y_angle.append(Y_angle)

            angle_tweak = math.radians(angle)
            # extract data
            flipped = 0
            if self.allow_flip:
                if random.randint(0, 100) <= 50:
                    flipped = 1

            X = self.extract(g, size, angle_tweak=angle_tweak,flipped=flipped)

            batch_x.append(X)

        batch_x = np.array(batch_x)
        batch_x_final = self.preprocessing_fct(batch_x)


        del batch_x
        if self.b_classifier:
            batch_y = np.array(batch_y)
            if self.num_classes ==2:
                batch_y = batch_y.reshape((-1)).astype(np.uint8)
            else:
                batch_y = batch_y.reshape((-1, self.num_classes)).astype(np.uint8)

            # print("My_Custom_Generator: ")
            if self.verbose:
                print("batch_x_final.shape: ", batch_x_final.shape)
                print("batch_y.shape: ", batch_y.shape)
            return batch_x_final, batch_y
        else:
            if self.verbose:
                print("batch_x_final.shape: ", batch_x_final.shape)

            return batch_x_final, batch_x_final

#cnn
class My_Custom_Generator_bvae(Sequence):

    def __init__(self, grasp_list, batch_size, preprocessing_fct,
                 dic_images_rgb=None,
                 dic_depth_raw_data=None, b_with_augmentation=True, dict_encoded_mu = {}, dict_encoded_var = {}, b_classifier=False, latent_dim=32, label_dim=11, num_classes=2):
        self.grasp_list = grasp_list
        self.n = len(grasp_list)  # .shape[0]
        self.b_with_augmentation = b_with_augmentation

        self.batch_size = batch_size
        self.preprocessing_fct = preprocessing_fct

        self.dic_images_rgb = dic_images_rgb
        self.dic_depth_raw_data = dic_depth_raw_data

        self.dict_encoded_mu = dict_encoded_mu
        self.dict_encoded_var = dict_encoded_var
        self.b_classifier = b_classifier
        self.latent_dim = latent_dim
        self.label_dim = label_dim
        self.num_classes = num_classes

    def __len__(self):
        return len(self.grasp_list)

    def extract(self, grasps_candidate, c=None, fliped=None):
        # print("extract: ", grasps_candidate)
        temp_grasp = list(grasps_candidate).copy()
        if self.b_with_augmentation:
            if c is None:
                c = random.choice([0, 1, 2])
            if c == 1:
                temp_grasp[8] -= np.pi
            if c == 2:
                temp_grasp[8] += np.pi

            if fliped is None:
                fliped = random.choice([0, 1])
                # fliped = 0
        else:
            fliped = 0

        key_str = str(int(temp_grasp[1])) +"_" +str(int(temp_grasp[3])) + "_" + str(int(temp_grasp[4])) + "_" + str(math.degrees(temp_grasp[8])) + "_" + str(fliped)
        # print("My_Custom_Generator_bvae key_str: ", key_str)

        if key_str not in self.dict_encoded_mu:
            self.dict_encoded_mu[key_str], self.dict_encoded_var[key_str] = self.preprocessing_fct([temp_grasp], self.dic_images_rgb, self.dic_depth_raw_data, flipped=[fliped])

        return self.dict_encoded_mu[key_str], self.dict_encoded_var[key_str]

    def __getitem__(self, idx):
        # print("idx: ", idx)
        # print("self.grasp_list: ", self.grasp_list)

        batch_y = []
        g = self.grasp_list[idx]
        mu, var = self.extract(g)

        label = g[self.label_dim]
        if self.label_dim == 11:
            if label < 1:
                # label = -1.0
                label = 0.0
            batch_y = np.array([label])
            batch_y = batch_y.reshape((-1)).astype(np.uint8)

        else:
            batch_y = to_categorical(label, self.num_classes)
            batch_y = np.array([batch_y])


        if self.b_classifier:
            mu = mu.reshape((-1,self.latent_dim))
            # print("mu.shape: ", mu.shape)
            # print("batch_y: ", batch_y)
            # batch_y = batch_y.reshape((-1,self.num_classes))
            # print("batch_y.shape: ", batch_y.shape)

            return mu, batch_y
        else:
            return mu, var, batch_y

#todo refactor name
class My_Custom_Generator_gpy(Dataset):

    def __init__(self, grasp_list, batch_size, preprocessing_fct,
                 dic_images_rgb=None,
                 dic_depth_raw_data=None, b_with_augmentation=True, dict_encoded_mu = {}, dict_encoded_var = {}, label_dim=11, b_correction=True, prior_length=0.0, b_force_fix_prior = False, prior_length_function=None, jitter=False):
        self.grasp_list = grasp_list
        self.n = len(grasp_list)  # .shape[0]
        self.b_with_augmentation = b_with_augmentation
        self.jitter = jitter

        self.batch_size = batch_size
        self.preprocessing_fct = preprocessing_fct

        self.dic_images_rgb = dic_images_rgb
        self.dic_depth_raw_data = dic_depth_raw_data

        self.dict_encoded_mu = dict_encoded_mu
        self.dict_encoded_var = dict_encoded_var
        self.label_dim = label_dim
        self.b_correction = b_correction
        self.prior_length=prior_length
        self.b_force_fix_prior = b_force_fix_prior
        self.prior_length_function = prior_length_function
        self.dict_scenes_masks = {}

    def get_all(self):
        mus = []
        vars = []
        labels = []
        for g in self.grasp_list:
            temp_mu, temp_var = self.extract(g, c=0, flipped=0)
            # print("temp_mu.shape: ", temp_mu.shape)
            # print("temp_var.shape: ", temp_var.shape)

            mus.append(temp_mu)
            vars.append(temp_var)
            label = g[self.label_dim]
            if self.label_dim == 11:
                if label < 1:
                    # label = -1.0
                    label = 0.0

            if self.label_dim == 9 and self.b_correction:
                    if self.b_force_fix_prior or self.prior_length_function is None:
                        label = label - self.prior_length#g[9]
                    else:
                        scene_id = g[1]
                        if scene_id in self.dict_scenes_masks:
                            current_mask = self.dict_scenes_masks[scene_id]
                            current_prior_length, _, _ = self.prior_length_function(g, current_mask)
                        else:
                            current_prior_length, _, current_mask = self.prior_length_function(g)
                            self.dict_scenes_masks[scene_id] = current_mask


                        if current_prior_length == 1 or current_prior_length == 200:
                            current_prior_length = self.prior_length
                        label = label - current_prior_length

            # print("g: ", g)
            # print("label: ", label)

            labels.append(label)
            if self.b_with_augmentation:
                temp_mu, temp_var = self.extract(g, c=0, flipped=1)
                # print("temp_mu.shape: ", temp_mu.shape)
                # print("temp_var.shape: ", temp_var.shape)
                mus.append(temp_mu)
                vars.append(temp_var)
                labels.append(label)

                temp_mu, temp_var = self.extract(g, c=1, flipped=0)
                # print("temp_mu.shape: ", temp_mu.shape)
                # print("temp_var.shape: ", temp_var.shape)
                mus.append(temp_mu)
                vars.append(temp_var)
                labels.append(label)

                temp_mu, temp_var = self.extract(g, c=1, flipped=1)
                # print("temp_mu.shape: ", temp_mu.shape)
                # print("temp_var.shape: ", temp_var.shape)
                mus.append(temp_mu)
                vars.append(temp_var)
                labels.append(label)

                temp_mu, temp_var = self.extract(g, c=2, flipped=0)
                # print("temp_mu.shape: ", temp_mu.shape)
                # print("temp_var.shape: ", temp_var.shape)
                mus.append(temp_mu)
                vars.append(temp_var)
                labels.append(label)

                temp_mu, temp_var = self.extract(g, c=2, flipped=1)
                # print("temp_mu.shape: ", temp_mu.shape)
                # print("temp_var.shape: ", temp_var.shape)
                mus.append(temp_mu)
                vars.append(temp_var)
                labels.append(label)

        mus = np.array(mus).reshape((-1,temp_mu.shape[-1]))
        vars = np.array(vars).reshape((-1,temp_mu.shape[-1]))
        labels = np.array(labels)

        return mus, vars, labels

    def __len__(self):
        return len(self.grasp_list)

    def extract(self, grasps_candidate, c=None, flipped=None, jitter=False):
        # print("extract: ", grasps_candidate)
        temp_grasp = list(grasps_candidate).copy()
        if self.b_with_augmentation:
            if c is None:
                c = random.choice([0, 1, 2])
            if c == 1:
                temp_grasp[8] -= np.pi
            if c == 2:
                temp_grasp[8] += np.pi

            if flipped is None:
                flipped = random.choice([0, 1])
                # fliped = 0
        else:
            flipped = 0
        if jitter:
            # print("before: ", temp_grasp)
            # temp_grasp[3]+=random.choice([-2.0,-1.0,0.0,1.0,2.0])
            # temp_grasp[4]+=random.choice([-2.0,-1.0,0.0,1.0,2.0])
            # temp_grasp[3]+=random.choice([-1.0,0.0,1.0])
            # temp_grasp[4]+=random.choice([-1.0,0.0,1.0])
            temp_grasp[8]+=random.choice([-np.pi/2*0.2, -np.pi/2*0.1, 0.0, np.pi/2*0.1, np.pi/2*0.2])
            # temp_grasp[8]+=random.choice(np.arange(-0.5,0.5,0.1).tolist())

            # print("after: ", temp_grasp)

        key_str = str(int(temp_grasp[1])) + "_" + str(int(temp_grasp[3])) + "_" + str(int(temp_grasp[4])) + "_" + str(math.degrees(temp_grasp[8])) + "_" + str(flipped)

        # print("key_str: ", key_str)

        if key_str not in self.dict_encoded_mu:# or key_str not in self.dict_encoded_var:
            self.dict_encoded_mu[key_str], self.dict_encoded_var[key_str] = self.preprocessing_fct([temp_grasp], self.dic_images_rgb, self.dic_depth_raw_data, flipped=[flipped])

        # return self.dict_encoded_mu[key_str], self.dict_encoded_var[key_str]

        mu = np.array(self.dict_encoded_mu[key_str])
        if key_str not in self.dict_encoded_var:
            var = np.zeros_like(mu)
        else:
            var = np.array(self.dict_encoded_var[key_str])
        return mu.reshape((-1,mu.shape[-1])), var.reshape((-1,var.shape[-1]))

    def __getitem__(self, idx):
        # batch_x_grasps = self.grasp_list[idx * self.batch_size: (idx + 1) * self.batch_size]
        g = self.grasp_list[idx]
        # print("idx: ", idx," g[9]: ", g[9])

        mu, var = self.extract(g, jitter=self.jitter)
        label = g[self.label_dim]
        if self.label_dim == 11:
            if label < 1:
                # label = -1.0
                label = 0.0


        if self.label_dim == 9 and self.b_correction:
            if self.b_force_fix_prior or self.prior_length_function is None:
                label = label - self.prior_length  # g[9]
            else:
                scene_id = g[1]
                if scene_id in self.dict_scenes_masks:
                    current_mask = self.dict_scenes_masks[scene_id]
                    current_prior_length, _ = self.prior_length_function(g, current_mask)
                else:
                    current_prior_length, _, current_mask = self.prior_length_function(g)
                    self.dict_scenes_masks[scene_id] = current_mask

                if current_prior_length == 1 or current_prior_length == 200:
                    current_prior_length = self.prior_length
                label = label - current_prior_length

        mu = np.array(mu).reshape((-1, mu.shape[-1]))
        var = np.array(var).reshape((-1, var.shape[-1]))
        # print("g: ", g)
        # print("label: ", label)
        return mu, var, label