from .scaler_utils import select_scaler
from scripts.models.common.data_generator import My_Custom_Generator_gpy
from shutil import copyfile
from scripts.common.pickle_util import save_obj

import sys, os
sys.path.append(os.path.dirname(sys.path[0]))
import os
import time
import torch
import matplotlib
matplotlib.rcParams['figure.figsize'] = (8, 5)
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.size'] = 16
matplotlib.rcParams['font.family'] = 'serif'
import GPy

import warnings
warnings.simplefilter("ignore")
import numpy as np

from joblib import dump, load

class GPyClassifier:
    def __init__(self, config, preprocessing_function):
        self.config = config
        self.preprocessing_function = preprocessing_function
        self.scaler_type = self.config.get('classifier', 'scaler_type')
        if self.scaler_type != 'None':
            self.scaler = select_scaler(self.scaler_type)
        else:
            self.scaler = None

        self.device = torch.device('cpu')
        self.b_with_augmentation = self.config.getboolean('classifier','use_augmentation')
        self.max_number_of_epochs = self.config.getint('classifier','max_number_of_epochs')

        self.coef_generator_score = self.config.getfloat('classifier', 'coef_generator_score')
        self.coef_classification_score = self.config.getfloat('classifier', 'coef_classification_score')
        self.norm = self.config.getboolean('classifier', 'norm')
        self.label_augmentation = self.config.getboolean('classifier', 'use_label_augmentation')
        self.type = self.config.get("classifier", "type")
        # self.preprocessing_type_classifier_obj = self.cfg.get('classifier', 'preprocessing')
        # self.preprocessing_type_classifier = self.cfg.get('classifier', 'preprocessing')

        self.by_object = self.config.getboolean('classifier','by_object')
        self.latest_id_obj_trained = 1
        self.model = None
        self.trained_model = self.config.get('classifier', 'trained_model')
        if self.trained_model == "None":
            self.trained_model = None

    def setup_model(self, X, Y, optimize=True):
        # kernel = GPy.kern.RBF(input_dim=X.shape[1], variance=1, lengthscale=0.4)
        kernel = GPy.kern.Matern52(input_dim=X.shape[1], variance=1, lengthscale=0.4, ARD=True)

        lik = GPy.likelihoods.Bernoulli()

        # self.model =GPy.models.GPClassification(
        #                 X, Y, kernel=ker)
        self.model = GPy.core.GP(X=X,
                        Y=Y,
                        kernel=kernel,
                        inference_method=GPy.inference.latent_function_inference.expectation_propagation.EP(),
                        likelihood=lik)
        if optimize:
            '''
            optimizers = {'fmin_tnc': opt_tnc,
            'simplex': opt_simplex,
            'lbfgsb': opt_lbfgsb,
            'org-bfgs': opt_bfgs,
            'scg': opt_SCG,
            'adadelta':Opt_Adadelta,
            'rprop':RProp,
            'adam':Adam} 
            '''
            self.model.optimize_restarts(optimizer='lbfgsb', num_restarts = 5, max_iters=self.max_number_of_epochs, parallel=False, num_processes=8, verbose=True)

    def load_model(self,path_to_weights):
        print('loading GPy classifier: ', path_to_weights,' exists ', os.path.isfile(path_to_weights))
        # X_numpy = np.load(path_to_weights.replace('model.npy','X_mu.npy'), allow_pickle=True)
        # Y_numpy = np.load(path_to_weights.replace('model.npy','Y.npy'), allow_pickle=True)

        # print('Y_numpy.shape: ', Y_numpy.shape)
        # kernel = GPy.kern.RBF(input_dim=X_numpy.shape[1], variance=1, lengthscale=0.4)
        # lik = GPy.likelihoods.Bernoulli()

        # self.model = GPy.core.GP(X=X_numpy,
        #                 Y=Y_numpy,
        #                 kernel=kernel,
        #                 inference_method=GPy.inference.latent_function_inference.expectation_propagation.EP(),
        #                 likelihood=lik,initialize=False)
        #
        # self.model.update_model(False)  # do not call the underlying expensive algebra on load
        # self.model.initialize_parameter()  # Initialize the parameters (connect the parameters up)
        # self.model[:] = np.load(path_to_weights)  # Load the parameters
        # self.model.update_model(True)  # Call the algebra only once
        self.model = GPy.core.GP.load_model(path_to_weights)

        if self.scaler is not None:
            self.scaler = load(path_to_weights.replace('.json.zip','_scaler.joblib'))

    def save_weights(self, path_to_weights):
        # self.model.save_model(self, path_to_weights + 'model_scaler.joblib', compress=True, save_data=True)
        # np.save(path_to_weights+'model.npy', self.model.param_array)
        self.model.save_model(path_to_weights+"model.json", compress=True, save_data=True)
        if self.scaler is not None:
            dump(self.scaler, path_to_weights + 'model_scaler.joblib')


    def fit(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation, where_to_save_model, config_path, verbose=False, dict_encoded_mu={}, dict_encoded_var={}, with_timestamp =False):
        GT_grasps = GT_grasps_training + GT_grasps_validation
        if self.by_object:
            self.dict_rgb_scene = dict_rgb_scene
            self.dict_depth_scene = dict_depth_scene
            self.dict_background_scene = dict_background_scene
            self.dict_id_scene_id_obj = dict_id_scene_id_obj
            self.GT_grasps_training = GT_grasps_training
            self.GT_grasps_validation = GT_grasps_validation
            self.dict_obj_grasp_training = {}
            self.dict_obj_grasp_validation= {}

            #check which objects are in labeled data
            for g in self.GT_grasps_training:
                id_obj = self.dict_id_scene_id_obj[g[1]]
                if id_obj not in self.dict_obj_grasp_training:
                    self.dict_obj_grasp_training[id_obj] = [g]
                else:
                    self.dict_obj_grasp_training[id_obj].append(g)

            for g in self.GT_grasps_validation:
                id_obj = self.dict_id_scene_id_obj[g[1]]
                if id_obj not in self.dict_obj_grasp_validation:
                    self.dict_obj_grasp_validation[id_obj] = [g]
                else:
                    self.dict_obj_grasp_validation[id_obj].append(g)

        size_training = len(GT_grasps)
        batch_size = size_training

        train_dataset = My_Custom_Generator_gpy(GT_grasps, batch_size,
                                                preprocessing_fct=self.preprocessing_function,
                                                dic_images_rgb=dict_rgb_scene,
                                                dic_depth_raw_data=dict_depth_scene,
                                                b_with_augmentation=self.b_with_augmentation,
                                                dict_encoded_mu=dict_encoded_mu, dict_encoded_var=dict_encoded_var)

        X_mu, X_var, labels = train_dataset.get_all()
        labels = labels.reshape((-1,1))

        if where_to_save_model is not None:
            if with_timestamp:
                timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())
            else:
                timestamp = ''
            save_dir = where_to_save_model
            dir_path = save_dir + "/" + timestamp + "/"

            if not os.path.exists(save_dir):
                os.mkdir(save_dir)

            if not os.path.exists(dir_path):
                os.mkdir(dir_path)

            np.save(dir_path + 'X_mu.npy', X_mu)
            np.save(dir_path + 'X_var.npy', X_var)
            np.save(dir_path + 'Y.npy', labels)


        start_time = time.time()
        if self.scaler is not None:
            self.scaler.fit(X_mu, labels)

            X_scaled = self.scaler.transform(X_mu)
            X_scaled = torch.from_numpy(X_scaled).to(self.device).float()
            # self.model.fit(X_scaled, labels)
            self.setup_model(X_scaled, labels)
        else:
            # self.model.fit(X_mu, labels)
            self.setup_model(X_mu, labels)

        elapsed_time = time.time() - start_time

        if where_to_save_model is not None:
            elapsed_time_file = os.path.join(dir_path, "elapsed_time_file.pkl")
            save_obj(elapsed_time, elapsed_time_file)

            copyfile(config_path, dir_path + "config.ini")

            self.save_weights(dir_path)

        if self.by_object:
            self.default_scaler = self.scaler
            self.default_model = self.model

    def predict(self,grasps_candidates, dict_rgb_scene, dict_depth_raw, dim_id_obj = 16):
        if self.by_object:
            print('predict by_object')
            r_full = []
            X_mu_full = None
            X_var_full = None
            elapsed_time_full = 0.0
            self.by_object = False
            #grasp candidates are already sorted
            # id_obj is in 12

            index_last=0
            while index_last != len(grasps_candidates)-1:
                index_start = index_last
                for i in range(index_start,len(grasps_candidates)-1):
                    if grasps_candidates[i][dim_id_obj] != grasps_candidates[i+1][dim_id_obj]:
                        break
                    index_last=i+1

                print("index_start: ", index_start)
                print("index_last: ", index_last)

                #only one label
                id_obj = grasps_candidates[index_start][dim_id_obj]
                if id_obj in self.dict_obj_grasp_training:
                    GT_grasps_training = self.dict_obj_grasp_training[id_obj]
                else:
                    GT_grasps_training = []

                if id_obj in self.dict_obj_grasp_validation:
                    GT_grasps_validation = self.dict_obj_grasp_validation[id_obj]
                else:
                    GT_grasps_validation = []

                print('GT_grasps_training: ', GT_grasps_training)
                print('GT_grasps_validation: ', GT_grasps_validation)

                sum_positive = 0
                sum_negative = 0
                for g in GT_grasps_training:
                    if g[11] > 0:
                        sum_positive+= 1
                    else:
                        sum_negative+=1

                for g in GT_grasps_validation:
                    if g[11] > 0:
                        sum_positive+= 1
                    else:
                        sum_negative+=1

                #todo maybe check taht we have at least one pos and one neg example
                if (len(GT_grasps_training) + len(GT_grasps_validation)) >= 2 and (sum_positive!=0 and sum_negative!=0):
                    if self.latest_id_obj_trained != id_obj:
                        print('fitting model ', id_obj)
                        self.latest_id_obj_trained = id_obj
                        self.fit(self.dict_rgb_scene, self.dict_depth_scene, self.dict_background_scene, self.dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation, where_to_save_model=None, config_path=None, verbose=False, dict_encoded_mu={}, dict_encoded_var={}, with_timestamp =False)
                else:
                    self.model = self.default_model
                    self.scaler = self.default_scaler
                current_grasp_candidates = grasps_candidates[index_start:index_last]
                print('len(current_grasp_candidates): ', len(current_grasp_candidates))
                X_mu, X_var = self.preprocessing_function(current_grasp_candidates, dict_rgb_scene, dict_depth_raw)

                if self.scaler is not None:
                    X_mu = self.scaler.transform(X_mu)

                # try:
                #     X_mu = torch.from_numpy(X_mu).to(self.device).float()
                # except:
                #     X_mu = torch.from_numpy(np.array(X_mu)).to(self.device).float()

                start_time = time.time()
                r, var = self.model.predict(X_mu)
                # proba = proba.reshape((-1))

                elapsed_time = time.time() - start_time
                # r = list([float(proba[i][1]) for i in range(len(proba))])

                r_full+=r
                if X_mu_full is None:
                    X_mu_full = X_mu
                else:
                    X_mu_full+=X_mu

                if X_var_full is None:
                    X_var_full = X_var
                else:
                    X_var_full += X_var

                elapsed_time_full+=elapsed_time


            self.by_object = True
            return r_full, X_mu_full, X_var_full, elapsed_time

        else:
            X_mu, X_var = self.preprocessing_function(grasps_candidates, dict_rgb_scene, dict_depth_raw)

            if self.scaler is not None:
                X_mu = self.scaler.transform(X_mu)

            # try:
            #     X_mu = torch.from_numpy(X_mu).to(self.device).float()
            # except:
            #     X_mu = torch.from_numpy(np.array(X_mu)).to(self.device).float()


            start_time = time.time()
            r, var = self.model.predict(X_mu)
            # proba = proba.reshape((-1))
            # print('proba.shape: ', proba.shape)
            elapsed_time = time.time() - start_time
            # r = list([float(proba[i][1]) for i in range(len(proba))])
            try:
                return r, X_mu.cpu().data.numpy(), X_var.cpu().data.numpy(), elapsed_time
            except:
                return r, X_mu, X_var, elapsed_time
