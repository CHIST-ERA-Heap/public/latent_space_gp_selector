from .sklearn_classifier_template import SklearnClassifier
from sklearn.neural_network import MLPClassifier

class MLPClassification(SklearnClassifier):
    def __init__(self, config, preprocessing_function):
        super().__init__(config, preprocessing_function)

    def setup_model(self):
        self.model =  MLPClassifier(random_state=1, max_iter=100000)