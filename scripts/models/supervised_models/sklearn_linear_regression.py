from sklearn.linear_model import LinearRegression
from .sklearn_regression_template import SklearnRegressor

class LinearRegressor(SklearnRegressor):
    #joblib
    def __init__(self, config, preprocessing_function, prior_length_function=None):
        super().__init__(config, preprocessing_function, prior_length_function)

    def setup_model(self):
        self.model_length = LinearRegression()
