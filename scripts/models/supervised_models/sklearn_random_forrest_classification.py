from .sklearn_classifier_template import SklearnClassifier
from sklearn.ensemble import RandomForestClassifier

class RandomForestClassification(SklearnClassifier):
    def __init__(self, config, preprocessing_function):
        super().__init__(config, preprocessing_function)

    def setup_model(self):
        self.model = RandomForestClassifier(random_state=1)