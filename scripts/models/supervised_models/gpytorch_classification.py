from gpytorch.variational import CholeskyVariationalDistribution, TrilNaturalVariationalDistribution#, TrilNaturalVariationalDistribution, NaturalVariationalDistribution, MeanFieldVariationalDistribution
from gpytorch.variational import VariationalStrategy#, CiqVariationalStrategy, LMCVariationalStrategy

from gpytorch.models import ApproximateGP
import numpy as np
import torch
import gpytorch
import math
from torch.utils.data import TensorDataset, DataLoader

import gc
import os
import time
from scripts.models.common.data_generator import My_Custom_Generator_gpy
from scripts.common.pickle_util import save_obj
from shutil import copyfile
from joblib import dump, load
from .scaler_utils import select_scaler

class ClassificationModel(ApproximateGP):
    def __init__(self, train_x, lengthscale=None, kernel="matern", num_dims=True, mean='ZeroMean', learn_inducing_locations=True):
    # def __init__(self, train_x, lengthscale=None, kernel="matern", num_dims = False, mean = 'ZeroMean',learn_inducing_locations=True):
    # def __init__(self, train_x, lengthscale=None, kernel="matern", num_dims=True, mean='ZeroMean', learn_inducing_locations=True):

        # print("train_x.size(0): ", train_x.size(0))
        # print("train_x.size(1): ", train_x.size(1))
        # variational_distribution = CholeskyVariationalDistribution(train_x.size(0))
        # variational_distribution = NaturalVariationalDistribution(train_x.size(0))
        variational_distribution = TrilNaturalVariationalDistribution(train_x.size(0))
        # variational_distribution = DeltaVariationalDistribution(train_x.size(0))
        # variational_distribution = MeanFieldVariationalDistribution(train_x.size(0))
        # variational_distribution = gpytorch.variational.NaturalVariationalDistribution(inducing_points.size(0))

        # variational_strategy = CiqVariationalStrategy(
        #     self, train_x, variational_distribution, learn_inducing_locations=learn_inducing_locations
        # )

        variational_strategy = VariationalStrategy(
            self, train_x, variational_distribution, learn_inducing_locations=learn_inducing_locations
        )
        #torch.DoubleTensor
        # variational_strategy = UnwhitenedVariationalStrategy(
        #     self, train_x, variational_distribution, learn_inducing_locations=learn_inducing_locations
        # )

        # variational_strategy = LMCVariationalStrategy(
        #     self, train_x, variational_distribution
        # )

        super(ClassificationModel, self).__init__(variational_strategy)
        if mean == 'ZeroMean':
            if num_dims:
                self.mean_module = gpytorch.means.ZeroMean(ard_num_dims=train_x.size(1))
            else:
                self.mean_module = gpytorch.means.ZeroMean()
        elif mean== 'ConstantMean':
            if num_dims:
                self.mean_module = gpytorch.means.ConstantMean(ard_num_dims=train_x.size(1))
            else:
                self.mean_module = gpytorch.means.ConstantMean()
        elif mean== 'LinearMean':
            if num_dims:
                self.mean_module = gpytorch.means.LinearMean(input_size=train_x.size(1))
            else:
                self.mean_module = gpytorch.means.LinearMean()


        ## Linear kernel
        if (kernel == 'linear'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.LinearKernel(ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.LinearKernel(ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.LinearKernel(lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.LinearKernel())

        # RFFKernel
        elif (kernel == 'RFFKernel'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RFFKernel(num_samples=1,ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RFFKernel(num_samples=1,ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RFFKernel(num_samples=1,lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RFFKernel(num_samples=1))

        elif (kernel == 'Periodic' or kernel == 'periodic'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.PeriodicKernel(ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PeriodicKernel(ard_num_dims=train_x.size(1)))

            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PeriodicKernel(lengthscale=lengthscale))

                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PeriodicKernel())

        elif (kernel == 'SpectralMixtureKernel'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.SpectralMixtureKernel(num_mixtures=4,ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.SpectralMixtureKernel(num_mixtures=4,ard_num_dims=train_x.size(1)))

            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.SpectralMixtureKernel(num_mixtures=4,lengthscale=lengthscale))

                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.SpectralMixtureKernel(num_mixtures=4))

        elif (kernel == 'SpectralDeltaKernel'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.SpectralDeltaKernel(num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.SpectralDeltaKernel(num_dims=train_x.size(1)))

            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.SpectralDeltaKernel(lengthscale=lengthscale))

                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.SpectralDeltaKernel())

        elif (kernel == 'RQKernel'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RQKernel(ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RQKernel(ard_num_dims=train_x.size(1)))

            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RQKernel(lengthscale=lengthscale))

                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RQKernel())
        ## RBF kernel
        elif (kernel == 'rbf' or kernel == 'RBF'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RBFKernel(ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RBFKernel(ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RBFKernel(lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.RBFKernel())

        ## Matern kernel nu=2.5
        elif (kernel == 'matern'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.MaternKernel(nu=2.5, ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.MaternKernel(nu=2.5, ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.MaternKernel(nu=2.5, lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.MaternKernel(nu=2.5))



        ## Polynomial (p=1)
        elif (kernel == 'poli1'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=1, ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=1, ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=1, lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=1))
        ## Polynomial (p=2)
        elif (kernel == 'poli2'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=2, ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=2, ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=2, lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=2))

        elif (kernel == 'poli3'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=3, ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=3, ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=3, lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=3))

        elif (kernel == 'poli4'):
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=4, ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=4, ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=4, lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.PolynomialKernel(power=4))

        elif (kernel == 'cosine'):
            ## Cosine distance and BatchNorm Cosine distance
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.CosineKernel(ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.CosineKernel(ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.CosineKernel(lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.CosineKernel())

        elif (kernel == 'cossim' or kernel == 'bncossim'):
            ## Cosine distance and BatchNorm Cosine distance
            if num_dims:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.LinearKernel(ard_num_dims=train_x.size(1), lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.LinearKernel(ard_num_dims=train_x.size(1)))
            else:
                if lengthscale is not None:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.LinearKernel(lengthscale=lengthscale))
                else:
                    self.covar_module = gpytorch.kernels.ScaleKernel(
                        gpytorch.kernels.LinearKernel())


        else:
            raise ValueError("[ERROR] the kernel '" + str(kernel) + "' is not supported!")

        self.covar_module.base_kernel.variance = 10.0#2.0
        # self.covar_module.base_kernel.raw_variance.requires_grad = False

    def set_data(self, new_X):
        self.__init__(new_X);

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        latent_pred = gpytorch.distributions.MultivariateNormal(mean_x, covar_x)
        return latent_pred

class GPModel(ApproximateGP):
    def __init__(self, inducing_points):
        variational_distribution = CholeskyVariationalDistribution(inducing_points.size(0))
        variational_strategy = VariationalStrategy(self, inducing_points, variational_distribution, learn_inducing_locations=True)
        super(GPModel, self).__init__(variational_strategy)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

# define the actual GP model (kernels, inducing points, etc.)
class PG_GPModel(gpytorch.models.ApproximateGP):
    def __init__(self, inducing_points):
        # variational_distribution = gpytorch.variational.NaturalVariationalDistribution(inducing_points.size(0))
        variational_distribution = TrilNaturalVariationalDistribution(inducing_points.size(0))

        variational_strategy = gpytorch.variational.VariationalStrategy(
            self, inducing_points, variational_distribution, learn_inducing_locations=True
        )

        super(PG_GPModel, self).__init__(variational_strategy)
        self.mean_module = gpytorch.means.ZeroMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

class PGLikelihood(gpytorch.likelihoods._OneDimensionalLikelihood):
    # this method effectively computes the expected log likelihood
    # contribution to Eqn (10) in Reference [1].
    def expected_log_prob(self, target, input, *args, **kwargs):
        mean, variance = input.mean, input.variance
        # Compute the expectation E[f_i^2]
        raw_second_moment = variance + mean.pow(2)

        # Translate targets to be -1, 1
        target = target.to(mean.dtype).mul(2.).sub(1.)

        # We detach the following variable since we do not want
        # to differentiate through the closed-form PG update.
        c = raw_second_moment.detach().sqrt()
        # Compute mean of PG auxiliary variable omega: 0.5 * Expectation[omega]
        # See Eqn (11) and Appendix A2 and A3 in Reference [1] for details.
        half_omega = 0.25 * torch.tanh(0.5 * c) / c

        # Expected log likelihood
        res = 0.5 * target * mean - half_omega * raw_second_moment
        # Sum over data points in mini-batch
        res = res.sum(dim=-1)

        return res

    # define the likelihood
    def forward(self, function_samples):
        return torch.distributions.Bernoulli(logits=function_samples)

    # define the marginal likelihood using Gauss Hermite quadrature
    def marginal(self, function_dist):
        prob_lambda = lambda function_samples: self.forward(function_samples).probs
        probs = self.quadrature(prob_lambda, function_dist)
        return torch.distributions.Bernoulli(probs=probs)

def reparameterize( mu, var, b_sample=True):
    """
    :param mu: mean from the encoder's latent space
    :param log_var: log variance from the encoder's latent space
    """
    if b_sample:
        std = torch.exp(0.5 * var)  # standard deviation
        eps = torch.randn_like(std)  # `randn_like` as we need the same size
        tweak = (eps * std)
        # print("eps[0]: ", eps[0])
        # print("std[0]: ", var[0])
        # print("tweak[0]: ", tweak[0])
        sample = mu + tweak  # sampling
        return sample
    else:
        return mu

class GPyTorchClassification:
    def __init__(self, config, preprocessing_function):
        self.config = config
        self.preprocessing_function = preprocessing_function
        self.class_model = ClassificationModel

        # self.use_standard_scaler = self.config.getboolean('classifier', 'use_standard_scaler')
        self.scaler_type = self.config.get('classifier', 'scaler_type')
        if self.scaler_type != 'None':
            self.scaler = select_scaler(self.scaler_type)
        else:
            self.scaler = None

        self.dim_id_obj = self.config.get("grasp_object_classifier", "dim_id_obj")

        self.use_gpu = self.config.getboolean('classifier', 'use_gpu')
        if self.use_gpu:
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        else:
            self.device = torch.device('cpu')

        self.latest_id_obj_trained = 1
        self.by_object = self.config.getboolean('classifier','by_object')
        self.lr = self.config.getfloat('classifier','lr')
        self.scheduler_patience = self.config.getint('classifier','scheduler_patience')
        self.max_number_of_epochs = self.config.getint('classifier','max_number_of_epochs')
        self.max_epoch_without_improvement = self.config.getint('classifier', 'max_epoch_without_improvement')
        self.batch_size = self.config.getint('classifier','max_batch_size')
        self.min_lr = self.config.getfloat('classifier','min_lr')
        self.min_time_to_stop_training = self.config.getfloat('classifier','min_time_to_stop_training')
        self.b_with_augmentation = self.config.getboolean('classifier','use_augmentation')

        self.coef_generator_score = self.config.getfloat('classifier', 'coef_generator_score')
        self.coef_classification_score = self.config.getfloat('classifier', 'coef_classification_score')
        self.norm = self.config.getboolean('classifier', 'norm')
        self.label_augmentation = self.config.getboolean('classifier', 'use_label_augmentation')
        self.type = self.config.get("classifier", "type")
        # self.preprocessing_type_classifier_obj = self.cfg.get('classifier', 'preprocessing')
        # self.preprocessing_type_classifier = self.cfg.get('classifier', 'preprocessing')

        self.likelihood = gpytorch.likelihoods.BernoulliLikelihood()
        # self.likelihood = PGLikelihood()
        # self.likelihood = gpytorch.likelihoods.GaussianLikelihood()

        self.trained_model = self.config.get('classifier', 'trained_model')
        if self.trained_model == "None":
            self.trained_model = None
        else:
            if not os.path.exists(self.trained_model):
                self.trained_model = None
        self.model = None

    def load_model(self, path_to_weights):
        print("load_weights GPyTorchClassification: ", path_to_weights, 'exists: ', os.path.isfile(path_to_weights))

        state_dict = torch.load(path_to_weights, map_location=self.device)
        X_numpy = np.load(path_to_weights.replace('model.pth','X_mu.npy'), allow_pickle=True)
        # print("X_numpy.shape: ", X_numpy.shape)
        #Y_numpy = np.load(path_to_weights.replace('.pth','_Y.npy'))
        try:
            X = torch.from_numpy(X_numpy).float()
        except Exception as e:
            print(e)
            X = torch.as_tensor(np.array(X_numpy).astype('float'))
        self.model = self.class_model(X).to(self.device)

        self.model.load_state_dict(state_dict, strict=False)
        if self.scaler is not None:
            self.scaler = load(path_to_weights.replace('.pth','_scaler.joblib'))

    def save_weights(self, path_to_weights):
        torch.save(self.model.state_dict(), path_to_weights+"model.pth")
        if self.scaler is not None:
            dump(self.scaler, path_to_weights + 'model_scaler.joblib')

    def predict(self,grasps_candidates, dict_rgb_scene, dict_depth_raw, augmented=False):
        dim_id_obj = self.dim_id_obj

        if self.by_object:
            print('predict by_object')
            r_full = []
            X_mu_full = None
            X_var_full = None
            elapsed_time_full = 0.0
            self.by_object = False
            # grasp candidates are already sorted
            # id_obj is in 12

            index_last = 0
            while index_last != len(grasps_candidates) - 1:
                index_start = index_last
                for i in range(index_start, len(grasps_candidates) - 1):
                    if grasps_candidates[i][dim_id_obj] != grasps_candidates[i + 1][dim_id_obj]:
                        break
                    index_last = i + 1

                # print("index_start: ", index_start)
                # print("index_last: ", index_last)

                # only one label
                id_obj = grasps_candidates[index_start][dim_id_obj]
                if id_obj in self.dict_obj_grasp_training:
                    GT_grasps_training = self.dict_obj_grasp_training[id_obj]
                else:
                    GT_grasps_training = []

                if id_obj in self.dict_obj_grasp_validation:
                    GT_grasps_validation = self.dict_obj_grasp_validation[id_obj]
                else:
                    GT_grasps_validation = []

                # print('GT_grasps_training: ', GT_grasps_training)
                # print('GT_grasps_validation: ', GT_grasps_validation)

                sum_positive = 0
                sum_negative = 0
                for g in GT_grasps_training:
                    if g[11] > 0:
                        sum_positive+= 1
                    else:
                        sum_negative+=1

                for g in GT_grasps_validation:
                    if g[11] > 0:
                        sum_positive+= 1
                    else:
                        sum_negative+=1

                #todo maybe check taht we have at least one pos and one neg example
                if (len(GT_grasps_training) + len(GT_grasps_validation)) >= 2 and (sum_positive!=0 and sum_negative!=0):
                    if self.latest_id_obj_trained != id_obj:
                        print('fitting model ', id_obj)
                        self.latest_id_obj_trained = id_obj
                        self.fit(self.dict_rgb_scene, self.dict_depth_scene, self.dict_background_scene,
                                 self.dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation,
                                 where_to_save_model=None, config_path=None, verbose=False, dict_encoded_mu={},
                                 dict_encoded_var={}, with_timestamp=False)
                else:
                    self.model = self.default_model
                    self.scaler = self.default_scaler
                current_grasp_candidates = grasps_candidates[index_start:index_last]
                # print('len(current_grasp_candidates): ', len(current_grasp_candidates))
                X_mu, X_var = self.preprocessing_function(current_grasp_candidates, dict_rgb_scene, dict_depth_raw)

                if self.scaler is not None:
                    X_mu = self.scaler.transform(X_mu)

                try:
                    X_mu = torch.from_numpy(X_mu).to(self.device).float()
                except:
                    X_mu = torch.from_numpy(np.array(X_mu)).to(self.device).float()

                self.model.eval()
                self.likelihood.eval()

                with torch.no_grad() and gpytorch.settings.fast_pred_var() and gpytorch.settings.fast_pred_samples():
                    start_time = time.time()
                    pred = self.model(X_mu)
                    observed_pred = self.likelihood(pred)
                    elapsed_time = time.time() - start_time

                    # var = observed_pred.variance.cpu().data.numpy()
                    # vars = observed_pred.variance.cpu().data.numpy()
                    means = list(observed_pred.mean.cpu().data.numpy())


                r_full += means
                if X_mu_full is None:
                    X_mu_full = X_mu
                else:
                    X_mu_full += X_mu

                if X_var_full is None:
                    X_var_full = X_var
                else:
                    X_var_full += X_var

                elapsed_time_full += elapsed_time

            self.by_object = True
            return r_full, X_mu_full, X_var_full, elapsed_time

        else:
            X_mu, X_var = self.preprocessing_function(grasps_candidates, dict_rgb_scene, dict_depth_raw)
            list_X_mu = [X_mu]
            if augmented:
                means_sums = np.zeros((len(grasps_candidates)))
                X_mu_1, _ = self.preprocessing_function(grasps_candidates, dict_rgb_scene, dict_depth_raw,
                                                  flipped=[1 for i in range(len(grasps_candidates))])
                list_X_mu.append(X_mu_1)

                # grasps_candidates_tweaked = []
                # for g in grasps_candidates:
                #     g = list(g)
                #     g[8] -= np.pi
                #     grasps_candidates_tweaked.append(g)
                #
                # X_mu_2, _ = self.preprocessing_function(grasps_candidates_tweaked, dict_rgb_scene, dict_depth_raw)
                # list_X_mu.append(X_mu_2)
                #
                # X_mu_3, _ = self.preprocessing_function(grasps_candidates_tweaked, dict_rgb_scene, dict_depth_raw,
                #                                   flipped=[1 for i in range(len(grasps_candidates_tweaked))])
                # list_X_mu.append(X_mu_3)
                #
                # grasps_candidates_tweaked = []
                # for g in grasps_candidates:
                #     g = list(g)
                #     g[8] += np.pi
                #     grasps_candidates_tweaked.append(g)
                #
                # X_mu_4, _ = self.preprocessing_function(grasps_candidates_tweaked, dict_rgb_scene, dict_depth_raw)
                # list_X_mu.append(X_mu_4)
                #
                # X_mu_5, _ = self.preprocessing_function(grasps_candidates_tweaked, dict_rgb_scene, dict_depth_raw,
                #                                   flipped=[1 for i in range(len(grasps_candidates_tweaked))])
                # list_X_mu.append(X_mu_5)

            nb_of_augmented = 0
            for X in list_X_mu:
                nb_of_augmented+=1
                if self.scaler is not None:
                    X = self.scaler.transform(X)

                X = torch.from_numpy(X).float().to(self.device)

                self.model.eval()
                self.likelihood.eval()

                with torch.no_grad() and gpytorch.settings.fast_pred_var() and gpytorch.settings.fast_pred_samples():
                    start_time = time.time()
                    pred = self.model(X)
                    observed_pred = self.likelihood(pred)
                    elapsed_time = time.time() - start_time

                    # var = observed_pred.variance.cpu().data.numpy()
                    # vars = observed_pred.variance.cpu().data.numpy()
                    means = observed_pred.mean.cpu().data.numpy()
                    # max = np.max(means)
                    # print("max: ", max)
                    # print("var mean: ", np.mean(vars))
                    # print("var std: ", np.std(vars))

                    # means = list(means)
                    # vars = list(vars)
                    # for i in range(len(vars)):
                    #     # means[i] = means[i]*(1.0-vars[i])
                    #     # means[i] -= vars[i]
                    #     if means[i] >= max-0.1:
                    #         means[i] -=vars[i]
                    #     else:
                    #         means[i] -= 2.0*vars[i]
                    if not augmented:
                        return means, X_mu, X_var, elapsed_time
                    else:
                        means = np.asarray(means)
                        means_sums += means
            return means_sums/nb_of_augmented, X_mu, X_var, elapsed_time

    def fit(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation,
            where_to_save_model, config_path, verbose=True, dict_encoded_mu={}, dict_encoded_var={}, with_timestamp=False):
        GT_grasps = GT_grasps_training + GT_grasps_validation
        if self.by_object:
            self.dict_rgb_scene = dict_rgb_scene
            self.dict_depth_scene = dict_depth_scene
            self.dict_background_scene = dict_background_scene
            self.dict_id_scene_id_obj = dict_id_scene_id_obj
            self.GT_grasps_training = GT_grasps_training
            self.GT_grasps_validation = GT_grasps_validation
            self.dict_obj_grasp_training = {}
            self.dict_obj_grasp_validation= {}

            #check which objects are in labeled data
            for g in self.GT_grasps_training:
                id_obj = self.dict_id_scene_id_obj[g[1]]
                if id_obj not in self.dict_obj_grasp_training:
                    self.dict_obj_grasp_training[id_obj] = [g]
                else:
                    self.dict_obj_grasp_training[id_obj].append(g)

            for g in self.GT_grasps_validation:
                id_obj = self.dict_id_scene_id_obj[g[1]]
                if id_obj not in self.dict_obj_grasp_validation:
                    self.dict_obj_grasp_validation[id_obj] = [g]
                else:
                    self.dict_obj_grasp_validation[id_obj].append(g)

        s = 0.0
        for g in GT_grasps:
            s += g[11]
        # print("gpytorch classifier GT_grasps: ", GT_grasps)
        size_training = len(GT_grasps)

        if self.batch_size > size_training:
            batch_size = size_training
        else:
            batch_size = self.batch_size

        print("size_training: ", size_training)
        print("batch_size: ", batch_size)
        print("number of positive labels: ", s)

        if self.use_gpu is None:
            self.use_gpu = (batch_size > 1000)

        print("self.use_gpu: ", self.use_gpu)
        train_dataset = My_Custom_Generator_gpy(GT_grasps, batch_size, preprocessing_fct=self.preprocessing_function,
                                                dic_images_rgb=dict_rgb_scene,
                                                dic_depth_raw_data=dict_depth_scene,
                                                b_with_augmentation=False,
                                                dict_encoded_mu=dict_encoded_mu, dict_encoded_var=dict_encoded_var)

        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, drop_last=True)
        X_mu, X_var, labels = next(iter(train_loader))
        X_mu = torch.reshape(X_mu, (batch_size, X_mu.size()[-1]))
        X_var = torch.reshape(X_var, (batch_size, X_var.size()[-1]))
        train_dataset.b_with_augmentation = self.b_with_augmentation

        save_dir = where_to_save_model
        if save_dir is not None:
            if with_timestamp:
                timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())
            else:
                timestamp = ''
            dir_path = save_dir + "/" + timestamp + "/"

            if not os.path.exists(save_dir):
                os.mkdir(save_dir)

            if not os.path.exists(dir_path):
                os.mkdir(dir_path)

            np.save(dir_path + 'X_mu.npy', X_mu.cpu().data.numpy())
            np.save(dir_path + 'X_var.npy', X_var.cpu().data.numpy())
            np.save(dir_path + 'Y.npy', labels.cpu().data.numpy())

        if not self.b_with_augmentation and not train_dataset.jitter and batch_size == size_training:
            train_dataset = TensorDataset(X_mu, X_var, labels)
            train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=False, drop_last=True)

        print("X_mu.size(): ", X_mu.size())
        # print("X_var.size(): ", X_var.size())

        del X_var
        X_mu = torch.reshape(X_mu, (batch_size, -1))

        if self.scaler is not None:
            self.scaler.fit(X_mu, labels)
            # self.scaler = preprocessing.RobustScaler().fit(X_mu)
            # X_mu = X_mu.mean.cpu().data.numpy()

            X_scaled = self.scaler.transform(X_mu)
            X_scaled = torch.from_numpy(X_scaled).float().to(self.device)
            self.model = self.class_model(X_scaled[:batch_size])
        else:
            self.model = self.class_model(X_mu[:batch_size].to(self.device))

        self.model.type('torch.DoubleTensor')
        del X_mu
        gc.collect()

        training_steps = math.ceil(size_training // batch_size)


        # optimizer = torch.optim.Adagrad([
        #     {'params': self.model.parameters()},
        #     {'params': self.likelihood.parameters()},
        #     {'params': self.likelihood.hyperparameters()},
        # ], lr=self.lr)
        # optimizer.n_iter = 0
        # lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', patience=self.scheduler_patience*training_steps, min_lr=self.min_lr, verbose=True)

        variational_optimizer = gpytorch.optim.NGD(self.model.variational_parameters(), num_data=batch_size, lr=self.lr)
        # variational_optimizer = torch.optim.Adagrad(self.model.variational_parameters(), lr=self.lr)
        for state in variational_optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.to(self.device)

        variational_optimizer.n_iter = 0
        hyperparameter_optimizer = torch.optim.Adam([
            {'params': self.model.parameters()},
            {'params': self.likelihood.parameters()},
        ], lr=self.lr*0.01)
        hyperparameter_optimizer.n_iter = 0
        variational_lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(variational_optimizer, 'min',
                                                                              patience=self.scheduler_patience * training_steps,
                                                                              verbose=True, min_lr=self.min_lr)
        hyperparameter_lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(hyperparameter_optimizer, 'min',
                                                                                 patience=self.scheduler_patience * training_steps,
                                                                                 verbose=True,min_lr=self.min_lr)
        # variational_lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(variational_optimizer, milestones=[0.5 * 100000, 0.75 * 100000], gamma=0.1)
        # hyperparameter_lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(variational_optimizer, milestones=[0.5 * 100000, 0.75 * 100000], gamma=0.1)

        best_loss = 100000
        counter_max_epoch_without_improvement = 0

        with gpytorch.settings.use_toeplitz(not self.use_gpu) and gpytorch.settings.memory_efficient(not self.use_gpu):
            # and gpytorch.settings.max_root_decomposition_size(10000) and gpytorch.settings.max_cg_iterations(10000):
            # and  gpytorch.settings.max_cholesky_size(800) and  gpytorch.settings.max_eager_kernel_size(512) and  gpytorch.settings.max_preconditioner_size\
            # and gpytorch.settings.num_likelihood_samples(10):

            self.model = self.model.to(self.device)
            # self.model.variational_strategy = self.model.variational_strategy.to(self.device)

            self.likelihood = self.likelihood.to(self.device)
            self.model.train()
            self.likelihood.train()
            mll = gpytorch.mlls.VariationalELBO(self.likelihood, self.model, num_data=len(train_loader.dataset),
                                                beta=1.0)#.to(self.device)

            # mll = gpytorch.mlls.PredictiveLogLikelihood(self.likelihood, self.model, num_data=len(train_loader.dataset), beta=0.5)

            start_time = time.time()
            for epoch in range(0, self.max_number_of_epochs):
                batch_iter = 0
                loss_epoch = 0
                for batch_idx, (data_mu, data_var, target) in enumerate(train_loader):
                    # print("sum target: ", target.sum())
                    # optimizer.zero_grad()
                    variational_optimizer.zero_grad()
                    hyperparameter_optimizer.zero_grad()

                    data_mu = torch.reshape(data_mu, (batch_size, data_mu.size()[-1]))
                    data_var = torch.reshape(data_var, (batch_size, data_var.size()[-1]))

                    train_x_sample = reparameterize(data_mu, data_var)
                    if self.scaler is not None:
                        train_x_sample = self.scaler.transform(train_x_sample.cpu().data.numpy())
                        train_x_sample = torch.from_numpy(train_x_sample).float()

                    # print("train_x_sample.size: ", train_x_sample.size())

                    if self.use_gpu:
                        del data_mu
                        del data_var
                        gc.collect()
                        torch.cuda.empty_cache()
                        train_x_sample = train_x_sample.to(self.device)

                    # output = self.model(data_mu)
                    # print("train_x_sample.size(): ", train_x_sample.size())
                    # print("target.size(): ", target.size())
                    # print("train_x_sample.is_cuda: ", train_x_sample.is_cuda)
                    # print("train_x_sample.device: ", train_x_sample.device)
                    # print("next(self.model.parameters()).device: ", next(self.model.parameters()).device)
                    # print("next(self.model.variational_parameters()).device: ", next(self.model.variational_parameters()).device)

                    output = self.model(train_x_sample)
                    if self.use_gpu:
                        del train_x_sample
                        gc.collect()
                        torch.cuda.empty_cache()
                        target = target.to(self.device)

                    # Calc loss and backprop gradients
                    # print("output.device: ", output.device)
                    # print("target.device: ", target.device)


                    loss = -mll(output, target)
                    if self.use_gpu:
                        del output
                        del target
                        gc.collect()
                        torch.cuda.empty_cache()

                    loss.backward()

                    # optimizer.n_iter += 1
                    # optimizer.step()
                    variational_optimizer.step()
                    variational_optimizer.n_iter += 1
                    hyperparameter_optimizer.step()
                    hyperparameter_optimizer.n_iter += 1

                    batch_iter += 1
                    loss_epoch += loss.item()
                elapsed_time = time.time() - start_time

                loss_epoch = round(loss_epoch / batch_iter, 3)
                if loss_epoch < best_loss:
                    if verbose:
                        print('best_loss ', best_loss, ' epoch: ', epoch)
                        best_loss = loss_epoch
                    counter_max_epoch_without_improvement = 0
                else:
                    counter_max_epoch_without_improvement += 1
                if counter_max_epoch_without_improvement >= self.max_epoch_without_improvement:
                    if best_loss < 0.69 or counter_max_epoch_without_improvement > 2 * self.max_epoch_without_improvement and elapsed_time > self.min_time_to_stop_training:
                        break

                # lr_scheduler.step(loss_epoch)
                variational_lr_scheduler.step(loss_epoch)
                hyperparameter_lr_scheduler.step(loss_epoch)

                # print('epoch ', epoch, ' batch_iter ', batch_iter, ' loss: ', loss.item())

            print(elapsed_time, 's ', 'epoch ', epoch, ' batch_iter ', batch_iter, ' loss: ', loss.item())

        if save_dir is not None:
            elapsed_time_file = os.path.join(dir_path, "elapsed_time_file.pkl")
            save_obj(elapsed_time, elapsed_time_file)

            copyfile(config_path, dir_path + "config.ini")

            self.save_weights(dir_path)

        if self.by_object:
            self.default_scaler = self.scaler
            self.default_model = self.model
