import os
from joblib import dump, load
from .scaler_utils import select_scaler
from scripts.models.common.data_generator import My_Custom_Generator_gpy
import torch
from shutil import copyfile
import time
from scripts.common.pickle_util import save_obj, load_obj
import numpy as np
from scipy.spatial.distance import cdist
import skimage.metrics as metrics

#todo no augmentation yet
#mod best of all: check distance for each positive/negative example, return lowest
#mod average: make an average for each positive/negative example
'''
skimage.metrics.adapted_rand_error([...])
	

Compute Adapted Rand error as defined by the SNEMI3D contest.

skimage.metrics.contingency_table(im_true, ...)
	

Return the contingency table for all regions in matched segmentations.

skimage.metrics.hausdorff_distance(image0, ...)
	

Calculate the Hausdorff distance between nonzero elements of given images.

skimage.metrics.hausdorff_pair(image0, image1)
	

Returns pair of points that are Hausdorff distance apart between nonzero elements of given images.

skimage.metrics.mean_squared_error(image0, ...)
	

Compute the mean-squared error between two images.

skimage.metrics.normalized_mutual_information(...)
	

Compute the normalized mutual information (NMI).

skimage.metrics.normalized_root_mse(...[, ...])
	

Compute the normalized root mean-squared error (NRMSE) between two images.

skimage.metrics.peak_signal_noise_ratio(...)
	

Compute the peak signal to noise ratio (PSNR) for an image.

skimage.metrics.structural_similarity(im1, ...)
	

Compute the mean structural similarity index between two images.

skimage.metrics.variation_of_information([...])
	

Return symmetric conditional entropies associated with the VI.
'''
class ZeroShotPatchClassifier:
    def __init__(self, config, preprocessing_function, mode='best'):
        self.dist_metric = 'raw'#structural_similarity , mean_squared_error, hausdorff_distance
        self.mode = mode
        self.config = config
        self.preprocessing_function = preprocessing_function
        self.scaler_type = self.config.get('classifier', 'scaler_type')
        self.feature_extraction_preprocessing_function = self.config.get('feature_extraction', 'preprocessing')

        # print('self.feature_extraction_preprocessing_function: ', self.feature_extraction_preprocessing_function)

        if self.scaler_type != 'None':
            self.scaler = select_scaler(self.scaler_type)
        else:
            self.scaler = None

        self.dim_id_obj = self.config.getint("grasp_object_classifier", "dim_id_obj")
        self.coef_generator_score = self.config.getfloat('classifier', 'coef_generator_score')
        self.coef_classification_score = self.config.getfloat('classifier', 'coef_classification_score')
        self.norm = self.config.getboolean('classifier', 'norm')
        self.label_augmentation = self.config.getboolean('classifier', 'use_label_augmentation')
        self.type = self.config.get("classifier", "type")
        # self.preprocessing_type_classifier_obj = self.cfg.get('classifier', 'preprocessing')
        # self.preprocessing_type_classifier = self.cfg.get('classifier', 'preprocessing')

        self.representation_positive = None
        self.representation_negative = None

        self.device = torch.device('cpu')
        self.b_with_augmentation = self.config.getboolean('classifier','use_augmentation')

        self.by_object = self.config.getboolean('classifier','by_object')
        self.model = None
        self.trained_model = self.config.get('classifier', 'trained_model')
        if self.trained_model == "None":
            self.trained_model = None

    def setup_model(self):
        pass

    def load_model(self,path_to_weights):
        print('loading zeroshot classifier: ', path_to_weights)
        if self.scaler is not None:
            self.scaler = load(path_to_weights.replace('.pth', '_scaler.joblib'))

        self.GT_grasps = load_obj(path_to_weights + 'GT_grasps.pkl')
        self.GT_grasps_training = load_obj(path_to_weights + 'GT_grasps_training.pkl')
        self.GT_grasps_validation = load_obj(path_to_weights + 'GT_grasps_validation.pkl')
        self.dict_obj_grasp = load_obj(path_to_weights + 'dict_obj_grasp.pkl')
        self.dict_scene_grasp = load_obj(path_to_weights + 'dict_scene_grasp.pkl')
        self.dict_id_grasp_feature = load_obj(path_to_weights + 'dict_id_grasp_feature.pkl')
        self.dict_id_grasp_feature_flipped = load_obj(path_to_weights + 'dict_id_grasp_feature_flipped.pkl')
        self.dict_id_grasp_feature_minuspi = load_obj(path_to_weights + 'dict_id_grasp_feature_minuspi.pkl')
        self.dict_id_grasp_feature_minuspi_flipped = load_obj(path_to_weights + 'dict_id_grasp_feature_minuspi_flipped.pkl')
        self.dict_id_grasp_feature_pluspi = load_obj(path_to_weights + 'dict_id_grasp_feature_pluspi.pkl')
        self.dict_id_grasp_feature_pluspi_flipped = load_obj(path_to_weights + 'dict_id_grasp_feature_pluspi_flipped.pkl')

        self.model = True

    def save_weights(self, path_to_weights):
        save_obj(self.dict_id_scene_id_obj, path_to_weights + 'dict_id_scene_id_obj.pkl')
        if self.scaler is not None:
            dump(self.scaler, path_to_weights + 'model_scaler.joblib')


        save_obj(self.GT_grasps, path_to_weights + 'GT_grasps.pkl')
        save_obj(self.GT_grasps_training, path_to_weights + 'GT_grasps_training.pkl')
        save_obj(self.GT_grasps_validation, path_to_weights + 'GT_grasps_validation.pkl')
        save_obj(self.dict_obj_grasp, path_to_weights + 'dict_obj_grasp.pkl')
        save_obj(self.dict_scene_grasp, path_to_weights + 'dict_scene_grasp.pkl')
        save_obj(self.dict_id_grasp_feature, path_to_weights + 'dict_id_grasp_feature.pkl')
        save_obj(self.dict_id_grasp_feature_flipped, path_to_weights + 'dict_id_grasp_feature_flipped.pkl')
        save_obj(self.dict_id_grasp_feature_minuspi, path_to_weights + 'dict_id_grasp_feature_minuspi.pkl')
        save_obj(self.dict_id_grasp_feature_minuspi_flipped, path_to_weights + 'dict_id_grasp_feature_minuspi_flipped.pkl')
        save_obj(self.dict_id_grasp_feature_pluspi, path_to_weights + 'dict_id_grasp_feature_pluspi.pkl')
        save_obj(self.dict_id_grasp_feature_pluspi_flipped, path_to_weights + 'dict_id_grasp_feature_pluspi_flipped.pkl')


    def fit(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation, where_to_save_model, config_path, verbose=False, dict_encoded_mu={}, dict_encoded_var={}, with_timestamp =False):
        self.GT_grasps = GT_grasps_training + GT_grasps_validation
        print('ZeroShotClassifier has ', len(self.GT_grasps), " grasps")
        # self.dict_rgb_scene = dict_rgb_scene
        # self.dict_depth_scene = dict_depth_scene
        # self.dict_background_scene = dict_background_scene
        self.dict_id_scene_id_obj = dict_id_scene_id_obj
        self.GT_grasps_training = GT_grasps_training
        self.GT_grasps_validation = GT_grasps_validation
        self.dict_obj_grasp = {}
        self.dict_scene_grasp = {}
        self.dict_id_grasp_feature = {}
        self.dict_id_grasp_feature_flipped = {}
        self.dict_id_grasp_feature_minuspi = {}
        self.dict_id_grasp_feature_minuspi_flipped = {}
        self.dict_id_grasp_feature_pluspi = {}
        self.dict_id_grasp_feature_pluspi_flipped = {}
        start_time = time.time()

        #check which objects are in labeled data
        for g in self.GT_grasps:
            id_scene = g[1]
            if id_scene in self.dict_scene_grasp:
                self.dict_scene_grasp[id_scene].append(g)
            else:
                self.dict_scene_grasp[id_scene] = [g]

            id_obj = self.dict_id_scene_id_obj[id_scene]
            if id_obj not in self.dict_obj_grasp:
                self.dict_obj_grasp[id_obj] = [g]
            else:
                self.dict_obj_grasp[id_obj].append(g)

        list_id_scene = self.dict_scene_grasp.keys()
        print('from ', len(list_id_scene), ' scenes from ', len(self.dict_obj_grasp.keys()),' objects')


        X_mu_full = None
        X_var_full = None
        labels = []
        for id_scene in list_id_scene:


            X_mu, X_var = self.preprocessing_function(self.dict_scene_grasp[id_scene], dict_rgb_scene, dict_depth_scene)
            X_mu = list(X_mu)
            X_var = list(X_var)
            if X_mu_full is None:
                X_mu_full = X_mu
            else:
                X_mu_full += X_mu

            if X_var_full is None:
                X_var_full = X_var
            else:
                X_var_full += X_var
            j=0
            for g in self.dict_scene_grasp[id_scene]:
                labels.append(g[11])
                self.dict_id_grasp_feature[g[0]] = X_mu[j]
                j+=1

            if self.b_with_augmentation:
                GT_grasps = self.dict_scene_grasp[id_scene]
                X_mu, X_var = self.preprocessing_function(GT_grasps, dict_rgb_scene, dict_depth_scene,
                                            flipped=[1 for i in range(len(GT_grasps))])
                j=0
                for g in GT_grasps:
                    self.dict_id_grasp_feature_flipped[g[0]] = X_mu[j]
                    j += 1

                GT_grasps_training_tweaked = []
                for g in GT_grasps:
                    g = list(g)
                    g[8] -= np.pi
                    GT_grasps_training_tweaked.append(g)

                X_mu, X_var = self.preprocessing_function(GT_grasps_training_tweaked, dict_rgb_scene,
                                            dict_depth_scene)
                j=0
                for g in GT_grasps_training_tweaked:
                    self.dict_id_grasp_feature_minuspi[g[0]] = X_mu[j]
                    j += 1

                X_mu, X_var = self.preprocessing_function(GT_grasps_training_tweaked, dict_rgb_scene,
                                            dict_depth_scene,
                                            flipped=[1 for i in range(len(GT_grasps_training_tweaked))])
                j = 0
                for g in GT_grasps_training_tweaked:
                    self.dict_id_grasp_feature_minuspi_flipped[g[0]] = X_mu[j]
                    j += 1

                GT_grasps_training_tweaked = []
                for g in GT_grasps:
                    g = list(g)
                    g[8] += np.pi
                    GT_grasps_training_tweaked.append(g)

                X_mu, X_var = self.preprocessing_function(GT_grasps_training_tweaked, dict_rgb_scene,
                                            dict_depth_scene)
                j = 0
                for g in GT_grasps_training_tweaked:
                    self.dict_id_grasp_feature_pluspi[g[0]] = X_mu[j]
                    j += 1

                X_mu, X_var = self.preprocessing_function(GT_grasps_training_tweaked, dict_rgb_scene,
                                            dict_depth_scene,
                                            flipped=[1 for i in range(len(GT_grasps_training_tweaked))])
                j = 0
                for g in GT_grasps_training_tweaked:
                    self.dict_id_grasp_feature_pluspi_flipped[g[0]] = X_mu[j]
                    j += 1

        if self.scaler is not None:
            self.scaler.fit(X_mu_full, labels)

        self.model = True

        save_dir = where_to_save_model
        if save_dir is not None:
            if with_timestamp:
                timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())
            else:
                timestamp = ''
            dir_path = save_dir + "/" + timestamp + "/"

            if not os.path.exists(save_dir):
                os.mkdir(save_dir)

            if not os.path.exists(dir_path):
                os.mkdir(dir_path)

        elapsed_time = time.time() - start_time

        if save_dir is not None:
            elapsed_time_file = os.path.join(dir_path, "elapsed_time_file.pkl")
            save_obj(elapsed_time, elapsed_time_file)

            copyfile(config_path, dir_path + "config.ini")

            self.save_weights(dir_path)


    def compute_distance(self, current_grasp_candidates, dict_rgb_scene, dict_depth_raw, GT_grasps, distances_to_pos, distances_to_neg, smallest_distance_pos, highest_distance_pos,
             smallest_distance_neg, highest_distance_neg):

        if self.representation_positive is None or self.representation_negative is None:
            print('filling self.representation_positive and self.representation_negative')
            self.representation_positive = []
            self.representation_negative = []
            X_mu_GT = []
            labels = []
            for gt in GT_grasps:
                if gt[11] > 0:
                    self.representation_positive.append(self.dict_id_grasp_feature[gt[0]])
                    if self.b_with_augmentation:
                        self.representation_positive.append(self.dict_id_grasp_feature_flipped[gt[0]])
                        self.representation_positive.append(self.dict_id_grasp_feature_minuspi[gt[0]])
                        self.representation_positive.append(self.dict_id_grasp_feature_minuspi_flipped[gt[0]])
                        self.representation_positive.append(self.dict_id_grasp_feature_pluspi[gt[0]])
                        self.representation_positive.append(self.dict_id_grasp_feature_pluspi_flipped[gt[0]])

                else:
                    self.representation_negative.append(self.dict_id_grasp_feature[gt[0]])
                    if self.b_with_augmentation:
                        self.representation_negative.append(self.dict_id_grasp_feature_flipped[gt[0]])
                        self.representation_negative.append(self.dict_id_grasp_feature_minuspi[gt[0]])
                        self.representation_negative.append(self.dict_id_grasp_feature_minuspi_flipped[gt[0]])
                        self.representation_negative.append(self.dict_id_grasp_feature_pluspi[gt[0]])
                        self.representation_negative.append(self.dict_id_grasp_feature_pluspi_flipped[gt[0]])

                if self.scaler is not None and self.by_object:
                    X_mu_GT.append(self.dict_id_grasp_feature[gt[0]])
                    labels.append(gt[11])

            if self.scaler is not None and self.by_object:
                self.scaler.fit(X_mu_GT, labels)


        print('comparing distance to ',len(self.representation_positive),' positive examples')
        print('comparing distance to ',len(self.representation_negative),' negative examples')

        self.representation_positive = np.asarray(self.representation_positive)
        self.representation_negative = np.asarray(self.representation_negative)
        print('representation_positive.shape: ', self.representation_positive.shape)
        print('representation_negative.shape: ', self.representation_negative.shape)

        if self.scaler is not None:
            self.representation_positive = self.scaler.transform(self.representation_positive)
            self.representation_negative = self.scaler.transform(self.representation_negative)
            print('after scaler representation_positive.shape: ', self.representation_positive.shape)
            print('after scaler representation_negative.shape: ', self.representation_negative.shape)

        if self.mode == 'average':
            self.representation_positive = np.mean(self.representation_positive, axis=0).reshape((1,-1))
            self.representation_negative = np.mean(self.representation_negative, axis=0).reshape((1,-1))

            print('after average representation_positive.shape: ', self.representation_positive.shape)
            print('after average representation_negative.shape: ', self.representation_negative.shape)

        print('len(current_grasp_candidates): ', len(current_grasp_candidates))
        X_mu, X_var = self.preprocessing_function(current_grasp_candidates, dict_rgb_scene, dict_depth_raw)

        if self.scaler is not None:
            X_mu = self.scaler.transform(X_mu)

        for i in range(len(current_grasp_candidates)):
            current_representation = np.asarray([X_mu[i]])
            if self.feature_extraction_preprocessing_function != 'average_hash':
                shape = (current_representation.shape[1], current_representation.shape[1], current_representation.shape[-1])
                current_representation = np.reshape(current_representation, shape)
            # print('current_representation.shape: ', current_representation.shape)
            if len(self.representation_positive) > 0:
                min_distance = None
                for representation_positive in self.representation_positive:
                    if self.feature_extraction_preprocessing_function != 'average_hash':
                        representation_positive = np.reshape(representation_positive, shape)

                    # print('representation_positive.shape: ', current_representation.shape)

                    # distance_to_pos = np.min(
                    #     cdist(current_representation, self.representation_positive, metric=self.dist_metric))
                    if self.dist_metric == 'mean_squared_error':
                        curr_dist = metrics.mean_squared_error(current_representation, representation_positive)
                    elif self.dist_metric == 'structural_similarity':
                        curr_dist = 1.0-metrics.structural_similarity(current_representation, representation_positive, multichannel=True)
                    elif self.dist_metric == 'raw':
                        curr_dist = abs(current_representation - representation_positive)

                    if min_distance is None:
                        min_distance = curr_dist
                    if min_distance > curr_dist:
                        min_distance = curr_dist
                distance_to_pos = min_distance

            else:
                distance_to_pos = 0.0
            if len(self.representation_negative) > 0:
                # distance_to_neg = np.min(cdist(current_representation, self.representation_negative, metric=self.dist_metric))
                for representation_negative in self.representation_negative:
                    if self.feature_extraction_preprocessing_function != 'average_hash':
                        representation_negative = np.reshape(representation_negative, shape)

                    # distance_to_pos = np.min(
                    #     cdist(current_representation, self.representation_positive, metric=self.dist_metric))
                    if self.dist_metric == 'mean_squared_error':
                        curr_dist = metrics.mean_squared_error(current_representation, representation_negative)
                    elif self.dist_metric == 'structural_similarity':
                        curr_dist = 1.0-metrics.structural_similarity(current_representation, representation_negative, multichannel=True)
                    elif self.dist_metric == 'raw':
                        curr_dist = abs(current_representation - representation_negative)

                    if min_distance is None:
                        min_distance = curr_dist
                    if min_distance > curr_dist:
                        min_distance = curr_dist
                distance_to_neg = min_distance

            else:
                distance_to_neg = 0.0
            # d = np.linalg.norm(np.asarray(mu_GT[j]) - np.asarray(mu[i]))
            # print("np.asarray(mu_GT[j]).shape: ", np.asarray(mu_GT[j]).shape)
            # d = distance.cosine(np.asarray(mu[j]).reshape(-1, self.representation_class.projection_dim), np.asarray(best_mu[0]).reshape(-1, self.representation_class.projection_dim))#okay
            # d  = distance.cdist(np.asarray(mu[j]).reshape(-1, self.representation_class.projection_dim), np.asarray(best_mu[0]).reshape(-1, self.representation_class.projection_dim), metric='cityblock')#not okay
            # d = cosine_similarity(np.asarray(mu[j]).reshape(-1, self.representation_class.projection_dim), np.asarray(best_mu[0]).reshape(-1, self.representation_class.projection_dim))#okay
            # d  = distance.chebyshev(np.asarray(mu[j]).reshape(-1, self.representation_class.projection_dim), np.asarray(best_mu[0]).reshape(-1, self.representation_class.projection_dim))#better
            # d  = -distance.correlation(np.asarray(mu[j]).reshape(-1, self.representation_class.projection_dim), np.asarray(best_mu[0]).reshape(-1, self.representation_class.projection_dim))

            distances_to_pos.append(distance_to_pos)
            distances_to_neg.append(distance_to_neg)

            if smallest_distance_pos is None:
                smallest_distance_pos = distance_to_pos

            if smallest_distance_pos > distance_to_pos:
                smallest_distance_pos = distance_to_pos

            if highest_distance_pos is None:
                highest_distance_pos = distance_to_pos

            if highest_distance_pos < distance_to_pos:
                highest_distance_pos = distance_to_pos

            if smallest_distance_neg is None:
                smallest_distance_neg = distance_to_neg

            if smallest_distance_neg > distance_to_neg:
                smallest_distance_neg = distance_to_neg

            if highest_distance_neg is None:
                highest_distance_neg = distance_to_neg

            if highest_distance_neg < distance_to_neg:
                highest_distance_neg = distance_to_neg
        return X_mu, X_var, distances_to_pos, distances_to_neg, smallest_distance_pos, highest_distance_pos, smallest_distance_neg, highest_distance_neg

    def predict(self,grasps_candidates, dict_rgb_scene, dict_depth_raw):
        dim_id_obj = self.dim_id_obj
        smallest_distance_pos = None
        highest_distance_pos = None

        smallest_distance_neg = None
        highest_distance_neg = None

        distances_to_pos = []
        distances_to_neg = []
        scores = []
        X_mu_full = None
        X_var_full = None
        elapsed_time_full = 0.0
        start_time = time.time()

        if self.by_object:

            print('predict by_object')
            # grasp candidates are already sorted
            # id_obj is in 12

            index_last = 0
            while index_last != len(grasps_candidates) - 1:
                index_start = index_last
                for i in range(index_start, len(grasps_candidates) - 1):
                    if grasps_candidates[i][dim_id_obj] != grasps_candidates[i + 1][dim_id_obj]:
                        break
                    index_last = i + 1

                print("index_start: ", index_start)
                print("index_last: ", index_last)


                id_obj = grasps_candidates[index_start][dim_id_obj]
                print('id_obj: ', id_obj)
                #get grasp of obejcts
                if id_obj in self.dict_obj_grasp:
                    GT_grasps = self.dict_obj_grasp[id_obj]
                else:
                    GT_grasps = self.GT_grasps

                current_grasp_candidates = grasps_candidates[index_start:index_last]
                X_mu, X_var, distances_to_pos, distances_to_neg, smallest_distance_pos, highest_distance_pos, smallest_distance_neg, highest_distance_neg = self.compute_distance(current_grasp_candidates, dict_rgb_scene, dict_depth_raw, GT_grasps, distances_to_pos, distances_to_neg, smallest_distance_pos, highest_distance_pos,
                 smallest_distance_neg, highest_distance_neg)

                if X_mu_full is None:
                    X_mu_full = X_mu
                else:
                    X_mu_full += X_mu

                if X_var_full is None:
                    X_var_full = X_var
                else:
                    X_var_full += X_var

        else:
            GT_grasps = self.GT_grasps
            X_mu, X_var, distances_to_pos, distances_to_neg, smallest_distance_pos, highest_distance_pos, smallest_distance_neg, highest_distance_neg = self.compute_distance(grasps_candidates, dict_rgb_scene, dict_depth_raw, GT_grasps, distances_to_pos, distances_to_neg, smallest_distance_pos,
                                  highest_distance_pos,
                                  smallest_distance_neg, highest_distance_neg)
            X_mu_full = X_mu
            X_var_full = X_var

        if self.by_object:
            self.representation_positive = None
            self.representation_negative = None

        print('smallest_distance_pos: ', smallest_distance_pos)
        print('highest_distance_pos: ', highest_distance_pos)

        print('smallest_distance_neg: ', smallest_distance_neg)
        print('highest_distance_neg: ', highest_distance_neg)
        if highest_distance_pos > 0.0:
            distances_to_pos = distances_to_pos/highest_distance_pos
        else:
            distances_to_pos = distances_to_pos
        # print('distances_to_pos: ', distances_to_pos)
        if highest_distance_neg > 0.0:
            distances_to_neg = distances_to_neg/highest_distance_neg
        # if smallest_distance_neg > 0.0:
        #     distances_to_neg = distances_to_neg/smallest_distance_neg
        else:
            distances_to_neg =distances_to_neg
        # print('distances_to_neg: ', distances_to_neg)
        #
        # #distance to pos [0.0,1.0]
        # scores = (1.0 - distances_to_pos)
        scores = (1.0 - distances_to_pos) - (1.0 - distances_to_neg)

        # scores = (1.0 - distances_to_pos) + (1-distances_to_neg)

        # scores = -(1.0 - distances_to_neg)

        # scores = (1.0 - distances_to_pos)
        # scores = np.asarray(distances_to_pos) - np.asarray(distances_to_neg)

        elapsed_time = time.time() - start_time

        return scores, X_mu_full, X_var_full, elapsed_time


