from .cnn_classifier import CNNClassifier
from .gpytorch_classification import GPyTorchClassification
from .sklearn_logistic_regression_classification import LogisticRegressionClassification
from .sklearn_MLP_classification import MLPClassification
from .sklearn_gp_classification import SklearnGPClassifier
from .sklearn_gp_regression import GPRegression
from .sklearn_linear_regression import LinearRegressor
from .sklearn_random_forrest_classification import RandomForestClassification
from .sklearn_KNC_classification import KNeighborsClassification
from .fine_tuning import FineTuningClassifier
from .gt_classifier import GroundTruthClassifier
from .sklearn_passive_aggressive_classifier import PassiveAggressiveClassification
from .gpy_classifier import GPyClassifier
from .zero_shot_classifier import ZeroShotClassifier
from .zero_shot_patch_classifier import ZeroShotPatchClassifier
from .sklearn_svm_classification import SVMClassification
from .sklearn_svr_regression import SVRegressor

def select_object_classifier_class(classifier_type):
    print("object_classifier ", classifier_type )
    if classifier_type == 'CNNClassifier':
        return CNNClassifier
    # elif classifier_type == 'GT':
    elif classifier_type == "ft":
        return FineTuningClassifier

    else:
        return GroundTruthClassifier

def select_classifier_class(classifier_type):
    print("classifier_class ", classifier_type)

    if classifier_type == 'CNNClassifier':
        return CNNClassifier
    elif classifier_type == "GPy":
        return GPyClassifier
    elif classifier_type == "gpytorch":
        return GPyTorchClassification
    elif classifier_type == "logistic":
        return LogisticRegressionClassification
    elif classifier_type == "MLP":
        return MLPClassification
    elif classifier_type == "skgp":
        return SklearnGPClassifier
    elif classifier_type == "rf":
        return RandomForestClassification
    elif classifier_type == "svm":
        return SVMClassification
    elif classifier_type == "knc":
        return KNeighborsClassification
    elif classifier_type == "ft":
        return FineTuningClassifier
    elif classifier_type == "zs":
        return ZeroShotClassifier
    elif classifier_type == "zspatch":
        return ZeroShotPatchClassifier

    elif classifier_type == "pa":
        return PassiveAggressiveClassification

    else:
        print("classifier type not implemented")
        return None

def select_length_regressor(regressor_type):
    print("length_regressor ", regressor_type)
    if regressor_type == 'skgp':
        return GPRegression
    elif regressor_type == 'linear':
        return LinearRegressor
    elif regressor_type == 'svr':
        return SVRegressor
    else:
        print("regressor type not implemented")
        return None