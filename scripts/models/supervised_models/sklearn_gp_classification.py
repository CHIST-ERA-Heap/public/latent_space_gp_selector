from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import Matern, RBF
from .sklearn_classifier_template import SklearnClassifier
import numpy as np

class SklearnGPClassifier(SklearnClassifier):
    def __init__(self, config, preprocessing_function):
        super().__init__(config, preprocessing_function)

    def setup_model(self):
        # self.model = GaussianProcessClassifier(kernel= 1**2 * Matern(length_scale=1, nu=1.5), random_state=0, n_restarts_optimizer=1)
        # self.model = GaussianProcessClassifier(kernel=  1.0 * RBF(1.0), random_state=0, n_restarts_optimizer=1)
        # self.model = GaussianProcessClassifier(n_restarts_optimizer=1)
        """Gaussian process classification (GPC) based on Laplace approximation.

        The implementation is based on Algorithm 3.1, 3.2, and 5.1 of
        Gaussian Processes for Machine Learning (GPML) by Rasmussen and
        Williams.

        Internally, the Laplace approximation is used for approximating the
        non-Gaussian posterior by a Gaussian.

        Currently, the implementation is restricted to using the logistic link
        function. For multi-class classification, several binary one-versus rest
        classifiers are fitted. Note that this class thus does not implement
        a true multi-class Laplace approximation.

        Read more in the :ref:`User Guide <gaussian_process>`.

        Parameters
        ----------
        kernel : kernel instance, default=None
            The kernel specifying the covariance function of the GP. If None is
            passed, the kernel "1.0 * RBF(1.0)" is used as default. Note that
            the kernel's hyperparameters are optimized during fitting.

        optimizer : 'fmin_l_bfgs_b' or callable, default='fmin_l_bfgs_b'
            Can either be one of the internally supported optimizers for optimizing
            the kernel's parameters, specified by a string, or an externally
            defined optimizer passed as a callable. If a callable is passed, it
            must have the  signature::

                def optimizer(obj_func, initial_theta, bounds):
                    # * 'obj_func' is the objective function to be maximized, which
                    #   takes the hyperparameters theta as parameter and an
                    #   optional flag eval_gradient, which determines if the
                    #   gradient is returned additionally to the function value
                    # * 'initial_theta': the initial value for theta, which can be
                    #   used by local optimizers
                    # * 'bounds': the bounds on the values of theta
                    ....
                    # Returned are the best found hyperparameters theta and
                    # the corresponding value of the target function.
                    return theta_opt, func_min

            Per default, the 'L-BFGS-B' algorithm from scipy.optimize.minimize
            is used. If None is passed, the kernel's parameters are kept fixed.
            Available internal optimizers are::

                'fmin_l_bfgs_b'

        n_restarts_optimizer : int, default=0
            The number of restarts of the optimizer for finding the kernel's
            parameters which maximize the log-marginal likelihood. The first run
            of the optimizer is performed from the kernel's initial parameters,
            the remaining ones (if any) from thetas sampled log-uniform randomly
            from the space of allowed theta-values. If greater than 0, all bounds
            must be finite. Note that n_restarts_optimizer=0 implies that one
            run is performed.

        max_iter_predict : int, default=100
            The maximum number of iterations in Newton's method for approximating
            the posterior during predict. Smaller values will reduce computation
            time at the cost of worse results.

        warm_start : bool, default=False
            If warm-starts are enabled, the solution of the last Newton iteration
            on the Laplace approximation of the posterior mode is used as
            initialization for the next call of _posterior_mode(). This can speed
            up convergence when _posterior_mode is called several times on similar
            problems as in hyperparameter optimization. See :term:`the Glossary
            <warm_start>`.

        copy_X_train : bool, default=True
            If True, a persistent copy of the training data is stored in the
            object. Otherwise, just a reference to the training data is stored,
            which might cause predictions to change if the data is modified
            externally.

        random_state : int, RandomState instance or None, default=None
            Determines random number generation used to initialize the centers.
            Pass an int for reproducible results across multiple function calls.
            See :term: `Glossary <random_state>`.

        multi_class : {'one_vs_rest', 'one_vs_one'}, default='one_vs_rest'
            Specifies how multi-class classification problems are handled.
            Supported are 'one_vs_rest' and 'one_vs_one'. In 'one_vs_rest',
            one binary Gaussian process classifier is fitted for each class, which
            is trained to separate this class from the rest. In 'one_vs_one', one
            binary Gaussian process classifier is fitted for each pair of classes,
            which is trained to separate these two classes. The predictions of
            these binary predictors are combined into multi-class predictions.
            Note that 'one_vs_one' does not support predicting probability
            estimates.

        n_jobs : int, default=None
            The number of jobs to use for the computation: the specified
            multiclass problems are computed in parallel.
            ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
            ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
            for more details.

        Attributes
        ----------
        base_estimator_ : ``Estimator`` instance
            The estimator instance that defines the likelihood function
            using the observed data.

        kernel_ : kernel instance
            The kernel used for prediction. In case of binary classification,
            the structure of the kernel is the same as the one passed as parameter
            but with optimized hyperparameters. In case of multi-class
            classification, a CompoundKernel is returned which consists of the
            different kernels used in the one-versus-rest classifiers.

        log_marginal_likelihood_value_ : float
            The log-marginal-likelihood of ``self.kernel_.theta``

        classes_ : array-like of shape (n_classes,)
            Unique class labels.

        n_classes_ : int
            The number of classes in the training data

        Examples
        --------
        # >>> from sklearn.datasets import load_iris
        # >>> from sklearn.gaussian_process import GaussianProcessClassifier
        # >>> from sklearn.gaussian_process.kernels import RBF
        # >>> X, y = load_iris(return_X_y=True)
        # >>> kernel = 1.0 * RBF(1.0)
        # >>> gpc = GaussianProcessClassifier(kernel=kernel,
        # ...         random_state=0).fit(X, y)
        # >>> gpc.score(X, y)
        # 0.9866...
        # >>> gpc.predict_proba(X[:2,:])
        array([[0.83548752, 0.03228706, 0.13222543],
               [0.79064206, 0.06525643, 0.14410151]])

        .. versionadded:: 0.18
        """

        self.model = GaussianProcessClassifier(warm_start = False, kernel= Matern(), random_state=0, n_restarts_optimizer=1,
                                               max_iter_predict=10000)
        # kernel = RBF()
        # a = np.random.randn(32)[:, np.newaxis]
        # print('a: ', a)
        # b = a
        # kernel(a, b)
        # self.model = GaussianProcessClassifier(warm_start = False, kernel=kernel, random_state=0, n_restarts_optimizer=1,
        #                                        max_iter_predict=10000)
