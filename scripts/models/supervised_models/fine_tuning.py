import gc
import time
import math

import tensorflow as tf
from tensorflow.keras.layers import  BatchNormalization, Dense, Flatten

from tensorflow.keras import backend as K

from tensorflow.keras.applications import EfficientNetB0, EfficientNetB1, EfficientNetB7
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input
from scripts.models.common.model import set_model_as_untrainable, get_vae_model_yolo2, get_vae_model_vgg16, get_vae_model_vgg19, get_vae_model_default, get_vgg16_base, get_vgg19_base
from scripts.models.common.data_generator import My_Custom_Generator, My_Custom_Generator_bvae
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, ReduceLROnPlateau
from scripts.models.common.utils import LRTensorBoard, select_optimizer, \
    get_name_network
from scripts.common.process_patch_util import extract_single_feature
import numpy as np
import os
from shutil import copyfile
from scripts.common.pickle_util import save_obj, load_obj
from tensorflow.keras.models import Sequential
from sklearn.model_selection import train_test_split
import pickle

tf.get_logger().setLevel('ERROR')

def get_key(val,my_dict):
    for key, value in my_dict.items():
         if val == value:
             return key
    return 1

class FineTuningClassifier:
    def __init__(self, config, preprocessing_function, base = None, is_object_classifier=False ):
        self.config = config
        self.base = base
        self.is_object_classifier = is_object_classifier
        self.preprocessing_classifier = preprocessing_function
        self.preprocessing_type_classifier = self.config.get('feature_extraction', 'preprocessing')

        # args = parse_arguments(sys.argv[1:])[0]

        self.projection_dim = self.config.getint('feature_extraction', 'projection_dim')
        # self.max_input_size = self.config.getint('feature_extraction', 'max_input_size')

        self.dim_id_obj = self.config.getint("grasp_object_classifier", "dim_id_obj")

        if self.is_object_classifier:
            self.nb_of_class = self.config.getint('grasp_object_classifier', 'nb_of_class')

            self.classifier_type = self.config.get("grasp_object_classifier", "type")
            self.train_with_classifier_data = self.config.getboolean('grasp_object_classifier', 'train_with_classifier_data')
            self.train_batch_size = self.config.getint('grasp_object_classifier', 'max_batch_size')
            self.validation_batch_size = self.config.getint('grasp_object_classifier', 'max_batch_size')
            self.allow_flip = self.config.getboolean('grasp_object_classifier', 'use_augmentation')
            self.base_learning_rate = self.config.getfloat('grasp_object_classifier', 'lr')
            self.min_lr = self.config.getfloat('grasp_object_classifier','min_lr')
            self.max_epoch_without_improvement = self.config.getint('grasp_object_classifier', 'max_epoch_without_improvement')
            self.scheduler_patience = self.config.getint('grasp_object_classifier', 'scheduler_patience')
            self.max_epochs = self.config.getint('grasp_object_classifier', 'max_number_of_epochs')
            self.trained_model = self.config.get('grasp_object_classifier', 'trained_model')
            self.by_object = None
            self.label_augmentation = self.config.getboolean('grasp_object_classifier', 'use_label_augmentation')
            self.preprocessing_type = self.config.get('grasp_object_classifier', 'preprocessing')
            self.type = self.config.get("grasp_object_classifier", "type")

            # self.random_sizes = [self.dim_grasp_patch]
            random_sizes_str = self.config.get('grasp_object_classifier', 'random_sizes')
            random_sizes = random_sizes_str.split('\n')
            self.random_sizes = [int(s) for s in random_sizes]

            self.b_force_cpu = not self.config.getboolean('feature_extraction', 'use_gpu')

            self.decay_factor = self.config.getfloat('grasp_object_classifier', 'decay_factor')
            self.from_where = self.config.get('grasp_object_classifier', 'from_where')
            self.optimizer = self.config.get('grasp_object_classifier', 'optimizer')
            self.pretrained_model = self.config.get('grasp_object_classifier', 'pretrained_model')
            self.fine_tuning = self.config.getboolean('grasp_object_classifier', 'fine_tuning')

            self.model_type = self.config.get('grasp_object_classifier', 'model_type')
            self.with_avg_pool = self.config.get('grasp_object_classifier', 'with_avg_pool')

            self.use_SN = self.config.getboolean('grasp_object_classifier', 'use_SN')
            self.use_gray_depth = self.config.getboolean('grasp_object_classifier', 'use_gray_depth')
            self.use_rgb = self.config.getboolean('grasp_object_classifier', 'use_rgb')

            self.max_depth_value = self.config.getfloat('grasp_object_classifier', 'max_depth_value')
            self.channels_number = self.config.getint('grasp_object_classifier', 'channels_number')
            self.dim_resized_patch = self.config.getint('grasp_object_classifier', 'dim_resized_patch')
            self.dim_grasp_patch = self.config.getint('grasp_object_classifier', 'dim_grasp_patch')

        else:
            self.use_SN = self.config.getboolean('data_modalities', 'use_SN')
            self.use_gray_depth = self.config.getboolean('data_modalities', 'use_gray_depth')
            self.use_rgb = self.config.getboolean('data_modalities', 'use_rgb')

            self.max_depth_value = self.config.getfloat('data_modalities', 'max_depth_value')
            self.channels_number = self.config.getint('data_modalities', 'channels_number')
            self.dim_resized_patch = self.config.getint('data_modalities', 'dim_resized_patch')
            self.dim_grasp_patch = self.config.getint('data_modalities', 'dim_grasp_patch')

            self.nb_of_class = self.config.getint('classifier', 'nb_of_class')

            self.train_batch_size = self.config.getint('classifier', 'max_batch_size')
            self.validation_batch_size = self.config.getint('classifier', 'max_batch_size')
            self.allow_flip = self.config.getboolean('classifier', 'use_augmentation')
            self.base_learning_rate = self.config.getfloat('classifier', 'lr')
            self.min_lr = self.config.getfloat('classifier','min_lr')
            self.random_sizes = [self.dim_grasp_patch]
            self.max_epoch_without_improvement = self.config.getint('classifier', 'max_epoch_without_improvement')
            self.scheduler_patience = self.config.getint('classifier', 'scheduler_patience')
            self.max_epochs = self.config.getint('classifier', 'max_number_of_epochs')
            self.trained_model = self.config.get('classifier', 'trained_model')
            self.by_object = self.config.getboolean('classifier','by_object')

            self.coef_generator_score = self.config.getfloat('classifier', 'coef_generator_score')
            self.coef_classification_score = self.config.getfloat('classifier', 'coef_classification_score')
            self.norm = self.config.getboolean('classifier', 'norm')
            self.label_augmentation = self.config.getboolean('classifier', 'use_label_augmentation')
            self.type = self.config.get("classifier", "type")
            # self.preprocessing_type_classifier_obj = self.config.get('classifier', 'preprocessing')
            # self.preprocessing_type_classifier = self.config.get('classifier', 'preprocessing')

            self.b_force_cpu = not self.config.getboolean('feature_extraction', 'use_gpu')

            self.decay_factor = self.config.getfloat('classifier', 'decay_factor')
            self.from_where = self.config.get('classifier', 'from_where')
            self.optimizer = self.config.get('classifier', 'optimizer')
            self.pretrained_model = self.config.get('classifier', 'pretrained_model')
            self.fine_tuning = self.config.getboolean('classifier', 'fine_tuning')

            self.model_type = self.config.get('classifier', 'model_type')
            self.with_avg_pool = self.config.get('classifier', 'with_avg_pool')
        self.dict_sqlid_categorical_id = {}
        self.dict_id_scene_id_obj = {}

        self.new = 0

        if self.trained_model == "None":
            self.trained_model = None

        if self.pretrained_model == "None":
            self.pretrained_model = None

        self.model = None
        self.base = None


    # todo copy from bvae
    def get_vae_model(self, weights=None, verbose=False, old=False):
        image_shape = (self.dim_resized_patch, self.dim_resized_patch, self.channels_number)

        if self.model_type == 'yolo2':
            return get_vae_model_yolo2(image_shape, self.projection_dim, n=5, preprocessing_function_type=self.preprocessing_type_classifier, old=old)
        if self.model_type == 'vgg16':
            if weights is None:
                b_untrainable = False
            else:
                b_untrainable = True
            return get_vae_model_vgg16(image_shape, self.projection_dim, weights=weights, b_untrainable=b_untrainable,
                                       preprocessing_function_type=self.preprocessing_type_classifier, old=old)
        if self.model_type == 'vgg19':
            if weights is None:
                b_untrainable = False
            else:
                b_untrainable = True
            return get_vae_model_vgg19(image_shape, self.projection_dim, weights=weights, b_untrainable=b_untrainable,
                                       preprocessing_function_type=self.preprocessing_type_classifier, old=old)

        if self.model_type == 'default':
            return get_vae_model_default(image_shape, self.projection_dim, preprocessing_function_type=self.preprocessing_type_classifier, old=old)


    def get_base_from_bvae(self):
        model, _, _ = self.get_vae_model()
        try:
            model.load_weights(self.pretrained_model)
        except Exception as e:
            model, _, _ = self.get_vae_model(old=True)
            model.load_weights(self.pretrained_model)

        model = model.get_layer('encoder')
        model.summary()
        self.base = Sequential()
        to_remove = ['t_mean', 't_log_var', 'conv2d', 'conv2d_1', 'conv2d_2', 'conv2d_3']
        for layer in model.layers:  # go through until last layer
            if layer.name not in to_remove:
                print("adding layer.name: ", layer.name)
                self.base.add(layer)

        print('self.base.summary')
        self.base.summary()
        # print('get_base_from_bvae self.base: ', self.base)


    def get_base_from_mae(self):
        self.base = tf.keras.models.load_model(self.trained_model + 'feature_extraction_model')

    def get_model_with_base(self):
        # print('get_model_with_base self.base: ', self.base)
        for layer in self.base.layers:
            layer.trainable = False

        # m = set_model_as_untrainable(m)
        # return get_feature_extraction_model_with_base(m)

        if self.nb_of_class == 2:
            activation = "sigmoid"
            units = 1
        else:
            activation = "softmax"
            units = self.nb_of_class

        top_dropout_rate = 0.2
        if self.with_avg_pool:
            classifier_model = Sequential(
                [
                    self.base,
                    Flatten(),
                    Dense(1024, activation='relu'),
                    # GlobalAveragePooling2D(name="avg_pool"),
                    # BatchNormalization(),
                    # BatchNormalization(),
                    # GlobalAveragePooling1D(),
                    # Dropout(top_dropout_rate, name="top_dropout"),
                    Dense(units, activation=activation, name="pred")
                ],
                name="classifier_model",
            )
        else:
            classifier_model = Sequential(
                [
                    self.base,
                    Flatten(),
                    Dense(1024, activation='relu'),
                    Dense(units, activation=activation, name="pred")
                ],
                name="classifier_model",
            )

        # classifier_model.summary()

        return classifier_model
    # def get_model_with_base(self, base, with_avg_pool=True):
    #     base.trainable = False
    #     from tensorflow.keras.models import Sequential
    #     top_dropout_rate = 0.2
    #     if with_avg_pool:
    #         classifier_model = Sequential(
    #             [
    #                 base,
    #                 GlobalAveragePooling2D(name="avg_pool"),
    #                 BatchNormalization(),
    #                 # BatchNormalizationget_base_from_bvae(),
    #                 # GlobalAveragePooling1D(),
    #                 Dropout(top_dropout_rate, name="top_dropout"),
    #                 Dense(1, activation="sigmoid", name="pred")
    #             ],
    #             name="classifier_model",
    #         )
    #     else:
    #         classifier_model = Sequential(
    #             [
    #                 base,
    #                 Dropout(top_dropout_rate, name="top_dropout"),
    #                 Dense(1, activation="sigmoid", name="pred")
    #             ],
    #             name="classifier_model",
    #         )
    #
    #     return classifier_model

    def get_pretrained_model(self, image_shape):
        input_pretrained = Input(shape=image_shape, name='encoder_input')

        b_untrainable = True

        if self.model_type == 'vgg16':
            return get_vgg16_base(input_pretrained, self.pretrained_model, image_shape, b_untrainable)

        if self.model_type == 'vgg19':
            return get_vgg19_base(input_pretrained, self.pretrained_model, image_shape, b_untrainable)

        if self.model_type == 'EfficientNetB0':
            m =  EfficientNetB0(input_tensor=input_pretrained, weights=self.pretrained_model, include_top=False)

            return set_model_as_untrainable(m)

        if self.model_type == 'EfficientNetB1':
            m = EfficientNetB1(input_tensor=input_pretrained, weights=self.pretrained_model, include_top=False)
            return set_model_as_untrainable(m)

        if self.model_type == 'EfficientNetB7':
            m = EfficientNetB7(input_tensor=input_pretrained, weights=self.trained_model, include_top=False)
            return set_model_as_untrainable(m)


    def get_ft_model(self, pretrained_model, verbose=False):
        self.base = None
        image_shape = (self.dim_resized_patch, self.dim_resized_patch, self.channels_number)

        if self.from_where == 'h5':
            self.model = tf.keras.models.load_model(pretrained_model)
        else:
            if self.base is None:
                if self.from_where == 'bvae':
                    self.get_base_from_bvae()

                elif self.from_where == 'mae':
                    self.get_base_from_mae()

                elif self.from_where == 'keras':
                    self.base = self.get_pretrained_model(image_shape)

                # print('get_ft_model self.base: ', self.base)
                model = self.get_model_with_base()

        return model

    def load_model(self, path_to_weights):
        print('loading fine_tuning classifier: ', path_to_weights,' exists ', os.path.isfile(path_to_weights))

        if self.is_object_classifier:
            path_to_weights_splitted = path_to_weights.split('/')
            root_path ='/'
            for s in path_to_weights_splitted[:-1]:
                root_path+=(s+'/')

            self.dict_id_scene_id_obj = load_obj(root_path + 'dict_id_scene_id_obj.pkl')
            self.dict_sqlid_categorical_id = load_obj(root_path + 'dict_sqlid_categorical_id.pkl')

            keys = [k for k in self.dict_sqlid_categorical_id.keys()]
            keys.sort()
            start = 0
            for k in keys:
                self.dict_sqlid_categorical_id[k] = start
                start += 1

            # todo check

            self.nb_of_class = start

        self.setup_model()

        print("load ft network ", self.model_type)
        # Failed to memcopy into scratch buffer for device
        # tf.config.experimental.list_physical_devices(device_type=None)
        if self.b_force_cpu:
            device = "cpu:0"
        else:
            device = "gpu:0"

        with tf.device(device):

            # config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
            # config.gpu_options.per_process_gpu_memory_fraction = 0.3
            # config.gpu_options.allow_growth = True
            # sess = tf.compat.v1.Session(config=config)
            # tf.compat.v1.keras.backend.set_session(tf.Session(graph=tf.compat.v1.get_default_graph(), config=config))

            gpus = tf.config.list_physical_devices('GPU')
            if self.b_force_cpu:
                # Restrict TensorFlow to only use the first GPU
                try:
                    tf.config.set_visible_devices(gpus[0], 'GPU')
                    logical_gpus = tf.config.list_logical_devices('GPU')
                    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
                except RuntimeError as e:
                    # Visible devices must be set before GPUs have been initialized
                    print(e)

            # self.model.summary()

            if self.trained_model is not None:
                # Load VAE that was jointly trained with a
                # predictor returned from create_predictor()
                print('load_weights: ', self.trained_model, ' exists ',
                      os.path.isfile(self.trained_model + ".index"))

                self.model.load_weights(self.trained_model)

            # K.clear_session()
            gc.collect()

    def setup_model(self):
        print("load FT network ", self.model_type)
        # Failed to memcopy into scratch buffer for device
        # tf.config.experimental.list_physical_devices(device_type=None)
        if self.b_force_cpu:
            device = "cpu:0"
        else:
            device = "gpu:0"

        with tf.device(device):

            # config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
            # config.gpu_options.per_process_gpu_memory_fraction = 0.3
            # config.gpu_options.allow_growth = True
            # sess = tf.compat.v1.Session(config=config)
            # tf.compat.v1.keras.backend.set_session(tf.Session(graph=tf.compat.v1.get_default_graph(), config=config))

            gpus = tf.config.list_physical_devices('GPU')
            if self.b_force_cpu:
                # Restrict TensorFlow to only use the first GPU
                try:
                    tf.config.set_visible_devices(gpus[0], 'GPU')
                    logical_gpus = tf.config.list_logical_devices('GPU')
                    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
                except RuntimeError as e:
                    # Visible devices must be set before GPUs have been initialized
                    print(e)


            self.model = self.get_ft_model(self.pretrained_model)
            # np_config.enable_numpy_behavior()
            print('self.model.summary')

            # self.model.summary()
            # K.clear_session()
            gc.collect()


    def del_network(self):
        print("del FT network")

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        gc.collect

        del self.model
        del self.base

        tf.reset_default_graph()
        # K._SESSION.close()
        # K._SESSION = None

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        # cuda.select_device(0)
        # cuda.close()
        gc.collect()

        self.model = None
        self.base = None

    def save_model(self, where_to_save_model):
        path_dict_id_scene_id_obj = where_to_save_model + 'dict_id_scene_id_obj.pkl'

        with open(path_dict_id_scene_id_obj, 'wb') as f:  # Python 3: open(..., 'wb')
            pickle.dump(self.dict_id_scene_id_obj, f)

        path_dict_sqlid_categorical_id = where_to_save_model + 'dict_sqlid_categorical_id.pkl'
        with open(path_dict_sqlid_categorical_id, 'wb') as f:  # Python 3: open(..., 'wb')
            pickle.dump(self.dict_sqlid_categorical_id, f)


    def update_grasps_candidates_with_obj(self,grasps_candidates, dict_rgb_scene, dict_depth_raw, reverse=True, verbose=False):
        dim_id_obj = self.dim_id_obj
        start_time = time.time()
        if verbose:
            print('self.dict_sqlid_categorical_id: ', self.dict_sqlid_categorical_id)

        scores, _, _, elapsed_time = self.predict(grasps_candidates, dict_rgb_scene, dict_depth_raw)
        print('scores.shape: ', scores.shape)

        for i in range(len(grasps_candidates)):
            id_categorical = np.argmax(scores[i])

            if verbose:
                print('before grasps_candidates[i]: ', grasps_candidates[i])

                print('scores[i]: ', scores[i])
                # yhat = scores[i].round()
                # print('yhat: ', yhat)
                print('np.argmax(scores[i]: ', id_categorical)

            # id_scene = grasps_candidates[i][1]
            id_obj = get_key(id_categorical, self.dict_sqlid_categorical_id)


            #grasps_candidates[i][1] should be predicted
            grasps_candidates[i].append(id_obj)
            if verbose:
                # print('id_scene: ', id_scene)
                print('id_obj: ', id_obj)
                # print('id_categorical: ', id_categorical)
                print('after grasps_candidates[i]: ', grasps_candidates[i])

        #sort
        grasps_candidates.sort(key=lambda x: x[dim_id_obj], reverse=reverse)
        elapsed_time = time.time() - start_time

        return grasps_candidates, None, None, elapsed_time

    def predict(self,grasps_candidates, dict_rgb_scene, dict_depth_raw):
        X = []
        for g in grasps_candidates:
            # X.append(extract_single_feature(dict_rgb_scene[g[1]], dict_depth_raw[g[1]], g[3], g[4],
            #                        math.degrees(g[8]), self.use_rgb, self.use_gray_depth, self.use_SN,
            #                        self.use_SN_filter, self.dim_resized_patch, self.dim_resized_patch, flipped=0))
            # X.append(extract_single_feature(dict_rgb_scene[g[1]], dict_depth_raw[g[1]], g, self.use_rgb, self.use_gray_depth, self.use_SN,
            #                        self.use_SN_filter, self.dim_resized_patch, self.dim_resized_patch, flipped=0))
            X.append(extract_single_feature(rgb_scene=dict_rgb_scene[g[1]], depth_raw=dict_depth_raw[g[1]], grasp_position_x=g[3], grasp_position_y=g[4], angle_degrees=math.degrees(g[8]),
                                            use_rgb=self.use_rgb, use_gray_depth=self.use_gray_depth, use_SN=self.use_SN,
                                            dim_grasp_patch=self.dim_resized_patch, dim_resized_patch=self.dim_resized_patch, flipped=0))

        X = np.array(X)
        X  = self.preprocessing_classifier(X)


        start_time = time.time()
        proba = self.model.predict(X)
        elapsed_time = time.time() - start_time

        if self.nb_of_class == 2:
            r = list([float(proba[i][0]) for i in range(len(proba))])
        else:
            r = proba
        # print("r: ", r)
        # for i in range(len(proba)):
        #     print("Predicted: ", proba[i])
        return r, None, None, elapsed_time

    def fill_dict_sqlid_categorical_id(self, X_train_grasps, X_val_grasps, dict_id_scene_id_obj):
        current_label_dim = self.dim_id_obj
        self.dict_sqlid_categorical_id = {}

        for i in range(len(X_train_grasps)):
            # print("before X_train_grasps[i]: ", X_train_grasps[i])

            X_train_grasps[i] = list(X_train_grasps[i])
            X_train_grasps[i].append(dict_id_scene_id_obj[X_train_grasps[i][1]])
            # print("after X_train_grasps[i]: ", X_train_grasps[i])
            if X_train_grasps[i][current_label_dim] not in self.dict_sqlid_categorical_id:
                self.dict_sqlid_categorical_id[X_train_grasps[i][current_label_dim]] = X_train_grasps[i][
                    current_label_dim]

        if X_val_grasps is not None:
            for i in range(len(X_val_grasps)):
                X_val_grasps[i] = list(X_val_grasps[i])
                X_val_grasps[i].append(dict_id_scene_id_obj[X_val_grasps[i][1]])
                if X_val_grasps[i][current_label_dim] not in self.dict_sqlid_categorical_id:
                    self.dict_sqlid_categorical_id[X_val_grasps[i][current_label_dim]] = X_val_grasps[i][current_label_dim]

        # print("before self.dict_sqlid_categorical_id: ", self.dict_sqlid_categorical_id)
        keys = [k for k in self.dict_sqlid_categorical_id.keys()]
        keys.sort()
        start = 0
        for k in keys:
            self.dict_sqlid_categorical_id[k] = start
            start += 1

        # todo check
        if self.is_object_classifier:
            self.nb_of_class = start

        return X_train_grasps, X_val_grasps
        # print("after self.dict_sqlid_categorical_id: ", self.dict_sqlid_categorical_id)

    def fit(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation, where_to_save_model, config_path, verbose=False, dict_encoded_mu=None, dict_encoded_var=None, with_timestamp=True):
        if self.is_object_classifier:
            self.nb_of_class = self.config.getint('grasp_object_classifier', 'nb_of_class')
        else:
            self.nb_of_class = self.config.getint('classifier', 'nb_of_class')



        if GT_grasps_validation is None:
            X_train_grasps, X_val_grasps = train_test_split(GT_grasps_training, test_size=0.3,
                                                            random_state=1)
        else:
            X_train_grasps = GT_grasps_training
            X_val_grasps = GT_grasps_validation
        if not self.is_object_classifier:
            current_label_dim = 11
        else:
            #todo
            current_label_dim = self.dim_id_obj
            X_train_grasps, X_val_grasps = self.fill_dict_sqlid_categorical_id( X_train_grasps, X_val_grasps, dict_id_scene_id_obj)
            # for i in range(len(X_train_grasps)):
            #     # print("before X_train_grasps[i]: ", X_train_grasps[i])
            #
            #     X_train_grasps[i] = list(X_train_grasps[i])
            #     X_train_grasps[i].append(dict_id_scene_id_obj[X_train_grasps[i][1]])
            #     # print("after X_train_grasps[i]: ", X_train_grasps[i])
            #     if X_train_grasps[i][current_label_dim] not in self.dict_sqlid_categorical_id:
            #         self.dict_sqlid_categorical_id[X_train_grasps[i][current_label_dim]] = X_train_grasps[i][current_label_dim]
            #
            # for i in range(len(X_val_grasps)):
            #     X_val_grasps[i] = list(X_val_grasps[i])
            #     X_val_grasps[i].append(dict_id_scene_id_obj[X_val_grasps[i][1]])
            #     if X_val_grasps[i][current_label_dim] not in self.dict_sqlid_categorical_id:
            #         self.dict_sqlid_categorical_id[X_val_grasps[i][current_label_dim]] = X_val_grasps[i][current_label_dim]
            #
            # # print("before self.dict_sqlid_categorical_id: ", self.dict_sqlid_categorical_id)
            # keys = [k for k in self.dict_sqlid_categorical_id.keys()]
            # keys.sort()
            # start = 0
            # for k in keys:
            #     self.dict_sqlid_categorical_id[k]=start
            #     start+=1
            # print("after self.dict_sqlid_categorical_id: ", self.dict_sqlid_categorical_id)

            for i in range(len(X_train_grasps)):
                X_train_grasps[i][current_label_dim] = self.dict_sqlid_categorical_id[X_train_grasps[i][current_label_dim]]

            for i in range(len(X_val_grasps)):
                X_val_grasps[i][current_label_dim] = self.dict_sqlid_categorical_id[X_val_grasps[i][current_label_dim]]



        if self.model is not None:
            del self.model
            K.clear_session()
            # ops.reset_default_graph()
            gc.collect()

        self.setup_model()
        #model, X_train_grasps, X_val_grasps, dict_rgb_scene, dict_depth_scene, config, preprocessing_fct



        network_type = 'fine_tuned_classifer'
        network_name = get_name_network(network_type, self.use_rgb, self.use_gray_depth, self.use_SN, None,
                                        self.channels_number,
                                        None, self.dim_resized_patch, dim_grasp_patch=self.dim_grasp_patch)

        if self.train_batch_size > len(X_train_grasps):
            train_batch_size = len(X_train_grasps)
        else:
            train_batch_size = self.train_batch_size

        if self.validation_batch_size > len(X_val_grasps):
            validation_batch_size = len(X_val_grasps)
        else:
            validation_batch_size = self.validation_batch_size

        if self.allow_flip:
            random_angle_augmentation_degrees = [0.0, -180.0, 180.0]
        else:
            random_angle_augmentation_degrees = [0.0]

        optimizer = select_optimizer(optimizer=self.optimizer, base_learning_rate=self.base_learning_rate)
        loss_list = []
        if self.nb_of_class == 2:
            loss_list.append('binary_crossentropy')
        else:
            loss_list.append('categorical_crossentropy')

        loss_weights_list = []
        loss_weights_list.append(1.0)

        self.model.compile(optimizer=optimizer,
                           loss=loss_list,
                           loss_weights=loss_weights_list, metrics=['accuracy'])

        if self.preprocessing_type_classifier == 'bvae':
            my_training_batch_generator = My_Custom_Generator_bvae(X_train_grasps, train_batch_size,
                                                    preprocessing_fct=self.preprocessing_classifier,
                                                    dic_images_rgb=dict_rgb_scene,
                                                    dic_depth_raw_data=dict_depth_scene,
                                                    b_with_augmentation=self.allow_flip,
                                                   dict_encoded_mu=dict_encoded_mu, dict_encoded_var=dict_encoded_var, b_classifier=True, latent_dim=self.bottleneck, label_dim=current_label_dim, num_classes=self.nb_of_class)



            my_validation_batch_generator = My_Custom_Generator_bvae(X_val_grasps, validation_batch_size,
                                                    preprocessing_fct=self.preprocessing_classifier,
                                                    dic_images_rgb=dict_rgb_scene,
                                                    dic_depth_raw_data=dict_depth_scene,
                                                    b_with_augmentation=False,
                                                     dict_encoded_mu=dict_encoded_mu, dict_encoded_var=dict_encoded_var, b_classifier=True, latent_dim=self.bottleneck, label_dim=current_label_dim, num_classes=self.nb_of_class)

            training_steps = int(math.ceil(my_training_batch_generator.n // train_batch_size))
            print("training_steps - my_training_batch_generator.n // train_batch_size: ", training_steps)

            validation_steps = int(math.ceil(my_validation_batch_generator.n // validation_batch_size))

        else:
            my_training_batch_generator = My_Custom_Generator(X_train_grasps, train_batch_size,
                                                              channels_number=self.channels_number,
                                                              allow_flip=self.allow_flip,
                                                              random_angles=random_angle_augmentation_degrees,
                                                              label_shape=(-1, len(X_train_grasps)),
                                                              final_size=self.dim_resized_patch, random_sizes=self.random_sizes,
                                                              dic_images_rgb=dict_rgb_scene,
                                                              dic_depth_raw_data=dict_depth_scene, use_rgb=self.use_rgb,
                                                              use_gray_depth=self.use_gray_depth, use_SN=self.use_SN,
                                                              b_classifier=True,
                                                              preprocessing_fct=self.preprocessing_classifier, label_dim=current_label_dim, num_classes=self.nb_of_class, max_depth_value=self.max_depth_value)

            my_validation_batch_generator = My_Custom_Generator(X_val_grasps, validation_batch_size,
                                                                allow_flip=False,
                                                                # random_angles=random_angle_augmentation_degrees,
                                                                channels_number=self.channels_number,
                                                                label_shape=(-1, len(X_val_grasps)),
                                                                final_size=self.dim_resized_patch, random_sizes=self.random_sizes,
                                                                dic_images_rgb=dict_rgb_scene,
                                                                dic_depth_raw_data=dict_depth_scene, use_rgb=self.use_rgb,
                                                                use_gray_depth=self.use_gray_depth, use_SN=self.use_SN,
                                                                b_classifier=True,
                                                                preprocessing_fct=self.preprocessing_classifier, label_dim=current_label_dim, num_classes=self.nb_of_class, max_depth_value=self.max_depth_value)

            training_steps = int(math.ceil(my_training_batch_generator.n // train_batch_size))
            print("training_steps - my_training_batch_generator.n // train_batch_size: ", training_steps)

            validation_steps = int(math.ceil(my_validation_batch_generator.n // validation_batch_size))

        if with_timestamp:
            timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())
        else:
            timestamp = ''

        save_dir = where_to_save_model
        if save_dir is not None:
            dir_path = save_dir + "/" + timestamp + "/"
            print('dir_path: ', dir_path)

            if not os.path.exists(save_dir):
                os.mkdir(save_dir)

            if not os.path.exists(dir_path):
                os.mkdir(dir_path)

            copyfile(config_path, dir_path + "config.ini")

            print("saving: X_train_grasps")
            save_obj(X_train_grasps, dir_path + "X_train_grasps.pkl")

            print("saving: X_val_grasps")
            save_obj(X_val_grasps, dir_path + "X_val_grasps.pkl")

            save_file = os.path.join(dir_path, network_name)

            tb = TensorBoard(log_dir=dir_path + "/", write_graph=True, update_freq='epoch', profile_batch = 100000000)

            checkpoint = ModelCheckpoint(filepath=save_file, monitor='val_loss',
                                     verbose=0, save_best_only=True, save_weights_only=True, mode='min', )
            lr_monitor = LRTensorBoard(log_dir=dir_path + "/")

        else:
            tb = None
            checkpoint = None
            lr_monitor = None
            dir_path = None

        lr_schedule = ReduceLROnPlateau(monitor='val_loss', factor=self.decay_factor,
                                        mode='min', patience=self.scheduler_patience, min_lr=self.min_lr, min_delta=0.05)

        # lr_monitor = LRTensorBoard(log_dir=dir_path + "/")

        # lr_early_stopping = EarlyStoppingByLrVal(value=1e-06)
        lr_early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', patience=self.max_epoch_without_improvement,
                                                             restore_best_weights=True, min_delta=0.05)

        l_callbacks = []
        for c in [tb, checkpoint, lr_schedule, lr_early_stopping, lr_monitor]:
            if c is not None:
                l_callbacks.append(c)

        # for l in model.layers:
        #     print(l.name, l.trainable)

        print("len X_train_grasps: ", len(X_train_grasps))
        print("len X_val_grasps: ", len(X_val_grasps))
        print("train_batch_size: ", train_batch_size)
        print("validation_batch_size: ", validation_batch_size)
        print("my_training_batch_generator.n: ", my_training_batch_generator.n)

        start_time = time.time()
        # self.model.fit_generator(generator=my_training_batch_generator,
        #                          epochs=max_epochs,
        #                          steps_per_epoch=training_steps,
        #                          shuffle=True,
        #                          validation_data=my_validation_batch_generator,
        #                          validation_steps=validation_steps,
        #                          callbacks=l_callbacks,
        #                          # callbacks=[lr_schedule, lr_early_stopping],
        #                          verbose=1)
        # self.model.summary()

        try:
            self.model.fit(x=my_training_batch_generator,
                                epochs=self.max_epochs,
                                steps_per_epoch=training_steps,
                                shuffle=True,
                                validation_data=my_validation_batch_generator,
                                validation_steps=validation_steps,
                                callbacks=l_callbacks,
                                verbose=1)

        except KeyboardInterrupt:
            print('abort training')


        if self.fine_tuning:
            for layer in self.model.layers:
                if not isinstance(layer, BatchNormalization):
                    layer.trainable = True

            # optimizer = select_optimizer(optimizer=self.optimizer, base_learning_rate=self.min_lr)
            # loss_list = []
            # if self.nb_of_class == 2:
            #     loss_list.append('binary_crossentropy')
            # else:
            #     loss_list.append('categorical_crossentropy')
            #
            # loss_weights_list = []
            # loss_weights_list.append(1.0)

            # self.model.compile(optimizer=optimizer,
            #                    loss=loss_list,
            #                    loss_weights=loss_weights_list, metrics=['accuracy'])
            try:
                self.model.fit(x=my_training_batch_generator,
                                    epochs=self.max_epochs,
                                    steps_per_epoch=training_steps,
                                    shuffle=True,
                                    validation_data=my_validation_batch_generator,
                                    validation_steps=validation_steps,
                                    callbacks=l_callbacks,
                                    verbose=1)
            except KeyboardInterrupt:
                print('abort training')

        elapsed_time = time.time() - start_time
        if dir_path is not None:
            elapsed_time_file = os.path.join(dir_path, "elapsed_time_file.pkl")
            save_obj(elapsed_time, elapsed_time_file)

            # self.save_weights(save_file)
        if where_to_save_model is not None:
            self.save_model(dir_path)
