from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import NeighborhoodComponentsAnalysis
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler, RobustScaler, QuantileTransformer



def select_scaler(scaler_type):
    print("using ", scaler_type)
    if scaler_type == 'standard':
        return StandardScaler()
    elif scaler_type == "robust":
        return RobustScaler()
    elif scaler_type == "nca":
        return NeighborhoodComponentsAnalysis()
    elif scaler_type == "pca":
        return PCA(random_state=1)
    elif scaler_type == "lda":
        return LinearDiscriminantAnalysis()
    elif scaler_type == "quantile":
        return QuantileTransformer(random_state=1)
    elif scaler_type == "spca":
        return make_pipeline(StandardScaler(),
                            PCA(random_state=1))
    elif scaler_type == "slda":
        return make_pipeline(StandardScaler(),
                            LinearDiscriminantAnalysis())
    elif scaler_type == "snca":
        return make_pipeline(StandardScaler(),
                            NeighborhoodComponentsAnalysis(random_state=1))
    else:
        print("scaler type not implemented")
        return None