import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../../')))
from scripts.gui import PygameGraspInterraction


#todo argparse
if __name__ == '__main__':

    # config_path = './config_profiles/cornell_config_GP_training.ini'
    # where_to_save_model_classifier = "./trained_models/cornell/classifier_cornell_all/"
    # where_to_save_model_length_regressor = "./trained_models/cornell/length_regressor_cornell_all/"

    root = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/scripts/'
    gui_config_path = root+'./test_config_cornell/test_gui_config.ini'
    camera_config_path = root+ './test_config_cornell/test_camera_config.ini'
    database_config_path = root+ './test_config_cornell/test_database_config.ini'
    grasp_generator_config_path = root+ './test_config_cornell/test_grasp_generator_config.ini'
    evaluator_config_path = root+ './test_config_cornell/test_evaluator_config.ini'
    segmentation_config_path = root+ './test_config_cornell/test_segmentation_config.ini'
    panda_config_path = root+ './test_config_cornell/test_panda_config.ini'

    pygame_grasp_interraction = PygameGraspInterraction(gui_config_path=gui_config_path, database_config_path=database_config_path, camera_config_path=camera_config_path, generator_config_path=grasp_generator_config_path, evaluator_config_path=evaluator_config_path, segmentation_config_path=segmentation_config_path, panda_config_path=panda_config_path)

    where_to_save_model_classifier = "../../data/trained_models/inria/classifier_our_all2/"
    where_to_save_model_length_regressor = "../..//trained_models/inria/length_regressor_our_all2/"

    id_user_GT = pygame_grasp_interraction.database.dict_users_id[pygame_grasp_interraction.database.GT_user]


    # path_to_read_pkl = None
    # classifier_type_loaded, scenes_reserved_to_test_loaded, list_of_training_grasps_loaded, list_of_validation_grasps_loaded, t_dict_success_exp_loaded_loaded = load_obj(path_to_read_pkl)
    # pygame_grasp_interraction.all_scenes = scenes_reserved_to_test_loaded
    scenes_reserved_to_train_loaded = pygame_grasp_interraction.database.all_scenes
    GT_grasps = []
    for scene in scenes_reserved_to_train_loaded:
        gt = pygame_grasp_interraction.database.get_gt_request(pygame_grasp_interraction.database.conn_gt,
                                     sql_request='SELECT * from grasps where user_id =' + str(
                                         id_user_GT) + ' AND scene_id=' + str(scene[0]))
        GT_grasps += gt

    #this in only needed in you train a CNN, for gp it will learn both
    length = len(GT_grasps)
    split_index = int(length * 0.7)
    GT_grasps_training = GT_grasps[:split_index]
    GT_grasps_validation = GT_grasps[split_index:]

    print("train_classifier")
    dict_rgb_scene_GT, dict_depth_scene_GT, dict_background_scene_GT, dict_scene_grasp_GT, dict_id_scene_id_obj_GT = pygame_grasp_interraction.train_classifier_with_GT(
        GT_grasps_training, GT_grasps_validation,
        where_to_save_model=where_to_save_model_classifier)
    if pygame_grasp_interraction.predict_gripper_opening:
        pygame_grasp_interraction.train_length_regressor_with_GT(GT_grasps_training, GT_grasps_validation,
                                                                 where_to_save_model=where_to_save_model_length_regressor)
    pygame_grasp_interraction.representation_class.save_dicts()
    print("READY FOR PREDICTION")
