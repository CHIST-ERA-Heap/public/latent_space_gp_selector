import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../../')))
from scripts.gui import PygameGraspInterraction

#todo argparse
if __name__ == '__main__':
    root = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/config_files/'
    # root = '/home/panda1ws/Documents/Latent_space_GP_Selector/data/config_files/'
    # gui_config_path = root+'./test_config_cornell/test_gui_config.ini'
    # camera_config_path = root+ './test_config_cornell/test_camera_config.ini'
    # database_config_path = root+ './test_config_cornell/test_database_config.ini'
    # grasp_generator_config_path = root+ './test_config_cornell/test_grasp_generator_config.ini'
    # evaluator_config_path = root+ './test_config_cornell/test_evaluator_config.ini'
    # segmentation_config_path = root+ './test_config_cornell/test_segmentation_config.ini'
    # panda_config_path = root+ './test_config_cornell/test_panda_config.ini'

    # gui_config_path = root+'./test_config_YCB/test_gui_config.ini'
    # camera_config_path = root+'./test_config_YCB/test_camera_config.ini'
    # database_config_path = root+'./test_config_YCB/test_database_config.ini'
    # grasp_generator_config_path = root+'./test_config_YCB/test_grasp_generator_config.ini'
    # evaluator_config_path = root+'./test_config_YCB/test_evaluator_config.ini'
    # segmentation_config_path = root+'./test_config_YCB/test_segmentation_config.ini'
    # panda_config_path = root+'./test_config_YCB/test_panda_config.ini'

    # gui_config_path = root+'./test_config_live/test_gui_config.ini'
    # camera_config_path = root+'./test_config_live/test_camera_config.ini'
    # database_config_path = root+'./test_config_live/test_database_config.ini'
    # grasp_generator_config_path = root+'./test_config_live/test_grasp_generator_config.ini'
    # evaluator_config_path = root+'./test_config_live/test_evaluator_config.ini'
    # segmentation_config_path = root+'./test_config_live/test_segmentation_config.ini'
    # panda_config_path = root+'./test_config_live/test_panda_config.ini'

    # gui_config_path = root+'./test_config_table/test_gui_config.ini'
    # camera_config_path = root+'./test_config_table/test_camera_config.ini'
    # database_config_path = root+'./test_config_table/test_database_config.ini'
    # grasp_generator_config_path = root+'./test_config_table/test_grasp_generator_config.ini'
    # evaluator_config_path = root+'./test_config_table/test_evaluator_config.ini'
    # segmentation_config_path = root+'./test_config_table/test_segmentation_config.ini'
    # panda_config_path = root+'./test_config_table/test_panda_config.ini'


    # gui_config_path = root+'./test_config_lgps/test_gui_config.ini'
    # camera_config_path = root+'./test_config_lgps/test_camera_config.ini'
    # database_config_path = root+'./test_config_lgps/test_database_config.ini'
    # grasp_generator_config_path = root+'./test_config_lgps/test_grasp_generator_config.ini'
    # evaluator_config_path = root+'./test_config_lgps/test_evaluator_config.ini'
    # segmentation_config_path = root+'./test_config_lgps/test_segmentation_config.ini'
    # panda_config_path = root+'./test_config_lgps/test_panda_config.ini'

    # gui_config_path = root+'./test_config_live/test_gui_config.ini'
    # camera_config_path = root+'./test_config_live/test_camera_config.ini'
    # database_config_path = root+'./test_config_live/test_database_config.ini'
    # grasp_generator_config_path = root+'./test_config_live/test_grasp_generator_config.ini'
    # evaluator_config_path = root+'./test_config_live/test_evaluator_config.ini'
    # # evaluator_config_path = None
    # segmentation_config_path = root+'./test_config_live/test_segmentation_config.ini'
    # # segmentation_config_path = None
    # panda_config_path = root+'./test_config_live/test_panda_config.ini'

    # gui_config_path = root+'./test_config_fused/test_gui_config.ini'
    # camera_config_path = root+'./test_config_fused/test_camera_config.ini'
    # database_config_path = root+'./test_config_fused/test_database_config.ini'
    # grasp_generator_config_path = root+'./test_config_fused/test_grasp_generator_config.ini'
    # evaluator_config_path = root+'./test_config_fused/test_evaluator_config.ini'
    # # segmentation_config_path = root+'./test_config_fused/test_segmentation_config.ini'
    # segmentation_config_path = None
    # panda_config_path = root+'./test_config_fused/test_panda_config.ini'

    gui_config_path = root+'./test_config_fused_vae_training/test_gui_config.ini'
    camera_config_path = root+'./test_config_fused_vae_training/test_camera_config.ini'
    database_config_path = root+'./test_config_fused_vae_training/test_database_config.ini'
    grasp_generator_config_path = root+'./test_config_fused_vae_training/test_grasp_generator_config.ini'
    evaluator_config_path = root+'./test_config_fused_vae_training/test_evaluator_config.ini'
    # segmentation_config_path = root+'./test_config_fused_vae_training/test_segmentation_config.ini'
    segmentation_config_path = None
    panda_config_path = root+'./test_config_fused_vae_training/test_panda_config.ini'


    # grasp_generator_config_path = None
    # evaluator_config_path = None
    #segmentation_config_path = None

    pygame_grasp_interraction = PygameGraspInterraction(gui_config_path=gui_config_path, database_config_path=database_config_path, camera_config_path=camera_config_path, generator_config_path=grasp_generator_config_path, evaluator_config_path=evaluator_config_path, segmentation_config_path=segmentation_config_path, panda_config_path=panda_config_path)
    # pygame_grasp_interraction.all_scenes = [(40, 10, '../../data/data_files/LGPS_data/scenes/YCB_adjustable_wrench/YCB_adjustable_wrench_wrench_1056', '.png', '.npy', None), (46, 11, '../../data/data_files/LGPS_data/scenes/YCB_airplane_toy/YCB_airplane_toy_plane_toy_777', '.png', '.npy', None), (100, 21, '../../data/data_files/LGPS_data/scenes/YCB_chips_can/YCB_chips_can_pringles_782', '.png', '.npy', None), (109, 22, '../../data/data_files/LGPS_data/scenes/YCB_chocolate_pudding_box/YCB_chocolate_pudding_box_jello_604', '.png', '.npy', None), (110, 23, '../../data/data_files/LGPS_data/scenes/YCB_coffee_can/YCB_coffee_can_coffee_529', '.png', '.npy', None), (112, 24, '../../data/data_files/LGPS_data/scenes/YCB_cooking_skillet_with_glass_lid/YCB_cooking_skillet_with_glass_lid_cooking_pan_533', '.png', '.npy', None), (118, 25, '../../data/data_files/LGPS_data/scenes/YCB_cracker_box/YCB_cracker_box_cheezit_182', '.png', '.npy', None), (123, 27, '../../data/data_files/LGPS_data/scenes/YCB_dice/YCB_dice_dice_561', '.png', '.npy', None), (134, 30, '../../data/data_files/LGPS_data/scenes/YCB_foam_brick/YCB_foam_brick_plug_brown_thing_779', '.png', '.npy', None), (135, 31, '../../data/data_files/LGPS_data/scenes/YCB_gelatin_box/YCB_gelatin_box_jello_box_606', '.png', '.npy', None), (143, 32, '../../data/data_files/LGPS_data/scenes/YCB_glass_cleaner/YCB_glass_cleaner_windex_1039', '.png', '.npy', None), (182, 40, '../../data/data_files/LGPS_data/scenes/YCB_medium_spring clamps/YCB_medium_spring clamps_medium_black_plier_760', '.png', '.npy', None), (189, 41, '../../data/data_files/LGPS_data/scenes/YCB_metal_mug/YCB_metal_mug_red_cup_833', '.png', '.npy', None), (208, 44, '../../data/data_files/LGPS_data/scenes/YCB_mustard_container/YCB_mustard_container_yellow_mustard_1097', '.png', '.npy', None), (218, 47, '../../data/data_files/LGPS_data/scenes/YCB_orange/YCB_orange_orange_684', '.png', '.npy', None), (241, 54, '../../data/data_files/LGPS_data/scenes/YCB_plastic_wine_glass/YCB_plastic_wine_glass_transparent_glass_1000', '.png', '.npy', None), (247, 55, '../../data/data_files/LGPS_data/scenes/YCB_plum/YCB_plum_1110', '.png', '.npy', None), (266, 60, '../../data/data_files/LGPS_data/scenes/YCB_scissors/YCB_scissors_scissors_864', '.png', '.npy', None), (298, 64, '../../data/data_files/LGPS_data/scenes/YCB_softball/YCB_softball_balls_26', '.png', '.npy', None), (303, 66, '../../data/data_files/LGPS_data/scenes/YCB_stacking_blocks/YCB_stacking_blocks_stacking_pots_955', '.png', '.npy', None), (320, 69, '../../data/data_files/LGPS_data/scenes/YCB_tennis_ball/YCB_tennis_ball_balls_22', '.png', '.npy', None), (323, 71, '../../data/data_files/LGPS_data/scenes/YCB_tomato_soup_can/YCB_tomato_soup_can_1117', '.png', '.npy', None), (347, 77, '../../data/data_files/LGPS_data/scenes/asterix_fig/asterix_fig_11', '.png', '.npy', None), (375, 79, '../../data/data_files/LGPS_data/scenes/battery/battery_1078', '.png', '.npy', None), (383, 81, '../../data/data_files/LGPS_data/scenes/bird_house/bird_house_62', '.png', '.npy', None), (392, 83, '../../data/data_files/LGPS_data/scenes/black_bbq_cleaner/black_bbq_cleaner_66', '.png', '.npy', None), (395, 84, '../../data/data_files/LGPS_data/scenes/black_candlestick/black_candlestick_75', '.png', '.npy', None), (398, 85, '../../data/data_files/LGPS_data/scenes/black_earmuff/black_earmuff_82', '.png', '.npy', None), (426, 92, '../../data/data_files/LGPS_data/scenes/bleue_bross/bleue_bross_119', '.png', '.npy', None), (466, 99, '../../data/data_files/LGPS_data/scenes/bleue_vise/bleue_vise_157', '.png', '.npy', None), (468, 100, '../../data/data_files/LGPS_data/scenes/brown_plush/brown_plush_749', '.png', '.npy', None), (471, 101, '../../data/data_files/LGPS_data/scenes/cap/cap_168', '.png', '.npy', None), (472, 102, '../../data/data_files/LGPS_data/scenes/cars/cars_173', '.png', '.npy', None), (477, 103, '../../data/data_files/LGPS_data/scenes/cat_plush/cat_plush_174', '.png', '.npy', None), (483, 105, '../../data/data_files/LGPS_data/scenes/cheap_joystcik/cheap_joystcik_179', '.png', '.npy', None), (496, 107, '../../data/data_files/LGPS_data/scenes/circle_handle/circle_handle_196', '.png', '.npy', None), (834, 110, '../../data/data_files/LGPS_data/scenes/corkscrew/corkscrew_535', '.png', '.npy', None), (836, 111, '../../data/data_files/LGPS_data/scenes/cow_plush/cow_plush_538', '.png', '.npy', None), (854, 114, '../../data/data_files/LGPS_data/scenes/donkey/donkey_570', '.png', '.npy', None), (855, 115, '../../data/data_files/LGPS_data/scenes/drain_cleaner/drain_cleaner_1092', '.png', '.npy', None), (863, 118, '../../data/data_files/LGPS_data/scenes/gardening_glove/gardening_glove_1111', '.png', '.npy', None), (868, 120, '../../data/data_files/LGPS_data/scenes/green_fruit_thing/green_fruit_thing_585', '.png', '.npy', None), (915, 126, '../../data/data_files/LGPS_data/scenes/long_dog_plush/long_dog_plush_648', '.png', '.npy', None), (936, 131, '../../data/data_files/LGPS_data/scenes/metal_vase/metal_vase_676', '.png', '.npy', None), (941, 132, '../../data/data_files/LGPS_data/scenes/mirror/mirror_1099', '.png', '.npy', None), (981, 138, '../../data/data_files/LGPS_data/scenes/orange_shark/orange_shark_729', '.png', '.npy', None), (989, 140, '../../data/data_files/LGPS_data/scenes/pearl/pearl_1106', '.png', '.npy', None), (995, 143, '../../data/data_files/LGPS_data/scenes/pikachu/pikachu_773', '.png', '.npy', None), (999, 145, '../../data/data_files/LGPS_data/scenes/racoon_plush/racoon_plush_1107', '.png', '.npy', None), (1009, 147, '../../data/data_files/LGPS_data/scenes/red_cap_gourd/red_cap_gourd_1117', '.png', '.npy', None), (1013, 148, '../../data/data_files/LGPS_data/scenes/red_cutter/red_cutter_1109', '.png', '.npy', None), (1039, 153, '../../data/data_files/LGPS_data/scenes/red_target_lego/red_target_lego_624', '.png', '.npy', None), (1051, 154, '../../data/data_files/LGPS_data/scenes/rubick_cube_tapped/rubick_cube_tapped_548', '.png', '.npy', None), (1056, 155, '../../data/data_files/LGPS_data/scenes/security_glasses/security_glasses_878', '.png', '.npy', None), (1067, 158, '../../data/data_files/LGPS_data/scenes/shovel/shovel_885', '.png', '.npy', None), (1080, 160, '../../data/data_files/LGPS_data/scenes/slinky/slinky_895', '.png', '.npy', None), (1100, 164, '../../data/data_files/LGPS_data/scenes/stacking_circle_game/stacking_circle_game_946', '.png', '.npy', None), (1112, 165, '../../data/data_files/LGPS_data/scenes/taped_cube/taped_cube_913', '.png', '.npy', None), (1140, 170, '../../data/data_files/LGPS_data/scenes/toy_drill/toy_drill_993', '.png', '.npy', None), (1146, 172, '../../data/data_files/LGPS_data/scenes/toy_saw/toy_saw_997', '.png', '.npy', None), (1155, 176, '../../data/data_files/LGPS_data/scenes/white_glove/white_glove_1020', '.png', '.npy', None), (1157, 177, '../../data/data_files/LGPS_data/scenes/white_pipe/white_pipe_1024', '.png', '.npy', None), (1169, 181, '../../data/data_files/LGPS_data/scenes/wine_cap/wine_cap_1046', '.png', '.npy', None), (1171, 182, '../../data/data_files/LGPS_data/scenes/wires/wires_1049', '.png', '.npy', None), (1174, 183, '../../data/data_files/LGPS_data/scenes/yellow_asteroid/yellow_asteroid_1071', '.png', '.npy', None)]
    # pygame_grasp_interraction.movement_happened = True

    # scenes_to_test = ['pcd0730', 'pcd0217', 'pcd0844', 'pcd0805', 'pcd0655', 'pcd0567', 'pcd0226', 'pcd0867', 'pcd0596', 'pcd0928', 'pcd1021', 'pcd0745', 'pcd0714', 'pcd0465', 'pcd0435', 'pcd0434', 'pcd0198', 'pcd0598', 'pcd0549', 'pcd0201', 'pcd0790', 'pcd0635', 'pcd0554', 'pcd0135', 'pcd0420', 'pcd0694', 'pcd0809', 'pcd0164', 'pcd0104', 'pcd1018', 'pcd0778', 'pcd1013', 'pcd0645', 'pcd0213', 'pcd0385', 'pcd1012', 'pcd0816', 'pcd0766', 'pcd0717', 'pcd0453', 'pcd0835', 'pcd0836', 'pcd0740', 'pcd0526', 'pcd0428', 'pcd0403', 'pcd0620', 'pcd0757', 'pcd0831', 'pcd0173', 'pcd0534', 'pcd0402', 'pcd0122', 'pcd0355', 'pcd0243', 'pcd0542', 'pcd0165', 'pcd0350', 'pcd0604', 'pcd0609', 'pcd0931', 'pcd0336', 'pcd0366', 'pcd0530', 'pcd0675', 'pcd0779', 'pcd0301', 'pcd0150', 'pcd0793', 'pcd0891', 'pcd0156', 'pcd0423', 'pcd0285', 'pcd0663', 'pcd0346', 'pcd0772', 'pcd0306', 'pcd0362', 'pcd0129', 'pcd0227', 'pcd0895', 'pcd0590', 'pcd0701', 'pcd0146', 'pcd0581', 'pcd0172', 'pcd0387', 'pcd0487', 'pcd0707', 'pcd0924', 'pcd0389', 'pcd0892', 'pcd0509', 'pcd0883', 'pcd0296', 'pcd0118', 'pcd1007', 'pcd0752', 'pcd0721', 'pcd0158', 'pcd0770', 'pcd0689', 'pcd0723', 'pcd0334', 'pcd0263', 'pcd0255', 'pcd0750', 'pcd0496', 'pcd0239', 'pcd0276', 'pcd0906', 'pcd0475', 'pcd0499', 'pcd0393', 'pcd0459', 'pcd0183', 'pcd0731', 'pcd0308', 'pcd0380', 'pcd0291', 'pcd1032', 'pcd1002', 'pcd0190', 'pcd0455', 'pcd0840', 'pcd0321', 'pcd0489', 'pcd0673', 'pcd0797', 'pcd0900', 'pcd0825', 'pcd0373', 'pcd0851', 'pcd0786', 'pcd0695', 'pcd0877', 'pcd0737', 'pcd0264', 'pcd0761', 'pcd0849', 'pcd0682', 'pcd0109', 'pcd1034', 'pcd0274', 'pcd0247', 'pcd0398', 'pcd0562', 'pcd0472', 'pcd0251', 'pcd0233', 'pcd0222', 'pcd0317', 'pcd0506', 'pcd0326', 'pcd0185', 'pcd0178', 'pcd0143', 'pcd0859', 'pcd0558', 'pcd0613', 'pcd0936', 'pcd0367', 'pcd0943', 'pcd0536', 'pcd0270', 'pcd0665', 'pcd0919', 'pcd0112', 'pcd0140', 'pcd0117', 'pcd0822', 'pcd0450', 'pcd0872', 'pcd0587', 'pcd0469', 'pcd0294', 'pcd0553', 'pcd0684', 'pcd0518', 'pcd0657', 'pcd0862', 'pcd0904', 'pcd0128', 'pcd1024', 'pcd0523', 'pcd0257', 'pcd0312', 'pcd0940', 'pcd0884', 'pcd0479', 'pcd0410', 'pcd0280', 'pcd0352', 'pcd0212', 'pcd0641', 'pcd0480', 'pcd0868', 'pcd0539', 'pcd0626', 'pcd1006', 'pcd0803', 'pcd0445', 'pcd0854', 'pcd0502', 'pcd0815', 'pcd0623', 'pcd0417', 'pcd0376', 'pcd0515', 'pcd0237', 'pcd1026', 'pcd0801', 'pcd0679', 'pcd1030', 'pcd0101', 'pcd0637', 'pcd0703', 'pcd0669', 'pcd0194', 'pcd0616', 'pcd0574', 'pcd0413', 'pcd0340', 'pcd0911', 'pcd1027', 'pcd0948', 'pcd0207', 'pcd0630', 'pcd0441', 'pcd0628', 'pcd0914', 'pcd0325', 'pcd0235', 'pcd0328', 'pcd0573', 'pcd0652', 'pcd0388', 'pcd0484', 'pcd0796', 'pcd0584', 'pcd0324', 'pcd0236', 'pcd0894', 'pcd1000']
    # path_to_weight_dir = '../../experiments_results/cornell_test_vae/'

    # path_to_weight_dir = '/home/panda1ws/Documents/Latent_space_GP_Selector/experiments_results/test_config_fused_config_vae_training/'
    path_to_weight_dir = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/experiments_results/test_config_fused_config_vae_training/'

    scenes_to_test = []

    pygame_grasp_interraction.train_unsupervised(path_to_weight_dir=path_to_weight_dir, scenes_reserved_to_test=scenes_to_test, b_compute_missing=False)
