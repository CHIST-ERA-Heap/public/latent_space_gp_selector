import configparser
import sensor_msgs.point_cloud2
import cv2
import rospy
import sensor_msgs.point_cloud2
from sensor_msgs.point_cloud2 import read_points
from grasping_benchmarks.base.transformations import quaternion_to_matrix, matrix_to_quaternion
from grasping_benchmarks_ros.srv import GraspPlanner, GraspPlannerRequest, GraspPlannerResponse
import numpy as np
from random import randint, shuffle
from scripts.common.setup_stuff_util import setup_feature_extraction_stuff, setup_object_classifier, setup_classifier, setup_length_regressor
from scripts.common.drawings_util import generate_image_with_grasps
from PIL import Image
import math
import os

#this could use the reranking from the grasping-benchmarks-panda, but it doesnt for now
class GraspEvaluator:
    def __init__(self, evaluator_config_path):
        self.evaluator_config_path =evaluator_config_path
        self.cfg = configparser.ConfigParser()
        self.cfg.read(evaluator_config_path)
        print(evaluator_config_path, "evaluator_config_path: " + str(os.path.isfile(evaluator_config_path)))
        self.read_cfg()
        self.setup_models()

    def read_cfg(self):
        # self.change_too_far_to_cam = self.cfg.getboolean('preprocessing','change_too_far_to_cam')
        # self.too_far_to_cam_to_what = self.cfg.get('preprocessing','too_far_to_cam_to_what')
        # self.too_far_to_cam_threshold = self.cfg.getfloat('preprocessing','too_far_to_cam_threshold')
        #
        # self.change_nans = self.cfg.getboolean('preprocessing','change_nans')
        # self.nans_to_what = self.cfg.get('preprocessing','nans_to_what')
        #
        # self.change_too_close_to_cam = self.cfg.getboolean('preprocessing','change_too_close_to_cam')
        # self.too_close_to_cam_to_what = self.cfg.get('preprocessing','too_close_to_cam_to_what')
        # self.too_close_to_cam_threshold = self.cfg.getfloat('preprocessing','too_close_to_cam_threshold')

        self.visualization = self.cfg.getboolean('visualization','visualization')

        self.predict_gripper_opening = self.cfg.getboolean('length_regressor', 'activated')





    def setup_models(self):
        self.representation_class = setup_feature_extraction_stuff(self.cfg)
        self.classifier_object = setup_object_classifier(self.cfg, self.representation_class)
        self.classifier = setup_classifier(self.cfg, self.representation_class)
        if self.predict_gripper_opening:
            self.length_regressor = setup_length_regressor(self.cfg, self.representation_class)
        else:
            self.length_regressor = None

    #work if all the grasps candidates are from the same scene
    def rerank(self, grasps_candidates_list, dict_rgb_scene = {}, dict_depth_raw = {}, n_candidates=None, sort=True, verbose=False):
        current_scene_id = grasps_candidates_list[0][1]

        if current_scene_id not in dict_rgb_scene:
            rgb_image = self.camera_data.rgb_img
            depth_image = self.camera_data.depth_img

            dict_rgb_scene[current_scene_id] = rgb_image
            dict_depth_raw[current_scene_id] = depth_image

        if self.classifier.by_object:
            # sort grasps_candidates by object prediction
            grasps_candidates_list, _, _, elapsed_time = self.classifier_object.update_grasps_candidates_with_obj(
                grasps_candidates_list,
                dict_rgb_scene, dict_depth_raw, verbose=True)

        scores, bvae_mu, bvae_var, elapsed_time = self.classifier.predict(grasps_candidates_list, dict_rgb_scene, dict_depth_raw)

        if self.classifier.norm:
            if len(scores) > 0:
                min_score = np.min(scores)
                max_score = np.max(scores)

                for i in range(len(scores)):
                    scores[i] = (scores[i] - min_score) / (max_score - min_score)
        if verbose:
            print('scores: ', scores)
            print('bvae_mu: ', bvae_mu.shape)
            print('bvae_var: ', bvae_var.shape)
            print('elapsed_time: ', elapsed_time)

        predicted_lengths = None
        if self.predict_gripper_opening and self.length_regressor.model_length is not None:
            if bvae_mu is None:
                predicted_lengths, bvae_mu, _, _ = self.length_regressor.predict_length(grasps_candidates_list, dict_rgb_scene, dict_depth_raw)
            else:
                predicted_lengths = self.length_regressor.predict_preprocessed_length(bvae_mu)
        else:
            print('no length prediction')

        #apply the new scores (and predicted_lengths)
        for i in range(len(scores)):
            '''
            id integer PRIMARY KEY,
            scene_id integer NOT NULL,
            user_id integer NOT NULL,
            x real NOT NULL,
            y real NOT NULL,
            depth real NOT NULL,
            euler_x real NOT NULL,
            euler_y real NOT NULL,
            euler_z real NOT NULL,
            length real NOT NULL,
            width real NOT NULL,
            quality real NOT NULL,
            FOREIGN KEY (scene_id) REFERENCES scenes (id),
            FOREIGN KEY (user_id) REFERENCES user (id)
            '''
            if predicted_lengths is not None:
                if not self.length_regressor.b_predict_correction:
                    grasps_candidates_list[i][9] = float(predicted_lengths[i])
                else:
                    if self.length_regressor.b_force_fix_prior:
                        grasps_candidates_list[i][9] = float(
                            predicted_lengths[i] + self.length_regressor.prior_length)
                    else:
                        grasps_candidates_list[i][9] = float(
                            predicted_lengths[i] + grasps_candidates_list[i][9])

            grasps_candidates_list[i][11] = (scores[i] * self.classifier.coef_classification_score + grasps_candidates_list[i][11] * self.classifier.coef_generator_score)/(self.classifier.coef_classification_score+self.classifier.coef_generator_score)
            # print('self.grasps_candidates[scene_id]['+str(i)+']: ', self.grasps_candidates[scene_id][i])

        if sort:
            # print('predict_grasp_of_currently_loaded_grasps - get_candidates_sorted_by_scores')
            grasps_candidates_list = self.get_candidates_sorted_by_scores(grasps_candidates_list)

        #keep n_of_candidates grasps
        if n_candidates is not None:
            grasps_candidates_list = grasps_candidates_list[:n_candidates]

        if self.visualization:
            self.visualize(grasps_candidates_list, dict_rgb_scene[current_scene_id])

        return grasps_candidates_list

    # def get_list_of_lengths_in_meters(self, grasps_candidates_list):
    #     length_meters_list = []
    #     for grasp in grasps_candidates_list:
    #         # my method
    #         '''
    #         id integer PRIMARY KEY,
    #         scene_id integer NOT NULL,
    #         user_id integer NOT NULL,
    #         x real NOT NULL,
    #         y real NOT NULL,
    #         depth real NOT NULL,
    #         euler_x real NOT NULL,
    #         euler_y real NOT NULL,
    #         euler_z real NOT NULL,
    #         length real NOT NULL,
    #         width real NOT NULL,
    #         quality real NOT NULL,
    #         FOREIGN KEY (scene_id) REFERENCES scenes (id),
    #         FOREIGN KEY (user_id) REFERENCES user (id)
    #         '''
    #         x = grasp[3]
    #         y = grasp[4]
    #         z = grasp[5]
    #         angle = grasp[8]
    #         length_px = grasp[9]
    #         length_meters = self.get_length_in_meters( x, y, z, angle, length_px, self.camera_data.intrinsic_params)
    #         length_meters_list.append(length_meters)
    #     return length_meters_list
    #
    # def get_length_in_meters(self,  x, y, z, angle, length_dx, camera_intrinsics, verbose=False):
    #     if verbose:
    #         print('x: ', x)
    #         print('y: ', y)
    #         print('z: ', z)
    #         print('angle: ', angle)
    #         print('length_dx: ', length_dx)
    #
    #
    #     rotation_matrix = np.zeros((2, 2))
    #     rotation_matrix[0][0] = math.cos(angle)
    #     rotation_matrix[0][1] = -math.sin(angle)
    #     rotation_matrix[1][0] = math.sin(angle)
    #     rotation_matrix[1][1] = math.cos(angle)
    #
    #     p1 = np.zeros((2, 1))
    #     p2 = np.zeros((2, 1))
    #
    #     p1[0][0] = -int(length_dx / 2)
    #     p2[0][0] = int(length_dx / 2)
    #
    #     p1_rotated = np.dot(rotation_matrix, p1)
    #     p2_rotated = np.dot(rotation_matrix, p2)
    #
    #     p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
    #     p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))
    #
    #
    #     p_grasp_right = self.project_image_point_to_pointcloud(p2_recentered[0], p2_recentered[1], z, camera_intrinsics)
    #
    #     p_grasp_left = self.project_image_point_to_pointcloud(p1_recentered[0], p1_recentered[1], z, camera_intrinsics)
    #
    #     length_in_meters = np.linalg.norm(np.asarray(p_grasp_right) - np.asarray(p_grasp_left)) + self.fix_value_meter_to_add
    #
    #     if verbose:
    #         # print('center: ', p_grasp)
    #         print('right_finger: ', p_grasp_right)
    #         print('left_finger: ', p_grasp_left)
    #         print('length_in_meters: ', length_in_meters)
    #
    #     return length_in_meters

    def project_image_point_to_pointcloud(self, pixel_x, pixel_y, depth_meters, camera_intrinsics):
        u = pixel_x - camera_intrinsics['cx']
        v = pixel_y - camera_intrinsics['cy']

        X = (depth_meters * u) / camera_intrinsics['fx']
        Y = (depth_meters * v) / camera_intrinsics['fy']

        point_cloud_point = [X, Y, depth_meters]
        # print('point_cloud_point: ', point_cloud_point)

        return point_cloud_point

    def get_candidates_sorted_by_scores(self, grasps_candidates, reverse=True):
        grasps_candidates.sort(key=lambda x: x[11], reverse=reverse)
        return grasps_candidates

    def visualize(self, grasps, rgb):
        """Visualize point cloud and last batch of computed grasps in a 2D visualizer
        """
        cv2_img = generate_image_with_grasps(grasps, rgb.copy(), destination=None, b_with_rectangles = False, thickness_line=5, default_color=None, b_with_circles=False, normalize_scores=False)

        window_name = 'reranked grasps'
        img = Image.fromarray(cv2_img)
        img.show(window_name)
        input('enter any key to continue:')