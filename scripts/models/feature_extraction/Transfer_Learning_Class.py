import math
from scripts.common.pickle_util import load_obj
import gc
from pathlib import Path
import cv2
from scripts.common.drawings_util import generate_image_with_grasps
from scripts.models.feature_extraction.util import *
from tensorflow.keras import backend as K
from scripts.models.common.model import get_model_feature_extraction_vgg16, get_model_feature_extraction_vgg19
from tensorflow.keras.applications import EfficientNetB0, EfficientNetB1, EfficientNetB7, ResNet50, InceptionV3
from tensorflow.keras.layers import Input
from scripts.models.common.model import get_feature_extraction_model_with_base, set_model_as_untrainable

from scripts.common.process_patch_util import extract_single_feature
import numpy as np
import os
from scripts.common.pickle_util import save_obj
from tensorflow.python.ops.numpy_ops import np_config
# np_config.enable_numpy_behavior()
tf.get_logger().setLevel('ERROR')
tf.compat.v1.enable_eager_execution()

class Transfer_Learning_Class_RGBDSN:
    def __init__(self, config, preprocessing_function, postprocessing_function):
        self.config = config
        self.base = None
        self.preprocessing_function = preprocessing_function
        self.postprocessing_function = postprocessing_function
        self.preprocessing_function_type = self.config.get('feature_extraction', 'preprocessing')

        # args = parse_arguments(sys.argv[1:])[0]
        self.use_SN = self.config.getboolean('data_modalities', 'use_SN')
        self.use_gray_depth = self.config.getboolean('data_modalities', 'use_gray_depth')
        self.use_rgb = self.config.getboolean('data_modalities', 'use_rgb')

        self.max_depth_value = self.config.getfloat('data_modalities', 'max_depth_value')
        self.channels_number = self.config.getint('data_modalities', 'channels_number')
        self.dim_resized_patch = self.config.getint('data_modalities', 'dim_resized_patch')
        self.dim_grasp_patch = self.config.getint('data_modalities', 'dim_grasp_patch')
        self.projection_dim = self.config.getint('feature_extraction', 'projection_dim')
        self.max_input_size = self.config.getint('feature_extraction', 'max_input_size')

        self.b_force_cpu = not self.config.getboolean('feature_extraction', 'use_gpu')

        self.model_type = self.config.get('feature_extraction', 'model_type')
        self.pretrained_keras = self.config.get('feature_extraction', 'pretrained_keras')
        self.new = 0

        if self.pretrained_keras == "None":
            self.pretrained_keras = None

        self.trained_model = self.config.get('feature_extraction', 'trained_model')
        if self.trained_model == "None":
            self.trained_model = None

        # if self.trained_model is None:
        #     print('Transfer_Learning_Class_RGBDSN not setup correctly')
        self.feature_extraction_model = None

        self.feature_dict_path = self.config.get('feature_extraction', 'feature_dict_path')
        if self.feature_dict_path == "None":
            print("computed encoding will not be saved")
            self.feature_dict_path = None
            self.encoded_mu_dict = {}
        else:
            path = Path(self.feature_dict_path)
            path.mkdir(parents=True, exist_ok=True)
            self.encoded_path = self.feature_dict_path + 'dict_encoded.pkl'
            if os.path.isfile(self.encoded_path):
                self.encoded_mu_dict = load_obj(self.encoded_path)
            else:
                self.encoded_mu_dict = {}
        self.encoded_var_dict = {}

        self.load_model()

    def save_dicts(self):
        if self.new > 0:
            if self.feature_dict_path is not None:
                print("new: ", self.new)
                print("saving: ", self.encoded_path)
                save_obj(self.encoded_mu_dict, self.encoded_path)
                self.new = 0
            else:
                print("can not save encoded dicts")

    def get_tl_model(self, verbose=False):
        image_shape = (self.dim_resized_patch, self.dim_resized_patch, self.channels_number)
        input_pretrained = Input(shape=image_shape, name='encoder_input')
        if self.pretrained_keras is not None:
            print('get_tl_model with ', self.pretrained_keras, ' weights')

        if self.model_type == 'vgg16':
            return get_model_feature_extraction_vgg16(image_shape, weights=self.pretrained_keras, b_untrainable=True)

        if self.model_type == 'vgg19':
            return get_model_feature_extraction_vgg19(image_shape, weights=self.pretrained_keras, b_untrainable=True)

        if self.model_type == 'EfficientNetB0':
            self.base =  EfficientNetB0(input_tensor=input_pretrained, weights=self.pretrained_keras, include_top=False)
            self.base = set_model_as_untrainable(self.base)
            return get_feature_extraction_model_with_base(self.base)

        if self.model_type == 'EfficientNetB1':
            self.base = EfficientNetB1(input_tensor=input_pretrained, weights=self.pretrained_keras, include_top=False)
            self.base = set_model_as_untrainable(self.base)
            return get_feature_extraction_model_with_base(self.base)

        if self.model_type == 'EfficientNetB7':
            self.base = EfficientNetB7(input_tensor=input_pretrained, weights=self.pretrained_keras, include_top=False)
            self.base = set_model_as_untrainable(self.base)
            return get_feature_extraction_model_with_base(self.base)

        if self.model_type == 'ResNet50':
            self.base = ResNet50(input_tensor=input_pretrained, weights=self.pretrained_keras, include_top=False)
            self.base = set_model_as_untrainable(self.base)
            return get_feature_extraction_model_with_base(self.base)

        if self.model_type == 'InceptionV3':
            self.base = InceptionV3(input_tensor=input_pretrained, weights=self.pretrained_keras, include_top=False)
            self.base = set_model_as_untrainable(self.base)
            return get_feature_extraction_model_with_base(self.base)


    def load_model(self):
        print("load TL network ", self.model_type)
        # Failed to memcopy into scratch buffer for device
        # tf.config.experimental.list_physical_devices(device_type=None)
        if self.b_force_cpu:
            device = "cpu:0"
        else:
            device = "gpu:0"

        with tf.device(device):

            # config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
            # config.gpu_options.per_process_gpu_memory_fraction = 0.3
            # config.gpu_options.allow_growth = True
            # sess = tf.compat.v1.Session(config=config)
            # tf.compat.v1.keras.backend.set_session(tf.Session(graph=tf.compat.v1.get_default_graph(), config=config))

            gpus = tf.config.list_physical_devices('GPU')
            if self.b_force_cpu:
                # Restrict TensorFlow to only use the first GPU
                try:
                    tf.config.set_visible_devices(gpus[0], 'GPU')
                    logical_gpus = tf.config.list_logical_devices('GPU')
                    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
                except RuntimeError as e:
                    # Visible devices must be set before GPUs have been initialized
                    print(e)


            self.feature_extraction_model = self.get_tl_model()
            if self.trained_model is not None:
                print('loading weights: ', self.trained_model)
                self.feature_extraction_model.load_weights(self.trained_model)

            self.feature_extraction_model.summary()
            self.projection_dim = self.feature_extraction_model.output_shape[-1]
            print('new projection_dim: ', self.projection_dim)
            np_config.enable_numpy_behavior()

            # K.clear_session()
            gc.collect()


    def del_network(self):
        print("del TL network")

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        gc.collect

        del self.feature_extraction_model


        tf.reset_default_graph()
        # K._SESSION.close()
        # K._SESSION = None

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        # cuda.select_device(0)
        # cuda.close()
        gc.collect()

        self.feature_extraction_model = None
        self.base = None

    def extract_single_feature(self, rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, flipped=0,
                               use_rgb=None, use_gray_depth=None, use_SN=None, dim_grasp_patch=None,
                               dim_resized_patch=None):
        # return extract_single_feature(rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, self.use_rgb, self.use_gray_depth, self.use_SN, self.b_filter, self.dim_grasp_patch, self.dim_resized_patch, flipped=flipped)
        if use_rgb is None:
            use_rgb = self.use_rgb
        if use_gray_depth is None:
            use_gray_depth = self.use_gray_depth
        if use_SN is None:
            use_SN = self.use_SN
        if dim_grasp_patch is None:
            dim_grasp_patch = self.dim_grasp_patch
        if dim_resized_patch is None:
            dim_resized_patch = self.dim_resized_patch

        return extract_single_feature(rgb_scene=rgb_scene, depth_raw=depth_raw, grasp_position_x=grasp_position_x,
                                      grasp_position_y=grasp_position_y, angle_degrees=angle_degrees, use_rgb=use_rgb,
                                      use_gray_depth=use_gray_depth, use_SN=use_SN,
                                      dim_grasp_patch=dim_grasp_patch, dim_resized_patch=dim_resized_patch,
                                      flipped=flipped, max_depth_value=self.max_depth_value)


    def encode_array(self, X, b_raw=False):
        X = np.asarray(X)
        X = X.reshape((-1, self.dim_resized_patch, self.dim_resized_patch, self.channels_number))
        X = self.preprocessing_function(X)

        # if self.feature_extraction_model is None:
        #     print("encoder not setup in get_features :(")
        #     self.load_model()
        # print("encode_array X.shape: ", X.shape)

        if self.max_input_size is not None and X.shape[0] > self.max_input_size:
            n_splits = (X.shape[0] // self.max_input_size) + 1
            b_first = True
            for d in np.array_split(X, n_splits):
                if b_first:
                    d_temp = d  # self.test_augmentation_model(d)

                    encoded = self.feature_extraction_model(d_temp)
                    b_first = False
                else:
                    d_temp = d  # self.test_augmentation_model(d)
                    encoded_temp = self.feature_extraction_model(d_temp)

                    encoded = np.concatenate((encoded, encoded_temp), axis=0)
        else:
            X_temp = X  # self.test_augmentation_model(X)

            encoded = self.feature_extraction_model(X_temp)

        gc.collect()
        # encoded = np.array(encoded).reshape((-1, self.projection_dim))

        return encoded


    def encode_candidates_raw(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, verbose=False):
        if flipped is None:
            flipped = [0 for i in range(len(grasp_candidates))]

        missing_X = []
        i = -1
        for g in grasp_candidates:
            # print('encode_candidates_raw: ', g)
            i += 1
            missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                         math.degrees(g[8]), flipped=flipped[i]))
        return self.encode_array(missing_X, b_raw=True), None


    def encode_candidates(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, verbose=False):
        if flipped is None:
            flipped = [0 for i in range(len(grasp_candidates))]
        if self.feature_dict_path is None:
            missing_X = []
            i = -1
            for g in grasp_candidates:
                i += 1
                missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                             math.degrees(g[8]), flipped=flipped[i]))

            r = self.encode_array(missing_X)
            return r, np.zeros_like(r)
        else:
            if self.feature_extraction_model is None:
                np_config.enable_numpy_behavior()
            missing_X = []
            missing_keys = []
            i = -1
            for g in grasp_candidates:
                # print("encode_candidates: ", g)
                i += 1

                key_str = str(int(g[1])) + "_" + str(int(g[3])) + "_" + str(int(g[4])) + "_" + str(
                    math.degrees(g[8])) + "_" + str(flipped[i])
                if key_str not in self.encoded_mu_dict:
                    # print("g: ", g)
                    # print("missing_keys key_str: ", key_str)
                    missing_keys.append(key_str)

                    missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                                 math.degrees(g[8]), flipped=flipped[i]))

            if len(missing_X) > 0:
                self.new += len(missing_X)
                # print('predicting  ', len(missing_keys), " missing grasps")
                encoded_missing = self.encode_array(missing_X)

                for i in range(0, len(missing_keys)):
                    key_str = missing_keys[i]
                    temp_encoded = encoded_missing[i, :]

                    self.encoded_mu_dict[key_str] = temp_encoded

            encoded = []
            for g in grasp_candidates:
                # key_str = str(g[0]) + "_" + str(g[5])
                key_str = str(int(g[1])) + "_" + str(int(g[3])) + "_" + str(int(g[4])) + "_" + str(
                    math.degrees(g[8])) + "_" + str(flipped[i])
                if key_str in self.encoded_mu_dict:
                    temp_encoded = self.encoded_mu_dict[key_str]

                    encoded.append(temp_encoded.reshape((self.projection_dim)))

            # #todo remove?
            # if len(missing_X) > 1:
            #     self.save_dicts()

            encoded = np.array(encoded).reshape((-1, self.projection_dim))
            if verbose and self.decoder is not None:
                for j in range(len(grasp_candidates)):
                    x_latent = encoded[j, :]
                    self.save_decoded_patch(x_latent.reshape((-1, self.projection_dim)),
                                            base_name=self.feature_dict_path + str(grasp_candidates[j][1]) + '_' + str(
                                                j))

                    generated = generate_image_with_grasps([grasp_candidates[j]],
                                                           dict_rgb[grasp_candidates[j][1]].copy(),
                                                           b_with_rectangles=True,
                                                           thickness_line=1)

                    cv2.imwrite(self.feature_dict_path + str(grasp_candidates[j][1]) + '_' + str(j) + "_full.png",
                                generated)

            return encoded, np.zeros_like(encoded)

        # def post_process_output(self, x, b_between_minus_1=True):
        #     return post_process_output(x,b_between_minus_1)


    def save_decoded_patch(self, mu, base_name):
        print('no decoder')
        return