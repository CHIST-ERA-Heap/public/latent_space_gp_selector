from tensorflow.keras.callbacks import TensorBoard
from shutil import copyfile
from scripts.models.common.data_generator import My_Custom_Generator_Scenes, My_Custom_Generator
import time
from scripts.common.process_patch_util import extract_single_feature
import math
import os
from scripts.common.pickle_util import load_obj, save_obj
import gc
from pathlib import Path
import tensorflow_addons as tfa
from datetime import datetime
import cv2
from sklearn.model_selection import train_test_split
from scripts.common.drawings_util import generate_image_with_grasps
from scripts.models.feature_extraction.util import PatchEncoder, Patches, create_encoder, create_decoder, \
    MaskedAutoencoder
from scripts.models.common.utils import get_name_network,LRTensorBoard
from tensorflow.keras import backend as K
from tensorflow.keras import layers
from tensorflow import keras
import tensorflow as tf
import numpy as np
from tensorflow.python.ops.numpy_ops import np_config
from tensorflow.keras.optimizers.schedules import LearningRateSchedule
from scripts.models.common.utils import select_optimizer
from tensorflow.keras.callbacks import TensorBoard, ReduceLROnPlateau

# np_config.enable_numpy_behavior()
tf.get_logger().setLevel('ERROR')

class MAE_Class_RGBDSN:
    def __init__(self, config=None, preprocessing_function=None, postprocessing_function=None):
        if config is not None:
            self.config = config
            self.base = None

            self.preprocessing_function = preprocessing_function
            self.postprocessing_function = postprocessing_function
            self.preprocessing_function_type = self.config.get('feature_extraction', 'preprocessing')

            # args = parse_arguments(sys.argv[1:])[0]
            self.use_SN = self.config.getboolean('data_modalities', 'use_SN')
            self.use_gray_depth = self.config.getboolean('data_modalities', 'use_gray_depth')
            self.use_rgb = self.config.getboolean('data_modalities', 'use_rgb')

            self.max_depth_value = self.config.getfloat('data_modalities', 'max_depth_value')
            self.channels_number = self.config.getint('data_modalities', 'channels_number')
            self.dim_resized_patch = self.config.getint('data_modalities', 'dim_resized_patch')
            self.dim_grasp_patch = self.config.getint('data_modalities', 'dim_grasp_patch')
            self.projection_dim = self.config.getint('feature_extraction', 'projection_dim')
            self.max_input_size = self.config.getint('feature_extraction', 'max_input_size')

            self.INPUT_SHAPE = (self.dim_resized_patch, self.dim_resized_patch)

            # timestamp = datetime.utcnow().strftime("%y%m%d-%H%M%S")
            # INPUT_SHAPE = (32, 32, 3)
            self.IMAGE_SIZE = self.dim_resized_patch  # We'll resize input images to this size.
            self.PATCH_SIZE = 16  # int(6*(self.dim_resized_patch/48))  # Size of the patches to be extract from the input images.
            self.NUM_PATCHES = (self.IMAGE_SIZE // self.PATCH_SIZE) ** 2

            self.ENC_PROJECTION_DIM = self.projection_dim
            self.training_MASK_PROPORTION = 0.5
            self.ENC_NUM_HEADS = 4
            self.ENC_LAYERS = 3
            self.LAYER_NORM_EPS = 1e-6
            self.ENC_TRANSFORMER_UNITS = [
                self.ENC_PROJECTION_DIM * 2,
                self.ENC_PROJECTION_DIM,
            ]  # Size of the transformer layers.

            self.DEC_PROJECTION_DIM = 64
            self.DEC_NUM_HEADS = 4
            self.DEC_LAYERS = 1  # The decoder is lightweight but should be reasonably deep for reconstruction.
            self.DEC_TRANSFORMER_UNITS = [
                self.DEC_PROJECTION_DIM * 2,
                self.DEC_PROJECTION_DIM,
            ]

            # Dimensionality of latent space
            self.b_decoder = self.config.getboolean('feature_extraction', 'load_decoder')
            self.b_force_cpu = not self.config.getboolean('feature_extraction', 'use_gpu')

            self.model_type = self.config.get('feature_extraction', 'model_type')
            self.encoder = None
            self.base = None
            self.decoder = None
            self.trained_model = self.config.get('feature_extraction', 'trained_model')
            self.new = 0

            if self.trained_model == "None":
                self.trained_model = None

            self.feature_dict_path = self.config.get('feature_extraction', 'feature_dict_path')
            if self.feature_dict_path == "None":
                print("computed encoding will not be saved")
                self.feature_dict_path = None
                self.encoded_mu_dict = {}
            else:
                path = Path(self.feature_dict_path)
                path.mkdir(parents=True, exist_ok=True)
                self.encoded_path = self.feature_dict_path + 'dict_encoded.pkl'
                if os.path.isfile(self.encoded_path):
                    self.encoded_mu_dict = load_obj(self.encoded_path)
                else:
                    self.encoded_mu_dict = {}
            self.encoded_var_dict = {}

    def save_dicts(self):
        if self.new > 0:
            if self.feature_dict_path is not None:
                print("new: ", self.new)
                # print("saving: ", self.encoded_mu_dict)
                save_obj(self.encoded_mu_dict, self.encoded_path)
                self.new = 0
            else:
                print("can not save encoded dicts")

    def load_model(self):
        print("load mae network ", self.model_type)
        # Failed to memcopy into scratch buffer for device
        # tf.config.experimental.list_physical_devices(device_type=None)
        if self.b_force_cpu:
            device = "cpu:0"
        else:
            device = "gpu:0"

        with tf.device(device):

            # config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
            # config.gpu_options.per_process_gpu_memory_fraction = 0.3
            # config.gpu_options.allow_growth = True
            # sess = tf.compat.v1.Session(config=config)
            # tf.compat.v1.keras.backend.set_session(tf.Session(graph=tf.compat.v1.get_default_graph(), config=config))

            gpus = tf.config.list_physical_devices('GPU')
            if self.b_force_cpu:
                # Restrict TensorFlow to only use the first GPU
                try:
                    tf.config.set_visible_devices(gpus[0], 'GPU')
                    logical_gpus = tf.config.list_logical_devices('GPU')
                    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
                except RuntimeError as e:
                    # Visible devices must be set before GPUs have been initialized
                    print(e)

            mae_model = self.get_model()
            if self.trained_model is not None:
                mae_model.load_weights(self.trained_model)
                self.encoder = tf.keras.Sequential(
                    [
                        layers.Input((self.IMAGE_SIZE, self.IMAGE_SIZE, self.channels_number)),
                        mae_model.patch_layer,
                        mae_model.patch_encoder,
                        mae_model.encoder,
                    ],
                    name="encoder_model",
                )

                self.feature_extraction_model = tf.keras.Sequential(
                    [
                        layers.Input((self.IMAGE_SIZE, self.IMAGE_SIZE, self.channels_number)),
                        mae_model.patch_layer,
                        mae_model.patch_encoder,
                        self.encoder,
                        layers.BatchNormalization(),  # Refer to A.1 (Linear probing)
                        layers.GlobalAveragePooling1D(),
                    ],
                    name="feature_extraction_model",
                )
                self.feature_extraction_model = tf.keras.models.load_model(
                    self.trained_model + 'feature_extraction_model')

            # todo check that
            # self.test_augmentation_model = get_test_augmentation_model(resized_size=self.IMAGE_SIZE)
            # self.test_augmentation_model = get_train_augmentation_model(input_shape=self.INPUT_SHAPE, resized_size=self.IMAGE_SIZE)

            # if self.trained_model is not None:
            #     self.encoder = tf.keras.models.load_model(self.trained_model+'encoder_model')
            #     self.encoder.summary()
            #     # encoder = tf.keras.models.load_model(self.trained_model+'encoder_only')
            #     # #set mask proportion to 0.0 and downstream to True for inference
            #     # MASK_PROPORTION = 0.0
            #     # # image_shape = (self.dim_resized_patch, self.dim_resized_patch, self.channels_number)
            #     # # Pack as a model.
            #     # patch_layer = Patches(image_size=IMAGE_SIZE, patch_size=self.PATCH_SIZE, number_of_channels=self.channels_number)
            #     # patch_encoder = PatchEncoder(patch_size=self.PATCH_SIZE,
            #     #                              projection_dim=self.ENC_PROJECTION_DIM,
            #     #                              mask_proportion=MASK_PROPORTION,
            #     #                              downstream=True,
            #     #                              number_of_channels=self.channels_number)
            #     #
            #     # self.encoder = tf.keras.Sequential(
            #     #     [
            #     #         layers.Input((self.IMAGE_SIZE, self.IMAGE_SIZE, self.channels_number)),
            #     #         patch_layer,
            #     #         patch_encoder,
            #     #         encoder,
            #     #     ],
            #     #     name="encoder_model",
            #     # )
            #
            #     #
            #     # self.feature_extraction_model = tf.keras.Sequential(
            #     #     [
            #     #         layers.Input(image_shape),
            #     #         patch_layer,
            #     #         patch_encoder,
            #     #         self.encoder,
            #     #         layers.BatchNormalization(),  # Refer to A.1 (Linear probing)
            #     #         layers.GlobalAveragePooling1D(),
            #     #     ],
            #     #     name="feature_extraction_model",
            #     # )
            #     self.feature_extraction_model = tf.keras.models.load_model(self.trained_model+'feature_extraction_model')
            #
            #     if self.b_decoder:
            #         self.decoder = tf.keras.models.load_model(self.trained_model+'decoder_only')
            #         self.decoder.summary()
            #     else:
            #         self.decoder = None
            np_config.enable_numpy_behavior()

            # K.clear_session()
            gc.collect()

    def del_network(self):
        print("del mae network")

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        gc.collect

        del self.encoder

        del self.feature_extraction_model

        if self.b_decoder:
            del self.decoder
            self.decoder = None

        tf.reset_default_graph()
        # K._SESSION.close()
        # K._SESSION = None

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        # cuda.select_device(0)
        # cuda.close()
        gc.collect()

        self.encoder = None
        self.base = None

    def extract_single_feature(self, rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, flipped=0,
                               use_rgb=None, use_gray_depth=None, use_SN=None, b_filter=None, dim_grasp_patch=None,
                               dim_resized_patch=None):
        # return extract_single_feature(rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, self.use_rgb, self.use_gray_depth, self.use_SN, self.b_filter, self.dim_grasp_patch, self.dim_resized_patch, flipped=flipped)
        if use_rgb is None:
            use_rgb = self.use_rgb
        if use_gray_depth is None:
            use_gray_depth = self.use_gray_depth
        if use_SN is None:
            use_SN = self.use_SN

        if dim_grasp_patch is None:
            dim_grasp_patch = self.dim_grasp_patch
        if dim_resized_patch is None:
            dim_resized_patch = self.dim_resized_patch

        return extract_single_feature(rgb_scene=rgb_scene, depth_raw=depth_raw, grasp_position_x=grasp_position_x,
                                      grasp_position_y=grasp_position_y, angle_degrees=angle_degrees, use_rgb=use_rgb,
                                      use_gray_depth=use_gray_depth, use_SN=use_SN,
                                      dim_grasp_patch=dim_grasp_patch, dim_resized_patch=dim_resized_patch,
                                      flipped=flipped, max_depth_value=self.max_depth_value)

    def encode_array(self, X, b_raw=False):
        X = np.asarray(X)
        X = X.reshape((-1, self.dim_resized_patch, self.dim_resized_patch, self.channels_number))
        X = self.preprocessing_function(X)

        if self.encoder is None:
            print("encoder not setup in get_features :(")
            self.load_model()
        # print("encode_array X.shape: ", X.shape)

        if self.max_input_size is not None and X.shape[0] > self.max_input_size:
            n_splits = (X.shape[0] // self.max_input_size) + 1
            b_first = True
            for d in np.array_split(X, n_splits):
                if b_first:
                    d_temp = d  # self.test_augmentation_model(d)
                    if b_raw:
                        encoded = self.encoder(d_temp)
                    else:
                        encoded = self.feature_extraction_model(d_temp)
                    b_first = False
                else:
                    d_temp = d  # self.test_augmentation_model(d)
                    if b_raw:
                        encoded_temp = self.encoder(d_temp)
                    else:
                        encoded_temp = self.feature_extraction_model(d_temp)

                    encoded = np.concatenate((encoded, encoded_temp), axis=0)
        else:
            X_temp = X  # self.test_augmentation_model(X)
            if b_raw:
                encoded = self.encoder(X_temp)
            else:
                encoded = self.feature_extraction_model(X_temp)

        gc.collect()
        # encoded = np.array(encoded).reshape((-1, self.projection_dim))

        return encoded

    def encode_candidates_raw(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, base_name=None):
        if flipped is None:
            flipped = [0 for i in range(len(grasp_candidates))]

        missing_X = []
        i = -1
        for g in grasp_candidates:
            # print('encode_candidates_raw: ', g)
            i += 1
            features = self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                   math.degrees(g[8]), flipped=flipped[i])
            if base_name is not None:
                self.save_path(features, base_name + str(i), ending='original.png')

            missing_X.append(features)
        return self.encode_array(missing_X, b_raw=True), None

    def encode_candidates(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, verbose=False):

        if flipped is None:
            flipped = [0 for i in range(len(grasp_candidates))]
        if self.feature_dict_path is None:
            missing_X = []
            i = -1
            for g in grasp_candidates:
                i += 1
                missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                             math.degrees(g[8]), flipped=flipped[i]))

            r = self.encode_array(missing_X)
            return r.numpy(), np.zeros_like(r)
        else:
            if self.encoder is None:
                np_config.enable_numpy_behavior()
            missing_X = []
            missing_keys = []
            i = -1
            for g in grasp_candidates:
                # print("encode_candidates: ", g)
                i += 1

                key_str = str(int(g[1])) + "_" + str(int(g[3])) + "_" + str(int(g[4])) + "_" + str(
                    math.degrees(g[8])) + "_" + str(flipped[i])
                if key_str not in self.encoded_mu_dict:
                    # print("g: ", g)
                    # print("missing_keys key_str: ", key_str)
                    missing_keys.append(key_str)

                    missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                                 math.degrees(g[8]), flipped=flipped[i]))

            if len(missing_X) > 0:
                self.new += len(missing_X)
                # print('predicting  ', len(missing_keys), " missing grasps")
                encoded_missing = self.encode_array(missing_X)

                for i in range(0, len(missing_keys)):
                    key_str = missing_keys[i]
                    temp_encoded = encoded_missing[i, :]

                    self.encoded_mu_dict[key_str] = temp_encoded

            encoded = []
            for g in grasp_candidates:
                # key_str = str(g[0]) + "_" + str(g[5])
                key_str = str(int(g[1])) + "_" + str(int(g[3])) + "_" + str(int(g[4])) + "_" + str(
                    math.degrees(g[8])) + "_" + str(flipped[i])
                if key_str in self.encoded_mu_dict:
                    temp_encoded = self.encoded_mu_dict[key_str]

                    encoded.append(temp_encoded.reshape((self.projection_dim)))

            # #todo remove?
            # if len(missing_X) > 1:
            #     self.save_dicts()

            encoded = np.array(encoded).reshape((-1, self.projection_dim))
            if verbose and self.decoder is not None:
                for j in range(len(grasp_candidates)):
                    x_latent = encoded[j, :]
                    self.save_decoded_patch(x_latent.reshape((-1, self.projection_dim)),
                                            base_name=self.feature_dict_path + str(grasp_candidates[j][1]) + '_' + str(
                                                j))

                    generated = generate_image_with_grasps([grasp_candidates[j]],
                                                           dict_rgb[grasp_candidates[j][1]].copy(),
                                                           b_with_rectangles=True,
                                                           thickness_line=1)

                    cv2.imwrite(self.feature_dict_path + str(grasp_candidates[j][1]) + '_' + str(j) + "_full.png",
                                generated)

            return encoded.numpy(), np.zeros_like(encoded)

    # def post_process_output(self, x, b_between_minus_1=True):
    #     return post_process_output(x,b_between_minus_1)

    def save_path(self, gen_data, base_name, ending='decoded.png'):
        if self.use_rgb:
            # r = batch_x[:, :, :, 0:3]
            r = gen_data[:, :, 0:3]
            rgb_name = base_name + '_rgb_' + ending
            print("saving ", rgb_name)
            cv2.imwrite(rgb_name, r)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

        last_shape = gen_data.shape[-1]

        if last_shape == 7:
            if self.use_gray_depth:
                # gray = batch_x[:, :, :, 3:4]
                gray = gen_data[:, :, 3:4]
                gray_name = base_name + '_gray_depth_' + ending
                print("saving ", gray_name)
                cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

            if self.use_SN:
                # sn = batch_x[:, :, :, 4:]
                sn = gen_data[:, :, 4:]
                sn_name = base_name + '_sn_depth_' + ending
                print("saving ", sn_name)
                cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

        elif last_shape == 4:
            if not self.use_rgb:
                if self.use_gray_depth:
                    # gray = batch_x[:, :, :, 3:4]
                    gray = gen_data[:, :, 0:1]
                    gray_name = base_name + '_gray_depth_' + ending
                    print("saving ", gray_name)
                    cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                if self.use_SN:
                    # sn = batch_x[:, :, :, 4:]
                    sn = gen_data[:, :, 2:]
                    sn_name = base_name + '_sn_depth_' + ending
                    print("saving ", sn_name)
                    cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
            else:
                if self.use_gray_depth:
                    # gray = batch_x[:, :, :, 3:4]
                    gray = gen_data[:, :, 3:4]
                    gray_name = base_name + '_gray_depth_' + ending
                    print("saving ", gray_name)
                    cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

        elif last_shape == 3 and not self.use_rgb:
            if self.use_SN:
                # r = batch_x[:, :, :, 0:3]
                sn = gen_data[:, :, 0:3]
                sn_name = base_name + '_sn_depth_' + ending
                print("saving ", sn_name)
                cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
        else:
            if self.use_gray_depth:
                # gray = batch_x[:, :, :, 3:4]
                gray = gen_data[:, :, 0:1]
                gray_name = base_name + '_gray_depth_' + ending
                print("saving ", gray_name)
                cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

    def save_decoded_patch(self, mu, base_name):
        shape = mu.shape
        if len(shape) == 2:
            mu = np.reshape(mu, (-1, shape[0], shape[1]))
        gen_data = self.decoder(mu)
        # print("1 gen_data.shape: ", gen_data.shape)

        gen_data = self.postprocessing_function(gen_data)

        gen_data = gen_data[0]
        # print("2 gen_data.shape: ", gen_data.shape)
        self.save_path(gen_data, base_name)

    def get_model(self):
        patch_layer = Patches(image_size=self.IMAGE_SIZE, patch_size=self.PATCH_SIZE,
                              number_of_channels=self.channels_number)

        patch_encoder = PatchEncoder(patch_size=self.PATCH_SIZE,
                                     projection_dim=self.ENC_PROJECTION_DIM,
                                     mask_proportion=self.training_MASK_PROPORTION,
                                     downstream=False,
                                     number_of_channels=self.channels_number)

        encoder = create_encoder(enc_num_heads=self.ENC_NUM_HEADS, enc_layers=self.ENC_LAYERS,
                                 enc_projection_dim=self.ENC_PROJECTION_DIM, layer_norm_epsilon=self.LAYER_NORM_EPS,
                                 enc_transformer_units=self.ENC_TRANSFORMER_UNITS)


        decoder = create_decoder(dec_layers=self.DEC_LAYERS, dec_num_heads=self.DEC_NUM_HEADS,
                                 image_size=self.IMAGE_SIZE, number_of_channels=self.channels_number,
                                 num_patches=self.NUM_PATCHES, enc_projection_dim=self.ENC_PROJECTION_DIM,
                                 dec_projection_dim=self.DEC_PROJECTION_DIM, layer_norm_epsilon=self.LAYER_NORM_EPS,
                                 dec_transformer_units=self.DEC_TRANSFORMER_UNITS, preprocessing_function_type=self.preprocessing_function_type)

        # encoder = tf.keras.models.load_model(pretrained_model + 'encoder_only')
        # decoder = tf.keras.models.load_model(pretrained_model + 'decoder_only')

        mae_model = MaskedAutoencoder(
            # train_augmentation_model=test_augmentation_model,
            # test_augmentation_model=test_augmentation_model,
            patch_layer=patch_layer,
            patch_encoder=patch_encoder,
            encoder=encoder,
            decoder=decoder,
        )
        return mae_model

    def fit(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, config_path,
            where_to_save_model):
        # disable_eager_execution()

        dim_resized_patch = self.config.getint('data_modalities', 'dim_resized_patch')
        channels_number = self.config.getint('data_modalities', 'channels_number')
        bottleneck = self.config.getint('feature_extraction', 'projection_dim')

        random_sizes_str = self.config.get('feature_extraction', 'random_sizes')
        random_sizes = random_sizes_str.split('\n')
        random_sizes = [int(s) for s in random_sizes]

        optimizer = self.config.get('feature_extraction', 'optimizer')
        base_learning_rate = self.config.getfloat('feature_extraction', 'base_learning_rate')
        reg = self.config.getfloat('feature_extraction', 'reg')

        number_of_scenes_per_batch = self.config.getint('feature_extraction', 'number_of_scenes_per_batch')
        number_of_labels_per_scenes = self.config.getint('feature_extraction', 'number_of_labels_per_scenes')

        decay_factor = self.config.getfloat('feature_extraction', 'decay_factor')
        scheduler_patience = self.config.getint('feature_extraction', 'scheduler_patience')
        max_epochs = self.config.getint('feature_extraction', 'max_epochs')
        validation_split = self.config.getfloat('feature_extraction', 'validation_split')
        random_seed = self.config.getint('feature_extraction', 'random_seed')
        use_rgb = self.config.getboolean('data_modalities', 'use_rgb')
        use_gray_depth = self.config.getboolean('data_modalities', 'use_gray_depth')
        use_SN = self.config.getboolean('data_modalities', 'use_SN')

        network_name = get_name_network(self.model_type, use_rgb, use_gray_depth, use_SN, reg, channels_number,
                                        bottleneck, dim_resized_patch, random_sizes)

        pretrained_model = self.config.get('feature_extraction', 'pretrained_model')
        if pretrained_model == "None":
            pretrained_model = None

        # network_name = get_name_network(self.model_type, use_rgb, use_gray_depth, use_SN, reg, use_SN_filter,
        #                                 channels_number, bottleneck, dim_resized_patch, random_sizes)

        # train_augmentation_model = get_train_augmentation_model(input_shape=self.INPUT_SHAPE, resized_size=self.IMAGE_SIZE)
        # test_augmentation_model = get_train_augmentation_model(input_shape=self.INPUT_SHAPE, resized_size=self.IMAGE_SIZE)
        # test_augmentation_model = get_test_augmentation_model(resized_size=self.IMAGE_SIZE)

        mae_model = self.get_model()

        pretrained_model = self.config.get('feature_extraction', 'pretrained_model')
        if pretrained_model == "None":
            pretrained_model = None

        if pretrained_model is not None:
            mae_model.load_weights(pretrained_model)

        mae_model.encoder.summary()
        mae_model.decoder.summary()
        # mae_model.summary()

        grasps = []
        for key in dict_scene_grasp.keys():
            # print("key: ", key)
            for g in dict_scene_grasp[key]:
                # print("adding ", g)
                grasps.append(g)

        X_train_grasps, X_val_grasps = train_test_split(grasps, test_size=validation_split,
                                                        random_state=random_seed)
        print('len X_train_grasps: ', len(X_train_grasps))
        print('len X_val_grasps: ', len(X_val_grasps))

        allow_flip = self.config.getboolean('feature_extraction', 'use_augmentation')
        if allow_flip:
            random_angle_augmentation_degrees = [i for i in range(-180, 181, 1)]
        else:
            random_angle_augmentation_degrees = [0.0]

        if number_of_scenes_per_batch == -1:
            number_of_scenes_per_batch = 1
            number_of_scenes_per_batch_val = 1
            my_training_batch_generator = My_Custom_Generator(X_train_grasps, number_of_labels_per_scenes,
                                                              channels_number=channels_number,
                                                              allow_flip=allow_flip,
                                                              preprocessing_fct=self.preprocessing_function,
                                                              random_angles=random_angle_augmentation_degrees,
                                                              final_size=dim_resized_patch,
                                                              random_sizes=random_sizes,
                                                              dic_images_rgb=dict_rgb_scene,
                                                              dic_depth_raw_data=dict_depth_scene,
                                                              use_rgb=use_rgb, use_gray_depth=use_gray_depth,
                                                              use_SN=use_SN,
                                                              b_remember_patch=False,
                                                              max_depth_value=self.max_depth_value)

            number_of_labels_per_scenes_val = number_of_labels_per_scenes
            my_validation_batch_generator = My_Custom_Generator(X_val_grasps, number_of_labels_per_scenes,
                                                                channels_number=channels_number,
                                                                preprocessing_fct=self.preprocessing_function,
                                                                allow_flip=False,
                                                                random_angles=[0.0],
                                                                final_size=dim_resized_patch,
                                                                random_sizes=random_sizes,
                                                                dic_images_rgb=dict_rgb_scene,
                                                                dic_depth_raw_data=dict_depth_scene,
                                                                use_rgb=use_rgb, use_gray_depth=use_gray_depth,
                                                                use_SN=use_SN,
                                                                b_remember_patch=False, verbose=False,
                                                                max_depth_value=self.max_depth_value)

            training_steps = len(my_training_batch_generator)
            validation_steps = len(my_validation_batch_generator)
        else:
            X_train_grasps_dict = {}
            for g in X_train_grasps:
                # print("g: ", g)
                if g[1] not in X_train_grasps_dict:
                    X_train_grasps_dict[g[1]] = []
                X_train_grasps_dict[g[1]].append(g)

            X_val_grasps_dict = {}
            for g in X_val_grasps:
                if g[1] not in X_val_grasps_dict:
                    X_val_grasps_dict[g[1]] = []
                X_val_grasps_dict[g[1]].append(g)

            my_training_batch_generator = My_Custom_Generator_Scenes(X_train_grasps_dict, number_of_labels_per_scenes,
                                                                     number_of_scenes_per_batch,
                                                                     allow_flip=allow_flip,
                                                                     channels_number=channels_number,
                                                                     preprocessing_fct=self.preprocessing_function,
                                                                     random_angles=random_angle_augmentation_degrees,
                                                                     final_size=dim_resized_patch,
                                                                     random_sizes=random_sizes,
                                                                     dic_images_rgb=dict_rgb_scene,
                                                                     dic_depth_raw_data=dict_depth_scene,
                                                                     use_rgb=use_rgb, use_gray_depth=use_gray_depth,
                                                                     use_SN=use_SN,
                                                                     b_remember_patch=False,
                                                                     max_depth_value=self.max_depth_value)
            training_steps = my_training_batch_generator.n
            number_of_labels_per_scenes_val = int(number_of_labels_per_scenes * 0.3)
            if number_of_labels_per_scenes_val > len(X_val_grasps):
                number_of_labels_per_scenes_val = len(X_val_grasps)
            if number_of_labels_per_scenes_val == 0:
                number_of_labels_per_scenes_val = 1
            print('number_of_labels_per_scenes_val: ', number_of_labels_per_scenes_val)
            number_of_scenes_per_batch_val = int(
                number_of_scenes_per_batch * number_of_labels_per_scenes / number_of_labels_per_scenes_val)
            print('number_of_scenes_per_batch_val: ', number_of_scenes_per_batch_val)
            my_validation_batch_generator = My_Custom_Generator_Scenes(X_val_grasps_dict,
                                                                       number_of_labels_per_scenes_val,
                                                                       number_of_scenes_per_batch_val,
                                                                       allow_flip=False,
                                                                       channels_number=channels_number,
                                                                       preprocessing_fct=self.preprocessing_function,
                                                                       random_angles=[0.0],
                                                                       final_size=dim_resized_patch,
                                                                       random_sizes=random_sizes,
                                                                       dic_images_rgb=dict_rgb_scene,
                                                                       dic_depth_raw_data=dict_depth_scene,
                                                                       use_rgb=use_rgb, use_gray_depth=use_gray_depth,
                                                                       use_SN=use_SN,
                                                                       b_remember_patch=False, verbose=False,
                                                                       max_depth_value=self.max_depth_value)
            validation_steps = int(my_validation_batch_generator.n / number_of_scenes_per_batch_val)
            if validation_steps == 0:
                validation_steps = 1

        #

        timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())

        save_dir = where_to_save_model
        dir_path = save_dir + "/" + timestamp + "/"

        if not os.path.exists(save_dir):
            os.mkdir(save_dir)

        if not os.path.exists(dir_path):
            os.mkdir(dir_path)

        copyfile(config_path, dir_path + "config.ini")

        print("saving: X_train_grasps")
        save_obj(X_train_grasps, dir_path + "X_train_grasps.pkl")

        print("saving: X_val_grasps")
        save_obj(X_val_grasps, dir_path + "X_val_grasps.pkl")

        # save_file = os.path.join(dir_path, network_name)

        # training_steps = math.ceil(my_training_batch_generator.n // number_of_scenes_per_batch)

        # model.fit(x=my_training_batch_generator,
        #           epochs=max_epochs,
        #           steps_per_epoch=training_steps,
        #           shuffle=True,
        #           validation_data=my_validation_batch_generator,
        #           validation_steps=validation_steps,
        #           callbacks=[tb, checkpoint, lr_schedule, lr_monitor, lr_early_stopping],
        #           verbose=1)
        test_features, _ = my_validation_batch_generator[0]

        class TrainMonitor(tf.keras.callbacks.Callback):
            def __init__(self, path_to_save_dir, use_rgb, use_gray_depth, use_SN, epoch_interval=None,
                         post_process_fct=None):
                self.epoch_interval = epoch_interval
                self.path_to_save_dir = path_to_save_dir
                self.post_process_fct = post_process_fct
                self.use_rgb = use_rgb
                self.use_gray_depth = use_gray_depth
                self.use_SN = use_SN

            def save_image(self, gen_data, base_name):
                if self.use_rgb:
                    # r = batch_x[:, :, :, 0:3]
                    r = gen_data[:, :, 0:3]
                    rgb_name = base_name + '_rgb.png'
                    print("saving ", rgb_name)
                    cv2.imwrite(rgb_name, r)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                last_shape = gen_data.shape[-1]

                if last_shape == 7:
                    if self.use_gray_depth:
                        # gray = batch_x[:, :, :, 3:4]
                        gray = gen_data[:, :, 3:4]
                        gray_name = base_name + '_gray_depth.png'
                        print("saving ", gray_name)
                        cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                    if self.use_SN:
                        # sn = batch_x[:, :, :, 4:]
                        sn = gen_data[:, :, 4:]
                        sn_name = base_name + '_sn_depth.png'
                        print("saving ", sn_name)
                        cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                elif last_shape == 4:
                    if not self.use_rgb:
                        if self.use_gray_depth:
                            # gray = batch_x[:, :, :, 3:4]
                            gray = gen_data[:, :, 0:1]
                            gray_name = base_name + '_gray_depth.png'
                            print("saving ", gray_name)
                            cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                        if self.use_SN:
                            # sn = batch_x[:, :, :, 4:]
                            sn = gen_data[:, :, 2:]
                            sn_name = base_name + '_sn_depth.png'
                            print("saving ", sn_name)
                            cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                    else:
                        if self.use_gray_depth:
                            # gray = batch_x[:, :, :, 3:4]
                            gray = gen_data[:, :, 3:4]
                            gray_name = base_name + '_gray_depth.png'
                            print("saving ", gray_name)
                            cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                elif last_shape == 3 and not self.use_rgb:
                    if self.use_SN:
                        # r = batch_x[:, :, :, 0:3]
                        sn = gen_data[:, :, 0:3]
                        sn_name = base_name + '_sn_depth.png'
                        print("saving ", sn_name)
                        cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                else:
                    if self.use_gray_depth:
                        # gray = batch_x[:, :, :, 3:4]
                        gray = gen_data[:, :, 0:1]
                        gray_name = base_name + '_gray_depth.png'
                        print("saving ", gray_name)
                        cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

            def on_epoch_end(self, epoch, logs=None):
                if self.epoch_interval and epoch % self.epoch_interval == 0:
                    # np_config.enable_numpy_behavior()
                    # test_augmeneted_images = self.model.test_augmentation_model(test_features)
                    print('test_features.shape: ', test_features.shape)

                    test_augmeneted_images = test_features
                    test_patches = self.model.patch_layer(test_augmeneted_images)

                    (
                        test_unmasked_embeddings,
                        test_masked_embeddings,
                        test_unmasked_positions,
                        test_mask_indices,
                        test_unmask_indices,
                    ) = self.model.patch_encoder(test_patches)

                    test_encoder_outputs = self.model.encoder(test_unmasked_embeddings)
                    test_encoder_outputs = test_encoder_outputs + test_unmasked_positions
                    test_decoder_inputs = tf.concat(
                        [test_encoder_outputs, test_masked_embeddings], axis=1
                    )
                    test_decoder_outputs = self.model.decoder(test_decoder_inputs)

                    # Show a maksed patch image.
                    test_masked_patch, idx = self.model.patch_encoder.show_masked_image(
                        test_patches, test_unmask_indices
                    )
                    print(f"\nIdx chosen: {idx}")
                    original_image = test_augmeneted_images[idx]

                    masked_image = self.model.patch_layer.reconstruct_from_patch(
                        test_masked_patch
                    )
                    reconstructed_image = test_decoder_outputs[idx]
                    original_image = self.post_process_fct(original_image)
                    masked_image = self.post_process_fct(masked_image)
                    reconstructed_image = self.post_process_fct(reconstructed_image)

                    base_name = self.path_to_save_dir + str(epoch) + '_original_'
                    self.save_image(original_image, base_name)
                    base_name = self.path_to_save_dir + str(epoch) + '_masked_'
                    self.save_image(masked_image, base_name)
                    base_name = self.path_to_save_dir + str(epoch) + '_reconstructed_'
                    self.save_image(reconstructed_image, base_name)

                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_original.png', original_brg)
                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_masked.png', masked_image_bgr)
                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_reconstructed.png', reconstructed_image_bgr)

                    # fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))
                    # # ax[0].imshow(original_image)
                    # ax[0].imshow(cv2.cvtColor(np.asarray(original_image), cv2.COLOR_BGR2RGB))
                    # ax[0].set_title(f"Original: {epoch:03d}")
                    #
                    # # ax[1].imshow(masked_image)
                    # ax[1].imshow(cv2.cvtColor(np.asarray(masked_image), cv2.COLOR_BGR2RGB))
                    # ax[1].set_title(f"Masked: {epoch:03d}")
                    #
                    # # ax[2].imshow(reconstructed_image)
                    # ax[2].imshow(cv2.cvtColor(np.asarray(reconstructed_image), cv2.COLOR_BGR2RGB))
                    # ax[2].set_title(f"Resonstructed: {epoch:03d}")
                    #
                    # plt.show()
                    # plt.close()

        # Some code is taken from:
        # https://www.kaggle.com/ashusma/training-rfcx-tensorflow-tpu-effnet-b2.

        total_steps = training_steps * max_epochs
        warmup_steps = int(total_steps * 0.15)

        class WarmUpCosine(LearningRateSchedule):
            def __init__(
                    self, learning_rate_base, total_steps, warmup_learning_rate, warmup_steps
            ):
                super(WarmUpCosine, self).__init__()

                self.learning_rate_base = learning_rate_base
                self.total_steps = total_steps
                self.warmup_learning_rate = warmup_learning_rate
                self.warmup_steps = warmup_steps
                self.pi = tf.constant(np.pi)

            def get_config(self):
                config = {
                    'learning_rate_base': self.learning_rate_base,
                    'total_steps': self.total_steps,
                    'warmup_learning_rate': self.warmup_learning_rate,
                    # 'pi': self.pi,
                    'warmup_steps': self.warmup_steps,
                }
                return config

            def __call__(self, step):
                if self.total_steps < self.warmup_steps:
                    raise ValueError("Total_steps must be larger or equal to warmup_steps.")

                cos_annealed_lr = tf.cos(
                    self.pi
                    * (tf.cast(step, tf.float32) - self.warmup_steps)
                    / float(self.total_steps - self.warmup_steps)
                )
                learning_rate = 0.5 * self.learning_rate_base * (1 + cos_annealed_lr)

                if self.warmup_steps > 0:
                    if self.learning_rate_base < self.warmup_learning_rate:
                        raise ValueError(
                            "Learning_rate_base must be larger or equal to "
                            "warmup_learning_rate."
                        )
                    slope = (
                                    self.learning_rate_base - self.warmup_learning_rate
                            ) / self.warmup_steps
                    warmup_rate = slope * tf.cast(step, tf.float32) + self.warmup_learning_rate
                    learning_rate = tf.where(
                        step < self.warmup_steps, warmup_rate, learning_rate
                    )
                return tf.where(
                    step > self.total_steps, 0.0, learning_rate, name="learning_rate"
                )

        # scheduled_lrs = WarmUpCosine(
        #     learning_rate_base=base_learning_rate,
        #     total_steps=total_steps,
        #     warmup_learning_rate=0.0,
        #     warmup_steps=warmup_steps,
        # )
        # WEIGHT_DECAY = 1e-4
        # optimizer = tfa.optimizers.AdamW(learning_rate=scheduled_lrs, weight_decay=WEIGHT_DECAY)

        optimizer = select_optimizer(optimizer=optimizer, base_learning_rate=base_learning_rate)

        mae_model.compile(
            optimizer=optimizer, loss=keras.losses.MeanSquaredError(), metrics=["mae"], run_eagerly=True
        )

        checkpoint_filepath = dir_path + "/checkpoint"
        train_callbacks = [
            TensorBoard(log_dir=dir_path + "/", write_graph=True, update_freq='epoch', profile_batch=100000000),
            TrainMonitor(dir_path + "/", self.use_rgb, self.use_gray_depth, self.use_SN, epoch_interval=1,
                         post_process_fct=self.postprocessing_function),
            keras.callbacks.ModelCheckpoint(
                checkpoint_filepath,
                monitor="val_loss",
                save_best_only=True,
                save_weights_only=True,
            ),
            ReduceLROnPlateau(monitor='val_loss', factor=decay_factor,
                              mode='min', patience=scheduler_patience, min_lr=1e-08),
            LRTensorBoard(log_dir=dir_path + "/"),
            tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min',
                                                             patience=scheduler_patience*1.5,
                                                             restore_best_weights=True)

        ]

        print('max_epochs: ', max_epochs)
        print('total_steps: ', total_steps)
        print('size batch train: ', number_of_labels_per_scenes * number_of_scenes_per_batch)
        print('training_steps: ', training_steps)
        print('size batch val: ', number_of_labels_per_scenes_val * number_of_scenes_per_batch_val)
        print('validation_steps: ', validation_steps)

        for l in mae_model.layers:
            print(l.name, l.trainable)

        try:
            history = mae_model.fit(x=my_training_batch_generator,
                                    # batch_size=number_of_labels_per_scenes*number_of_scenes_per_batch,
                                    epochs=max_epochs,
                                    steps_per_epoch=training_steps,
                                    shuffle=True,
                                    validation_data=my_validation_batch_generator,
                                    validation_steps=validation_steps,
                                    # validation_batch_size=number_of_labels_per_scenes_val*number_of_scenes_per_batch_val,
                                    callbacks=train_callbacks,
                                    verbose=1)
            # use_multiprocessing=True,
            # workers=8)

        except KeyboardInterrupt:
            print('abort training')

        print('mae_model: ', mae_model)
        mae_model.save(dir_path + "mae_model/", include_optimizer=False)

        # Extract the augmentation layers
        # train_augmentation_model = mae_model.train_augmentation_model
        # test_augmentation_model = mae_model.test_augmentation_model

        # Extract the patchers.
        patch_layer = mae_model.patch_layer
        patch_encoder = mae_model.patch_encoder
        patch_encoder.downstream = True  # Swtich the downstream flag to True.

        # Extract the encoder.
        encoder = mae_model.encoder
        decoder = mae_model.decoder
        # Pack as a model.
        encoder_model = tf.keras.Sequential(
            [
                layers.Input((self.IMAGE_SIZE, self.IMAGE_SIZE, self.channels_number)),
                patch_layer,
                patch_encoder,
                encoder,
            ],
            name="encoder_model",
        )

        encoder_model.summary()
        encoder_model.save(dir_path + "encoder_model/", include_optimizer=False)

        encoder.save(dir_path + "encoder_only/", include_optimizer=False)

        decoder.summary()
        decoder.save(dir_path + "decoder_only/", include_optimizer=False)

        # Pack as a model.
        feature_extraction_model = keras.Sequential(
            [
                layers.Input((self.IMAGE_SIZE, self.IMAGE_SIZE, self.channels_number)),
                patch_layer,
                patch_encoder,
                encoder,
                layers.BatchNormalization(),  # Refer to A.1 (Linear probing)
                layers.GlobalAveragePooling1D(),
            ],
            name="feature_extraction_model",
        )

        feature_extraction_model.summary()
        feature_extraction_model.save(dir_path + "feature_extraction_model/", include_optimizer=False)

    def test_fit(self):

        timestamp = datetime.utcnow().strftime("%y%m%d-%H%M%S")
        INPUT_SHAPE = (32, 32, 3)
        IMAGE_SIZE = 48  # We'll resize input images to this size.
        PATCH_SIZE = 6  # Size of the patches to be extract from the input images.
        NUM_PATCHES = (IMAGE_SIZE // PATCH_SIZE) ** 2

        ENC_PROJECTION_DIM = 128
        MASK_PROPORTION = 0.75
        ENC_NUM_HEADS = 4
        ENC_LAYERS = 3
        LAYER_NORM_EPS = 1e-6
        ENC_TRANSFORMER_UNITS = [
            ENC_PROJECTION_DIM * 2,
            ENC_PROJECTION_DIM,
        ]  # Size of the transformer layers.

        DEC_PROJECTION_DIM = 64
        DEC_NUM_HEADS = 4
        DEC_LAYERS = 1  # The decoder is lightweight but should be reasonably deep for reconstruction.
        DEC_TRANSFORMER_UNITS = [
            DEC_PROJECTION_DIM * 2,
            DEC_PROJECTION_DIM,
        ]

        encoder_model_model_path = 'encoder_model_model211128-093656/'
        decoder_model_model_path = 'decoder_model211128-093656/'
        feature_extraction_model_path = 'feature_extraction_model211128-093656/'

        # train_augmentation_model = get_train_augmentation_model(input_shape=INPUT_SHAPE, resized_size=IMAGE_SIZE)
        # test_augmentation_model = get_test_augmentation_model(resized_size=IMAGE_SIZE)
        patch_layer = Patches(image_size=IMAGE_SIZE, patch_size=PATCH_SIZE, number_of_channels=3)
        patch_encoder = PatchEncoder(patch_size=PATCH_SIZE,
                                     projection_dim=ENC_PROJECTION_DIM,
                                     mask_proportion=MASK_PROPORTION,
                                     downstream=False,
                                     number_of_channels=3)

        encoder = create_encoder(enc_num_heads=ENC_NUM_HEADS, enc_layers=ENC_LAYERS,
                                 enc_projection_dim=ENC_PROJECTION_DIM, layer_norm_epsilon=LAYER_NORM_EPS,
                                 enc_transformer_units=ENC_TRANSFORMER_UNITS)



        decoder = create_decoder(dec_layers=DEC_LAYERS, dec_num_heads=DEC_NUM_HEADS, image_size=IMAGE_SIZE,
                                 number_of_channels=3,
                                 num_patches=NUM_PATCHES, enc_projection_dim=ENC_PROJECTION_DIM,
                                 dec_projection_dim=DEC_PROJECTION_DIM, layer_norm_epsilon=LAYER_NORM_EPS,
                                 dec_transformer_units=DEC_TRANSFORMER_UNITS, preprocessing_function_type=self.preprocessing_function_type)

        mae_model = MaskedAutoencoder(
            # train_augmentation_model=train_augmentation_model,
            # test_augmentation_model=test_augmentation_model,
            patch_layer=patch_layer,
            patch_encoder=patch_encoder,
            encoder=encoder,
            decoder=decoder,
        )

        mae_model.encoder.summary()
        mae_model.decoder.summary()

        (x_train, y_train), (x_test, y_test) = keras.datasets.cifar10.load_data()
        (x_train, y_train), (x_val, y_val) = (
            (x_train[:40000], y_train[:40000]),
            (x_train[40000:], y_train[40000:]),
        )
        print(f"Training samples: {len(x_train)}")
        print(f"Validation samples: {len(x_val)}")
        print(f"Testing samples: {len(x_test)}")

        # DATA
        BUFFER_SIZE = 1024
        BATCH_SIZE = 256
        AUTO = tf.data.AUTOTUNE

        # OPTIMIZER
        LEARNING_RATE = 5e-3
        WEIGHT_DECAY = 1e-4

        # TRAINING
        EPOCHS = 100

        train_ds = tf.data.Dataset.from_tensor_slices((x_train))
        train_ds = train_ds.shuffle(BUFFER_SIZE).batch(BATCH_SIZE).prefetch(AUTO)

        val_ds = tf.data.Dataset.from_tensor_slices((x_val))
        val_ds = val_ds.batch(BATCH_SIZE).prefetch(AUTO)

        test_ds = tf.data.Dataset.from_tensor_slices((x_test))
        test_ds = test_ds.batch(BATCH_SIZE).prefetch(AUTO)

        # Taking a batch of test inputs to measure model's progress.
        test_features = next(iter(test_ds))

        class TrainMonitor(tf.keras.callbacks.Callback):
            def __init__(self, path_to_save_dir, use_rgb, use_gray_depth, use_SN, epoch_interval=None,
                         post_process_fct=None):
                self.epoch_interval = epoch_interval
                self.path_to_save_dir = path_to_save_dir
                self.post_process_fct = post_process_fct
                self.use_rgb = use_rgb
                self.use_gray_depth = use_gray_depth
                self.use_SN = use_SN

            def save_image(self, gen_data, base_name):
                if self.use_rgb:
                    # r = batch_x[:, :, :, 0:3]
                    r = gen_data[:, :, 0:3]
                    rgb_name = base_name + '_rgb.png'
                    print("saving ", rgb_name)
                    cv2.imwrite(rgb_name, r)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                last_shape = gen_data.shape[-1]

                if last_shape == 7:
                    if self.use_gray_depth:
                        # gray = batch_x[:, :, :, 3:4]
                        gray = gen_data[:, :, 3:4]
                        gray_name = base_name + '_gray_depth.png'
                        print("saving ", gray_name)
                        cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                    if self.use_SN:
                        # sn = batch_x[:, :, :, 4:]
                        sn = gen_data[:, :, 4:]
                        sn_name = base_name + '_sn_depth.png'
                        print("saving ", sn_name)
                        cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                elif last_shape == 4:
                    if not self.use_rgb:
                        if self.use_gray_depth:
                            # gray = batch_x[:, :, :, 3:4]
                            gray = gen_data[:, :, 0:1]
                            gray_name = base_name + '_gray_depth.png'
                            print("saving ", gray_name)
                            cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                        if self.use_SN:
                            # sn = batch_x[:, :, :, 4:]
                            sn = gen_data[:, :, 2:]
                            sn_name = base_name + '_sn_depth.png'
                            print("saving ", sn_name)
                            cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                    else:
                        if self.use_gray_depth:
                            # gray = batch_x[:, :, :, 3:4]
                            gray = gen_data[:, :, 3:4]
                            gray_name = base_name + '_gray_depth.png'
                            print("saving ", gray_name)
                            cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                elif last_shape == 3 and not self.use_rgb:
                    if self.use_SN:
                        # r = batch_x[:, :, :, 0:3]
                        sn = gen_data[:, :, 0:3]
                        sn_name = base_name + '_sn_depth.png'
                        print("saving ", sn_name)
                        cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                else:
                    if self.use_gray_depth:
                        # gray = batch_x[:, :, :, 3:4]
                        gray = gen_data[:, :, 0:1]
                        gray_name = base_name + '_gray_depth.png'
                        print("saving ", gray_name)
                        cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

            def on_epoch_end(self, epoch, logs=None):
                if self.epoch_interval and epoch % self.epoch_interval == 0:
                    # np_config.enable_numpy_behavior()
                    # test_augmeneted_images = self.model.test_augmentation_model(test_features)
                    print('test_features.shape: ', test_features.shape)

                    test_augmeneted_images = test_features
                    test_patches = self.model.patch_layer(test_augmeneted_images)

                    (
                        test_unmasked_embeddings,
                        test_masked_embeddings,
                        test_unmasked_positions,
                        test_mask_indices,
                        test_unmask_indices,
                    ) = self.model.patch_encoder(test_patches)

                    test_encoder_outputs = self.model.encoder(test_unmasked_embeddings)
                    test_encoder_outputs = test_encoder_outputs + test_unmasked_positions
                    test_decoder_inputs = tf.concat(
                        [test_encoder_outputs, test_masked_embeddings], axis=1
                    )
                    test_decoder_outputs = self.model.decoder(test_decoder_inputs)

                    # Show a maksed patch image.
                    test_masked_patch, idx = self.model.patch_encoder.show_masked_image(
                        test_patches, test_unmask_indices
                    )
                    print(f"\nIdx chosen: {idx}")
                    original_image = test_augmeneted_images[idx]

                    masked_image = self.model.patch_layer.reconstruct_from_patch(
                        test_masked_patch
                    )
                    reconstructed_image = test_decoder_outputs[idx]
                    original_image = self.post_process_fct(original_image)
                    masked_image = self.post_process_fct(masked_image)
                    reconstructed_image = self.post_process_fct(reconstructed_image)

                    base_name = self.path_to_save_dir + str(epoch) + '_original_'
                    self.save_image(original_image, base_name)
                    base_name = self.path_to_save_dir + str(epoch) + '_masked_'
                    self.save_image(masked_image, base_name)
                    base_name = self.path_to_save_dir + str(epoch) + '_reconstructed_'
                    self.save_image(reconstructed_image, base_name)

                    # self.path_to_save_dir
                    #
                    # original_brg = cv2.cvtColor(np.asarray(original_image*255.0), cv2.COLOR_BGR2RGB)
                    # masked_image_bgr = cv2.cvtColor(np.asarray(masked_image*255.0), cv2.COLOR_BGR2RGB)
                    # reconstructed_image_bgr = cv2.cvtColor(np.asarray(reconstructed_image*255.0), cv2.COLOR_BGR2RGB)
                    original_image = self.post_process_fct(original_image)
                    base_name = self.path_to_save_dir + str(epoch) + '_original_'
                    self.save_image(original_image, base_name)
                    # masked_image_bgr = np.asarray(masked_image*255.0)
                    gen_data = self.post_process_fct(reconstructed_image)
                    base_name = self.path_to_save_dir + str(epoch) + '_reconstructed_'
                    self.save_image(gen_data, base_name)

                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_original.png', original_brg)
                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_masked.png', masked_image_bgr)
                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_reconstructed.png', reconstructed_image_bgr)

                    # fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))
                    # # ax[0].imshow(original_image)
                    # ax[0].imshow(cv2.cvtColor(np.asarray(original_image), cv2.COLOR_BGR2RGB))
                    # ax[0].set_title(f"Original: {epoch:03d}")
                    #
                    # # ax[1].imshow(masked_image)
                    # ax[1].imshow(cv2.cvtColor(np.asarray(masked_image), cv2.COLOR_BGR2RGB))
                    # ax[1].set_title(f"Masked: {epoch:03d}")
                    #
                    # # ax[2].imshow(reconstructed_image)
                    # ax[2].imshow(cv2.cvtColor(np.asarray(reconstructed_image), cv2.COLOR_BGR2RGB))
                    # ax[2].set_title(f"Resonstructed: {epoch:03d}")
                    #
                    # plt.show()
                    # plt.close()

        # Some code is taken from:
        # https://www.kaggle.com/ashusma/training-rfcx-tensorflow-tpu-effnet-b2.

        class WarmUpCosine(keras.optimizers.schedules.LearningRateSchedule):
            def __init__(
                    self, learning_rate_base, total_steps, warmup_learning_rate, warmup_steps
            ):
                super(WarmUpCosine, self).__init__()

                self.learning_rate_base = learning_rate_base
                self.total_steps = total_steps
                self.warmup_learning_rate = warmup_learning_rate
                self.warmup_steps = warmup_steps
                self.pi = tf.constant(np.pi)

            def __call__(self, step):
                if self.total_steps < self.warmup_steps:
                    raise ValueError("Total_steps must be larger or equal to warmup_steps.")

                cos_annealed_lr = tf.cos(
                    self.pi
                    * (tf.cast(step, tf.float32) - self.warmup_steps)
                    / float(self.total_steps - self.warmup_steps)
                )
                learning_rate = 0.5 * self.learning_rate_base * (1 + cos_annealed_lr)

                if self.warmup_steps > 0:
                    if self.learning_rate_base < self.warmup_learning_rate:
                        raise ValueError(
                            "Learning_rate_base must be larger or equal to "
                            "warmup_learning_rate."
                        )
                    slope = (
                                    self.learning_rate_base - self.warmup_learning_rate
                            ) / self.warmup_steps
                    warmup_rate = slope * tf.cast(step, tf.float32) + self.warmup_learning_rate
                    learning_rate = tf.where(
                        step < self.warmup_steps, warmup_rate, learning_rate
                    )
                return tf.where(
                    step > self.total_steps, 0.0, learning_rate, name="learning_rate"
                )

        # total_steps = int((len(x_train) / BATCH_SIZE) * EPOCHS)
        # warmup_steps = int(total_steps * 0.15)
        # scheduled_lrs = WarmUpCosine(
        #     learning_rate_base=LEARNING_RATE,
        #     total_steps=total_steps,
        #     warmup_learning_rate=0.0,
        #     warmup_steps=warmup_steps,
        # )

        timestamp = datetime.utcnow().strftime("%y%m%d-%H%M%S")

        def postprocessing_between0and1(x):
            # print('postprocessing_between0and1 x.shape: ', x.shape)
            x = np.asarray(x, dtype=np.float32)
            x = np.uint8(x * 255)
            x = np.clip(x, 0.0, 255.0)

            return x

        train_callbacks = [
            keras.callbacks.TensorBoard(log_dir=f"mae_logs_{timestamp}", profile_batch=100000000),
            # SaveMonitor(epoch_interval=5)
            # tf.keras.callbacks.ModelCheckpoint(
            #     f"mae_model_{timestamp}", monitor='val_loss', verbose=0, save_best_only=True,
            #     save_weights_only=False, mode='auto', save_freq='epoch',
            #     options=None),
            TrainMonitor(f"mae_logs_{timestamp}" + "/", True, False, False, epoch_interval=1,
                         post_process_fct=postprocessing_between0and1),
        ]

        optimizer = tfa.optimizers.AdamW(learning_rate=scheduled_lrs, weight_decay=WEIGHT_DECAY)

        mae_model.compile(
            optimizer=optimizer, loss=keras.losses.MeanSquaredError(), metrics=["mae"], run_eagerly=True
        )

        # history = mae_model.fit(
        #     train_ds, epochs=EPOCHS, validation_data=val_ds, callbacks=train_callbacks,
        # )
        history = mae_model.fit(x=train_ds,
                                batch_size=256,
                                # epochs=max_epochs,
                                # steps_per_epoch=training_steps,
                                shuffle=True,
                                validation_data=val_ds,
                                # validation_steps=validation_steps,
                                validation_batch_size=256,
                                callbacks=train_callbacks,
                                verbose=1)

        loss, mae = mae_model.evaluate(test_ds)
        print(f"Loss: {loss:.2f}")
        print(f"MAE: {mae:.2f}")

        # Extract the augmentation layers.
        # train_augmentation_model = mae_model.train_augmentation_model
        # test_augmentation_model = mae_model.test_augmentation_model

        # Extract the patchers.
        patch_layer = mae_model.patch_layer
        patch_encoder = mae_model.patch_encoder
        patch_encoder.downstream = True  # Swtich the downstream flag to True.

        # Extract the encoder.
        encoder = mae_model.encoder
        decoder = mae_model.decoder
        # Pack as a model.
        encoder_model = keras.Sequential(
            [
                layers.Input((IMAGE_SIZE, IMAGE_SIZE, 3)),
                patch_layer,
                patch_encoder,
                encoder,
            ],
            name="encoder_model",
        )

        encoder_model.summary()
        encoder_model.save(f"encoder_model_{timestamp}", include_optimizer=False)

        encoder.save(f"encoder_only_{timestamp}", include_optimizer=False)

        decoder.summary()
        decoder.save(f"decoder_model_{timestamp}", include_optimizer=False)

        # Pack as a model.
        feature_extraction_model = keras.Sequential(
            [
                layers.Input((IMAGE_SIZE, IMAGE_SIZE, 3)),
                patch_layer,
                patch_encoder,
                encoder,
                layers.BatchNormalization(),  # Refer to A.1 (Linear probing)
                layers.GlobalAveragePooling1D(),
            ],
            name="feature_extraction_model",
        )

        feature_extraction_model.summary()
        feature_extraction_model.save(f"feature_extraction_model_{timestamp}", include_optimizer=False)


if __name__ == '__main__':
    Test = MAE_Class_RGBDSN()
    Test.test_fit()