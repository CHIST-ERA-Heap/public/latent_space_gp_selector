import cv2
# from skimage.feature import canny
from scipy import ndimage as ndi
from skimage.morphology import skeletonize, medial_axis, remove_small_objects
from skimage.segmentation import felzenszwalb
# import pandas as pd
from skimage import measure
import configparser
import numpy as np
from sklearn.cluster import MiniBatchKMeans
from PIL import Image
import open3d as o3d

def get_submasks_rgb(current_scene_rgb_image, current_mask, n_clusters=3):
    sub_masks = []
    reconstructed_image = cv2.bitwise_and(current_scene_rgb_image, current_scene_rgb_image, mask=current_mask)
    quant = color_quantization(reconstructed_image, n_clusters=n_clusters)
    # cv2.imwrite('./temp_quant.png', quant)

    # print('quant.shape: ', quant.shape)
    try:
        # colors = np.unique(quant.reshape(-1, ), axis=0)
        colors = np.unique(quant.reshape(-1, quant.shape[2]), axis=0)
    except:
        colors = np.unique(quant.reshape(-1, quant.shape[2]), axis=0)

    # print('colors.shape: ', colors.shape)

    # first = True
    for c in colors:
        # print('c: ', c)
        # if first:
        #     first = False
        # else:
        # if c > 0 and c is not False:
        mask = np.zeros((quant.shape[0], quant.shape[1], 1))
        # mask[np.where(quant == c)] = 255
        mask[np.where((quant == c).all(axis=-1))] = 255
        mask = np.reshape(mask, (quant.shape[0], quant.shape[1])).astype('uint8')
        mask[np.where(current_mask == 0)] = 0

        sub_masks.append(mask)

    return sub_masks

def get_grayscale_depth_from_raw(dst_d, min_depth=None, max_depth=None, b_Xue=False, missing_value=0, b_invert=False, max_depth_value = 0.7, blur=False):#https://arxiv.org/pdf/1604.05817.pdf

    invalid_px = np.where(dst_d > max_depth_value)
    dst_d[invalid_px] = max_depth_value

    if b_Xue:
        # dst_d = np.reshape(dst_d, (dst_d.shape[0], dst_d.shape[1], 1))
        im_data = cv2.copyMakeBorder(dst_d, 1, 1, 1, 1, cv2.BORDER_DEFAULT)
        mask = (im_data == missing_value).astype(np.uint8)

        # Scale to keep as float, but has to be in bounds -1:1 to keep opencv happy.
        scale = np.abs(im_data).max()
        # print('scale: ', scale)
        im_data = im_data.astype(np.float32) / scale  # Has to be float32, 64 not supported.
        im_data = cv2.inpaint(im_data, mask, 1, cv2.INPAINT_NS)

        im_data = im_data.astype(np.float32)
        im_data = np.reshape(im_data, (im_data.shape[0], im_data.shape[1], 1))

        # Back to original size and value range.
        im_data = im_data[1:-1, 1:-1]
        im_data = im_data * scale
        im_data = np.asarray(im_data)
        im_data = im_data.reshape((im_data.shape[0], im_data.shape[1], 1))
        # print("min im_data: ", np.min(im_data))
        # print("mean im_data: ", np.mean(im_data))
        # print("max im_data: ", np.max(im_data))

    else:
        twobyte = False
        normalize = True
        BINARY_IM_MAX_VAL = np.iinfo(np.uint8).max
        max_val = BINARY_IM_MAX_VAL
        if twobyte:
            max_val = np.iinfo(np.uint16).max

        if normalize:
            min_depth = np.min(dst_d)
            max_depth = np.max(dst_d)
            depth_data = (dst_d - min_depth) / (max_depth - min_depth)
            depth_data = float(max_val) * depth_data.squeeze()
        else:
            zero_px = np.where(dst_d == 0)
            zero_px = np.c_[zero_px[0], zero_px[1], zero_px[2]]
            depth_data = ((dst_d - min_depth) * \
                          (float(max_val) / (max_depth - min_depth))).squeeze()
            depth_data[zero_px[:, 0], zero_px[:, 1]] = 0
        im_data = np.zeros([dst_d.shape[0], dst_d.shape[1], 3])
        im_data[:, :, 0] = depth_data
        im_data[:, :, 1] = depth_data
        im_data[:, :, 2] = depth_data
        im_data = im_data.astype(np.uint8)


       # generate depth grayscale patch
        # cf autolab perception

        # BINARY_IM_MAX_VAL = np.iinfo(np.uint8).max
        #
        # max_val = BINARY_IM_MAX_VAL
        #
        # min_depth = 0.0
        # # if min_depth is None:
        # #     min_depth = np.min(dst_d)
        #
        # if max_depth is None:
        #     max_depth = np.max(dst_d)
        #
        # depth_data = (dst_d - min_depth) / (max_depth - min_depth)
        # depth_data = float(max_val) * depth_data.squeeze()
        #
        # try:
        #     zero_px = np.where(dst_d == 0)
        #     zero_px = np.c_[zero_px[0], zero_px[1], zero_px[2]]
        # except(IndexError):
        #     pass
        #
        # depth_data = ((dst_d - min_depth) * \
        #               (float(max_val) / (max_depth - min_depth))).squeeze()
        # try:
        #     depth_data[zero_px[:, 0], zero_px[:, 1]] = 0
        # except(IndexError, TypeError):
        #     pass
        #
        # im_data = np.zeros([depth_data.shape[0], depth_data.shape[1], 3]).astype(np.uint8)
        # im_data[:, :, 0] = depth_data
        # im_data[:, :, 1] = depth_data
        # im_data[:, :, 2] = depth_data

        # if twobyte:
        #     return im_data.astype(np.uint16)
        # return im_data.astype(np.uint8)

        # depth_image = im_data.astype(np.uint8)
    if b_invert:
        im_data = 255.0 - im_data


    depth_image = im_data.astype(np.uint8)


    # print("depth_image.shape: ", depth_image.shape)
    if (depth_image.shape[2] == 3):
        depth_image = depth_image[:, :, 0]

    if blur:
        kernel = np.ones((5, 5), np.float32) / 25
        depth_image = cv2.filter2D(depth_image, -1, kernel)

    return depth_image

def color_quantization(image, n_clusters=3):
    (h, w) = image.shape[:2]
    # convert the image from the RGB color space to the L*a*b*
    # color space -- since we will be clustering using k-means
    # which is based on the euclidean distance, we'll use the
    # L*a*b* color space where the euclidean distance implies
    # perceptual meaning
    image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    # reshape the image into a feature vector so that k-means
    # can be applied
    image = image.reshape((image.shape[0] * image.shape[1], 3))
    # apply k-means using the specified number of clusters and
    # then create the quantized image based on the predictions
    clt = MiniBatchKMeans(n_clusters=n_clusters)
    labels = clt.fit_predict(image)
    quant = clt.cluster_centers_.astype("uint8")[labels]
    # reshape the feature vectors to images
    quant = quant.reshape((h, w, 3))
    # image = image.reshape((h, w, 3))
    # convert from L*a*b* to RGB
    quant = cv2.cvtColor(quant, cv2.COLOR_LAB2BGR)
    # image = cv2.cvtColor(image, cv2.COLOR_LAB2BGR)
    return quant

def get_submasks_depth(current_scene_raw_depth_data, current_mask, n_clusters=3):
    # usual = np.median(current_scene_raw_depth_data)
    usual = np.mean(current_scene_raw_depth_data)

    depth_temp = current_scene_raw_depth_data.copy()
    depth_temp[np.where(current_mask == 0)] = usual
    # print('depth_temp.shape: ', depth_temp.shape)

    depth_image = get_grayscale_depth_from_raw(depth_temp, b_invert=False)
    depth_image = cv2.cvtColor(depth_image, cv2.COLOR_GRAY2BGR)

    sub_masks = []
    reconstructed_image = cv2.bitwise_and(depth_image, depth_image, mask=current_mask)
    quant = color_quantization(reconstructed_image, n_clusters=n_clusters)
    # cv2.imwrite('./temp_quant.png', quant)

    # print('quant.shape: ', quant.shape)
    try:
        # colors = np.unique(quant.reshape(-1, ), axis=0)
        colors = np.unique(quant.reshape(-1, quant.shape[2]), axis=0)
    except:
        colors = np.unique(quant.reshape(-1, quant.shape[2]), axis=0)

    # print('colors.shape: ', colors.shape)

    # first = True
    for c in colors:
        # if first:
        #     first = False
        # else:
        # print('c: ', c)
        # if c > 0 and c is not False:
        mask = np.zeros((quant.shape[0], quant.shape[1], 1))
        # mask[np.where(quant == c)] = 255
        mask[np.where((quant == c).all(axis=-1))] = 255
        mask = np.reshape(mask, (quant.shape[0], quant.shape[1])).astype('uint8')
        mask[np.where(current_mask == 0)] = 0

        sub_masks.append(mask)

    return sub_masks

def get_crop_mask(height, width, crop_left, crop_right, crop_top, crop_bottom, scale_factor=1.0):
    crop_left = int(crop_left * scale_factor)
    crop_right = int(crop_right * scale_factor)
    crop_top = int(crop_top * scale_factor)
    crop_bottom = int(crop_bottom * scale_factor)

    crop = [crop_left, crop_right, crop_top, crop_bottom, width, height]
    cropping_mask = np.ones([height, width])*255

    # collumn left
    for x in range(0, height):
        for y in range(0, crop_left):
            # print("(",x,",",y,")")
            # print(depth_policy[x][y])
            cropping_mask[x][y] = 0

    # collumn right
    for x in range(0, height):
        for y in range(width - crop_right, width):
            cropping_mask[x][y] = 0

    # line top
    for x in range(0, crop_top):
        for y in range(0, width):
            cropping_mask[x][y] = 0

    # line bottom
    for x in range(height - crop_bottom, height):
        for y in range(0, width):
            cropping_mask[x][y] = 0

    return cropping_mask, crop

# def closed_edges_mask(gray, canny_thresh1=255, canny_thresh2=100, GRID_SIZE=40, clipLimit=2.0):
def closed_edges_mask(gray, canny_thresh1 = 196, canny_thresh2 = 255 , GRID_SIZE = 100, clipLimit=2.0):

    clahe = cv2.createCLAHE(clipLimit, tileGridSize=(GRID_SIZE, GRID_SIZE))
    gray = clahe.apply(gray)

    edges = cv2.Canny(gray, canny_thresh1, canny_thresh2)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))  # 3 good
    edges = cv2.dilate(edges, kernel, iterations=1)

    # edges.shape:  (720, 1280)
    contour_mask_filed = ndi.binary_fill_holes(edges)
    contour_mask_filed = np.asarray(contour_mask_filed).astype(np.uint8)

    # print("contour_mask_filed.shape: ", contour_mask_filed.shape)
    # print("contour_mask_filed max: ", np.max(contour_mask_filed))
    # print("contour_mask_filed min: ", np.max(contour_mask_filed))
    return contour_mask_filed

def depth_segmentation(depth_data, usual_object_depth_range):
    mask = np.ones((depth_data.shape[0], depth_data.shape[1], 1))*255.0

    where_lower_superior_threshold = np.where(depth_data > usual_object_depth_range[0])
    # print('removing object further than ', usual_object_depth_range[0], 'meters')
    mask[where_lower_superior_threshold] = 0

    #sometimes the depth is buggy
    where_very_far = np.where(depth_data > usual_object_depth_range[1])
    mask[where_very_far] = 255.0

    mask = np.reshape(mask,(depth_data.shape[0], depth_data.shape[1])).astype(np.uint8)

    return mask

def background_subtraction(background_image, foreground_image):
    """Creates a binary image from a background subtraction of the foreground using cv2.BackgroundSubtractorMOG().
    The binary image returned is a mask that should contain mostly foreground pixels.
    The background image should be the same background as the foreground image except not containing the object
    of interest.
    Images must be of the same size and type.
    If not, larger image will be taken and downsampled to smaller image size.
    If they are of different types, an error will occur.
    Inputs:
    background_image       = img object, RGB or binary/grayscale/single-channel
    foreground_image       = img object, RGB or binary/grayscale/single-channel
    device                 = device number. Used to count steps in the pipeline
    debug                  = None, print, or plot. Print = save to file, Plot = print to screen.
    Returns:
    device                 = device number
    fgmask                 = background subtracted foreground image (mask)
    :param background_image: numpy array
    :param foreground_image: numpy array
    :param device: int
    :param debug: str
    :return device: int
    :return fgmask: numpy array
    """

    # Copying images to make sure not alter originals
    bg_img = np.copy(background_image)
    fg_img = np.copy(foreground_image)
    # Checking if images need to be resized or error raised
    if bg_img.shape != fg_img.shape:
        # If both images are not 3 channel or single channel then raise error.
        if len(bg_img.shape) != len(fg_img.shape):
            print("Images must both be single-channel/grayscale/binary or RGB")
        # Forcibly resizing largest image to smallest image
        print("WARNING: Images are not of same size.\nResizing")
        if bg_img.shape > fg_img.shape:
            width, height = fg_img.shape[1], fg_img.shape[0]
            bg_img = cv2.resize(bg_img, (width, height), interpolation=cv2.INTER_AREA)
        else:
            width, height = bg_img.shape[1], bg_img.shape[0]
            fg_img = cv2.resize(fg_img, (width, height), interpolation=cv2.INTER_AREA)

    # Instantiating the background subtractor, for a single history no default parameters need to be changed.
    # bgsub = cv2.BackgroundSubtractorMOG()
    bgsub = cv2.createBackgroundSubtractorMOG2()

    # Applying the background image to the background subtractor first.
    # Anything added after is subtracted from the previous iterations.
    # fgmask = bgsub.apply(bg_img)
    fgmask = bgsub.apply(bg_img)

    # Applying the foreground image to the background subtractor (therefore removing the background)
    fgmask = bgsub.apply(fg_img)

    return fgmask

def saliency_segmentation(image):
    # v2 SALIENCY
    # initialize OpenCV's static fine grained saliency detector and
    # compute the saliency map
    """"
    cv2.saliency.ObjectnessBING_create()
    cv2.saliency.StaticSaliencySpectralResidual_create()
    cv2.saliency.StaticSaliencyFineGrained_create()
    cv2.saliency.MotionSaliencyBinWangApr2014_create()
    """

    # # saliency = cv2.saliency.StaticSaliencyFineGrained_create()
    # saliency = cv2.saliency.StaticSaliencySpectralResidual_create()
    #
    # (success, saliencyMap) = saliency.computeSaliency(image)
    #
    # # if we would like a *binary* map that we could process for contours,
    # # compute convex hull's, extract bounding boxes, etc., we can
    # # additionally threshold the saliency map
    #
    # # TODO check Otsu’s Binarization
    # threshMap1 = cv2.threshold((saliencyMap * 255).astype('uint8'), 0, 255,
    #                           cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    try:
        saliency = cv2.saliency.StaticSaliencyFineGrained_create()
        # saliency = cv2.saliency.StaticSaliencySpectralResidual_create()
    except Exception as e:
        print(e)
        saliency = cv2.saliency.StaticSaliencySpectralResidual_create()

    (success, saliencyMap) = saliency.computeSaliency(image)

    # if we would like a *binary* map that we could process for contours,
    # compute convex hull's, extract bounding boxes, etc., we can
    # additionally threshold the saliency map

    # TODO check Otsu’s Binarization
    threshMap2 = cv2.threshold((saliencyMap * 255).astype('uint8'), 0, 255,
                               cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    threshMap2 = np.reshape(threshMap2, (image.shape[0], image.shape[1], 1)).astype('uint8')

    return threshMap2

def felzenszwalb_segmentation(image):
    # image_felzenszwalb = ndi.median_filter(image, size=20)

    segments_fz = np.uint8(felzenszwalb(image,scale=100, sigma=0.5, min_size=50))

    # segments_fz[cropping_mask > 0] = np.min(segments_fz)
    segments_fz_normed = cv2.normalize(segments_fz, segments_fz, 0, 255, cv2.NORM_MINMAX)
    # print("segments_fz.unique: ", np.unique(segments_fz))
    #
    # print("segments_fz.mean: ", np.mean(segments_fz))
    # print("segments_fz.max: ", np.max(segments_fz))
    # print("segments_fz.min: ", np.min(segments_fz))

    thresh = 127
    segments_fz_normed_bw = cv2.threshold(segments_fz_normed, thresh, 255, cv2.THRESH_BINARY)[1]
    # mask = np.zeros((image.shape[0], image.shape[1]))
    # mask[segments_fz_normed_bw > 30] = 255

    # mask = np.reshape(mask,(image.shape[0], image.shape[1])).astype(np.uint8)
    return segments_fz_normed_bw, segments_fz

def get_HSV_segmentation_mask(image, lower_hsv, upper_hsv, verbose=True):
    if verbose:
        print('lower_hsv: ', lower_hsv)
        print('upper_hsv: ', upper_hsv)

    image_seg = image.copy()
    image_seg = cv2.blur(image_seg, (5, 5))
    image_seg = cv2.normalize(image_seg, image_seg, 0, 255, cv2.NORM_MINMAX)

    image_hsv = cv2.cvtColor(image_seg, cv2.COLOR_BGR2HSV)
    return cv2.bitwise_not(cv2.inRange(src=image_hsv, lowerb=lower_hsv, upperb=upper_hsv))

def convert_bgrd_to_pcl(bgr_image, depth_image, cam_info, rgb=False, to_ros_msg=False,
                        convert_depth_from_mm_to_m=False):
    # cx = cam_info.K[2]
    # cy = cam_info.K[5]
    # fx = cam_info.K[0]
    # fy = cam_info.K[4]
    # cam_info = {
    #     'fx': fx,
    #     'fy': fy,
    #     'cx': cx,
    #     'cy': cy,
    #     'skew': skew,
    #     'w': w,
    #     'h': h,
    #     'frame': cam_intrinsic_frame,
    #     'matrix': intrinsic_matrix
    # }
    cx = cam_info['cx']
    cy = cam_info['cy']
    fx = cam_info['fx']
    fy = cam_info['fy']

    center_x = cx
    center_y = cy

    constant_x = 1 / fx
    constant_y = 1 / fy
    # if to_ros_msg:
    #     if rgb:
    #         pointcloud_xyzrgb_fields = [
    #             PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
    #             PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
    #             PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
    #             PointField(name='rgb', offset=12, datatype=PointField.UINT32, count=1)
    #         ]
    #     else:
    #         pointcloud_xyz_fields = [
    #             PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
    #             PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
    #             PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1)
    #         ]

    vs = np.array(
        [(v - center_x) * constant_x for v in range(0, depth_image.shape[1])])
    us = np.array(
        [(u - center_y) * constant_y for u in range(0, depth_image.shape[0])])

    # Convert depth from mm to m.
    if convert_depth_from_mm_to_m:
        depth_image = depth_image / 1000.0

    x = np.multiply(depth_image, vs)
    y = depth_image * us[:, np.newaxis]

    # if rgb:
    #     stacked = np.ma.dstack((x, y, depth_image, bgr_image))
    #
    #     compressed = stacked.compressed()
    #     pointcloud = compressed.reshape((int(compressed.shape[0] / 6), 6))
    #
    #     pointcloud = np.hstack((pointcloud[:, 0:3],
    #                             pack_bgr(*pointcloud.T[3:6])[:, None]))
    #     pointcloud = [[point[0], point[1], point[2], int(point[3])]
    #                   for point in pointcloud]
    # else:
    stacked = np.ma.dstack((x, y, depth_image))

    compressed = stacked.compressed()
    pointcloud = compressed.reshape((int(compressed.shape[0] / 3), 3))

    # pointcloud = np.hstack((pointcloud[:, 0:3],
    #                         pack_bgr(*pointcloud.T[3:6])[:, None]))
    pointcloud = [[point[0], point[1], point[2]]
                  for point in pointcloud]
    # if to_ros_msg:
    #     # print('pointcloud: ', pointcloud)
    #     header = Header()
    #     header.frame_id = "map"
    #     header.stamp = rospy.Time.now()
    #
    #     if rgb:
    #         pointcloud = sensor_msgs.point_cloud2.create_cloud(header, pointcloud_xyzrgb_fields,
    #                                                            pointcloud)
    #     pointcloud = sensor_msgs.point_cloud2.create_cloud(header, pointcloud_xyz_fields,
    #                                                        pointcloud)
    #     # pointcloud = sensor_msgs.point_cloud2.create_cloud(header=Header(), fields=pointcloud_xyzrgb_fields, points=pointcloud)
    #
    #     return pointcloud
    return np.array(pointcloud).reshape((-1, 3))

def convert_pcl_to_pixel_list(input_mask, point_cloud, cam_info, mask_value=255, convert_depth_from_m_to_mm=False):

    cx = cam_info['cx']
    cy = cam_info['cy']
    fx = cam_info['fx']
    fy = cam_info['fy']

    for p in point_cloud.points:
        X = p[0]
        Y = p[1]
        z = p[2]
        if z != 0.0:
            # Convert depth from m to mm.
            if convert_depth_from_m_to_mm:
                z = z * 1000.0
            x = (X * fx / z) + cx
            y = (Y * fy / z) + cy
            input_mask[int(y), int(x)] = mask_value

    return input_mask

def get_ransac_segmentation(depth_raw, cam_info, distance_threshold=0.01, ransac_n=3, num_iterations=1000,
                        voxel_size=0.003, convert_depth_from_mm_to_m=False, verbose=False):

    #only need depth
    points = convert_bgrd_to_pcl(None, np.reshape(depth_raw, (depth_raw.shape[0], depth_raw.shape[1])), cam_info,
                                 rgb=False, to_ros_msg=False, convert_depth_from_mm_to_m=convert_depth_from_mm_to_m)
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)
    # pcd.transform([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])
    if verbose:
        print('pcd.points: ', len(pcd.points))
    # pcd = pcd.uniform_down_sample(every_k_points=every_k_points)
    pcd = pcd.voxel_down_sample(voxel_size=voxel_size)
    if verbose:
        print('after down sample pcd.points: ', len(pcd.points))

    plane_model, inliers = pcd.segment_plane(distance_threshold=distance_threshold, ransac_n=ransac_n,
                                             num_iterations=num_iterations)
    [a, b, c, d] = plane_model
    outlier_cloud = pcd.select_by_index(inliers, invert=True)

    if verbose:
        print(f"Plane equation: {a:.2f}x + {b:.2f}y + {c:.2f}z + {d:.2f} = 0")
        inlier_cloud = pcd.select_by_index(inliers)

        # red plane
        inlier_cloud.paint_uniform_color([1.0, 0, 0])
        outlier_cloud.paint_uniform_color([0, 1, 0])
        o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])
        # mask[outlier_cloud] = 255

    ransac_mask = np.zeros((depth_raw.shape[0], depth_raw.shape[1]))
    # mask = np.ones((depth_raw.shape[0], depth_raw.shape[1])) * 255.0
    ransac_mask = convert_pcl_to_pixel_list(ransac_mask, outlier_cloud, cam_info,
                                     convert_depth_from_m_to_mm=convert_depth_from_mm_to_m)

    kernel = np.ones((10, 10), np.uint8)
    ransac_mask = cv2.morphologyEx(ransac_mask, cv2.MORPH_CLOSE, kernel)

    ransac_mask = ransac_mask.reshape((depth_raw.shape[0], depth_raw.shape[1], 1))

    return ransac_mask