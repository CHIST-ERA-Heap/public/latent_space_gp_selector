import argparse

import cv2
import numpy as np
import sys
from segmentation_util import get_HSV_segmentation_mask, get_submasks_rgb, get_submasks_depth

def create_figure_and_sliders(name, nb_of_slider):
    """
    Creating a window for the latent space visualization,
    and another one for the sliders to control it.
    :param name: name of model (str)
    :param state_dim: (int)
    :return:
    """
    # opencv gui setup
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name, 500, 500)
    cv2.namedWindow('slider for ' + name)
    # add a slider for each component of the latent space
    for i in range(nb_of_slider):
        if i < 3:
            cv2.createTrackbar(str(i), 'slider for ' + name, 0, 255, (lambda a: None))
        else:
            cv2.createTrackbar(str(i), 'slider for ' + name, 180, 255, (lambda a: None))


def main(args):

    image = cv2.imread(args.rgb_scene_path)
    # depth_raw = np.load(args.depth_scene_path)

    # median = np.median(depth_raw)
    # median = np.mean(depth_raw)

    # depth_image = get_grayscale_depth_from_raw(depth_raw, b_invert=False)
    # print('depth_image max: ', np.max(depth_image))
    # print('depth_image min: ', np.min(depth_image))

    # depth_image = cv2.cvtColor(depth_image, cv2.COLOR_GRAY2BGR)
    # cv2.imwrite('./temp_depth_original.png', depth_image)
    fig_name = "HSV selector"
    nb_of_slider = 6

    create_figure_and_sliders(fig_name, nb_of_slider)

    should_exit = False
    while not should_exit:
        # stop if escape is pressed
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break


        state = []
        for i in range(nb_of_slider):
            state.append(cv2.getTrackbarPos(str(i), 'slider for ' + fig_name))
        # Rescale the values to fit the bounds of the representation
        # state = (np.array(state) / 100) * (bound_max - bound_min) + bound_min
        lower_hsv = (state[0], state[1], state[2])
        upper_hsv = (state[3], state[4], state[5])
        # lower_hsv = (0,0,0)
        # upper_hsv = (180,33,90)

        mask = np.zeros((image.shape[0], image.shape[1], 1))
        hsv_mask = get_HSV_segmentation_mask(image, lower_hsv, upper_hsv, verbose=True)
        mask[hsv_mask > 0] = 255
        mask = np.reshape(mask, (image.shape[0], image.shape[1])).astype('uint8')

        # reconstructed_image = cv2.bitwise_and(image, image, mask=mask)
        mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        reconstructed_image = cv2.addWeighted(image, 0.7, mask, 0.3, 0)

        # masks = get_submasks_rgb(image, mask)
        # j = 0
        # for m in masks:
        #     m = np.reshape(m, (image.shape[0], image.shape[1])).astype('uint8')
        #     reconstructed_image_t = cv2.bitwise_and(reconstructed_image, reconstructed_image, mask=m)
        #     cv2.imwrite('temp_rgb_'+str(j)+'.png', reconstructed_image_t)
        #     j+=1
        #
        # masks = get_submasks_depth(depth_raw, mask)
        # j = 0
        # for m in masks:
        #     m = np.reshape(m, (image.shape[0], image.shape[1])).astype('uint8')
        #     reconstructed_image_t = cv2.bitwise_and(reconstructed_image, reconstructed_image, mask=m)
        #     cv2.imwrite('temp_gray_'+str(j)+'.png', reconstructed_image_t)
        #     j+=1

        # stop if user closed a window
        if (cv2.getWindowProperty(fig_name, 0) < 0) or (cv2.getWindowProperty('slider for ' + fig_name, 0) < 0):
            should_exit = True
            break
        cv2.imshow(fig_name, reconstructed_image)

    # gracefully close
    cv2.destroyAllWindows()

def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='test hsv mask')

    parser.add_argument('--rgb_scene_path', type=str, help='path to a rgb image file',
                        default='../../test_scene/inria/security_glasses.png')

    parser.add_argument('--depth_scene_path', type=str, help='path to raw depth file',
                        default='../../test_scene/inria/security_glasses.npy')


    return parser.parse_args(argv)

if __name__ == '__main__':
    args = parse_arguments(sys.argv[1:])
    #args.rgb_scene_path = '../../test_scene/cornell/pcd0100r.png'
    #args.depth_scene_path = '../../test_scene/cornell/pcd0100.npy'
    #args.rgb_scene_path = './input_image_hsv_mask.png'
    args.rgb_scene_path = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/current_data/current.png'
    args.depth_scene_path = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/current_data/current.npy'

    main(args)