import cv2
# from skimage.feature import canny
from scipy import ndimage as ndi
from skimage.morphology import skeletonize, medial_axis, remove_small_objects, opening, square
from skimage.segmentation import felzenszwalb
# import pandas as pd
from skimage import measure
import configparser
import numpy as np
from sklearn.cluster import MiniBatchKMeans
from PIL import Image
import open3d as o3d
from .segmentation_util import get_submasks_rgb, get_grayscale_depth_from_raw, color_quantization, get_submasks_depth, get_crop_mask, closed_edges_mask, depth_segmentation, background_subtraction, saliency_segmentation, felzenszwalb_segmentation, get_HSV_segmentation_mask, convert_bgrd_to_pcl, convert_pcl_to_pixel_list, get_ransac_segmentation

class CameraData:
    rgb_img = None
    background_img = None
    depth_img = None
    pc_img = None
    seg_img = None
    bounding_box = None
    intrinsic_params = None
    # extrinsic_params = {'position': np.ndarray((3, 1), float), 'rotation': np.eye(3)}


class SegmentationClass():
    """The base class for perfoming segmentation

    """
    def __init__(self, cfg_file:str):

        self.cfg = configparser.ConfigParser()
        self.cfg.read(cfg_file)

        self._camera_data = CameraData()
        self.computed_mask = None
        self.configure()

    def configure(self):
        # self.change_too_far_to_cam = self.cfg.getboolean('preprocessing','change_too_far_to_cam')
        # self.too_far_to_cam_to_what = self.cfg.get('preprocessing','too_far_to_cam_to_what')
        # self.too_far_to_cam_threshold = self.cfg.getfloat('preprocessing','too_far_to_cam_threshold')

        # self.change_nans = self.cfg.getboolean('preprocessing','change_nans')
        # self.nans_to_what = self.cfg.get('preprocessing','nans_to_what')

        # self.change_too_close_to_cam = self.cfg.getboolean('preprocessing','change_too_close_to_cam')
        # self.too_close_to_cam_to_what = self.cfg.get('preprocessing','too_close_to_cam_to_what')
        # self.too_close_to_cam_threshold = self.cfg.getfloat('preprocessing','too_close_to_cam_threshold')

        self.visualization = self.cfg.getboolean('visualization','visualization')
        self.crop_left = self.cfg.getfloat('segmentation', 'crop_left')
        self.crop_right = self.cfg.getfloat('segmentation', 'crop_right')
        self.crop_top = self.cfg.getfloat('segmentation', 'crop_top')
        self.crop_bottom = self.cfg.getfloat('segmentation', 'crop_bottom')
        self.apply_opening = self.cfg.getboolean('segmentation', 'apply_opening')
        self.keep_only_largest = self.cfg.getboolean('segmentation', 'keep_only_largest')
        self.use_felzenszwalb = self.cfg.getboolean('segmentation', 'use_felzenszwalb')
        lower_hsv_str = self.cfg.get('segmentation', 'lower_hsv')
        lower_hsv = lower_hsv_str.split('\n')
        lower_hsv = [int(s) for s in lower_hsv]
        self.lower_hsv = (int(lower_hsv[0]), int(lower_hsv[1]), int(lower_hsv[2]))

        upper_hsv_str = self.cfg.get('segmentation', 'upper_hsv')
        upper_hsv = upper_hsv_str.split('\n')
        self.upper_hsv = [int(s) for s in upper_hsv]
        self.upper_hsv = (int(upper_hsv[0]), int(upper_hsv[1]), int(upper_hsv[2]))

        self.use_closed_edges = self.cfg.getboolean('segmentation', 'use_closed_edges')
        self.use_hsv_mask = self.cfg.getboolean('segmentation', 'use_hsv_mask')
        self.use_saliency_mask = self.cfg.getboolean('segmentation', 'use_saliency_mask')
        self.use_background_segmentation_mask = self.cfg.getboolean('segmentation', 'use_background_segmentation_mask')
        self.use_grab_cut = self.cfg.getboolean('segmentation', 'use_grab_cut')
        self.use_depth_mask = self.cfg.getboolean('segmentation', 'use_depth_mask')
        usual_object_depth_range_str = self.cfg.get('segmentation', 'usual_object_depth_range')
        usual_object_depth_range = usual_object_depth_range_str.split('\n')
        self.usual_object_depth_range = [float(s) for s in usual_object_depth_range]

        self.use_ransac = self.cfg.getboolean('segmentation', 'use_ransac')
        self.distance_threshold = self.cfg.getfloat('segmentation', 'distance_threshold')
        self.ransac_n = self.cfg.getint('segmentation', 'ransac_n')
        self.num_iterations = self.cfg.getint('segmentation', 'num_iterations')
        self.voxel_size = self.cfg.getfloat('segmentation', 'voxel_size')

        print("Segmentation class configured")

    def compute_mask(self, rgb_image, background_rgb_image, raw_depth_data, intrinsic_params, verbose=False):
        # shape = rgb_image.shape
        width = rgb_image.shape[1]
        height = rgb_image.shape[0]

        if verbose:
            print('cv_image 0 mean: ', np.mean(rgb_image[:, :, 0]))
            print('depth_raw mean: ', np.mean(raw_depth_data))

            print('segmentation_code get_crop_mask')
        cropping_mask, crop = get_crop_mask(height, width,
                                                      self.crop_left, self.crop_right, self.crop_top,
                                                      self.crop_bottom, scale_factor=1.0)
        if verbose:
            print('segmentation_code get_scene_mask')
        computed_mask = self.get_scene_mask(rgb_image, background_rgb_image,
                                                 raw_depth_data, cropping_mask, intrinsic_params)

        if self.visualization:
            self.visualize(rgb_image, computed_mask)

        return computed_mask


    def preprocess_depth(self, depth_raw, verbose=False):
        if verbose:
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))
        if self.change_too_far_to_cam:
            min = np.min(depth_raw)
            where_are_far = np.where(depth_raw > self.too_far_to_cam_threshold)
            depth_raw[where_are_far] = min
            if self.too_far_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_far_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_far_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_far_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_far] = value

        if self.change_nans:
            where_are_NaNs = np.isnan(depth_raw)
            if self.too_far_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_far_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_far_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_far_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_NaNs] = value

        if self.change_too_close_to_cam:
            where_are_close = np.where(depth_raw < self.too_close_to_cam_threshold)
            # where_are_close = np.where(depth_raw < 0.61)
            if self.too_far_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_far_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_far_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_far_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_close] = value

        if verbose:
            print('after')
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))

        return depth_raw

    def create_camera_data(self, rgb_image : np.ndarray, background_image : np.ndarray, depth_image : np.ndarray, cam_intrinsic_frame :str, cam_extrinsic_matrix : np.ndarray,
                           fx: float, fy: float, cx: float, cy: float, skew: float, w: int, h: int, obj_cloud : np.ndarray = None) -> CameraData:

        """Create the CameraData object in the format expected by the graspnet planner

        Parameters
        ----------
        rgb_image : np.ndarray
            RGB image
        depth_image : np.ndarray
            Depth (float) image
        cam_intrinsic_frame : str
            The reference frame ID of the images. Grasp poses are computed wrt this
            frame
        cam_extrinsic_matrix : np.ndarray
            The 4x4 camera pose in world reference frame
        fx : float
            Focal length (x direction)
        fy : float
            Focal length (y direction)
        cx : float
            Principal point (x direction)
        cy : float
            Principal poin (y direction)
        skew : float
            Skew coefficient
        w : int
            Image width
        h : int
            Image height
        obj_cloud : np.ndarray, optional
            Object point cloud to use for grasp planning (a segmented portion of
            the point cloud could be given here)

        Returns
        -------
        """
        # print('segmentation_code create_camera_data' )

        camera_data = CameraData()
        camera_data.rgb_img = rgb_image
        camera_data.background_img = background_image

        if depth_image is not None:
            #todo think
            # camera_data.depth_img = self.preprocess_depth(depth_image.copy())
        # else:
            camera_data.depth_img = depth_image

        intrinsic_matrix = np.array([[fx, skew,  cx],
                                     [0,    fy,  cy],
                                     [0,     0,   1]])
        camera_data.intrinsic_params = {
                                        'fx' : fx,
                                        'fy' : fy,
                                        'cx' : cx,
                                        'cy' : cy,
                                        'skew' : skew,
                                        'w' : w,
                                        'h' : h,
                                        'frame' : cam_intrinsic_frame,
                                        'matrix' : intrinsic_matrix
                                        }

        self._camera_data = camera_data
        return camera_data

    def reset(self):
        self._camera_data = CameraData()
        self.computed_mask = None

    def segment(self, camera_data):
        """Segmentation.

        Parameters
        ---------
        req: :obj:`ROS ServiceRequest`
            ROS `ServiceRequest` for segmentation service.
        """
        print('segmentation_code segment')
        self._camera_data = camera_data

        if camera_data.rgb_img is not None:
            rgb_image = camera_data.rgb_img.copy()
        else:
            rgb_image = None

        if camera_data.depth_img is not None:
            depth_image = camera_data.depth_img.copy()
        else:
            depth_image = None

        if camera_data.background_img is not None:
            background_img = camera_data.background_img.copy()
        else:
            background_img = None

        # shape = rgb_image.shape
        width = rgb_image.shape[1]
        height = rgb_image.shape[0]

        print('cv_image 0 mean: ', np.mean(rgb_image[:, :, 0]))
        print('depth_raw mean: ', np.mean(depth_image))

        print('segmentation_code get_crop_mask')
        self.cropping_mask, self.crop = get_crop_mask(height, width,
                                                      self.crop_left, self.crop_right, self.crop_top,
                                                      self.crop_bottom, scale_factor=1.0)

        print('segmentation_code get_scene_mask')
        self.computed_mask = self.get_scene_mask(rgb_image, background_img,
                                      depth_image, self.cropping_mask, self._camera_data.intrinsic_params)
        print('segmentation_code computed_mask')
        if self.visualization:
            self.visualize(rgb_image, self.computed_mask)

        if self.computed_mask is not None:
            return True
        else:
            return False

    def visualize(self, image, mask):
        print('visualize:')

        # vis_mask = np.reshape(mask, (image.shape[0], image.shape[1], 1)).astype(np.float32)
        vis_mask = cv2.cvtColor(mask.astype(np.uint8), cv2.COLOR_GRAY2RGB)
        # print('saving segmentation_vis_mask')
        # cv2.imwrite('./segmentation_vis_mask.png', vis_mask)
        # cv2.imwrite('./segmentation_image.png', image)

        # print('image.shape: ', image.shape, ' type: ', image.dtype)
        # print('vis_mask.shape: ', vis_mask.shape, ' type: ', vis_mask.dtype)

        reconstructed_image = cv2.addWeighted(image.astype(np.uint8), 0.7, vis_mask, 0.3, 0)
        #visualisation stuff
        # cv2.imwrite('./segmentation_input_rgb.png', image)
        reconstructed_image = cv2.cvtColor(reconstructed_image, cv2.COLOR_BGR2RGB)

        window_name = 'scene mask'
        img = Image.fromarray(reconstructed_image)
        img.show(window_name)
        input('enter any key to continue:')

    def get_scene_mask(self, image, background_rgb_image, current_scene_raw_depth_data, cropping_mask, intrinsic_params, verbose=True):

        # cropping_mask_2 = np.reshape(cropping_mask, (image.shape[0], image.shape[1])).astype('uint8')
        # cropping_mask_2[cropping_mask > 0] = 0
        # cropping_mask_2[cropping_mask == 0] = 255
        #
        # image = cv2.bitwise_and(image, image,mask=cropping_mask_2)
        # cv2.imwrite('image_cropped.png', image)

        mask = np.zeros((image.shape[0], image.shape[1], 1))

        # grab_cut_guess = np.ones((image.shape[0], image.shape[1], 1))
        # grab_cut_guess= grab_cut_guess * 2
        grab_cut_guess = np.zeros((image.shape[0], image.shape[1], 1))
        grab_cut_guess_temp = grab_cut_guess.copy()
        # image = image_preprocessing(image)
        # ksize
        # ksize = (10, 10)
        # # Using cv2.blur() method
        # image = cv2.blur(image, ksize)

        if self.use_felzenszwalb:
            if verbose:
                print('use_felzenszwalb')
            cropping_mask_2 = np.reshape(cropping_mask, (image.shape[0], image.shape[1])).astype('uint8')
            cropping_mask_2[cropping_mask > 0] = 0
            cropping_mask_2[cropping_mask == 0] = 255
            image_felzenszwalb = cv2.bitwise_and(image, image,mask=cropping_mask_2)
            # image_felzenszwalb = cv2.bitwise_and(image, image, mask=cropping_mask)

            felzenszwalb_mask, segments_fz = felzenszwalb_segmentation(image_felzenszwalb)
            # cv2.imwrite('segments_fz.png', segments_fz)
            # cv2.imwrite('felzenszwalb_mask.png', felzenszwalb_mask)

            # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (8, 8))  # 6 good
            # felzenszwalb_mask = cv2.dilate(felzenszwalb_mask, kernel, iterations=1)
            mask[felzenszwalb_mask > 0] = 255
            grab_cut_guess[felzenszwalb_mask > 0] = 3
            grab_cut_guess[felzenszwalb_mask == 0] = 0

            # cropping_mask_2[felzenszwalb_mask == 0] = 0
            # image = cv2.bitwise_and(image, image, mask=cropping_mask_2)

            #
            # image = cv2.bitwise_and(image, image,mask=cropping_mask_2)
            # cv2.imwrite('test_bitwise.png', image)

        if self.use_background_segmentation_mask:
            if verbose:
                print('use_background_segmentation_mask')

            # print("background_rgb_image.shape: ", background_rgb_image.shape)

            background_mask = background_subtraction(background_rgb_image,
                                                     image)
            # print("background_mask.shape: ", background_mask.shape)
            # print("background_mask max: ", np.max(background_mask))
            # print("background_mask min: ", np.min(background_mask))
            mask[background_mask > 0] = 255
            grab_cut_guess[background_mask > 0] = 2

        if self.use_hsv_mask:
            if verbose:
                print('use_hsv_mask')

            # quant = color_quantization(image)
            # hsv_mask = get_HSV_segmentation_mask(quant)
            hsv_mask = get_HSV_segmentation_mask(image, self.lower_hsv, self.upper_hsv, verbose=verbose)
            # print('saving /workspace/catkin_ws/src/grasping-benchmarks-panda/grasping_benchmarks/segmentation/input_image_hsv_mask.png')
            # cv2.imwrite('/workspace/catkin_ws/src/grasping-benchmarks-panda/grasping_benchmarks/segmentation/input_image_hsv_mask.png', image)
            #
            # print('saving /workspace/catkin_ws/src/grasping-benchmarks-panda/grasping_benchmarks/segmentation/hsv_mask.png')
            # cv2.imwrite('/workspace/catkin_ws/src/grasping-benchmarks-panda/grasping_benchmarks/segmentation/hsv_mask.png', hsv_mask)

            mask[hsv_mask > 0] = 255
            mask = np.reshape(mask, (image.shape[0], image.shape[1])).astype('uint8')

            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))  # 6 good
            hsv_mask_bigger = cv2.dilate(hsv_mask, kernel, iterations=1)
            grab_cut_guess[hsv_mask_bigger == 0] = 0
            grab_cut_guess[hsv_mask_bigger > 0] = 3
            grab_cut_guess[hsv_mask > 0] = 1
            grab_cut_guess_temp = grab_cut_guess.copy()
            # if use_background_segmentation_mask:
            #     grab_cut_guess[np.where(np.logical_and(background_mask > 0, hsv_mask > 0))] = 3

        if self.use_saliency_mask:
            if verbose:
                print('use_saliency_mask')
            saliency_mask = saliency_segmentation(image)
            saliency_mask = np.reshape(saliency_mask, (image.shape[0], image.shape[1])).astype('uint8')

            mask[saliency_mask > 0] = 255
            grab_cut_guess[saliency_mask > 0] = 3

        if self.use_depth_mask:
            if verbose:
                print('use_depth_mask')
            depth_mask = depth_segmentation(current_scene_raw_depth_data, self.usual_object_depth_range)

            # list_points = reject_outliers(current_scene_raw_depth_data, m=1)
            # if list_points.shape[0] > 0:

            mask[depth_mask > 0] = 255
            # mask[depth_mask == 0] = 0
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))  # 6 good
            depth_mask_bigger = cv2.dilate(depth_mask, kernel, iterations=1)
            # grab_cut_guess[depth_mask_bigger == 0] = 2
            grab_cut_guess[depth_mask_bigger > 0] = 2
            grab_cut_guess[depth_mask > 0] = 1
            grab_cut_guess[grab_cut_guess_temp == 1] = 1
            # grab_cut_guess_temp = grab_cut_guess.copy()

        if self.use_ransac:
            if verbose:
                print('use_ransac')
            ransac_mask = get_ransac_segmentation(current_scene_raw_depth_data, intrinsic_params, distance_threshold=self.distance_threshold, ransac_n=self.ransac_n, num_iterations=self.num_iterations,
                                    voxel_size=self.voxel_size, convert_depth_from_mm_to_m=False)
            # list_points = reject_outliers(current_scene_raw_depth_data, m=1)
            ransac_mask = np.reshape(ransac_mask, (image.shape[0], image.shape[1])).astype('uint8')

            # if self.use_hsv_mask or self.use_closed_edges or self.use_saliency_mask or self.use_background_segmentation_mask or self.use_felzenszwalb:
            #     mask[ransac_mask == 0] = 0

            mask[ransac_mask > 0] = 255
            grab_cut_guess[ransac_mask > 0] = 1
            grab_cut_guess[grab_cut_guess_temp == 1] = 1
            # grab_cut_guess_temp = grab_cut_guess.copy()

        if self.use_closed_edges:
            if verbose:
                print('use_closed_edges')

            # get_edges
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            contour_mask_filed = closed_edges_mask(gray_image)

            mask[contour_mask_filed > 0] = 255
            grab_cut_guess[contour_mask_filed > 0] = 1

        if self.use_grab_cut:
            if verbose:
                print('use_grab_cut')

            '''
            GC_BGD   （=0）defines an obvious background pixels.
            GC_FGD   （=1）defines an obvious foreground (object) pixel.
            GC_PR_BGD（=2）defines a possible background pixel.
            GC_PR_BGD（=3）defines a possible foreground pixel.
            '''
            grab_cut_guess[cropping_mask == 0] = 0
            mask = np.reshape(mask, (image.shape[0], image.shape[1], 1)).astype('uint8')

            bgdModel = np.zeros((1, 65), np.float64)
            fgdModel = np.zeros((1, 65), np.float64)
            try:
                grab_cut_mask, bgdModel, fgdModel = cv2.grabCut(image, np.asarray(grab_cut_guess).astype(np.uint8),
                                                                None, bgdModel,
                                                                fgdModel, 5,
                                                                cv2.GC_INIT_WITH_MASK)
                # mask[np.where(grab_cut_mask == 2)] = 0
                mask[np.where(grab_cut_mask == 1)] = 255
                mask[np.where(grab_cut_mask == 3)] = 255

            except Exception as e:
                print("issue get_scene_mask grabcut: ", e)
                print("sum: ", np.sum(grab_cut_guess))

        # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 4))  # 6 good
        # mask = cv2.erode(mask, kernel, iterations=1)
        # mask = cv2.dilate(mask, kernel, iterations=1)
        mask[cropping_mask == 0] = 0
        if not self.use_closed_edges and not self.use_hsv_mask and not self.use_saliency_mask and not self.use_background_segmentation_mask and not self.use_grab_cut and not self.use_depth_mask and not self.use_ransac:
            mask[cropping_mask > 0] = 255


        mask = np.reshape(mask, (image.shape[0], image.shape[1], 1)).astype('uint8')


        if self.apply_opening:
            if verbose:
                print('apply_opening')
            mask = np.reshape(mask, (image.shape[0], image.shape[1])).astype('uint8')

            # mask = remove_small_objects(mask, min_size=64, connectivity=, in_place=False)
            # print('post_remove_small_objects: ', mask)
            # Remove thin structures with binary opening
            # mask = opening(mask, np.ones((3, 3)))
            mask = opening(mask, square(4))

        if self.keep_only_largest:
            if verbose:
                print('keep_only_largest')

            labels_mask = measure.label(mask)
            regions = measure.regionprops(labels_mask)
            regions.sort(key=lambda x: x.area, reverse=True)
            if len(regions) > 1:
                for rg in regions[1:]:
                    labels_mask[rg.coords[:, 0], rg.coords[:, 1]] = 0
            labels_mask[labels_mask != 0] = 255
            mask = labels_mask
        mask = np.reshape(mask, (image.shape[0], image.shape[1])).astype('uint8')

        return mask

