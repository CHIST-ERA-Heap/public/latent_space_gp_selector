import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))
from scripts.gui import PygameGraspInterraction
import rospy

#todo argparse
if __name__ == '__main__':

    #todo adjust

    # root = '/home/panda1ws/Documents/latent_space_gp_selector/data/config_files/'
    root = '/home/panda2/Documents/IIT/latent_space_gp_selector/data/config_profiles/'

    gui_config_path = root+'test_config_cornell/test_gui_config.ini'
    camera_config_path = root+ 'test_config_cornell/test_camera_config.ini'
    database_config_path = root+ 'test_config_cornell/test_database_config.ini'
    grasp_generator_config_path = root+ 'test_config_cornell/test_grasp_generator_config.ini'
    evaluator_config_path = root+ 'test_config_cornell/test_evaluator_config.ini'
    segmentation_config_path = root+ 'test_config_cornell/test_segmentation_config.ini'
    panda_config_path = root+ 'test_config_cornell/test_panda_config.ini'

    # gui_config_path = root+'test_config_lgps/test_gui_config.ini'
    # camera_config_path = root+'test_config_lgps/test_camera_config.ini'
    # database_config_path = root+'test_config_lgps/test_database_config.ini'
    # grasp_generator_config_path = root+'test_config_lgps/test_grasp_generator_config.ini'
    # evaluator_config_path = root+'test_config_lgps/test_evaluator_config.ini'
    # segmentation_config_path = root+'test_config_lgps/test_segmentation_config.ini'
    # panda_config_path = root+'test_config_lgps/test_panda_config.ini'

    # gui_config_path = root+'test_config_live_temp/test_gui_config.ini'
    # camera_config_path = root+'test_config_live_temp/test_camera_config.ini'
    # database_config_path = root+'test_config_live_temp/test_database_config.ini'
    # grasp_generator_config_path = root+'test_config_live_temp/test_grasp_generator_config.ini'
    # evaluator_config_path = root+'test_config_live_temp/test_evaluator_config.ini'
    # segmentation_config_path = root+'test_config_live_temp/test_segmentation_config.ini'
    # panda_config_path = root+'test_config_live_temp/test_panda_config.ini'

    pygame_grasp_interraction = PygameGraspInterraction(gui_config_path=gui_config_path, database_config_path=database_config_path, camera_config_path=camera_config_path, generator_config_path=grasp_generator_config_path, evaluator_config_path=evaluator_config_path, segmentation_config_path=segmentation_config_path, panda_config_path=panda_config_path)

    pygame_grasp_interraction.start()
