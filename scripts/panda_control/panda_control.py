from __future__ import print_function
import rospy
import tf
import tf.transformations
from geometry_msgs.msg import Pose, PoseStamped

from grasping_benchmarks.base.grasp import Grasp6D
from scripts.grasps_generators.grasps_generators import transform_grasp_to_6D
import numpy as np
from grasping_benchmarks_ros.msg import BenchmarkGrasp

import math
from scipy.spatial.transform import Rotation as R
from std_msgs.msg import Float32
import configparser
from grasping_benchmarks.base.transformations import quaternion_to_matrix, matrix_to_quaternion
# from panda_grasp_srv.srv import PandaGrasp, PandaGraspRequest, PandaGraspResponse
from panda_ros_common.srv import PandaGrasp, PandaGraspRequest, PandaGraspResponse

def transform_grasp_to_world( grasp_pose: PoseStamped, camera_viewpoint: PoseStamped, verbose=True) -> PoseStamped:
    """Transform the 6D grasp pose in the world reference frame, given the
    camera viewpoint
    Parameters
    ----------
    grasp_pose : geometry_msgs/PoseStamped
        Candidate grasp pose, in the camera ref frame
    camera_viewpoint : geometry_msgs/PoseStamped
        Camera pose wrt world ref frame
    Returns
    -------
    PoseStamped
        Candidate grasp pose, in the world reference frame
    """

    # w_T_cam : camera pose in world ref frame
    # cam_T_grasp : grasp pose in camera ref frame
    # w_T_grasp = w_T_cam * cam_T_grasp

    # Construct the 4x4 affine transf matrices from ROS stamped poses
    grasp_quat = grasp_pose.pose.orientation
    grasp_pos = grasp_pose.pose.position
    cam_T_grasp = np.eye(4)
    cam_T_grasp[:3, :3] = quaternion_to_matrix([grasp_quat.x,
                                                grasp_quat.y,
                                                grasp_quat.z,
                                                grasp_quat.w])
    cam_T_grasp[:3, 3] = np.array([grasp_pos.x, grasp_pos.y, grasp_pos.z])

    cam_quat = camera_viewpoint.pose.orientation
    cam_pos = camera_viewpoint.pose.position
    w_T_cam = np.eye(4)
    w_T_cam[:3, :3] = quaternion_to_matrix([cam_quat.x,
                                            cam_quat.y,
                                            cam_quat.z,
                                            cam_quat.w])
    w_T_cam[:3, 3] = np.array([cam_pos.x, cam_pos.y, cam_pos.z])

    # Obtain the w_T_grasp affine transformation
    w_T_grasp = np.matmul(w_T_cam, cam_T_grasp)

    if verbose:
        print("[DEBUG] Grasp pose reference system transform")
        print("w_T_cam\n ", w_T_cam)
        print("cam_T_grasp\n ", cam_T_grasp)
        print("w_T_grasp\n ", w_T_grasp)

    # Create and return a StampedPose object
    w_T_grasp_quat = matrix_to_quaternion(w_T_grasp[:3, :3])
    result_pose = PoseStamped()
    result_pose.pose.orientation.x = w_T_grasp_quat[0]
    result_pose.pose.orientation.y = w_T_grasp_quat[1]
    result_pose.pose.orientation.z = w_T_grasp_quat[2]
    result_pose.pose.orientation.w = w_T_grasp_quat[3]
    result_pose.pose.position.x = w_T_grasp[0, 3]
    result_pose.pose.position.y = w_T_grasp[1, 3]
    result_pose.pose.position.z = w_T_grasp[2, 3]
    result_pose.header = camera_viewpoint.header

    return result_pose


class PandaControl:
    def __init__(self, panda_config_path, verbose=True):
        self.cfg = configparser.ConfigParser()
        if verbose:
            print('loading database_config_path: ',panda_config_path)
        self.cfg.read(panda_config_path)
        self.read_cfg()
        self.setup_panda(verbose)

    def read_cfg(self):
        self.panda_service_name = self.cfg.get('panda_control','panda_service_name')
        self.timeout = self.cfg.getfloat('panda_control','timeout')
        self.robot_is_connected = self.cfg.getboolean('panda_control', 'robot_is_connected')

    def setup_panda(self, verbose=False):
        if self.robot_is_connected:
            # --- panda service --- #
            panda_service_name =  "/panda_grasp_server/panda_grasp"
            rospy.loginfo("GraspingBenchmarksManager: Waiting for panda control service...")
            rospy.wait_for_service(panda_service_name, timeout=self.timeout)
            self.panda = rospy.ServiceProxy(panda_service_name, PandaGrasp)
            rospy.loginfo("...Connected with service {}".format(panda_service_name))

    #take grasp as a vector without the grasp_id
    def execute_grasp(self, grasp, intrinsic_params, camera_pose, tweak_grasp_i = -1, minimum_grasp_depth = None, apply_offset=True, verbose=True):
        """Assumes the grasp pose is already in the root reference frame
        Parameters:
            - grasp (obj: BenchmarkGrasp msg)
        """


        x = grasp[3+tweak_grasp_i]
        y = grasp[4+tweak_grasp_i]
        z = grasp[5+tweak_grasp_i]


        euler = [grasp[6+tweak_grasp_i], grasp[7+tweak_grasp_i], grasp[8+tweak_grasp_i]]
        length_meters = grasp[12+tweak_grasp_i]
        candidate_offset = [grasp[13 + tweak_grasp_i], grasp[14 + tweak_grasp_i], grasp[15 + tweak_grasp_i]]

        if apply_offset:
            candidate_offset = [grasp[13+tweak_grasp_i], grasp[14+tweak_grasp_i], grasp[15+tweak_grasp_i]]
        else:
            candidate_offset = [0.0, 0.0, 0.0]

        quality = grasp[11+tweak_grasp_i]
        if quality is None:
            quality = -1.0

        pose_6d = transform_grasp_to_6D(x, y, z, euler, intrinsic_params, candidate_offset)
        pos = pose_6d[:3, 3]
        rot = pose_6d[:3, :3]


        '''
        position : (`numpy.ndarray` of float): 3-entry position vector wrt camera frame
        rotation (`numpy.ndarray` of float):3x3 rotation matrix wrt camera frame
        width : Distance between the fingers in meters.
        score: prediction score of the grasp pose
        ref_frame: frame of reference for camera that the grasp corresponds to.
        quaternion: rotation expressed as quaternion
        '''

        grasp_candidate = Grasp6D(position=pos, rotation=rot,
                           width=length_meters, score=float(quality),
                           ref_frame=intrinsic_params['frame'])

        if verbose:
            print('panda_control.execute_grasp:')
            print('x: ', x)
            print('y: ', y)
            print('z: ', z)
            print('euler: ', euler)
            print('length_meters: ', length_meters)
            print('candidate_offset: ', candidate_offset)
            print('quality: ', quality)
            print('pos: ', pos)
            print('rot: ', rot)
            print('grasp_candidate 6D: ', grasp_candidate)

        grasp_msg = BenchmarkGrasp()
        p = PoseStamped()
        p.header.frame_id = grasp_candidate.ref_frame
        p.header.stamp = rospy.Time.now()
        p.pose.position.x = grasp_candidate.position[0]
        p.pose.position.y = grasp_candidate.position[1]
        p.pose.position.z = grasp_candidate.position[2]
        p.pose.orientation.w = grasp_candidate.quaternion[3]
        p.pose.orientation.x = grasp_candidate.quaternion[0]
        p.pose.orientation.y = grasp_candidate.quaternion[1]
        p.pose.orientation.z = grasp_candidate.quaternion[2]

        camera_viewpoint = PoseStamped()
        camera_viewpoint.header = camera_pose.header
        camera_viewpoint.pose.orientation = camera_pose.transform.rotation
        camera_viewpoint.pose.position = camera_pose.transform.translation
        camera_viewpoint.header.stamp = rospy.Time.now()

        grasp_msg.pose = transform_grasp_to_world(p, camera_viewpoint)

        # Set candidate score and width
        grasp_msg.score.data = grasp_candidate.score
        grasp_msg.width.data = grasp_candidate.width
        grasp_msg.offset = candidate_offset

        panda_req = PandaGraspRequest()
        panda_req.grasp = grasp_msg.pose
        if minimum_grasp_depth is not None:
            if panda_req.grasp.pose.position.z < minimum_grasp_depth:
                panda_req.grasp.pose.position.z = minimum_grasp_depth

        panda_req.width = grasp_msg.width

        if verbose:
            print("request to panda is: \n{}" .format(panda_req))

        try:
            reply = self.panda(panda_req)
            print("Service {} reply is: \n{}" .format(self.panda.resolved_name, reply))
            return True

        except rospy.ServiceException as e:
            print("Service {} call failed: {}" .format(self.panda.resolved_name, e))
            return False