import rospy
import actionlib
from rosgraph_msgs.msg import Log
# from franka_control.msg import ErrorRecoveryAction, ErrorRecoveryGoal
from franka_msgs.msg import FrankaState, ErrorRecoveryAction, ErrorRecoveryGoal

def callback(data):
    rospy.loginfo("I heard %s",data.data)

class rossetup():
    def __init__(self):
        rospy.init_node('panda_state_tracker')
        rate = rospy.Rate(0.5)
        self.buisy = False

        self.last_message =""
        #self.reset_publisher = rospy.Publisher('/franka_control/error_recovery/goal', ErrorRecoveryActionGoal, queue_size=1)
        #self.sub_franka1 = rospy.Subscriber('/franka_control/error_recovery/feedback', ErrorRecoveryFeedback, callback, queue_size=1)
        #self.sub_franka2 = rospy.Subscriber('/franka_control/error_recovery/goal', ErrorRecoveryGoal, callback,queue_size=1)
        #self.sub_franka3 = rospy.Subscriber('/franka_control/error_recovery/result', ErrorRecoveryResult, callback, queue_size=1)
        #self.sub_franka = rospy.Subscriber('/franka_control/error_recovery/status', ErrorRecoveryFeedback, queue_size=1)
        self.reset_client = actionlib.SimpleActionClient('/franka_control/error_recovery', ErrorRecoveryAction)
        self.current_state = 0

        self.robot_state_sub = rospy.Subscriber('/franka_state_controller/franka_states', FrankaState,
                                                self.__robot_state_callback, queue_size=1)
        #self.sub = rospy.Subscriber('rosout', Log, self.handle_message,  queue_size=1)
        rospy.spin()

    def __robot_state_callback(self, data):
        if(not self.buisy):
            self.buisy = True
            print(data.robot_mode)
            if(self.current_state == 5 or self.current_state == 4):
                print('Sending state recover to inria kind Panda!')
                #envoy.run("rostopic pub -1 /franka_control/error_recovery/goal franka_control/ErrorRecoveryActionGoal \"{}\"")
                self.recover()
            self.buisy = False
            self.current_state = data.robot_mode

        '''   
        print(data.robot_mode)
        for s in FrankaErrors.__slots__:
            if getattr(data.current_errors, s):
                #self.stop()
                rospy.logerr('Robot Error Detected')
                self.ROBOT_ERROR_DETECTED = True
                print("OOOOOOO")
        '''
    def recover(self):
        self.reset_client.send_goal(ErrorRecoveryGoal())
        # self.reset_client.wait_for_result()

    def handle_message(self, data):
        print("in handle_message")
        log = Log()
        log = data
        self.check_rosout2(log)


    def check_rosout2(self, log):
        robot_state = log.level
        print('log.level: ', robot_state)

        self.last_message = log.msg

        if(robot_state==8 and (self.current_state==4 or self.current_state==5) ):
            print('log.name: ', log.name)
            print('log.msg: ', log.msg)
            #goal = ErrorRecoveryActionGoal()
            robot_state = log.level

            '''
            cases=[('libfranka: Move command aborted: User Stop pressed!', log.msg)]
            for a, b in cases:
                print('{} => {}'.format(a, b))
                for i, s in enumerate(difflib.ndiff(a, b)):
                    if s[0] == ' ':
                        continue
                    elif s[0] == '-':
                        print(u'Delete "{}" from position {}'.format(s[-1], i))
                    elif s[0] == '+':
                        print(u'Add "{}" to position {}'.format(s[-1], i))
                print()
            '''
            log_string = str(log.msg)
            if log_string.startswith("libfranka: Move command aborted") or log_string.startswith("libfranka: Move command rejected"):
                # 2s delay to wait for the robot state to be published as ERROR on the Logging and then send the Action
                #self.reset_publisher.publish(ErrorRecoveryActionGoal())
                if(self.last_message.startswith("libfranka: Move command aborted") or self.last_message.startswith("libfranka: Move command rejected")):
                    print('Sending state recover to inria kind Panda!')
                    #envoy.run("rostopic pub -1 /franka_control/error_recovery/goal franka_control/ErrorRecoveryActionGoal \"{}\"")
                    #rospy.sleep(3.0)
                    self.recover()
                '''
                command = raw_input("Do you want to recover one more time? y/n")
                if command=='y':
                    client.send_goal(goal)
                else:
                    pass
                '''

    # def check_rosout(log):
    #     client = actionlib.SimpleActionClient("franka_control/error_recovery", ErrorRecoveryAction)
    #     client.wait_for_server()
    #     goal = ErrorRecoveryActionGoal()
    #     robot_state = log.level
    #     goal.header.stamp.secs = 0
    #     goal.header.stamp.nsecs = 0
    #     goal.header.frame_id = ''
    #     goal.goal_id.stamp.secs = 0
    #     goal.goal_id.stamp.nsecs = 0
    #     goal.goal_id.id = ''
    #     goal.goal = {}
    #     print ('Pandas Level: ', robot_state)
    #     if robot_state == 8:
    #         print('Sending state recover to inria kind Panda!')
    #         # 2s delay to wait for the robot state to be published as ERROR on the Logging and then send the Action
    #         client.send_goal(goal)
    #         #client.wait_for_result(timeout=rospy.Duration.from_sec(4))
    #         print("before sleep")
    #         rospy.sleep(3.0)
    #         print("after sleep")
    #         client.cancel_all_goals()
    #         an_error_made_to_stop_this_function_lol()
    #     else:
    #         print("skip")


if __name__ == '__main__':
    r = rossetup()
