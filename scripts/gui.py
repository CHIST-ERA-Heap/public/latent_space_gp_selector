from pygame.locals import *
import numpy as np
import sys
import pygame
import math
import cv2
import configparser
import time
from scripts.common.drawings_util import generate_image_with_grasps
from scripts.common.pygame_util import blitRotate, move_in_direction, approximation_pixel_to_meters
from scripts.common.grasps_util import depth_txt_to_numpy, get_depth_with_width , grasp_to_rectangle, find_center_of_mask, get_grasp_vector_with_rectangle
from scripts.heap_human_preference_dataset.database import Database
from scripts.camera.camera import Camera
from scripts.grasps_generators.grasps_generators import GraspGenerators, get_grasps_as_vectors, get_length_in_meters
from scripts.models.grasps_evaluator import GraspEvaluator
from scripts.segmentation.segmentation import SegmentationClass
from scripts.panda_control.panda_control import PandaControl
import rospy
from random import randint, shuffle
import os
from scripts.experiments.grasp import GraspRectangle, GraspRectangles, grasp_to_rectangle
from termcolor import colored

# WHITE = (255, 255, 255, 20)
# GREEN = (127, 255, 0)
# BLACK = (0, 0, 0, 20)

BLACK = (0, 0, 0)
GRAY = (127, 127, 127)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
CYAN = (255, 255, 0)
YELLOW = (0, 255, 255)
MAGENTA = (255, 0, 255)

dict_rgb = {}
dict_depth = {}
dict_patches = {}

def events():
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit()

class PygameGraspInterraction:
    def __init__(self, gui_config_path, database_config_path, camera_config_path=None, generator_config_path=None, evaluator_config_path=None, segmentation_config_path=None, panda_config_path=None, setup_ros=True, speed_factor=4.0, verbose=True):
        if setup_ros:
            rospy.init_node("gui_node", anonymous=True)
            # rate = rospy.Rate(10)  # 30hz
            rospy.loginfo("GUI is runing")

        self.speed_factor = speed_factor

        self.config_path = gui_config_path
        self.config = configparser.ConfigParser()
        self.config.read(gui_config_path)
        self.read_cfg()

        if verbose:
            print('setting up Database')
        self.database = Database(database_config_path)
        #load all the scenes from the database
        self.database.load_all_scenes(verbose=False)

        if verbose:
            print('Database ready')

        if verbose:
            print('setting up Segmentation')
        if segmentation_config_path is not None:
            self.segmentation = SegmentationClass(segmentation_config_path)
        else:
            self.segmentation = None
        if verbose:
            print('Segmentation ready')

        if verbose:
            print('setting up GraspGenerators')
        if generator_config_path is not None:
            self.generators = GraspGenerators(generator_config_path)
        else:
            self.generators = None
        if verbose:
            print('GraspGenerators ready')

        if verbose:
            print('setting up GraspEvaluator')
        if evaluator_config_path is not None:
            self.evaluator = GraspEvaluator(evaluator_config_path)
        else:
            self.evaluator = None
        if verbose:
            print('GraspEvaluator ready')

        if verbose:
            print('setting up PandaControl')
        if panda_config_path is not None:
            self.panda_control = PandaControl(panda_config_path)
        else:
            self.panda_control = None

        if verbose:
            print('PandaControl ready')

        if verbose:
            print('setting up Camera')
        self.camera = Camera(camera_config_path)
        #loading camera data from the camera
        self.update_camera()

        if verbose:
            print('Camera ready')

        self.initialize_pygame()
        # self.setup_current_grasp()
        self.reset_positions()
        self.movement_happened = True
        self.movement_happened_movement = False

    def update_camera(self, verbose=False):
        if self.camera.camera_is_connected:
            print('camera.capture_data')
            self.camera.capture_data()
            if self.camera.latest_rgb_image is None:
                print("camera failed to capture new data, check if it is connected and the ros node is launched")
            else:
                '''
                id integer PRIMARY KEY,
                object_id integer NOT NULL,
                path text NOT NULL UNIQUE, 
                rgb_extension text,
                depth_extension text,
                background_path text,
                FOREIGN KEY (object_id) REFERENCES objects (id)
                '''

                temp_current_scene = [-1, 1, self.camera.data_saving_folder, self.camera.rgb_extension,
                                      self.camera.depth_extension, self.camera.path_to_background]
                self.database.load_current_data_from_camera(temp_current_scene, self.camera.latest_rgb_image, self.camera.latest_aligned_depth, self.camera.latest_background_rgb_image, verbose=verbose)
        else:
            self.database.load_current_data(verbose=verbose)
        self.reset_positions()
        self.update_pygame_background()
        self.movement_happened = True


    def read_cfg(self):

        self.show_pygame_interface = self.config.getboolean('GUI', 'show_pygame_interface')
        self.b_test_segmentation = self.config.getboolean('GUI', 'show_segmentation_mask')
        self.b_test_generation = self.config.getboolean('GUI', 'show_generation')
        self.b_test_show_GT = self.config.getboolean('GUI', 'show_ground_truth_labels')
        self.save_pygame_interface = self.config.getboolean('GUI', 'save_pygame_interface')
        self.where_to_save_new_models = self.config.get('GUI', 'where_to_save_new_models')


    def initialize_pygame(self):
        if self.show_pygame_interface:
            # initialise display
            pygame.init()
            self.CLOCK = pygame.time.Clock()
            self.pygame_width = self.database.width
            self.pygame_height = self.database.height
            self.game_surface = pygame.display.set_mode((self.database.width, self.database.height), RESIZABLE)
            # self.screen = pygame.display.set_mode((self.pygame_width, self.pygame_height), RESIZABLE)
            # Set the display mode to the current screen resolution
            self.screen = pygame.display.set_mode((self.pygame_width, self.pygame_height), RESIZABLE)

            # create a square pygame surface
            self.game_surface = pygame.Surface((self.pygame_width, self.pygame_height))

            pygame.display.set_caption(" Grasping Annotation tool ")

            self.update_pygame_background()

            self.FPS = 60

            self.reset_positions()

            self.default_old_k = (
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0)

            self.old_k = self.default_old_k

    def reset_positions(self):
        self.current_dexnet_grasp_iterator = 0

        # track stage position
        self.image_top_left_X = 0
        self.image_top_left_Y = 0
        # Unfiltered counterclockwise rotation. The angle argument represents degrees and can be any floating point value. Negative angle amounts will rotate clockwise.
        self.image_rotation_angle_degree = 0

        # grasp representation
        self.center_of_screen_x = self.database.width / 2
        self.center_of_screen_y = self.database.height / 2
        self.default_gripper_length_pixel = 150
        self.grasp_length_pixel = self.generators.prior_length  # the width between the 2 fingers of the end effector
        self.grasp_width_pixel = self.generators.prior_width

    def get_grasp_x_y_theta_radians(self):
        grasp_position_x = self.center_of_screen_x - self.image_top_left_X
        grasp_position_y = self.center_of_screen_y - self.image_top_left_Y
        rotation_radians = math.radians(self.image_rotation_angle_degree)
        return grasp_position_x, grasp_position_y, rotation_radians

    def update_pygame_background(self, b_test_show_GT_success=True, b_test_show_GT_failures=True, b_test_generation_forced = False):
        if self.show_pygame_interface:

            self.movement_happened = True
            self.current_scene_rgb_image_pygame = self.database.current_scene_rgb_image.copy()
            current_scene_id = self.database.current_scene[0]

            if current_scene_id in self.database.dict_masks:
                self.database.current_scene_mask = self.database.dict_masks[current_scene_id]
            else:
                if self.b_test_segmentation and self.segmentation is not None:
                    self.database.current_scene_mask = self.segmentation.compute_mask(self.database.current_scene_rgb_image, self.database.current_background_scene_rgb_image,
                                          self.database.current_scene_raw_depth_data, self.camera.camera_data.intrinsic_params)
                    print('updating dict_masks with mask from ', current_scene_id)
                    self.database.dict_masks[current_scene_id] = self.database.current_scene_mask

            if self.database.current_scene_mask is not None:
                centers = find_center_of_mask(self.database.current_scene_mask)
                mask = cv2.cvtColor(self.database.current_scene_mask, cv2.COLOR_GRAY2BGR)
                self.current_scene_rgb_image_pygame = cv2.addWeighted(self.current_scene_rgb_image_pygame, 0.7, mask, 0.3, 0)

                for center in centers:
                    self.current_scene_rgb_image_pygame = cv2.circle(self.current_scene_rgb_image_pygame,
                                                                     (int(center[1]), int(center[0])), radius=1,
                                                                     color=(255, 0, 0), thickness=5)

            if self.b_test_show_GT:
                id_user = self.database.get_id_of_GT_user()
                sql_request = 'SELECT * from grasps where user_id =' + str(id_user) + ' AND scene_id=' + str(
                    self.database.current_scene[0])
                # GT_grasps = get_with_request(self.database.conn_gt, sql_request)
                GT_grasps = self.database.get_gt_request(self.database.conn_gt, sql_request, verbose=True)

                print("b_test_show_GT GT_grasps: ", GT_grasps)
                # self.current_scene_rgb_image_pygame = generate_image_with_grasps(GT_grasps, self.current_scene_rgb_image_pygame, b_with_rectangles=True, thickness_line=2, color=(0, 255, 0))
                self.current_scene_rgb_image_pygame = generate_image_with_grasps(GT_grasps, self.current_scene_rgb_image_pygame, b_with_rectangles=True, thickness_line=2, default_color=None)

            if b_test_show_GT_success:
                id_user = self.database.dict_users_id['success']
                sql_request = 'SELECT * from grasps where user_id =' + str(id_user) + ' AND scene_id=' + str(
                    self.database.current_scene[0])
                # GT_grasps = get_with_request(self.database.conn_gt, sql_request)
                GT_grasps = self.database.get_gt_request(self.database.conn_gt, sql_request)

                print("b_test_show_GT_success: ", GT_grasps)
                # self.current_scene_rgb_image_pygame = generate_image_with_grasps(GT_grasps, self.current_scene_rgb_image_pygame, b_with_rectangles=True, thickness_line=2, color=(0, 255, 0))
                self.current_scene_rgb_image_pygame = generate_image_with_grasps(GT_grasps, self.current_scene_rgb_image_pygame, b_with_rectangles=False, thickness_line=2, default_color=CYAN)

            if b_test_show_GT_failures:
                id_user = self.database.dict_users_id['failures']
                sql_request = 'SELECT * from grasps where user_id =' + str(id_user) + ' AND scene_id=' + str(
                    self.database.current_scene[0])
                # GT_grasps = get_with_request(self.database.conn_gt, sql_request)
                GT_grasps = self.database.get_gt_request(self.database.conn_gt, sql_request)

                print("b_test_show_GT_failures: ", GT_grasps)
                # self.current_scene_rgb_image_pygame = generate_image_with_grasps(GT_grasps, self.current_scene_rgb_image_pygame, b_with_rectangles=True, thickness_line=2, color=(0, 255, 0))
                self.current_scene_rgb_image_pygame = generate_image_with_grasps(GT_grasps, self.current_scene_rgb_image_pygame, b_with_rectangles=False, thickness_line=2, default_color=WHITE)

            if self.b_test_generation or b_test_generation_forced:
                if self.database.current_scene[0] in self.database.grasps_candidates:
                    print('b_test_generation_forced: ')
                    print('self.database.grasps_candidates: ', self.database.grasps_candidates)
                    print('self.database.current_scene[0]: ', self.database.current_scene[0])
                    self.current_scene_rgb_image_pygame = generate_image_with_grasps(self.database.grasps_candidates[self.database.current_scene[0]], self.current_scene_rgb_image_pygame, b_with_rectangles=False, thickness_line=1, default_color=None)

            self.img = pygame.image.frombuffer(
                cv2.cvtColor(self.current_scene_rgb_image_pygame, cv2.COLOR_BGR2RGB).tostring(),
                self.database.current_scene_rgb_image.shape[1::-1],
                "RGB")

    def update_currently_loaded(self, skip_database_load_data=False):
        if not skip_database_load_data:
            self.database.load_current_data()
        self.reset_positions()
        self.update_pygame_background()
        scene_id = self.database.current_scene[0]
        if scene_id in self.database.grasps_candidates and len(self.database.grasps_candidates[scene_id]) > 0:
            # self.reset_positions()
            self.correspondance_grasp_to_pygame(self.database.grasps_candidates[scene_id][self.database.current_grasp_iterator])

        self.movement_happened = True

    def deal_with_movements_keys(self, k):
        self.movement_happened = False
        if k[K_q]:  # left
            # print("left")
            # image_top_left_X += 1
            t_x, t_y = move_in_direction(1, 0, self.image_rotation_angle_degree, rate=self.speed_factor)
            self.image_top_left_X += t_x
            self.image_top_left_Y += t_y
            self.movement_happened = True
            self.movement_happened_movement = True

        if k[K_d]:  # right
            # print("right")
            # image_top_left_X += -1
            t_x, t_y = move_in_direction(-1, 0, self.image_rotation_angle_degree,rate=self.speed_factor)
            self.image_top_left_X += t_x
            self.image_top_left_Y += t_y
            self.movement_happened = True
            self.movement_happened_movement = True

        if k[K_z]:  # up
            # print("up")
            # image_top_left_Y += 1
            t_x, t_y = move_in_direction(0, 1, self.image_rotation_angle_degree, rate=self.speed_factor)
            self.image_top_left_X += t_x
            self.image_top_left_Y += t_y
            self.movement_happened = True
            self.movement_happened_movement = True

        if k[K_s]:  # down
            # print("down")
            # image_top_left_Y += -1
            t_x, t_y = move_in_direction(0, -1, self.image_rotation_angle_degree, rate=self.speed_factor)
            self.image_top_left_X += t_x
            self.image_top_left_Y += t_y
            self.movement_happened = True
            self.movement_happened_movement = True

        if k[K_e]:  # antitrigo
            # print("antitrigo")
            self.image_rotation_angle_degree += 1*self.speed_factor
            # if (self.image_rotation_angle_degree > 90):
            #     self.image_rotation_angle_degree = -90
            self.movement_happened = True
            self.movement_happened_movement = True

        if k[K_a]:  # trigo
            # print("trigo")
            self.image_rotation_angle_degree += -1*self.speed_factor
            # if (self.image_rotation_angle_degree <= -90):
            #     self.image_rotation_angle_degree = 90

            self.movement_happened = True
            self.movement_happened_movement = True

        if k[K_w]:  # grasp_width plus
            # print("grasp_width plus")
            # if (self.grasp_length_pixel < self.max_gripper_length_pixel):
            self.grasp_length_pixel += 1*self.speed_factor
            # else:
            #     print("highest value possible")
            self.movement_happened = True
            self.movement_happened_movement = True

        if k[K_c]:  # grasp_width minus
            # print("grasp_width minus")
            if (self.grasp_length_pixel > 1):
                self.grasp_length_pixel += -1*self.speed_factor
            else:
                print("lowest value possible")
            self.movement_happened = True
            self.movement_happened_movement = True

    def remove_grasp(self):

        id_user_GT = self.database.get_id_of_GT_user()
        GT_grasps = self.database.get_grasps_with_request(sql_request='SELECT * from grasps where user_id =' + str(
                                         id_user_GT) + ' AND scene_id=' + str(self.database.current_scene[0]))
        print("len GT_grasps: ", len(GT_grasps))
        if len(GT_grasps) > 0:
            x, y, theta = self.get_grasp_x_y_theta_radians()
            min_dist = 1000000
            index = -1
            a = (x,y)
            for i in range(len(GT_grasps)):
                b = np.array([GT_grasps[i][3], GT_grasps[i][4]])

                dist = np.linalg.norm(a - b)
                if dist < min_dist:
                    min_dist = dist
                    index = i

            print("the closest grasp to ",x,y,theta," is ", GT_grasps[index], ' removed in yellow')
            self.current_scene_rgb_image_pygame = generate_image_with_grasps([GT_grasps[index]], self.current_scene_rgb_image_pygame,
                                                                             b_with_rectangles=True, thickness_line=2,
                                                                             default_color=(0,255,255))

            self.database.execute_with_request(self.database.conn_gt,
                                         sql_request='DELETE FROM grasps WHERE id='+str(GT_grasps[index][0]))

            self.img = pygame.image.frombuffer(
                cv2.cvtColor(self.current_scene_rgb_image_pygame, cv2.COLOR_BGR2RGB).tostring(),
                self.database.current_scene_rgb_image.shape[1::-1],
                "RGB")
            self.movement_happened = True

    def get_current_grasp_on_screen(self, id_scene, id_object, x=None, y=None, euler_z=None, length=None, label=None, user_id=None, verbose=True):
        if x is None:
            x, y, euler_z = self.get_grasp_x_y_theta_radians()
            length = float(self.grasp_length_pixel)
        width = None
        length_meters = None
        euler_x = None
        euler_y = None
        offset_x = None
        offset_y = None
        offset_z = None
        depth_min = None
        quality = None
        #check if the grasp is the one currently loaded:
        if id_scene in self.database.grasps_candidates:
            currently_loaded_grasp = self.database.grasps_candidates[id_scene][self.database.current_grasp_iterator]
            min_dist = 2
            a = (x,y)
            b = np.array([currently_loaded_grasp[3], currently_loaded_grasp[4]])
            dist = np.linalg.norm(a - b)
            if dist < min_dist:
                if user_id is None:
                    user_id = currently_loaded_grasp[2]
                depth_min = currently_loaded_grasp[5]
                euler_x = currently_loaded_grasp[6]
                euler_y = currently_loaded_grasp[7]
                euler_z = currently_loaded_grasp[8]
                length_meters = currently_loaded_grasp[12]
                offset_x = currently_loaded_grasp[13]
                offset_y = currently_loaded_grasp[14]
                offset_z = currently_loaded_grasp[15]
                quality = currently_loaded_grasp[11]
                width = currently_loaded_grasp[10]

        if euler_x is None:
            euler_x = 0.0

        if euler_y is None:
            euler_y = 0.0

        if width is None:
            width = float(self.grasp_width_pixel)

        if user_id is None:
            user_id = self.database.dict_users_id[self.database.GT_user]

        if depth_min is None:
            depth_min, depth_max = get_depth_with_width(grasp_position_x=x,
                                                        grasp_position_y=y,
                                                        rotation_in_radian=euler_z,
                                                        cloud_points=self.database.current_scene_raw_depth_data,
                                                        gripper_length_pixel=self.grasp_length_pixel,
                                                        verbose=False)
        if length_meters is None:
            length_meters = get_length_in_meters(x, y, depth_min, euler_z, length, self.camera.camera_data.intrinsic_params)

        if offset_x is None:
            offset_x = self.generators.expert_grasp_pose_offset_x

        if offset_y is None:
            offset_y = self.generators.expert_grasp_pose_offset_y

        if offset_z is None:
            offset_z = self.generators.expert_grasp_pose_offset_z

        # if quality is None:
        if label is not None:
            quality = label

        insert_grasp = [id_scene, user_id, x, y, float(depth_min), euler_x, euler_y, euler_z, length, width, quality, length_meters, offset_x, offset_y, offset_z, id_object]#todo fix?

        if verbose:
            print('get_current_grasp_on_screen grasp: ', insert_grasp)

        return insert_grasp

    #todo check if the current grasp is from a generator and if so use its parameters
    def save_current_grasp(self, label=0.0):
        id_scene = self.database.current_scene[0]

        if id_scene==-1:
            if self.database.default_object_name is not None:
                self.database.change_name_object(name=self.database.default_object_name)
            else:
                print("this scene is not in the sql database, write the name of the object (clutter or new names are options)")
                self.database.change_name_object()
            id_scene = self.database.current_scene[0]

            #add grasps to scene if computed
            if -1 in self.database.grasps_candidates:
                if len(self.database.grasps_candidates[-1]) > 0:
                    print('adding grasps to sqlite database')

                    for g in self.database.grasps_candidates[-1]:
                        # print('before: ', g)
                        g[1] = id_scene
                        # print('after: ', g)

                        self.database.save_grasp(self.database.conn_grasp_candidates, list(g[1:]))


        id_object = self.database.current_scene[1]
        if id_object == 1 or id_object == 2:
            # while not id_is_correct:
            for object in self.database.list_current_object:
                print(object)
            id_object = int(input("Enter the id of the grasped object: "))

            list_current_object = self.database.get_with_request(self.database.conn_gt, sql_request='SELECT * FROM objects where id='+str(id_object))
            id_is_correct = len(list_current_object) > 0
            if not id_is_correct:
                print('This id does not exists add object name to database')
                self.database.change_name_object(name=self.database.default_object_name)
                id_object = self.database.current_scene[1]

        insert_grasp = self.get_current_grasp_on_screen(id_scene=id_scene, id_object=id_object, label=label)

        insert_grasp_user_id = insert_grasp[1]

        self.database.save_grasp(self.database.conn_gt, insert_grasp, verbose=False)
        if insert_grasp_user_id != self.database.dict_users_id[self.database.GT_user]:
            insert_grasp[1] = self.database.dict_users_id[self.database.GT_user]
            self.database.save_grasp(self.database.conn_gt, insert_grasp, verbose=False)

        insert_grasp.insert(0,-1)
        print('drawn grasp: ', insert_grasp)

        self.current_scene_rgb_image_pygame = generate_image_with_grasps([insert_grasp],
                                                                         self.current_scene_rgb_image_pygame,
                                                                         b_with_rectangles=True, thickness_line=2)

        self.img = pygame.image.frombuffer(
            cv2.cvtColor(self.current_scene_rgb_image_pygame, cv2.COLOR_BGR2RGB).tostring(),
            self.database.current_scene_rgb_image.shape[1::-1],
            "RGB")

        self.movement_happened = True

    def get_grasps_candidates_dict(self, list_id_scenes, b_compute_missing=True, b_random_order=True, verbose=True):
        # current_scene_id == -1 id is temp scene not saved in database

        #shuffle
        if b_random_order:
            shuffle(list_id_scenes)

        #get which type is missing from the grasp candidates database
        for current_scene_id in list_id_scenes:
            if verbose:
                print("loading grasps candidates current_scene_id: ", current_scene_id)
            missing_grasp_planner_service_database_names = []
            missing_ids = []
            all_current_scene_grasps = []

            #if the current scene is in the sql database we search for the saved grasps candidates
            if current_scene_id != -1:
                sql_scene_request = 'SELECT * from grasps WHERE scene_id = '+str(current_scene_id)+' AND user_id = '
                for grasp_planner_service_database_name in self.generators.grasp_planner_service_list_database_names:
                    id_grasp_planner_service_database_name = self.database.dict_users_id[grasp_planner_service_database_name]
                    #get the grasps candidates saved with id_type
                    grasps = self.database.get_gt_request(self.database.conn_grasp_candidates, sql_scene_request+str(id_grasp_planner_service_database_name))

                    grasps = [list(x) for x in grasps]
                    if verbose:
                        print('grasp_planner_service_database_name: ', grasp_planner_service_database_name)
                        print("len grasps: ", len(grasps))

                    if len(grasps)==0:
                        missing_grasp_planner_service_database_names.append(grasp_planner_service_database_name)
                        missing_ids.append(id_grasp_planner_service_database_name)
                    else:
                        all_current_scene_grasps+=grasps
            #if it isnt, we set missing_grasp_planner_service_database_names to all the requested types
            else:
                missing_grasp_planner_service_database_names = self.generators.grasp_planner_service_list_database_names

            len_missing = len(missing_grasp_planner_service_database_names)

            # if b_compute_missing is set to true and there are missing grasps types
            if b_compute_missing and len_missing > 0:
                if verbose:
                    print('missing_grasp_planner_service_database_names: ', missing_grasp_planner_service_database_names)

                #todo this could cause trouble down the line, maybe at some point save it in the database as well
                current_intrinsic_params = self.camera.camera_data.intrinsic_params

                print('self.database.current_scene[0]: ', self.database.current_scene[0])
                if current_scene_id == self.database.current_scene[0]:
                    print('using current self.database rgbd data')
                    current_scene = self.database.current_scene
                    current_scene_rgb_image = self.database.current_scene_rgb_image
                    current_background_scene_rgb_image = self.database.current_background_scene_rgb_image
                    current_scene_raw_depth_data = self.database.current_scene_raw_depth_data
                    # current_intrinsic_params = self.camera.camera_data.intrinsic_params

                    # if self.database.current_scene_mask is not None:
                    #     mask = self.database.current_scene_mask
                    # else:
                    #     print('computing segmask during grasp generation')
                    #     mask = self.segmentation.compute_mask(current_scene_rgb_image,
                    #                                           current_background_scene_rgb_image,
                    #                                           current_scene_raw_depth_data,
                    #                                           current_intrinsic_params)
                    #     print('updating dict_masks with mask from ', current_scene_id)
                    #     self.database.dict_masks[current_scene_id] = mask
                    #     self.database.current_scene_mask = mask

                    if current_scene_id not in self.database.dict_masks and self.segmentation is not None:
                        print('computing segmask during grasp generation')
                        mask = self.segmentation.compute_mask(
                            current_scene_rgb_image, current_background_scene_rgb_image,
                            current_scene_raw_depth_data, current_intrinsic_params)
                        print('updating dict_masks with mask from ', current_scene_id)
                        self.database.dict_masks[current_scene_id] = mask
                    else:
                        if self.segmentation is not None:
                            mask = self.database.dict_masks[current_scene_id]
                        else:
                            mask = None

                else:
                    print('loading scene ',current_scene_id,' from self.database')
                    current_scene = \
                        self.database.get_with_request(self.database.conn_gt,
                                                       sql_request='SELECT * from scenes where id =' + str(
                                                           current_scene_id))[0]
                    print('loaded scene: ', current_scene)
                    current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, _, _, _ = self.database.get_rgb_depth_background_with_current_scene(
                        current_scene)
                    if self.segmentation is not None:
                        mask = self.segmentation.compute_mask(current_scene_rgb_image,
                                                              current_background_scene_rgb_image,
                                                              current_scene_raw_depth_data,
                                                              current_intrinsic_params)
                        print('updating dict_masks with mask from ', current_scene_id)
                        self.database.dict_masks[current_scene_id] = mask
                    else:
                        mask = None

                # create planner request
                planner_req = self.generators.create_planner_req(current_scene_rgb_image, current_scene_raw_depth_data, self.camera.cv_bridge, self.camera.cam_info, self.camera.camera_pose, self.camera.aruco_board_pose, self.camera.enable_grasp_filter,
                                                                 segmentation_image=mask, with_rgb=True, segment_cloud=self.generators.segment_cloud_in_request, verbose=False)
                current_object_id = current_scene[1]

                #query generator for grasps candidates
                for missing_grasp_planner_service_database_name in missing_grasp_planner_service_database_names:
                    print('querying grasps from ', missing_grasp_planner_service_database_name)
                    #get
                    missing_grasp_planner_service_database_id = self.database.dict_users_id[missing_grasp_planner_service_database_name]
                    grasps_from_missing_grasp_planner = self.get_candidates(current_scene_id, current_object_id, planner_req, missing_grasp_planner_service_database_name, missing_grasp_planner_service_database_id)

                    #insert new grasps in database conn_grasp_candidates
                    if current_scene_id != -1:
                        print('adding grasps to sqlite database')

                        for g in grasps_from_missing_grasp_planner:
                            self.database.save_grasp(self.database.conn_grasp_candidates, list(g[1:]))

                    all_current_scene_grasps += grasps_from_missing_grasp_planner

            if len(all_current_scene_grasps) > 0:
                # print('all_current_scene_grasps: ', all_current_scene_grasps)
                all_current_scene_grasps.sort(key=lambda x: x[11], reverse=True)
                self.database.grasps_candidates[current_scene_id] = all_current_scene_grasps

    def get_candidates(self, current_scene_id, current_object_id, planner_req, missing_grasp_planner_service_database_name, missing_grasp_planner_service_database_id):
        reply = self.generators.get_candidates_with_req(planner_req, missing_grasp_planner_service_database_name, verbose=True)

        if reply is None:
            print('generation failed')
            return []

        print('missing_grasp_planner_service_database_name reply: ', reply)
        #reply from generator
        grasps = reply.grasp_candidates
        print('grasps 6D in world: ', grasps)

        grasp_list = get_grasps_as_vectors(grasps, self.camera.camera_data.camera_extrinsic_matrix, self.camera.camera_data.intrinsic_params,
                    scene_id=current_scene_id, object_id=current_object_id, grasp_planner_database_id=missing_grasp_planner_service_database_id, fix_pixel_width=self.generators.prior_width)

        return grasp_list

    def evaluate_grasps(self, list_id_scenes, b_random_order=False, verbose=False):
        # current_scene_id == -1 id is temp scene not saved in database

        #shuffle
        if b_random_order:
            shuffle(list_id_scenes)

        #get which type is missing from the grasp candidates database
        for current_scene_id in list_id_scenes:
            if verbose:
                print("evaluating grasps candidates current_scene_id: ", current_scene_id)

            if current_scene_id in self.database.grasps_candidates:
                if current_scene_id == -1:
                    self.database.dict_rgb[current_scene_id] = self.database.current_scene_rgb_image
                    self.database.dict_depth[current_scene_id] = self.database.current_scene_raw_depth_data
                if current_scene_id not in self.database.dict_rgb:
                    print('loading scene ', current_scene_id, ' from self.database')
                    current_scene = \
                        self.database.get_with_request(self.database.conn_gt,
                                                       sql_request='SELECT * from scenes where id =' + str(
                                                           current_scene_id))[0]
                    print('loaded scene: ', current_scene)
                    current_scene_rgb_image, current_scene_raw_depth_data, _, _, _, _ = self.database.get_rgb_depth_background_with_current_scene(
                        current_scene)
                    self.database.dict_rgb[current_scene_id] = current_scene_rgb_image
                    self.database.dict_depth[current_scene_id] = current_scene_raw_depth_data

                if verbose:
                    print("self.evaluator.rerank: ", current_scene_id, ' len candidates: ', len(self.database.grasps_candidates[current_scene_id]))
                self.database.grasps_candidates[current_scene_id] = self.evaluator.rerank(self.database.grasps_candidates[current_scene_id], dict_rgb_scene = self.database.dict_rgb, dict_depth_raw = self.database.dict_depth)
            else:
                if verbose:
                    print(current_scene_id, ' not in self.database.grasps_candidates')

        self.evaluator.representation_class.save_dicts()

    def execute_grasp(self):
        id_scene = self.database.current_scene[0]
        id_object = self.database.current_scene[1]
        grasp_on_screen = self.get_current_grasp_on_screen(id_scene, id_object=id_object)
        # apply_offset = self.database.dict_users_id[self.database.GT_user] == grasp_on_screen[1]
        apply_offset = True
        self.panda_control.execute_grasp(grasp_on_screen, self.camera.camera_data.intrinsic_params, self.camera.camera_pose, minimum_grasp_depth = self.generators.minimum_grasp_depth, apply_offset=apply_offset)

    def generate_patches_of_currently_loaded_grasp(self, where_to_save_decoded, b_current_scene_only=True):
        if not os.path.exists(where_to_save_decoded):
            print("creating target_folder: ", where_to_save_decoded)
            os.mkdir(where_to_save_decoded)

        if b_current_scene_only:
            scene_iterator_candidates = [self.database.current_scene_iterator]
        else:
            scene_iterator_candidates = [i for i in range(len(self.database.all_scenes))]

        for scene_iterator in scene_iterator_candidates:
            scene_id = self.database.all_scenes[scene_iterator][0]
            if scene_id in self.database.grasps_candidates:
                print("key: ", scene_id)
                current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, height, width, channels = self.database.get_rgb_depth_background_with_current_scene(
                    self.database.all_scenes[scene_iterator])

            # if self.evaluator.representation_class.encoder is not None:
            mu, var = self.evaluator.representation_class.encode_candidates_raw(self.database.grasps_candidates[scene_id], self.database.dict_rgb,
                                                                  self.database.dict_depth, base_name=where_to_save_decoded + str(
                scene_id) + '_')

            print("generate_patches_of_currently_loaded_grasp mu shape: ", mu.shape)
            if var is not None:
                print("generate_patches_of_currently_loaded_grasp var shape: ", var.shape)
            # else:
            #     mu = None
            #     var = None

            if where_to_save_decoded is not None and self.evaluator.representation_class.b_decoder:  # and self.evaluator.representation_class.encoder is not None:
                for j in range(len(self.database.grasps_candidates[scene_id])):
                    if mu is not None:
                        x_latent = mu[j, :]
                        # self.evaluator.representation_class.save_decoded_patch(x_latent.reshape((-1, self.evaluator.representation_class.projection_dim)),
                        #                                              base_name=where_to_save_decoded + str(scene_id) + '_' + str(j))
                        self.evaluator.representation_class.save_decoded_patch(x_latent,
                                                                     base_name=where_to_save_decoded + str(
                                                                         scene_id) + '_' + str(j))

                    generated = generate_image_with_grasps([self.database.grasps_candidates[scene_id][j]],
                                                           current_scene_rgb_image.copy(),
                                                           b_with_rectangles=True,
                                                           thickness_line=1)

                    cv2.imwrite(where_to_save_decoded + str(scene_id) + '_' + str(j) + "_full.png", generated)

                    # g = self.database.grasps_candidates[scene_id][j]
                    # print('g: ', g)
                    # features = self.evaluator.representation_class.extract_single_feature(self.database.dict_rgb[g[1]], self.database.dict_depth[g[1]], g[3], g[4],
                    #                                        math.degrees(g[8]))
                    # self.evaluator.representation_class.save_path(features, where_to_save_decoded + str(scene_id) + '_' + str(j) , ending='_original.png')

    def rectangle_metric_eval(self, id_user, list_id_to_eval=None, b_all_gt=True, update_currently_loaded = False, verbose=True):
        print("rectangle_metric_eval")
        start_time = time.time()


        dict_success = {}
        elapsed_time = time.time() - start_time
        score = 0.0
        sum_success = 0.0
        number_of_scenes_used = 0.0
        dict_best_grasp = {}
        all_scenes_failed = []
        if list_id_to_eval is None:
            list_id_to_eval = self.database.grasps_candidates.keys()

        if verbose:
            print('len list_id_to_eval: ', len(list_id_to_eval))

            print('list_id_to_eval: ', list_id_to_eval)

        if self.evaluator.classifier.model is None:
            print('GP not setup, try pressing i')
        else:
            # self.predict_grasp_of_currently_loaded_grasps(b_current_scene_only=False,
            #                                               show_grasps_after_prediction=False, verbose=verbose)
            # self.evaluate_grasps(list_id_scenes=[scene[0] for scene in self.database.all_scenes], b_random_order=False,
            #                       verbose=verbose)
            self.evaluate_grasps(list_id_scenes=list_id_to_eval, b_random_order=False,
                                  verbose=verbose)


        for key_current_scene_id in list_id_to_eval:
            number_of_scenes_used += 1
            if verbose:
                print("key_id: ", key_current_scene_id)
            try:
                best_grasp = self.database.grasps_candidates[key_current_scene_id][0]
            except Exception as e:
                print(e)
                if key_current_scene_id in self.database.dict_rgb:
                    height, width, channels = self.database.dict_rgb[key_current_scene_id].shape
                else:
                    height = self.camera.camera_resolution_height
                    width = self.camera.camera_resolution_width
                best_grasp = [-1, -1, -1, width/2, height/2, self.database.too_far_to_cam_threshold, 0.0, 0.0, 0.0, self.generators.prior_length,
                     self.generators.prior_length, 1.0]

            dict_best_grasp[key_current_scene_id] = best_grasp
            print("best grasp: ", best_grasp)

            if b_all_gt:
                sql_request = 'SELECT * from grasps where quality =' + str(1.0) + ' AND scene_id=' + str(
                    key_current_scene_id)
            else:
                sql_request = 'SELECT * from grasps where user_id =' + str(
                    id_user) + ' AND quality =' + str(1.0) + ' AND scene_id=' + str(key_current_scene_id)
            print("sql_request: ", sql_request)
            # GT_grasps = get_with_request(self.conn_gt, sql_request)
            GT_grasps = self.database.get_gt_request(self.database.conn_gt, sql_request)

            if len(GT_grasps) > 0:
                print("GT_grasps: ", GT_grasps)
                gt_bbs = GraspRectangles()
                for GT_grasp in GT_grasps:
                    '''
                    id integer PRIMARY KEY,
                    scene_id integer NOT NULL,
                    user_id integer NOT NULL,
                    x real NOT NULL,
                    y real NOT NULL,
                    depth real NOT NULL,
                    euler_x real NOT NULL,
                    euler_y real NOT NULL,
                    euler_z real NOT NULL,
                    length real NOT NULL,
                    width real NOT NULL,
                    quality real NOT NULL,
                    FOREIGN KEY (scene_id) REFERENCES scenes (id),
                    FOREIGN KEY (user_id) REFERENCES user (id)
                    '''
                    gt_bbs.append(GraspRectangle(
                        grasp_to_rectangle(center=(GT_grasp[4], GT_grasp[3]),
                                           angle=GT_grasp[8], length=GT_grasp[9],
                                           width=GT_grasp[10], verbose=False)))

                rectangle = grasp_to_rectangle(center=(best_grasp[4], best_grasp[3]),
                                               angle=best_grasp[8], length=best_grasp[9], width=best_grasp[10],
                                               verbose=False)
                g_gp_rectangle = GraspRectangle(rectangle)
                max_iou = g_gp_rectangle.max_iou(gt_bbs)
                current_success_iou_eval = (max_iou >= 0.25)
            else:
                current_success_iou_eval = False
                max_iou = -1.0

            dict_success[key_current_scene_id] = [current_success_iou_eval, best_grasp,
                                                  elapsed_time / len(list_id_to_eval)]

            if current_success_iou_eval:
                sum_success += 1
            score = (sum_success / number_of_scenes_used) * 100.0
            if current_success_iou_eval:
                print(colored((sum_success, "/", number_of_scenes_used, " r eval: + ", score, " max_iou: ", max_iou),
                              'green'))
                # print(sum_success, "/", number_of_scenes_used, " r eval: + ", score, " max_iou: ", max_iou)
            else:
                print(colored((sum_success, "/", number_of_scenes_used, " r eval: - ", score, " max_iou: ", max_iou),
                              'red'))

                current_scene = \
                    self.database.get_with_request(self.database.conn_gt,
                                     sql_request='SELECT * from scenes where id =' + str(key_current_scene_id))[0]
                all_scenes_failed.append(current_scene)
        if update_currently_loaded:
            if len(all_scenes_failed) > 0:
                print("all_scenes_failed: ")
                print(all_scenes_failed)
                self.database.all_scenes = all_scenes_failed
                self.database.current_scene_iterator = 0
                self.database.current_grasp_iterator = 0
                self.movement_happened = True
            else:
                print("No failed scenes")

        return dict_success, score, dict_best_grasp

    def train_unsupervised(self, path_to_weight_dir=None, scenes_reserved_to_test=[], b_compute_missing=True, object_classifier=False, verbose=False):
        scenes_available_for_training = []
        # scenes = get_with_request(self.database.conn_gt, sql_request='SELECT * FROM scenes')
        scenes = self.database.all_scenes
        dict_rgb_scene = {}
        dict_depth_scene = {}
        dict_background_scene = {}
        dict_id_scene_id_obj = {}

        print('train_unsupervised loading scenes')
        for s in scenes:
            # if not (s[2].rsplit('/', 1)[-1] in scenes_reserved_to_test):
            if s not in scenes_reserved_to_test:
                scenes_available_for_training.append(s)
                current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, height, width, channels = self.database.get_rgb_depth_background_with_current_scene(
                    s)
                # print("adding scene from ", s[0])
                dict_rgb_scene[s[0]] = current_scene_rgb_image
                dict_depth_scene[s[0]] = current_scene_raw_depth_data
                dict_background_scene[s[0]] = current_background_scene_rgb_image
                dict_id_scene_id_obj[s[0]] = s[1]

        print("scenes_available_for_training len: ", len(scenes_available_for_training))

        # dict_scene_grasp = self.get_grasps_candidates_dict(
        #     con=self.database.conn_grasp_candidates,
        #     which_type_of_generated_grasps=self.generators.grasp_planner_service_list_database_names,
        #     b_compute_missing=b_compute_missing,
        #     b_replace_label_ratio_to_max_dist=False,
        #     b_random_order=True,
        #     list_id_scenes=[scene[0] for scene in scenes_available_for_training], verbose=False)

        self.database.grasps_candidates = {}
        self.get_grasps_candidates_dict(b_random_order=True, b_compute_missing=b_compute_missing, list_id_scenes=[scene[0] for scene in scenes_available_for_training], verbose=False)
        dict_scene_grasp = self.database.grasps_candidates
        print("dict_scene_grasp len", len(dict_scene_grasp))

        if object_classifier:
            grasp_list = []
            for scene in scenes_available_for_training:
                scene_id = scene[0]
                for g in dict_scene_grasp[scene_id] :
                    grasp_list.append(g)
            # print('grasp_list: ', grasp_list)
            #todo remove config_path stuff
            self.evaluator.classifier_object.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj, grasp_list,
                    GT_grasps_validation=None, where_to_save_model=path_to_weight_dir, config_path=self.evaluator.evaluator_config_path, verbose=False, dict_encoded_mu=None,
                    dict_encoded_var=None, with_timestamp=True)

        else:
            self.evaluator.representation_class.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, self.evaluator.evaluator_config_path,
                                          where_to_save_model=path_to_weight_dir)

    def train_classifier_with_GT(self, GT_grasps_training, GT_grasps_validation, dict_rgb_scene={}, dict_depth_scene={},
                                 dict_background_scene={}, dict_scene_grasp={}, dict_id_scene_id_obj={},
                                 where_to_save_model=None, b_object_classifier=False, verbose=False):
        if verbose:
            print('train_classifier_with_GT: ')
        # todo refactor this
        if b_object_classifier:
            classifier = self.evaluator.classifier_object
            label_augmentation = self.evaluator.classifier_object.label_augmentation
        else:
            classifier = self.evaluator.classifier
            label_augmentation = self.evaluator.classifier.label_augmentation


        dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj = self.get_dicts_with_grasps(
            GT_grasps_training, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp,
            dict_id_scene_id_obj)
        if label_augmentation:
            # dict_scene_grasp_safe = dict_scene_grasp.copy()
            # GT_grasps_training_safe = GT_grasps_training.copy()
            dict_scene_grasp, GT_grasps_training = self.generate_new_labels(dict_scene_grasp,
                                                                            GT_grasps_training)

        dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj = self.get_dicts_with_grasps(
            GT_grasps_validation, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp,
            dict_id_scene_id_obj)
        if label_augmentation:
            # dict_scene_grasp_safe = dict_scene_grasp.copy()
            # GT_grasps_training_safe = GT_grasps_training.copy()
            dict_scene_grasp, GT_grasps_validation = self.generate_new_labels(dict_scene_grasp,
                                                                              GT_grasps_validation)

        print("train_classifier_with_GT:")
        print("len GT_grasps_training: ", len(GT_grasps_training))
        print("len GT_grasps_validation: ", len(GT_grasps_validation))
        # print("GT_grasps_classifier: ", len(GT_grasps_classifier))

        # todo check that
        if classifier.preprocessing_type == 'feature_extraction':
            if self.evaluator.representation_class.feature_dict_path is not None:
                self.evaluator.representation_class.encode_candidates(GT_grasps_training, dict_rgb_scene, dict_depth_scene)
                self.evaluator.representation_class.encode_candidates(GT_grasps_validation, dict_rgb_scene, dict_depth_scene)

                if classifier.b_with_augmentation:
                    self.evaluator.representation_class.encode_candidates(GT_grasps_training, dict_rgb_scene, dict_depth_scene,
                                                                flipped=[1 for i in range(len(GT_grasps_training))])
                    self.evaluator.representation_class.encode_candidates(GT_grasps_validation, dict_rgb_scene, dict_depth_scene,
                                                                flipped=[1 for i in range(len(GT_grasps_validation))])
                    GT_grasps_training_tweaked = []
                    for g in GT_grasps_training:
                        g = list(g)
                        g[8] -= np.pi
                        GT_grasps_training_tweaked.append(g)
                    GT_grasps_validation_tweaked = []
                    for g in GT_grasps_validation:
                        g = list(g)
                        g[8] -= np.pi
                        GT_grasps_validation_tweaked.append(g)

                    self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
                                                                dict_depth_scene)
                    self.evaluator.representation_class.encode_candidates(GT_grasps_validation_tweaked, dict_rgb_scene,
                                                                dict_depth_scene)
                    self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
                                                                dict_depth_scene, flipped=[1 for i in range(
                            len(GT_grasps_training_tweaked))])
                    self.evaluator.representation_class.encode_candidates(GT_grasps_validation_tweaked, dict_rgb_scene,
                                                                dict_depth_scene, flipped=[1 for i in range(
                            len(GT_grasps_validation_tweaked))])

                    GT_grasps_training_tweaked = []
                    for g in GT_grasps_training:
                        g = list(g)
                        g[8] += np.pi
                        GT_grasps_training_tweaked.append(g)
                    GT_grasps_validation_tweaked = []
                    for g in GT_grasps_validation:
                        g = list(g)
                        g[8] += np.pi
                        GT_grasps_validation_tweaked.append(g)

                    self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
                                                                dict_depth_scene)
                    self.evaluator.representation_class.encode_candidates(GT_grasps_validation_tweaked, dict_rgb_scene,
                                                                dict_depth_scene)
                    self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
                                                                dict_depth_scene,
                                                                flipped=[1 for i in
                                                                         range(len(GT_grasps_training_tweaked))])
                    self.evaluator.representation_class.encode_candidates(GT_grasps_validation_tweaked, dict_rgb_scene,
                                                                dict_depth_scene,
                                                                flipped=[1 for i in
                                                                         range(len(GT_grasps_validation_tweaked))])

            if b_object_classifier:
                if verbose:
                    print('self.evaluator.classifier_object.fit: ')
                classifier.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene,
                                           dict_id_scene_id_obj,
                                           GT_grasps_training, GT_grasps_validation,
                                           where_to_save_model=where_to_save_model,
                                           config_path=self.config_path)
            else:
                if self.evaluator.classifier.by_object and self.evaluator.classifier_object.train_with_classifier_data:
                # TODO train unsupervised?
                    self.evaluator.classifier_object.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene,
                                                            dict_id_scene_id_obj,
                                                            GT_grasps_training, GT_grasps_validation,
                                                            where_to_save_model = where_to_save_model,
                                                            config_path = self.config_path)
                classifier.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj,
                           GT_grasps_training, GT_grasps_validation, where_to_save_model=where_to_save_model,
                           config_path=self.config_path)

            self.evaluator.representation_class.save_dicts()
        else:

            if b_object_classifier:
                if verbose:
                    print('self.evaluator.classifier_object.fit: ')

                classifier.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene,
                                           dict_id_scene_id_obj,
                                           GT_grasps_training, GT_grasps_validation,
                                           where_to_save_model=where_to_save_model,
                                           config_path=self.config_path)
            else:
                if self.evaluator.classifier.by_object and self.evaluator.classifier_object.train_with_classifier_data:
                # TODO train unsupervised?
                    self.evaluator.classifier_object.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene,
                                                            dict_id_scene_id_obj,
                                                            GT_grasps_training, GT_grasps_validation,
                                                            where_to_save_model = where_to_save_model,
                                                            config_path = self.config_path)

                classifier.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj,
                           GT_grasps_training, GT_grasps_validation, where_to_save_model=where_to_save_model,
                           config_path=self.config_path)

        return dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj

    def train_length_regressor_with_GT(self, GT_grasps_training, GT_grasps_validation, dict_rgb_scene={},
                                       dict_depth_scene={}, dict_background_scene={}, dict_scene_grasp={},
                                       dict_id_scene_id_obj={}, where_to_save_model=None):
        #                                                                                           get_dicts_with_grasps( GT_grasps, dict_rgb_scene = {}, dict_depth_scene = {}, dict_background_scene = {}, dict_scene_grasp = {}, dict_id_scene_id_obj = {}, positive_only=False)
        dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj = self.get_dicts_with_grasps(
            GT_grasps=GT_grasps_training, dict_rgb_scene=dict_rgb_scene, dict_depth_scene=dict_depth_scene,
            dict_background_scene=dict_background_scene, dict_scene_grasp=dict_scene_grasp,
            dict_id_scene_id_obj=dict_id_scene_id_obj)
        dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj = self.get_dicts_with_grasps(
            GT_grasps=GT_grasps_validation, dict_rgb_scene=dict_rgb_scene, dict_depth_scene=dict_depth_scene,
            dict_background_scene=dict_background_scene, dict_scene_grasp=dict_scene_grasp,
            dict_id_scene_id_obj=dict_id_scene_id_obj)

        print("length_regressor_with_GT:")
        print("len GT_grasps_training: ", len(GT_grasps_training))
        print("len GT_grasps_validation: ", len(GT_grasps_validation))
        # print("GT_grasps_classifier: ", len(GT_grasps_classifier))

        if self.evaluator.length_regressor.preprocessing_type == 'feature_extraction':

            self.evaluator.length_regressor.fit_length(dict_rgb_scene, dict_depth_scene, dict_background_scene,
                                             dict_id_scene_id_obj, GT_grasps_training,
                                             GT_grasps_validation, where_to_save_model=where_to_save_model,
                                             config_path=self.config_path,
                                             dict_encoded_mu=self.evaluator.representation_class.encoded_mu_dict,
                                             dict_encoded_var=self.evaluator.representation_class.encoded_var_dict)

            self.evaluator.representation_class.save_dicts()
        else:
            self.evaluator.length_regressor.fit_length(dict_rgb_scene, dict_depth_scene, dict_background_scene,
                                             dict_id_scene_id_obj, GT_grasps_training,
                                             GT_grasps_validation, where_to_save_model=where_to_save_model,
                                             config_path=self.config_path)

        return dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj

    def generate_new_labels(self, dict_scene_grasp, GT_grasps_classifier, false_only=False, balance=True,
                            angle_threshold=np.pi / 12,
                            iou_threshold=0.5):  # angle_threshold=np.pi/12, iou_threshold=0.5):
        # def generate_new_labels(self, dict_scene_grasp, GT_grasps_classifier, false_only=True, balance=False,
        #                             angle_threshold=np.pi / 12, iou_threshold=0.5):

        # print('generating_new_labels: ', self.label_augmentation)
        self.database.remove_temporary_gt()
        # execute_with_request(self.conn_gt,
        #                      sql_request='DELETE FROM grasps WHERE user_id=' + str(self.dict_users_id["success"]))

        # execute_with_request(self.conn_gt,
        #                      sql_request='DELETE FROM grasps WHERE user_id=' + str(self.dict_users_id["failures"]))

        for id_scene in dict_scene_grasp:

            if id_scene in self.database.grasps_candidates:
                candidates = self.database.grasps_candidates[id_scene]
            else:
                # candidates = self.get_grasps_candidates_dict(con=self.conn_grasp_candidates,
                #                                              which_type_of_generated_grasps=self.type_of_generated_grasps,
                #                                              b_compute_missing=True,
                #                                              b_replace_label_ratio_to_max_dist=False,
                #                                              list_id_scenes=[id_scene])
                self.get_grasps_candidates_dict(list_id_scenes=[id_scene], b_compute_missing=True,
                                                verbose=True)

                # candidates = candidates[id_scene]
                # self.database.grasps_candidates[id_scene] = candidates
                candidates = self.database.grasps_candidates[id_scene]

            count_pos = 0
            count_neg = 0
            gt_bbs = GraspRectangles()
            GT_grasps_x_y_theta = []
            GT_grasps_pos_only = []
            for GT_grasp in dict_scene_grasp[id_scene]:
                GT_grasps_x_y_theta.append([GT_grasp[3], GT_grasp[4], GT_grasp[8]])
                '''
                id integer PRIMARY KEY,
                scene_id integer NOT NULL,
                user_id integer NOT NULL,
                x real NOT NULL,
                y real NOT NULL,
                depth real NOT NULL,
                euler_x real NOT NULL,
                euler_y real NOT NULL,
                euler_z real NOT NULL,
                length real NOT NULL,
                width real NOT NULL,
                quality real NOT NULL,
                FOREIGN KEY (scene_id) REFERENCES scenes (id),
                FOREIGN KEY (user_id) REFERENCES user (id)
                '''
                if GT_grasp[11] > 0:
                    GT_grasps_pos_only.append(GT_grasp)
                    count_pos += 1
                    gt_bbs.append(GraspRectangle(
                        grasp_to_rectangle(center=(GT_grasp[4], GT_grasp[3]),
                                           angle=GT_grasp[8], length=GT_grasp[9],
                                           width=GT_grasp[10], verbose=False)))
                else:
                    count_neg += 1

            if count_pos > 0:

                for grasp in candidates:

                    g_x_y_theta = [grasp[3], grasp[4], grasp[8]]

                    if len(GT_grasps_pos_only) > 0:
                        x, y, theta = g_x_y_theta
                        min_dist = 1000000
                        index = -1
                        a = (x, y)
                        for i in range(len(GT_grasps_pos_only)):
                            b = np.array([GT_grasps_pos_only[i][3], GT_grasps_pos_only[i][4]])

                            dist = np.linalg.norm(a - b)
                            if dist < min_dist:
                                min_dist = dist
                                index = i
                        l = GT_grasps_pos_only[index][9]
                        # l = grasp[9]
                    # print('grasp: ', grasp)

                    rectangle = grasp_to_rectangle(center=(grasp[4], grasp[3]),
                                                   angle=grasp[8], length=l, width=grasp[10],
                                                   verbose=False)
                    g_gp_rectangle = GraspRectangle(rectangle)
                    max_iou = g_gp_rectangle.max_iou(gt_bbs, angle_threshold)

                    a = (max_iou == 0)
                    b = (max_iou >= iou_threshold)
                    if false_only:
                        b = False
                    if a or b:
                        if b:
                            grasp[11] = 1.0
                            id_user = self.database.dict_users_id['success']

                        else:
                            if a:
                                grasp[11] = 0.0
                                id_user = self.database.dict_users_id['failures']

                        go = True
                        if balance:
                            more_neg = count_neg < count_pos
                            if (more_neg and grasp[11] == 1.0) or (not more_neg and grasp[11] == 0.0):
                                go = False
                        if go:
                            if g_x_y_theta not in GT_grasps_x_y_theta:
                                dict_scene_grasp[id_scene].append(grasp)
                                GT_grasps_classifier.append(grasp)
                                GT_grasps_x_y_theta.append(g_x_y_theta)

                                #insert_grasp = [id_scene, user_id, x, y, float(depth_min), euler_x, euler_y, euler_z, length, width, quality, length_meters, offset_x, offset_y, offset_z]#todo fix?
                                # insert_grasp = [grasp[1], id_user, grasp[3], grasp[4], grasp[5], grasp[6], grasp[7],
                                # grasp[8], grasp[9], grasp[10], grasp[11], 0.0, 0.0, 0.0,
                                # 0.0]  # todo fix?
                                insert_grasp = self.get_current_grasp_on_screen(id_scene, id_object=grasp[16], x=grasp[3], y=grasp[4], euler_z=grasp[8], length=grasp[9],
                                                            label=grasp[11], user_id=id_user,
                                                            verbose=False)

                                # print('generate_new_labels insert_grasp: ', insert_grasp)
                                self.database.save_grasp(self.database.conn_gt, insert_grasp)

                            if b:
                                count_pos += 1
                            if a:
                                count_neg += 1

            # print('count_neg: ', count_neg)
            # print('count_pos: ', count_pos)
        return dict_scene_grasp, GT_grasps_classifier

    # def train_unsupervised(self, path_to_weight_dir=None, scenes_reserved_to_test=[], b_compute_missing=True,
    #                        object_classifier=False):
    #     scenes_available_for_training = []
    #     # scenes = get_with_request(self.conn_gt, sql_request='SELECT * FROM scenes')
    #     scenes = self.database.all_scenes
    #     dict_rgb_scene = {}
    #     dict_depth_scene = {}
    #     dict_background_scene = {}
    #     dict_id_scene_id_obj = {}
    #
    #     for s in scenes:
    #         # if not (s[2].rsplit('/', 1)[-1] in scenes_reserved_to_test):
    #         if s not in scenes_reserved_to_test:
    #             scenes_available_for_training.append(s)
    #             current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, height, width, channels = self.database.get_rgb_depth_background_with_current_scene(
    #                 s)
    #             # print("adding scene from ", s[0])
    #             dict_rgb_scene[s[0]] = current_scene_rgb_image
    #             dict_depth_scene[s[0]] = current_scene_raw_depth_data
    #             dict_background_scene[s[0]] = current_background_scene_rgb_image
    #             dict_id_scene_id_obj[s[0]] = s[1]
    #
    #     print("scenes_available_for_training len: ", len(scenes_available_for_training))
    #
    #     # dict_scene_grasp = self.get_grasps_candidates_dict(
    #     #     con=self.conn_grasp_candidates,
    #     #     which_type_of_generated_grasps=self.type_of_generated_grasps,
    #     #     b_compute_missing=b_compute_missing,
    #     #     b_replace_label_ratio_to_max_dist=False,
    #     #     b_random_order=True,
    #     #     list_id_scenes=[scene[0] for scene in scenes_available_for_training], verbose=False)
    #
    #     self.get_grasps_candidates_dict(list_id_scenes=[scene[0] for scene in scenes_available_for_training], b_compute_missing=False,
    #                                     verbose=False)
    #
    #     # print("dict_scene_grasp len", len(dict_scene_grasp))
    #
    #     if object_classifier:
    #         grasp_list = []
    #         for scene in scenes_available_for_training:
    #             scene_id = scene[0]
    #             for g in self.database.grasps_candidates[scene_id]:
    #                 grasp_list.append(g)
    #         # print('grasp_list: ', grasp_list)
    #         self.evaluator.classifier_object.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj,
    #                                    grasp_list,
    #                                    GT_grasps_validation=[], where_to_save_model=None, config_path=None,
    #                                    verbose=False, dict_encoded_mu=None,
    #                                    dict_encoded_var=None, with_timestamp=True)
    #
    #
    #     else:
    #         self.evaluator.representation_class.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, self.database.grasps_candidates,
    #                                       self.config_path,
    #                                       where_to_save_model=path_to_weight_dir)

    # def train_classifier_with_GT(self, GT_grasps_training, GT_grasps_validation, dict_rgb_scene={}, dict_depth_scene={},
    #                              dict_background_scene={}, dict_scene_grasp={}, dict_id_scene_id_obj={},
    #                              where_to_save_model=None, b_object_classifier=False):
    #
    #     # todo refactor this
    #     if b_object_classifier:
    #         classifier = self.evaluator.classifier_object
    #         label_augmentation = self.evaluator.classifier_object.label_augmentation
    #     else:
    #         classifier = self.evaluator.classifier
    #         label_augmentation = self.evaluator.classifier.label_augmentation
    #
    #     dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj = self.get_dicts_with_grasps(
    #         GT_grasps_training, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp,
    #         dict_id_scene_id_obj)
    #     if label_augmentation:
    #         # dict_scene_grasp_safe = dict_scene_grasp.copy()
    #         # GT_grasps_training_safe = GT_grasps_training.copy()
    #         dict_scene_grasp, GT_grasps_training = self.generate_new_labels(dict_scene_grasp,
    #                                                                         GT_grasps_training)
    #
    #     dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj = self.get_dicts_with_grasps(
    #         GT_grasps_validation, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp,
    #         dict_id_scene_id_obj)
    #     if label_augmentation:
    #         # dict_scene_grasp_safe = dict_scene_grasp.copy()
    #         # GT_grasps_training_safe = GT_grasps_training.copy()
    #         dict_scene_grasp, GT_grasps_validation = self.generate_new_labels(dict_scene_grasp,
    #                                                                           GT_grasps_validation)
    #
    #     print("train_classifier_with_GT:")
    #     print("len GT_grasps_training: ", len(GT_grasps_training))
    #     print("len GT_grasps_validation: ", len(GT_grasps_validation))
    #     # print("GT_grasps_classifier: ", len(GT_grasps_classifier))
    #
    #     # todo check that
    #     if not b_object_classifier and self.evaluator.classifier.preprocessing_type == 'feature_extraction':
    #         if self.evaluator.representation_class.feature_dict_path is not None:
    #             self.evaluator.representation_class.encode_candidates(GT_grasps_training, dict_rgb_scene, dict_depth_scene)
    #             self.evaluator.representation_class.encode_candidates(GT_grasps_validation, dict_rgb_scene, dict_depth_scene)
    #
    #             if self.evaluator.classifier.b_with_augmentation:
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_training, dict_rgb_scene, dict_depth_scene,
    #                                                             flipped=[1 for i in range(len(GT_grasps_training))])
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_validation, dict_rgb_scene, dict_depth_scene,
    #                                                             flipped=[1 for i in range(len(GT_grasps_validation))])
    #                 GT_grasps_training_tweaked = []
    #                 for g in GT_grasps_training:
    #                     g = list(g)
    #                     g[8] -= np.pi
    #                     GT_grasps_training_tweaked.append(g)
    #                 GT_grasps_validation_tweaked = []
    #                 for g in GT_grasps_validation:
    #                     g = list(g)
    #                     g[8] -= np.pi
    #                     GT_grasps_validation_tweaked.append(g)
    #
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
    #                                                             dict_depth_scene)
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_validation_tweaked, dict_rgb_scene,
    #                                                             dict_depth_scene)
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
    #                                                             dict_depth_scene, flipped=[1 for i in range(
    #                         len(GT_grasps_training_tweaked))])
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_validation_tweaked, dict_rgb_scene,
    #                                                             dict_depth_scene, flipped=[1 for i in range(
    #                         len(GT_grasps_validation_tweaked))])
    #
    #                 GT_grasps_training_tweaked = []
    #                 for g in GT_grasps_training:
    #                     g = list(g)
    #                     g[8] += np.pi
    #                     GT_grasps_training_tweaked.append(g)
    #                 GT_grasps_validation_tweaked = []
    #                 for g in GT_grasps_validation:
    #                     g = list(g)
    #                     g[8] += np.pi
    #                     GT_grasps_validation_tweaked.append(g)
    #
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
    #                                                             dict_depth_scene)
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_validation_tweaked, dict_rgb_scene,
    #                                                             dict_depth_scene)
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
    #                                                             dict_depth_scene,
    #                                                             flipped=[1 for i in
    #                                                                      range(len(GT_grasps_training_tweaked))])
    #                 self.evaluator.representation_class.encode_candidates(GT_grasps_validation_tweaked, dict_rgb_scene,
    #                                                             dict_depth_scene,
    #                                                             flipped=[1 for i in
    #                                                                      range(len(GT_grasps_validation_tweaked))])
    #
    #         if self.evaluator.classifier.by_object and self.evaluator.classifier_object.train_with_classifier_data:
    #             # TODO train unsupervised?
    #             self.evaluator.classifier_object.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene,
    #                                        dict_id_scene_id_obj, GT_grasps_training,
    #                                        GT_grasps_validation, where_to_save_model=where_to_save_model,
    #                                        config_path=self.config_path,
    #                                        dict_encoded_mu=self.evaluator.representation_class.encoded_mu_dict,
    #                                        dict_encoded_var=self.evaluator.representation_class.encoded_var_dict)
    #
    #         classifier.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj,
    #                        GT_grasps_training, GT_grasps_validation, where_to_save_model=where_to_save_model,
    #                        config_path=self.config_path, dict_encoded_mu=self.evaluator.representation_class.encoded_mu_dict,
    #                        dict_encoded_var=self.evaluator.representation_class.encoded_var_dict)
    #         self.evaluator.representation_class.save_dicts()
    #     else:
    #         if self.evaluator.classifier.by_object and self.evaluator.classifier_object.train_with_classifier_data:
    #             # TODO train unsupervised?
    #             self.evaluator.classifier_object.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene,
    #                                        dict_id_scene_id_obj,
    #                                        GT_grasps_training, GT_grasps_validation,
    #                                        where_to_save_model=where_to_save_model,
    #                                        config_path=self.config_path)
    #         classifier.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj,
    #                        GT_grasps_training, GT_grasps_validation, where_to_save_model=where_to_save_model,
    #                        config_path=self.config_path)
    #
    #     return dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj

    def get_data_for_expert_model_training(self, scenes_reserved_to_test=[], positve_only=False, max_size_training=80, b_loaded_only=True):
        scenes_available_for_training = []
        if b_loaded_only:
            scenes = self.database.all_scenes
        else:
            scenes = self.database.get_with_request(self.database.conn_gt, sql_request=self.sql_scene_request)

        for s in scenes:
            if not (s[2].rsplit('/', 1)[-1] in scenes_reserved_to_test):
                scenes_available_for_training.append(s)

        id_user_GT = self.database.dict_users_id[self.database.GT_user]

        grasps_available_for_training = []
        for scene in scenes_available_for_training:
            if positve_only:
                sql_request = 'SELECT * from grasps where user_id =' + str(
                    id_user_GT) + ' AND scene_id=' + str(scene[0]) + 'AND quality =' + str(1.0)
                # GT_grasps = get_with_request(self.database.conn_gt,
                #                              sql_request)
                GT_grasps = self.database.get_gt_request(self.database.conn_gt, sql_request)

            else:
                sql_request = 'SELECT * from grasps where user_id =' + str(
                    id_user_GT) + ' AND scene_id=' + str(scene[0])
                # GT_grasps = get_with_request(self.database.conn_gt,
                #                              sql_request)
                GT_grasps = self.database.get_gt_request(self.database.conn_gt, sql_request)


            grasps_available_for_training += GT_grasps
        print('grasps_available_for_training: ', grasps_available_for_training)
        if max_size_training is not None:
            if len(grasps_available_for_training) > max_size_training:
                grasps_available_for_training = grasps_available_for_training[:max_size_training]

        print(len(grasps_available_for_training), ' grasps_available_for_training from ',len(scenes_available_for_training), " scenes")

        dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj= self.get_dicts_with_grasps(grasps_available_for_training)
        return dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj, grasps_available_for_training

    def get_dicts_with_grasps(self, GT_grasps, dict_rgb_scene = {}, dict_depth_scene = {}, dict_background_scene = {}, dict_scene_grasp = {}, dict_id_scene_id_obj = {}, positive_only=False):
        # print("get_dicts_with_grasps: ", GT_grasps)
        for g in GT_grasps:
            if not positive_only or g[11] > 0:
                # print("g: ", g)
                scene_id = g[1]
                if scene_id not in dict_scene_grasp:
                    dict_scene_grasp[scene_id] = []

                dict_scene_grasp[scene_id].append(g)

                if scene_id not in dict_rgb_scene or scene_id not in dict_id_scene_id_obj:
                    # print('SELECT * from scenes where id =' + str(scene_id))
                    current_scene = \
                        self.database.get_with_request(self.database.conn_gt,
                                         sql_request='SELECT * from scenes where id =' + str(scene_id), verbose=False)[0]
                    # print("current_scene: ", current_scene)
                    current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, height, width, channels = self.database.get_rgb_depth_background_with_current_scene(
                        current_scene)

                    # print("adding scene from ", s[0])
                    dict_rgb_scene[scene_id] = current_scene_rgb_image
                    dict_depth_scene[scene_id] = current_scene_raw_depth_data
                    dict_background_scene[scene_id] = current_background_scene_rgb_image
                    dict_id_scene_id_obj[scene_id] = current_scene[1]


        return dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj

    def train_classifier(self, max_size_training=None, where_to_save_model=None, b_loaded_only=False):
        dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj, GT_grasps_classifier = self.get_data_for_expert_model_training(
            max_size_training=max_size_training, b_loaded_only=b_loaded_only)

        if len(GT_grasps_classifier) > 0:
            # print("GT_grasps_classifier: ", len(GT_grasps_classifier))
            shuffle(GT_grasps_classifier)
            length = len(GT_grasps_classifier)
            split_index = int(length * 0.7)
            GT_grasps_training = GT_grasps_classifier[:split_index]
            GT_grasps_validation = GT_grasps_classifier[split_index:]
            print("train_classifier:")
            print("len GT_grasps_training: ", len(GT_grasps_training))
            print("len GT_grasps_validation: ", len(GT_grasps_validation))

            if self.evaluator.classifier.label_augmentation:
                dict_scene_grasp_safe = dict_scene_grasp.copy()
                GT_grasps_training_safe = GT_grasps_training.copy()
                GT_grasps_validation_safe = GT_grasps_validation.copy()
                dict_scene_grasp, GT_grasps_training = self.generate_new_labels(dict_scene_grasp,
                                                                                GT_grasps_training)
                dict_scene_grasp, GT_grasps_validation = self.generate_new_labels(dict_scene_grasp,
                                                                                  GT_grasps_validation)

            # todo check that
            if self.evaluator.classifier.preprocessing_type == 'feature_extraction':
                if self.evaluator.representation_class.feature_dict_path is not None:
                    self.evaluator.representation_class.encode_candidates(GT_grasps_classifier, dict_rgb_scene, dict_depth_scene)
                    if self.evaluator.classifier.b_with_augmentation:
                        self.evaluator.representation_class.encode_candidates(GT_grasps_classifier, dict_rgb_scene,
                                                                    dict_depth_scene, flipped=[1 for i in range(
                                len(GT_grasps_classifier))])
                        GT_grasps_training_tweaked = []
                        for g in GT_grasps_classifier:
                            g = list(g)
                            g[8] -= np.pi
                            GT_grasps_training_tweaked.append(g)

                        self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
                                                                    dict_depth_scene)
                        self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
                                                                    dict_depth_scene, flipped=[1 for i in range(
                                len(GT_grasps_training_tweaked))])

                        GT_grasps_training_tweaked = []
                        for g in GT_grasps_classifier:
                            g = list(g)
                            g[8] += np.pi
                            GT_grasps_training_tweaked.append(g)

                        self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
                                                                    dict_depth_scene)
                        self.evaluator.representation_class.encode_candidates(GT_grasps_training_tweaked, dict_rgb_scene,
                                                                    dict_depth_scene,
                                                                    flipped=[1 for i in
                                                                             range(len(GT_grasps_training_tweaked))])

                if self.evaluator.classifier.by_object and self.evaluator.classifier_object.train_with_classifier_data:
                    # TODO train unsupervised?
                    self.evaluator.classifier_object.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene,
                                               dict_id_scene_id_obj,
                                               GT_grasps_training, GT_grasps_validation,
                                               where_to_save_model=where_to_save_model, config_path=self.config_path,
                                               dict_encoded_mu=self.evaluator.representation_class.encoded_mu_dict,
                                               dict_encoded_var=self.evaluator.representation_class.encoded_var_dict)

                print("self.evaluator.classifier.fit")
                self.evaluator.classifier.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj,
                                    GT_grasps_training, GT_grasps_validation, where_to_save_model=where_to_save_model,
                                    config_path=self.config_path,
                                    dict_encoded_mu=self.evaluator.representation_class.encoded_mu_dict,
                                    dict_encoded_var=self.evaluator.representation_class.encoded_var_dict)
                self.evaluator.representation_class.save_dicts()
            else:
                if self.evaluator.classifier.by_object and self.evaluator.classifier_object.train_with_classifier_data:
                    # TODO train unsupervised?
                    self.evaluator.classifier_object.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene,
                                               dict_id_scene_id_obj,
                                               GT_grasps_training, GT_grasps_validation,
                                               where_to_save_model=where_to_save_model, config_path=self.config_path)

                print("self.evaluator.classifier.fit")
                self.evaluator.classifier.fit(dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj,
                                    GT_grasps_training, GT_grasps_validation, where_to_save_model=where_to_save_model,
                                    config_path=self.config_path)
        else:
            print("No labels to train with")
            GT_grasps_training = []
            GT_grasps_validation = []


        # if b_object_classifier:
        #     classifier = self.evaluator.classifier_object
        #     label_augmentation = self.evaluator.classifier_object.label_augmentation
        # else:
        #     classifier = self.evaluator.classifier
        # label_augmentation = self.evaluator.classifier.label_augmentation

        if self.evaluator.classifier.label_augmentation:
            return dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp_safe, dict_id_scene_id_obj, GT_grasps_training_safe, GT_grasps_validation_safe
        else:
            return dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation

    def deal_with_one_keys_event(self, k):

        if (k[K_x]):
            if self.evaluator is not None:
                if self.evaluator.representation_class is not None:
                    print("train object_classifier unsupervised")
                    self.train_unsupervised(path_to_weight_dir=None, scenes_reserved_to_test=[], b_compute_missing=False, object_classifier=True)
            else:
                print('evaluator is None')

        if (k[K_i]):
            #todo give option to save these networks
            if self.evaluator is not None:
                #setup correct method
                print("train_classifier")
                dict_rgb_scene_GT, dict_depth_scene_GT, dict_background_scene_GT, dict_scene_grasp_GT, dict_id_scene_id_obj_GT, GT_grasps_training, GT_grasps_validation = self.train_classifier(b_loaded_only=True, where_to_save_model=self.where_to_save_new_models)
                if self.evaluator.predict_gripper_opening and len(GT_grasps_training) >0:
                    print("train_length_regressor")
                    self.train_length_regressor_with_GT(GT_grasps_training, GT_grasps_validation, dict_rgb_scene_GT, dict_depth_scene_GT, dict_background_scene_GT, dict_scene_grasp_GT, dict_id_scene_id_obj_GT, where_to_save_model=self.where_to_save_new_models)
                print("READY FOR PREDICTION")
            else:
                print('evaluator is None')

        #todo
        # if (k[K_x]):
        #     print("train object_classifier unsupervised")
        #     self.train_unsupervised(path_to_weight_dir=None, scenes_reserved_to_test=[], b_compute_missing=False, object_classifier=True)

        if (k[K_u]):
            print("compute furthest grasp from GT")
            self.AL_from_grasp_latent_of_currently_loaded_scene(show_grasps_after_prediction=False, sort_by_var=False,
                                                                sort_by_distance=True)

        if (k[K_y]):
            print("compute closest grasp from GT from other scenes")
            _ = self.compute_closest_grasp_from_GTs()
            self.evaluator.representation_class.save_dicts()
            # self.AL_from_grasp_latent_of_currently_loaded_scene(show_grasps_after_prediction=True, sort_by_var=False, sort_by_distance=True, reverse=False)

        if (k[K_k]):
            print("train with GT from other scenes of this object")
            _ = self.compute_closest_grasp_from_GTs(exclude_current=True, reverse=True, train=True,
                                                    train_length_regressor=False)  # todo set to true
            self.evaluator.representation_class.save_dicts()
            # self.AL_from_grasp_latent_of_currently_loaded_scene(show_grasps_after_prediction=True, sort_by_var=False, sort_by_distance=True, reverse=False)
            print("READY FOR PREDICTION")

        if (k[K_j]):
            print("train with GT from this scene")
            _ = self.compute_closest_grasp_from_GTs(exclude_current=False, with_others=False, reverse=True, train=True,
                                                    train_length_regressor=True)  # todo set to true
            self.evaluator.representation_class.save_dicts()
            # self.AL_from_grasp_latent_of_currently_loaded_scene(show_grasps_after_prediction=True, sort_by_var=False, sort_by_distance=True, reverse=False)
            print("READY FOR PREDICTION")

        if (k[K_h]):
            if self.evaluator is not None:
                print("testing length prediction")
                new_grasp_length, is_antipodal, _ = self.test_length_prediction()
                self.grasp_length_pixel = new_grasp_length
                print("new_grasp_length: ", self.grasp_length_pixel)

                # self.current_scene_rgb_image_pygame[edges_contour > 0] = (255, 255, 0)
                # self.img = pygame.image.frombuffer(
                #     cv2.cvtColor(self.current_scene_rgb_image_pygame, cv2.COLOR_BGR2RGB).tostring(),
                #     self.current_scene_rgb_image.shape[1::-1],
                #     "RGB")

                self.movement_happened = True
            else:
                print('evaluator is None')


        if (k[K_t]):
            print("compute closest grasp from GT")
            self.AL_from_grasp_latent_of_currently_loaded_scene(show_grasps_after_prediction=True, sort_by_var=False,
                                                                sort_by_distance=True, reverse=False)

            # if (k[K_t]):
            #     print("compute most unsure latent grasp")
            #     self.AL_from_grasp_latent_of_currently_loaded_scene(show_grasps_after_prediction=False, sort_by_var=True, sort_by_distance=False)

        if (k[K_l]):
            print("compute most unsure gp grasp")
            self.AL_from_grasp_latent_of_currently_loaded_scene(show_grasps_after_prediction=True, sort_by_var=False,
                                                                sort_by_distance=False, sort_by_pred_uncertainty=True)

        if (k[K_KP4]):
            print("save supervised models")

            path_to_weights = input("Enter the directory where the models should be: ")
            if not os.path.exists(path_to_weights):
                print("creating: ", path_to_weights)
                os.makedirs(path_to_weights)

            if self.evaluator.classifier.model is not None:
                self.evaluator.classifier.save_weights(path_to_weights)
            if self.evaluator.length_regressor.model_length is not None:
                self.evaluator.length_regressor.save_weights(path_to_weights)

            self.movement_happened = True

        if (k[K_KP1]):
            print("put robot in origin position")
            r = self.panda_control.go_home_joints()

        if (k[K_KP2]):
            print("executing grasp")
            self.execute_grasp()

        if (k[K_KP0]):
            print("get candidates for all scenes")
            start_time = time.time()

            self.get_grasps_candidates_dict(list_id_scenes=[scene[0] for scene in self.database.all_scenes], verbose=True)
            self.database.current_grasp_iterator = 0
            elapsed_time = time.time() - start_time
            print("candidates generated in: ", elapsed_time)
            self.update_pygame_background(b_test_generation_forced=True)

            self.database.current_grasp_iterator = 0
            self.correspondance_grasp_to_pygame(
                self.database.grasps_candidates[self.database.current_scene[0]][self.database.current_grasp_iterator])
            self.movement_happened = True

        if (k[K_o]):
            print("get candidates for current scene")
            start_time = time.time()

            current_scene_id = self.database.current_scene[0]
            self.get_grasps_candidates_dict(list_id_scenes=[current_scene_id], verbose=True)

            elapsed_time = time.time() - start_time
            print("candidates generated in: ", elapsed_time)
            self.update_pygame_background(b_test_generation_forced=True)
            if self.database.current_scene[0] in self.database.grasps_candidates:
                if len(self.database.grasps_candidates[self.database.current_scene[0]]) > 0:
                    self.database.current_grasp_iterator = 0
                    self.correspondance_grasp_to_pygame(
                        self.database.grasps_candidates[self.database.current_scene[0]][self.database.current_grasp_iterator])
            self.movement_happened = True

        if (k[K_p]):
            if self.evaluator is None:
                print("evaluate all scenes")
                if self.evaluator.classifier is None:
                    print('self.evaluator.classifier not setup, try pressing i')
                else:
                    self.evaluate_grasps(list_id_scenes=[scene[0] for scene in self.database.all_scenes], b_random_order=True, verbose=True)
                    self.update_pygame_background(b_test_generation_forced=True)
                    if self.database.current_scene[0] in self.database.grasps_candidates:
                        if len(self.database.grasps_candidates[self.database.current_scene[0]]) > 0:
                            self.database.current_grasp_iterator = 0
                            self.correspondance_grasp_to_pygame(
                                self.database.grasps_candidates[self.database.current_scene[0]][self.database.current_grasp_iterator])
                    self.movement_happened = True
            else:
                print('evaluator is None')

        #f eval current
        if (k[K_f]):
            if self.evaluator is not None:
                print("evaluate current scene")
                if self.evaluator.classifier is None:
                    print('self.evaluator.classifier not setup, try pressing i')
                else:
                    start_time = time.time()
                    self.evaluate_grasps(list_id_scenes=[self.database.current_scene[0]])
                    self.update_pygame_background(b_test_generation_forced=True)
                    self.database.current_grasp_iterator = 0
                    if len(self.database.grasps_candidates[self.database.current_scene[0]]) > 0:
                        self.correspondance_grasp_to_pygame(
                            self.database.grasps_candidates[self.database.current_scene[0]][self.database.current_grasp_iterator])
                    self.movement_happened = True
                    elapsed_time = time.time() - start_time
                    print("candidates ranked in: ", elapsed_time)
            else:
                print('evaluator is None')

        if (k[K_v]):
            #todo hard coded
            self.generate_patches_of_currently_loaded_grasp(where_to_save_decoded="../experiments_results/test_save_decoded/")


        if (k[K_KP3]):
            print("update camera scene")
            self.update_camera()

        if (k[K_KP5]):
            print("Load scenes of this object")
            self.database.all_scenes = \
                self.database.get_with_request(self.database.conn_gt,
                                 sql_request='SELECT * from scenes where object_id =' + str(self.database.current_scene[1]))
            self.database.current_scene_iterator = 0
            self.database.current_grasp_iterator = 0
            self.movement_happened = True


        if (k[K_KP9]):
            self.database.load_all_scenes()
            self.database.current_scene_iterator = 0
            self.database.current_grasp_iterator = 0
            self.movement_happened = True

        if (k[K_RIGHT]):  # next
            # print("next")
            self.database.current_scene_iterator += 1
            self.database.current_grasp_iterator = 0
            self.update_currently_loaded()


        if (k[K_LEFT]):  # previous
            # print("previous")
            self.database.current_scene_iterator -= 1
            self.database.current_grasp_iterator = 0
            self.update_currently_loaded()


        if (k[K_r]):  # reset #TODO grasp_width
            # print("reset")
            print("reload all scenes")
            self.database.remove_temporary_gt()

            self.reset_positions()
            self.database.current_grasp_iterator = 0
            self.update_currently_loaded()
            self.movement_happened = True

        if (k[K_g]):
            print("save current grasp as good")
            self.save_current_grasp(label=1.0)

        if (k[K_b]):
            print("save current grasp as bad")
            self.save_current_grasp(label=0.0)

        if (k[K_n]):
            print("remove closest grasp")
            self.remove_grasp()

        if (k[K_m]):
            print("change_name_object")
            self.database.change_name_object()

        if (k[K_UP]):
            # if self.current_scene_iterator <= len(self.all_scenes)-1:
            scene_id = self.database.current_scene[0]
            if scene_id in self.database.grasps_candidates:
                print("next grasp")
                self.database.current_grasp_iterator += 1
                len_database_grasps_candidates = len(self.database.grasps_candidates[scene_id])
                if (self.database.current_grasp_iterator > len_database_grasps_candidates - 1):
                    self.database.current_grasp_iterator = 0
                self.correspondance_grasp_to_pygame(self.database.grasps_candidates[scene_id][self.database.current_grasp_iterator])
                self.movement_happened = True

        if (k[K_DOWN]):
            # if self.current_scene_iterator <= len(self.all_scenes)-1:
            scene_id = self.database.current_scene[0]
            if scene_id in self.database.grasps_candidates:
                print("previous grasp")
                self.database.current_grasp_iterator += -1
                len_database_grasps_candidates = len(self.database.grasps_candidates[scene_id])
                if (self.database.current_grasp_iterator < 0):
                    self.database.current_grasp_iterator = len_database_grasps_candidates - 1
                self.correspondance_grasp_to_pygame(self.database.grasps_candidates[scene_id][self.database.current_grasp_iterator])
                self.movement_happened = True

    def correspondance_grasp_to_pygame(self, grasp):
        print("correspondance_grasp_to_pygame: ", grasp)
        self.image_top_left_X = self.center_of_screen_x - grasp[3]
        self.image_top_left_Y = self.center_of_screen_y - grasp[4]

        current_angle = math.degrees(grasp[8])

        self.image_rotation_angle_degree = current_angle
        self.grasp_length_pixel = grasp[9]


    # TODO key pressed is too sensible, maybe fix by using event.type == pygame.KEYDOWN or event.type == pygame.KEYUP
    def key_pressed(self, k):
        self.deal_with_movements_keys(k)

        if not (k == self.old_k):  # here check if no key was pressed
            self.old_k = k

            self.deal_with_one_keys_event(k)

    def events(self):
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                sys.exit()
            elif event.type == VIDEORESIZE:
                self.pygame_width = event.w
                self.pygame_height = event.h
                self.movement_happened = True

    def start(self):
        b_first = True
        while True:

            self.events()

            k = pygame.key.get_pressed()

            self.key_pressed(k)

            if self.movement_happened or b_first:
                print("self.image_top_left_X: ", self.image_top_left_X)
                print("self.image_top_left_Y: ", self.image_top_left_Y)
                print("self.image_rotation_angle_degree: ", self.image_rotation_angle_degree)
                print("current grasp_length_pixel: ", self.grasp_length_pixel)
                # print("x: ",x," y: ", y, "theta: ", theta)
                b_first = False

                self.game_surface.blit(self.img, (self.image_top_left_X, self.image_top_left_Y))
                pos = (self.center_of_screen_x, self.center_of_screen_y)

                blitRotate(self.game_surface, self.game_surface, pos, pos, self.image_rotation_angle_degree)

                pygame.draw.line(self.game_surface, (0, 255, 255, 127), (pos[0] - int(self.grasp_length_pixel / 2), pos[1]),
                                 (pos[0] + int(self.grasp_length_pixel / 2), pos[1]), width=3)



                # pygame.draw.line(self.game_surface, (0, 255, 255, 127), (self.crop_left, self.crop_top),
                #                  (self.crop_left, self.height - self.crop_bottom), width=1)
                #
                # pygame.draw.line(self.game_surface, (0, 255, 255, 127), (self.width - self.crop_right, self.crop_top),
                #                  (self.width - self.crop_right, self.height - self.crop_bottom), width=1)
                #
                # pygame.draw.line(self.game_surface, (0, 255, 255, 127), (self.crop_left, self.height - self.crop_bottom),
                #                  (self.width - self.crop_right, self.height - self.crop_bottom), width=1)
                #
                # pygame.draw.line(self.game_surface, (0, 255, 255, 127), (self.crop_left, self.crop_top),
                #                  (self.width - self.crop_right, self.crop_top), width=1)

                # pygame.draw.line(screen, (0, 255, 0), (pos[0], pos[1]-20), (pos[0], pos[1]+20), 3)
                circleRadius = 3


                pygame.draw.circle(self.game_surface, WHITE, (int(pos[0]), int(pos[1])), circleRadius, 0)

                x, y, theta = self.get_grasp_x_y_theta_radians()
                try:
                    depth_min, depth_max = get_depth_with_width(grasp_position_x=x,
                                                        grasp_position_y=y,
                                                        rotation_in_radian=theta,
                                                        cloud_points=self.database.current_scene_raw_depth_data,
                                                        gripper_length_pixel=self.grasp_length_pixel,
                                                        verbose=False)
                except Exception as e:
                    print(e)
                    # depth_min, depth_max = get_depth_with_width(grasp_position_x=x,
                    #                                             grasp_position_y=y,
                    #                                             rotation_in_radian=theta,
                    #                                             cloud_points=self.database.current_scene_raw_depth_data,
                    #                                             gripper_length_pixel=self.grasp_length_pixel,
                    #                                             verbose=False)
                    depth_min = self.database.too_far_to_cam_threshold
                '''
                some text to tract the variables
                '''
                pygame.draw.rect(self.game_surface, BLACK, (0, 0, 100, 100))
                # pygame.draw.rect(self.game_surface, BLACK, (0, 0, 150, 100))
                my_font = pygame.font.Font('freesansbold.ttf', 25)

                textsurface_depth = my_font.render(str(depth_min)[:6]+"m" , False,GREEN)
                self.game_surface.blit(textsurface_depth, (int(pos[0]), int(pos[1])))

                textsurface_current_scene_iterator = my_font.render(
                    (str(self.database.current_scene_iterator) + "/" + (str(len(self.database.all_scenes) - 1))), False,
                    GREEN)
                self.game_surface.blit(textsurface_current_scene_iterator, (0, 0))

                # textsurface_grasp_length_pixel = my_font.render(str(self.grasp_length_pixel), False, (127, 255, 0))
                # textsurface_image_top_left_X = my_font.render(str(self.image_top_left_X), False, (127, 255, 0))
                # textsurface_image_top_left_Y = my_font.render(str(self.image_top_left_Y), False, (127, 255, 0))
                # textsurface_image_rotation_angle_degree = my_font.render(str(self.image_rotation_angle_degree), False, (127, 255, 0))
                # self.game_surface.blit(textsurface_grasp_length_pixel, (10, 26))
                # self.game_surface.blit(textsurface_image_top_left_X, (10, 52))
                # self.game_surface.blit(textsurface_image_top_left_Y, (10, 88))
                # self.game_surface.blit(textsurface_image_rotation_angle_degree, (10, 114))

                textsurface_current_scene_iterator = my_font.render(
                    (str(self.database.current_scene_iterator) + "/" + (str(len(self.database.all_scenes) - 1))), False,
                    GREEN)
                self.game_surface.blit(textsurface_current_scene_iterator, (0, 0))

                scene_id = self.database.current_scene[0]
                if scene_id in self.database.grasps_candidates:
                    textsurface_current_grasp_iterator = my_font.render(
                        (str(self.database.current_grasp_iterator) + "/" + (str(len(self.database.grasps_candidates[scene_id]) - 1))), False,
                        GREEN)
                    self.game_surface.blit(textsurface_current_grasp_iterator, (0, 26))
                    try:
                        textsurface_predicted_score = my_font.render(
                                             str(self.database.grasps_candidates[scene_id][self.database.current_grasp_iterator][11])[:4],
                                             False, (127, 255, 0))
                    except Exception as e:
                        print(e)
                        textsurface_predicted_score = my_font.render(
                                             str(-1.0),
                                             False, (127, 255, 0))

                    self.game_surface.blit(textsurface_predicted_score, (0, 52))
                # else:
                #     print("scene_id not in self.database.grasps_candidates")

                # if (self.database.current_scene in self.dict_dexnet_grasps):
                #     textsurface5 = my_font.render(str(self.current_dexnet_grasp_iterator) + "/" + str(
                #         len(self.dict_dexnet_grasps[self.database.current_scene]) - 1), False, (127, 255, 0))
                #     textsurface6 = my_font.render(str(self.current_dexnet_grasp_evaluation), False, (127, 255, 0))
                #     self.game_surface.blit(textsurface5, (0, 26))
                #     self.game_surface.blit(textsurface6, (0, 52))
                #
                #     if (self.database.current_scene in self.dict_GP_scores_grasps):
                #         if (self.current_dexnet_grasp_iterator < len(self.dict_GP_scores_grasps[self.database.current_scene])):
                #             # print("len: "+str(len(self.list_GP_scores_grasps)))
                #             # print("current_dexnet_grasp_iterator: "+str(self.current_dexnet_grasp_iterator))
                #             # print(self.list_GP_scores_grasps)
                #             textsurface7 = my_font.render(
                #                 str(self.dict_GP_scores_grasps[self.database.current_scene][self.current_dexnet_grasp_iterator]),
                #                 False, (127, 255, 0))
                #         self.game_surface.blit(textsurface7, (0, 78))

                # make the largest square surface that will fit on the screen
                # screen_width = self.screen.get_width()
                # screen_height = self.screen.get_height()
                screen_width = self.pygame_width
                screen_height = self.pygame_height

                # smallest_side = min(screen_width, screen_height)
                # screen_surface = pygame.Surface((smallest_side, smallest_side))
                #
                # # scale the game surface up to the larger surface
                # pygame.transform.scale(
                #     self.game_surface,  # surface to be scaled
                #     (smallest_side, smallest_side),  # scale up to (width, height)
                #     screen_surface)  # surface that game_surface will be scaled onto
                #
                # # place the larger surface in the centre of the screen
                # self.screen.blit(
                #     screen_surface,
                #     ((screen_width - smallest_side) // 2,  # x pos
                #      (screen_height - smallest_side) // 2))  # y pos

                screen_surface = pygame.Surface((screen_width, screen_height))

                # scale the game surface up to the larger surface
                pygame.transform.scale(
                    self.game_surface,  # surface to be scaled
                    (screen_width, screen_height),  # scale up to (width, height)
                    screen_surface)  # surface that game_surface will be scaled onto

                # place the larger surface in the centre of the screen
                self.screen.blit(
                    screen_surface,
                    ((screen_width - screen_width) // 2,  # x pos
                     (screen_height - screen_height) // 2))  # y pos




                pygame.display.update()
                self.CLOCK.tick(self.FPS)

                pygame.display.flip()
                # pygame.time.delay(30)
                self.movement_happened = False
                if self.save_pygame_interface and not self.movement_happened_movement:
                    pygame.image.save(screen_surface, "./pygame_screen.png")
                self.movement_happened_movement = False

        pygame.quit()
