import configparser
import sensor_msgs.point_cloud2
import cv2
import rospy
import sensor_msgs.point_cloud2
from sensor_msgs.point_cloud2 import read_points
from grasping_benchmarks.base.transformations import quaternion_to_matrix, matrix_to_quaternion
from grasping_benchmarks_ros.srv import GraspPlanner, GraspPlannerRequest, GraspPlannerResponse
from grasping_benchmarks.base.grasp import Grasp6D
from scripts.camera.camera import convert_bgrd_to_pcl
import numpy as np
from random import randint, shuffle
from geometry_msgs.msg import PoseStamped
from scipy.spatial.transform import Rotation as R
from scipy.spatial import distance
import tf2_ros
import tf
from grasping_benchmarks_ros.msg import BenchmarkGrasp
import math
from scripts.camera.camera import CameraData
from sensor_msgs.msg import CameraInfo
from geometry_msgs.msg import TransformStamped
from cv_bridge import CvBridge
from scripts.segmentation.segmentation import SegmentationClass

def get_grasps_as_vectors(grasps, extrinsic_matrix, intrinsic_params, scene_id, object_id, grasp_planner_database_id, fix_pixel_width, verbose=False):
    # cam_T_world : world pose in camera ref frame

    cam_T_world = np.linalg.inv(extrinsic_matrix)

    grasp_list_pixels = []
    grasp_list_pixels_pos= []

    for candidate in grasps:
        candidate_pose_orientation = candidate.pose.pose.orientation
        candidate_pose_position = candidate.pose.pose.position
        candidate_pose_score = candidate.score.data
        candidate_length_meters = candidate.width.data
        candidate_offset = np.array(candidate.offset)

        candidate_pose_orientation, candidate_pose_position = transform_world_grasp_to_cam(
            candidate_pose_orientation, candidate_pose_position, cam_T_world)


        x, y, z, euler = transform_6Dgrasp_to_pixels(candidate_pose_orientation,
                                                     candidate_pose_position, candidate_offset,
                                                     intrinsic_params)
        #todo
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        length_meters real NOT NULL,
        offset_x real NOT NULL,
        offset_y real NOT NULL,
        offset_z real NOT NULL,
        FOREIGN KEY (scene_id) REFERENCES scenes (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        #convert gripper length to pixels:
        # length_meters = get_length_in_meters(x, y, z, euler[2], length_px, intrinsic_params)
        X = candidate_pose_position.x
        Y = candidate_pose_position.y
        # z = candidate_pose_position.z

        offset_x = candidate_offset[0]
        offset_y = candidate_offset[1]
        offset_z =  candidate_offset[2]

        length_pixels = get_length_in_pixels(X, Y, candidate_pose_position.z, euler, candidate_length_meters, intrinsic_params)

        new_grasp = [-1, scene_id, grasp_planner_database_id, x, y, z, euler[0], euler[1], euler[2], length_pixels, fix_pixel_width, candidate_pose_score, candidate_length_meters, offset_x, offset_y, offset_z, object_id]

        if new_grasp[:9] not in grasp_list_pixels_pos:
            grasp_list_pixels_pos.append(new_grasp[:9])
            grasp_list_pixels.append(new_grasp)
        if verbose:
            print('grasp_6D from reply: ')
            print('candidate_pose_position: ', candidate_pose_position)
            print('candidate_pose_orientation: ', candidate_pose_orientation)
            print('candidate_length_meters: ', candidate_length_meters)
            print('candidate_pose_score: ', candidate_pose_score)
            print('candidate_offset: ', candidate_offset)
            print('offset_x: ', offset_x)
            print('offset_y: ', offset_y)
            print('offset_z: ', offset_z)

            print('grasp vector from reply: ', new_grasp)

            angle = euler[2]
            length_meters = get_length_in_meters(x, y, z, angle, length_pixels, intrinsic_params)

            pose_6d = transform_grasp_to_6D(x, y, z, euler, intrinsic_params, candidate_offset)
            pos = pose_6d[:3, 3]
            rot = pose_6d[:3, :3]

            '''
            position : (`numpy.ndarray` of float): 3-entry position vector wrt camera frame
            rotation (`numpy.ndarray` of float):3x3 rotation matrix wrt camera frame
            width : Distance between the fingers in meters.
            score: prediction score of the grasp pose
            ref_frame: frame of reference for camera that the grasp corresponds to.
            quaternion: rotation expressed as quaternion
            '''

            grasp_6D = Grasp6D(position=pos, rotation=rot,
                               width=length_meters, score=float(candidate_pose_score),
                               ref_frame=intrinsic_params['frame'])
            print('grasp_6D from reply reconstructed: ')
            print('position: ', pos)
            print('rotation: ', rot)
            print('width: ', length_meters)
            print('score: ', candidate_pose_score)
            print('candidate_offset: ', candidate_offset)

    # print('grasp_list_pixels: ', grasp_list_pixels)
    return grasp_list_pixels

def transform_world_grasp_to_cam(candidate_pose_orientation, candidate_pose_position, cam_T_world, verbose=False) -> PoseStamped:

    # cam_T_world : camera pose in world ref frame

    # world_T_grasp : grasp pose in world ref frame
    # cam_T_grasp = w_T_cam * cam_T_grasp

    # Construct the 4x4 affine transf matrices from ROS stamped poses
    grasp_quat = candidate_pose_orientation
    grasp_pos = candidate_pose_position
    world_T_grasp = np.eye(4)
    world_T_grasp[:3,:3] = quaternion_to_matrix([grasp_quat.x,
                                               grasp_quat.y,
                                               grasp_quat.z,
                                               grasp_quat.w])
    world_T_grasp[:3,3] = np.array([grasp_pos.x, grasp_pos.y, grasp_pos.z])

    # Obtain the w_T_grasp affine transformation
    cam_T_grasp = np.matmul(cam_T_world, world_T_grasp)

    if verbose:
        print("[DEBUG] transform_world_grasp_to_cam Grasp pose reference system transform")
        print("cam_T_world\n ", cam_T_world)
        print("world_T_grasp\n ", world_T_grasp)
        print("cam_T_grasp\n ", cam_T_grasp)


    # Create and return a StampedPose object
    cam_T_grasp_quat = matrix_to_quaternion(cam_T_grasp[:3,:3])

    result_pose = PoseStamped()
    result_pose.pose.orientation.x = cam_T_grasp_quat[0]
    result_pose.pose.orientation.y = cam_T_grasp_quat[1]
    result_pose.pose.orientation.z = cam_T_grasp_quat[2]
    result_pose.pose.orientation.w = cam_T_grasp_quat[3]
    result_pose.pose.position.x = cam_T_grasp[0,3]
    result_pose.pose.position.y = cam_T_grasp[1,3]
    result_pose.pose.position.z = cam_T_grasp[2,3]


    return result_pose.pose.orientation, result_pose.pose.position

#todo grasp_offset
def transform_grasp_to_6D(x, y, z, angle, intrinsic_params_dict, grasp_offset):
    """Planar to 6D grasp pose
    Args:
        grasp_pose (obj:`gqcnn.grasping.Grasp2D` or :obj:`gqcnn.grasping.SuctionPoint2D`)
        camera_intrinsics (obj: `CameraIntrinsics`)
    Returns:
        cam_T_grasp (np.array(shape=(4,4))): 6D transform of the grasp pose wrt camera frame
    """
    '''
    camera_data.intrinsic_params = {
                            'fx' : fx,
                            'fy' : fy,
                            'cx' : cx,
                            'cy' : cy,
                            'skew' : skew,
                            'w' : w,
                            'h' : h,
                            'frame' : cam_intrinsic_frame,
                            'matrix' : intrinsic_matrix
                            }
    '''
    fx = intrinsic_params_dict['fx']
    fy = intrinsic_params_dict['fy']
    cx = intrinsic_params_dict['cx']
    cy = intrinsic_params_dict['cy']

    u = x - cx
    v = y - cy

    X = (z * u) / fx
    Y = (z * v) / fy

    grasp_pos = [X, Y, z]
    euler = [angle[0], angle[1], -1.57 + angle[2]]

    rot = R.from_euler('xyz', euler)
    cam_R_grasp = rot.as_dcm()

    cam_T_grasp = np.append(cam_R_grasp, np.array([grasp_pos]).T, axis=1)
    cam_T_grasp = np.append(cam_T_grasp, np.array([[0, 0, 0, 1]]), axis=0)

    grasp_target_T_panda_ef = np.eye(4)
    # grasp_target_T_panda_ef[2, 3] = -0.13
    grasp_target_T_panda_ef[:3, 3] = grasp_offset

    cam_T_grasp = np.matmul(cam_T_grasp, grasp_target_T_panda_ef)

    return cam_T_grasp

def transform_6Dgrasp_to_pixels(candidate_pose_orientation, candidate_pose_position, candidate_offset, intrinsic_params_dict):
   # candidate is in camera reference frame

    quat_list = [candidate_pose_orientation.x,
                candidate_pose_orientation.y,
                candidate_pose_orientation.z,
                candidate_pose_orientation.w]
    roll, pitch, yaw = tf.transformations.euler_from_quaternion(quat_list)

    X = candidate_pose_position.x
    Y = candidate_pose_position.y
    z = candidate_pose_position.z

    grasp_pos = [X, Y, z]
    euler = [roll, pitch, yaw +1.57]
    rot = R.from_euler('xyz', euler)
    cam_R_grasp = rot.as_dcm()

    cam_T_grasp = np.append(cam_R_grasp, np.array([grasp_pos]).T, axis=1)
    cam_T_grasp = np.append(cam_T_grasp, np.array([[0, 0, 0, 1]]), axis=0)

    grasp_target_T_panda_ef = np.eye(4)
    grasp_target_T_panda_ef[:3, 3] = -candidate_offset
    cam_T_grasp = np.matmul(grasp_target_T_panda_ef, cam_T_grasp)
    #we have taken away the offset

    X = cam_T_grasp[0,3]
    Y = cam_T_grasp[1,3]
    z = cam_T_grasp[2,3]

    # cam_intrinsic_frame = camera_info.header.frame_id,
    # fx = camera_info.K[0]
    # fy = camera_info.K[4]
    # cx = camera_info.K[2]
    # cy = camera_info.K[5]
    # skew = 0.0
    # w = camera_info.width
    # h = camera_info.height
    fx = intrinsic_params_dict['fx']
    fy = intrinsic_params_dict['fy']
    cx = intrinsic_params_dict['cx']
    cy = intrinsic_params_dict['cy']

    x = (X * fx / z) + cx
    y = (Y * fy / z) + cy

    #return yaw
    return x, y, z, euler


def project_image_point_to_pointcloud(pixel_x, pixel_y, depth_meters, intrinsic_params_dict, verbose=False):

    fx = intrinsic_params_dict['fx']
    fy = intrinsic_params_dict['fy']
    cx = intrinsic_params_dict['cx']
    cy = intrinsic_params_dict['cy']

    u = pixel_x - cx
    v = pixel_y - cy

    X = (depth_meters * u) / fx
    Y = (depth_meters * v) / fy

    point_cloud_point = [X, Y, depth_meters]
    if verbose:
        print('point_cloud_point: ', point_cloud_point)

    return point_cloud_point

def get_length_in_pixels(X, Y, z, euler, width_in_meters, intrinsic_params_dict, scale=1.0, verbose=False):
    fx = intrinsic_params_dict['fx']
    fy = intrinsic_params_dict['fy']
    cx = intrinsic_params_dict['cx']
    cy = intrinsic_params_dict['cy']

    point_3D = np.array([X, Y, z], dtype=np.float32)

    rot_matrix = R.from_euler('xyz', euler).as_matrix()

    # p1 = [width_in_meters, 0, 0]
    # p2 = [0, width_in_meters, 0]
    # p3 = [0, 0, width_in_meters]
    p1 = [width_in_meters/2.0, 0, 0]
    p2 = [-width_in_meters/2.0, 0, 0]

    p1 = np.dot(rot_matrix, p1)
    p2 = np.dot(rot_matrix, p2)
    # p3 = np.dot(rot_matrix, p3)
    # points = scale * np.float32([point_3D + p1, point_3D + p2, point_3D + p3, point_3D + [0, 0, 0]]).reshape(-1, 3)
    points = scale * np.float32([point_3D + p1, point_3D + p2]).reshape(-1, 3)
    if verbose:
        print('point_3D: ', point_3D)
        print('X: ', X)
        print('Y: ', Y)
        print('z: ', z)
        print('euler: ', euler)
        print('rot_matrix: ', rot_matrix)
        print('points[0]: ', points[0])
        print('points[1]: ', points[1])
        print('points: ', points)
        print('width_in_meters: ', width_in_meters)

        dst = distance.euclidean(points[0], points[1])
        print('scipy distance: ', dst)

    r_vec = [0, 0, 0]
    r_vec = np.array(r_vec, dtype=float)
    t = np.zeros((1, 3), dtype=float)
    distCoeffs = np.zeros(4, dtype=float)
    camera_matrix = np.zeros((3, 3), dtype=float)
    camera_matrix[0][0] = fx
    camera_matrix[0][2] = cx
    camera_matrix[1][1] = fy
    camera_matrix[1][2] = cy
    camera_matrix[2][2] = 1

    axis_points, _ = cv2.projectPoints(points, r_vec, t, camera_matrix, distCoeffs)

    dst_pixels = distance.euclidean(axis_points[0], axis_points[1])
    if verbose:
        print('axis_points: ', axis_points)
        print('dst_pixels: ', dst_pixels)

    return dst_pixels

def get_length_in_meters(x, y, z, angle, length_dx, intrinsic_params_dict, fix_value_meter_to_add=0.0, verbose=False):
    if verbose:
        print('x: ', x)
        print('y: ', y)
        print('z: ', z)
        print('angle: ', angle)
        print('length_dx: ', length_dx)

    rotation_matrix = np.zeros((2, 2))
    rotation_matrix[0][0] = math.cos(angle)
    rotation_matrix[0][1] = -math.sin(angle)
    rotation_matrix[1][0] = math.sin(angle)
    rotation_matrix[1][1] = math.cos(angle)

    p1 = np.zeros((2, 1))
    p2 = np.zeros((2, 1))

    p1[0][0] = -int(length_dx / 2)
    p2[0][0] = int(length_dx / 2)

    p1_rotated = np.dot(rotation_matrix, p1)
    p2_rotated = np.dot(rotation_matrix, p2)

    p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
    p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))

    p_grasp_right = project_image_point_to_pointcloud(p2_recentered[0], p2_recentered[1], z, intrinsic_params_dict)

    p_grasp_left = project_image_point_to_pointcloud(p1_recentered[0], p1_recentered[1], z, intrinsic_params_dict)

    length_in_meters = np.linalg.norm(
        np.asarray(p_grasp_right) - np.asarray(p_grasp_left)) + fix_value_meter_to_add

    if verbose:
        # print('center: ', p_grasp)
        print('right_finger: ', p_grasp_right)
        print('left_finger: ', p_grasp_left)
        print('length_in_meters: ', length_in_meters)

    return length_in_meters

#this uses the grasps candidates from the grasping-benchmarks-panda
class GraspGenerators:
    def __init__(self, generators_config_path):
        self.cfg = configparser.ConfigParser()
        self.cfg.read(generators_config_path)
        self.read_cfg()
        self.setup_generators()

    def read_cfg(self):
        self.prior_length = self.cfg.getfloat('grasp_generators', 'prior_length')
        self.prior_width = self.cfg.getfloat('grasp_generators', 'prior_width')
        self.expert_grasp_pose_offset_x = self.cfg.getfloat('grasp_generators', 'expert_grasp_pose_offset_x')
        self.expert_grasp_pose_offset_y = self.cfg.getfloat('grasp_generators', 'expert_grasp_pose_offset_y')
        self.expert_grasp_pose_offset_z = self.cfg.getfloat('grasp_generators', 'expert_grasp_pose_offset_z')
        self.minimum_grasp_depth = self.cfg.get('grasp_generators', 'minimum_grasp_depth')
        if self.minimum_grasp_depth == 'None':
            self.minimum_grasp_depth = None
        else:
            self.minimum_grasp_depth = float(self.minimum_grasp_depth)


        grasp_planner_service_list_str = self.cfg.get('grasp_generators', 'grasp_planner_service_list')
        grasp_planner_service_list_str = grasp_planner_service_list_str.split('\n')
        self.grasp_planner_service_name_list = [s for s in grasp_planner_service_list_str]
        print('self.grasp_planner_service_name_list: ', self.grasp_planner_service_name_list)


        grasp_planner_service_list_database_names_list_str = self.cfg.get('grasp_generators', 'grasp_planner_service_list_database_names_list')
        grasp_planner_service_list_database_names_list_str = grasp_planner_service_list_database_names_list_str.split('\n')
        self.grasp_planner_service_list_database_names = [s for s in grasp_planner_service_list_database_names_list_str]
        print('self.grasp_planner_service_list_database_names: ', self.grasp_planner_service_list_database_names)

        self.dict_database_name_planner_service_name = {}
        for i in range(len(self.grasp_planner_service_name_list)):
            self.dict_database_name_planner_service_name[self.grasp_planner_service_list_database_names[i]] = self.grasp_planner_service_name_list[i]

        print('self.dict_database_name_planner_service_name: ', self.dict_database_name_planner_service_name)

        number_of_grasps_candidates_list_str = self.cfg.get('grasp_generators', 'number_of_grasps_candidates_list')
        number_of_grasps_candidates_list_str = number_of_grasps_candidates_list_str.split('\n')
        self.number_of_grasps_candidates_list = [int(s) for s in number_of_grasps_candidates_list_str]
        print('self.number_of_grasps_candidates_list: ', self.number_of_grasps_candidates_list)

        #
        self.dict_database_name_number_of_grasps_candidates = {}
        for i in range(len(self.grasp_planner_service_name_list)):
            self.dict_database_name_number_of_grasps_candidates[self.grasp_planner_service_name_list[i]] = self.number_of_grasps_candidates_list[i]
        print('self.dict_database_name_number_of_grasps_candidates: ', self.dict_database_name_number_of_grasps_candidates)


        gripper_width_meter_bonus_candidates_list_str = self.cfg.get('grasp_generators', 'gripper_width_meter_bonus_candidates_list')
        gripper_width_meter_bonus_candidates_list_str = gripper_width_meter_bonus_candidates_list_str.split('\n')
        self.gripper_width_meter_bonus_candidates_list = [float(s) for s in gripper_width_meter_bonus_candidates_list_str]
        print('self.gripper_width_meter_bonus_candidates_list: ', self.gripper_width_meter_bonus_candidates_list)

        #
        self.dict_database_name_width_meter_bonus = {}
        for i in range(len(self.grasp_planner_service_name_list)):
            self.dict_database_name_width_meter_bonus[self.grasp_planner_service_name_list[i]] = self.gripper_width_meter_bonus_candidates_list[i]
        print('self.dict_database_name_width_meter_bonus: ', self.dict_database_name_width_meter_bonus)

        self.wait_for_service_timeout = self.cfg.getfloat('grasp_generators', 'wait_for_service_timeout')
        self.norm = self.cfg.getboolean('grasp_generators', 'norm')
        self.remove_worst = self.cfg.getboolean('grasp_generators', 'remove_worst')

        self.segment_cloud_in_request = self.cfg.getboolean('grasp_generators', 'segment_cloud_in_request')


    def setup_generators(self):
        self.grasp_planners = {}
        failed_list = []
        for grasp_planner_service_name in self.grasp_planner_service_name_list:
            try:
                rospy.loginfo("Minimum: Waiting for grasp planner service...")
                rospy.wait_for_service(grasp_planner_service_name, timeout=self.wait_for_service_timeout)
                self.grasp_planners[grasp_planner_service_name] = rospy.ServiceProxy(grasp_planner_service_name, GraspPlanner)
                rospy.loginfo("...Connected with service {}".format(grasp_planner_service_name))
            # except rospy.ServiceException as e:
            except Exception as e:
                print("Service call failed: ", e)
                failed_list.append(grasp_planner_service_name)
                # # get default NUMBER_OF_CANDIDATES

        #todo check that
        for i in range(len(self.grasp_planner_service_name_list)-1, -1, -1):
            print(i)
            if self.grasp_planner_service_name_list[i] in failed_list:
                print('removing: ', self.grasp_planner_service_name_list.pop(i), ' from the grasp planners')

    # def get_candidates_with_msg(self, rgb_msg, depth_msg, pc_msg=None, segmentation_msg=None, NUMBER_OF_CANDIDATES=1, with_rgb=False, verbose=True):

    def create_planner_req(self, cv_image, depth_raw, cv_bridge, cam_info, camera_pose, aruco_board_pose, enable_grasp_filter=False, segmentation_image=None, with_rgb=True, segment_cloud=False, verbose=False):
        #todo create pc_msg
        # PointCloud2

        depth_msg = cv_bridge.cv2_to_imgmsg(depth_raw*1000.0, encoding="passthrough")
        rgb_msg = cv_bridge.cv2_to_imgmsg(cv_image, encoding="bgr8")#rgb8

        if segment_cloud and segmentation_image is not None:
            depth_raw_cloud = depth_raw.copy()
            print('mean depth_raw_cloud: ', np.mean(depth_raw_cloud))
            depth_raw_cloud[np.where(segmentation_image == 0)] = np.nan
            print('mean depth_raw_cloud: ', np.mean(depth_raw_cloud))

            pc_msg = convert_bgrd_to_pcl(cv_image,
                                         np.reshape(depth_raw_cloud * 1000.0, (depth_raw_cloud.shape[0], depth_raw_cloud.shape[1])),
                                         cam_info, rgb=with_rgb)
        else:
            pc_msg = convert_bgrd_to_pcl(cv_image, np.reshape(depth_raw*1000.0, (depth_raw.shape[0],depth_raw.shape[1])), cam_info, rgb=with_rgb)

        # Create the service request
        planner_req = GraspPlannerRequest()

        # Fill in the 2D camera data
        planner_req.color_image = rgb_msg
        planner_req.depth_image = depth_msg
        planner_req.camera_info = cam_info
        if segmentation_image is not None:
            if verbose:
                print('create_planner_req segmentation_image.shape: ', segmentation_image.shape)
            segmentation_msg = cv_bridge.cv2_to_imgmsg(segmentation_image, encoding="passthrough")
            planner_req.seg_image = segmentation_msg

        # Fill in the camera viewpoint field
        planner_req.view_point.header = camera_pose.header
        planner_req.view_point.pose.orientation = camera_pose.transform.rotation
        planner_req.view_point.pose.position = camera_pose.transform.translation

        # Fill in the arcuo board wrt world reference frame

        planner_req.aruco_board.position = aruco_board_pose.transform.translation
        planner_req.aruco_board.orientation = aruco_board_pose.transform.rotation
        planner_req.grasp_filter_flag = enable_grasp_filter

        # Transform the point cloud in the root reference frame and include it
        transform = camera_pose.transform
        pc_in = pc_msg
        tr_matrix = np.identity(4)
        tr_matrix[:3, :3] = quaternion_to_matrix([transform.rotation.x,
                                                  transform.rotation.y,
                                                  transform.rotation.z,
                                                  transform.rotation.w])
        tr_matrix[:3, 3] = [transform.translation.x,
                            transform.translation.y,
                            transform.translation.z]
        points = []
        if with_rgb:
            field_names = ("x", "y", "z", "rgb")
        else:
            field_names = ("x", "y", "z")
        if pc_in is not None:
            for p_in in read_points(pc_in, skip_nans=False, field_names=field_names):
                p_transformed = np.dot(tr_matrix, np.array([p_in[0], p_in[1], p_in[2], 1.0]))
                p_out = []
                p_out.append(p_transformed[0])
                p_out.append(p_transformed[1])
                p_out.append(p_transformed[2])
                if with_rgb:
                    p_out.append(p_in[3])
                points.append(p_out)

            header = pc_in.header
            header.stamp = rospy.Time.now()
            header.frame_id = "camera_depth_optical_frame"
            pc_out = sensor_msgs.point_cloud2.create_cloud(header=header, fields=pc_in.fields, points=points)

            planner_req.cloud = pc_out
        else:
            '''
            seq: 37
            stamp:
            secs: 1637228656
            nsecs: 521927357
            frame_id: "camera_depth_optical_frame"
            '''
            # planner_req.cloud = None
            planner_req.cloud.header.seq = 37
            planner_req.cloud.header.stamp = rospy.Time.now()
            planner_req.cloud.header.frame_id = 'camera_depth_optical_frame'

        # if verbose:
        #     print('planner_req: ', planner_req)
        return planner_req

    #todo
    #def get_candidates_with_planner_req(self, planner_req, verbose=True):

    # get default NUMBER_OF_CANDIDATES
    # def get_candidates_with_images(self, cv_image, depth_raw, cv_bridge, cam_info, camera_pose, segmentation_image=None, with_rgb=False, verbose=False):
    #     planner_req = self.create_planner_req(cv_image, depth_raw, cv_bridge, cam_info, camera_pose, segmentation_image, with_rgb, verbose)
    #     for grasp_planner_service_name in self.grasp_planner_service_name_list:
    #
    #         current_number_of_candidates = self.number_of_grasps_candidates_list[grasp_planner_service_name]
    #         # Set number of candidates
    #         planner_req.n_of_candidates = current_number_of_candidates
    #
    #         if verbose:
    #             print("send request to server planner_req: ")
    #             print("planner_req.n_of_candidates: ", planner_req.n_of_candidates)
    #             print("planner_req.camera_info: ", planner_req.camera_info)
    #             print("planner_req.view_point: ", planner_req.view_point)
    #
    #         # Plan for grasps
    #         try:
    #             reply = self.grasp_planners[grasp_planner_service_name](planner_req)
    #             if verbose:
    #                 print("Service {} reply is: \n{}".format(self.grasp_planners[grasp_planner_service_name].resolved_name, reply))
    #         except rospy.ServiceException as e:
    #             print("Service {} call failed: {}".format(self.grasp_planners[grasp_planner_service_name].resolved_name, e))
    #
    #     return reply, cam_info, planner_req
    #
    #     # get default NUMBER_OF_CANDIDATES

    def get_candidates_with_req(self, planner_req, grasp_planner_service_database_name, verbose=False):
        #get ros service name
        grasp_planner_service_name = self.dict_database_name_planner_service_name[grasp_planner_service_database_name]

        #get ros service number of candidates
        current_number_of_candidates = self.dict_database_name_number_of_grasps_candidates[grasp_planner_service_name]

        #set ros service number of candidates
        planner_req.n_of_candidates = current_number_of_candidates
        if verbose:
            print("send request to server planner_req: ")
            print("planner_req.n_of_candidates: ", planner_req.n_of_candidates)
            print("planner_req.camera_info: ", planner_req.camera_info)
            print("planner_req.view_point: ", planner_req.view_point)

        # Plan for grasps
        try:
            reply = self.grasp_planners[grasp_planner_service_name](planner_req)
            if verbose:
                print("Service {} reply is: \n{}".format(self.grasp_planners[grasp_planner_service_name].resolved_name, reply))
        except rospy.ServiceException as e:
            print("Service {} call failed: {}".format(self.grasp_planners[grasp_planner_service_name].resolved_name, e))
            reply = None

        if reply is not None:
            if self.norm:
                if len(reply.grasp_candidates) > 1:
                    print('best grasp: ', reply.grasp_candidates[0])
                    min_quality = reply.grasp_candidates[-1].score.data
                    max_quality = reply.grasp_candidates[0].score.data
                    print('min_quality: ', min_quality)
                    print('max_quality: ', max_quality)

                    for i in range(len(reply.grasp_candidates)):
                        reply.grasp_candidates[i].score.data = (reply.grasp_candidates[
                                                                i].score.data - min_quality) / (max_quality - min_quality)

                    # if min_quality != 0.0:
                    #     if min_quality < 0.0 and min_quality != max_quality:
                    #         max_quality = max_quality - min_quality
                    #         for i in range(len(reply.grasp_candidates)) :
                    #             reply.grasp_candidates[i].score.data = (reply.grasp_candidates[i].score.data - min_quality)/max_quality
                    #     else:
                    #         for i in range(len(reply.grasp_candidates)) :
                    #             reply.grasp_candidates[i].score.data = (reply.grasp_candidates[i].score.data)/max_quality

            if len(reply.grasp_candidates) > 0:
                current_gripper_width_meter_bonus = self.dict_database_name_width_meter_bonus[grasp_planner_service_name]
                if current_gripper_width_meter_bonus != 0.0:
                    for i in range(len(reply.grasp_candidates)):
                        reply.grasp_candidates[i].width.data = reply.grasp_candidates[i].width.data + current_gripper_width_meter_bonus

            if self.remove_worst:
                if len(reply.grasp_candidates) > 1:
                    reply.grasp_candidates = reply.grasp_candidates[:-1]

        return reply

    def _create_grasp_planner_srv_msg(self, grasp_poses) -> GraspPlannerResponse:
        """Create service response message
        Returns
        -------
        GraspPlannerResponse
            Service response message
        """

        response = GraspPlannerResponse()

        for grasp_candidate in grasp_poses:
            # Transform Grasp6D candidates in PoseStamped candidates
            grasp_msg = BenchmarkGrasp()
            p = PoseStamped()
            p.header.frame_id = grasp_candidate.ref_frame
            p.header.stamp = rospy.Time.now()
            p.pose.position.x = grasp_candidate.position[0]
            p.pose.position.y = grasp_candidate.position[1]
            p.pose.position.z = grasp_candidate.position[2]
            p.pose.orientation.w = grasp_candidate.quaternion[3]
            p.pose.orientation.x = grasp_candidate.quaternion[0]
            p.pose.orientation.y = grasp_candidate.quaternion[1]
            p.pose.orientation.z = grasp_candidate.quaternion[2]
            grasp_msg.pose = self.transform_grasp_to_world(p, self.camera_viewpoint)

            # Set candidate score and width
            grasp_msg.score.data = grasp_candidate.score
            grasp_msg.width.data = grasp_candidate.width
            grasp_msg.offset = [o for o in self._grasp_offset]

            response.grasp_candidates.append(grasp_msg)

        if self.grasp_pose_publisher is not None:
            # Publish poses for direct rViz visualization
            # TODO: properly publish all the poses
            print("Publishing grasps on topic")
            self.grasp_pose_publisher.publish(response.grasp_candidates[0].pose)

        return response


def get_fake_camera_info(fake_camera_info='realsense'):
    cam_info = CameraInfo()

    if fake_camera_info == 'realsense':
        cam_info.width = 1280
        cam_info.height = 720

        cam_info.D = [0.0, 0.0, 0.0, 0.0, 0.0]
        cam_info.K = [895.9815063476562, 0.0, 638.4413452148438, 0.0, 895.9815063476562,
                           360.1416015625, 0.0, 0.0, 1.0]
        cam_info.R = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        cam_info.P = [895.9815063476562, 0.0, 638.4413452148438, 0.0, 0.0, 895.9815063476562,
                           360.1416015625, 0.0, 0.0, 0.0, 1.0, 0.0]
        cam_info.distortion_model = "plumb_bob"
        cam_info.binning_x = 0
        cam_info.binning_y = 0
        cam_info.roi.x_offset = 0
        cam_info.roi.y_offset = 0
        cam_info.roi.height = 0
        cam_info.roi.width = 0
        cam_info.roi.do_rectify = False
        cam_info.header.frame_id = 'realsense'

        camera_pose = TransformStamped()
        camera_pose.transform.translation.x = 0.5350971426872632
        camera_pose.transform.translation.y = 0.10169077163023918
        camera_pose.transform.translation.z = 0.6538488949190373

        camera_pose.transform.rotation.x = -0.0010397419468549662
        camera_pose.transform.rotation.y = 0.9999949281346481
        camera_pose.transform.rotation.z = 0.00018072985235736104
        camera_pose.transform.rotation.w = 0.00018072985235736104

    elif fake_camera_info == 'primesense':
        cam_info.width = 640
        cam_info.height = 480
        cam_info.D = [0.0, 0.0, 0.0, 0.0, 0.0]
        cam_info.K = [525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0]
        cam_info.R = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        cam_info.P = [525.0, 0.0, 319.5, 0.0, 0.0, 525.0, 239.5, 0.0, 0.0, 0.0, 1.0, 0.0]
        cam_info.distortion_model = "plumb_bob"
        cam_info.binning_x = 0
        cam_info.binning_y = 0
        cam_info.roi.x_offset = 0
        cam_info.roi.y_offset = 0
        cam_info.roi.height = 0
        cam_info.roi.width = 0
        cam_info.roi.do_rectify = False
        cam_info.header.frame_id = 'primesense'

        camera_pose = TransformStamped()
        camera_pose.transform.translation.x = 1.0
        camera_pose.transform.translation.y = 0.0
        camera_pose.transform.translation.z = 2.0


        camera_pose.transform.rotation.x = 0.7018607268549014
        camera_pose.transform.rotation.y = -0.6759673304256282
        camera_pose.transform.rotation.z = 0.1589571514088901
        camera_pose.transform.rotation.w = -0.15872096131476807

        # camera_pose.transform.rotation.x = 0.6176441157316129
        # camera_pose.transform.rotation.y = -0.776486503191127
        # camera_pose.transform.rotation.z = 0.09055100545513552
        # camera_pose.transform.rotation.w = -0.08593604205423959

        # camera_pose.transform.rotation.x = -0.6492176883224684
        # camera_pose.transform.rotation.y = 0.6956736792044113
        # camera_pose.transform.rotation.z = -0.2080270492641407
        # camera_pose.transform.rotation.w = 0.226449339267389

        # camera_pose.transform.rotation.x = 0.7870804155622836
        # camera_pose.transform.rotation.y = -0.5848545871479922
        # camera_pose.transform.rotation.z = 0.14072627243214142
        # camera_pose.transform.rotation.w = -0.13654918098020052

        # camera_pose.transform.rotation.x = 0.7870804155622836
        # camera_pose.transform.rotation.y = -0.5848545871479922
        # camera_pose.transform.rotation.z = 0.14072627243214142
        # camera_pose.transform.rotation.w = -0.13654918098020052
        '''
        x: 0.6948913912642045
        y: -0.6917883442987107
        z: 0.10968828959477107
        w: -0.16285991346514356
        '''
    '''
    #     [fx  0 cx]
    # K = [ 0 fy cy]
    #     [ 0  0  1]
    '''
    fx = cam_info.K[0]
    fy = cam_info.K[4]
    cx = cam_info.K[2]
    cy = cam_info.K[5]
    skew = 0.0
    intrinsic_matrix = np.array([[fx, skew, cx],
                                 [0, fy, cy],
                                 [0, 0, 1]])

    camera_data = CameraData()
    camera_data.intrinsic_params = {
        'fx': fx,
        'fy': fy,
        'cx': cx,
        'cy': cy,
        'skew': skew,
        'w': cam_info.width,
        'h': cam_info.height,
        'frame': cam_info.header.frame_id,
        'intrinsic_matrix': intrinsic_matrix
    }

    cam_info_msg_header_frame_id = "camera_depth_optical_frame"



    orientation = camera_pose.transform.rotation
    position = camera_pose.transform.translation

    camera_position = np.array([position.x,
                                position.y,
                                position.z])
    camera_orientation = quaternion_to_matrix([orientation.x,
                                               orientation.y,
                                               orientation.z,
                                               orientation.w])
    camera_extrinsic_matrix = np.eye(4)
    camera_extrinsic_matrix[:3, :3] = camera_orientation
    camera_extrinsic_matrix[:3, 3] = camera_position

    camera_data.camera_extrinsic_matrix = camera_extrinsic_matrix

    return cam_info, camera_data, camera_pose

if __name__ == "__main__":
    # Init node
    rospy.init_node('GraspGenerators')
    grasp_generator_config_path = '../test_config/test_grasp_generator_config.ini'
    gg = GraspGenerators(generators_config_path=grasp_generator_config_path)

    segmentation_config_path = '../test_config/cornell_segmentation_config.ini'
    segmentation_class = SegmentationClass(segmentation_config_path)

    # rgb_scene_path = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/current_data/current.png'
    # depth_scene_path = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/current_data/current.npy'

    rgb_scene_path = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/test_scene/cornell/pcd0100r.png'
    depth_scene_path = '/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/test_scene/cornell/pcd0100.npy'

    segmentation_path = None
    if rgb_scene_path is not None:
        current_scene_rgb_image = cv2.imread(rgb_scene_path)

    if depth_scene_path is not None:
        current_scene_raw_depth_data = np.load(depth_scene_path)

    cam_info, camera_data, camera_pose = get_fake_camera_info('primesense')


    mask = segmentation_class.compute_mask(current_scene_rgb_image,
                                          None,
                                          current_scene_raw_depth_data,
                                          camera_data.intrinsic_params)

    cv_bridge = CvBridge()

    planner_req = gg.create_planner_req(current_scene_rgb_image, current_scene_raw_depth_data,
                                                     cv_bridge, cam_info,
                                                     camera_pose,
                                                     segmentation_image=mask, with_rgb=True, segment_cloud=True, verbose=True)

    reply = gg.get_candidates_with_req(planner_req, 'GPD',
                                                    verbose=True)
